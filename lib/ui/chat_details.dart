import 'dart:async';

import 'package:ficiety/models/chat_messages_model.dart';
import 'package:ficiety/ui/order_summary.dart';
import 'package:ficiety/ui/success_payment.dart';
import 'package:ficiety/ui/user_exercise_program.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

enum MessageType {
  Sender,
  Receiver,
}

class ChatDetails extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final String userName;
  final Function(bool) isReturn;
  final String receiptId;

  ChatDetails({required this.scaffoldKey, required this.userName,required this.isReturn,required this.receiptId});

  @override
  _ChatDetailsState createState() => _ChatDetailsState();
}

class _ChatDetailsState extends State<ChatDetails> {
  late bool isSuccessPayment;
  late String selectedProgram;
  final _controller = ScrollController();
  late bool isShowLoader;
  late Future _future;
  List<ChatMessage> messages = [
    ChatMessage(
        messageContent:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
        messageType: MessageType.Receiver,
        time: '9:22'),
    ChatMessage(
        messageContent:
            'Lorem ipsum dolor sit amet, consecteturadipiscing elit.',
        messageType: MessageType.Receiver,
        time: '9:22'),
    ChatMessage(
        messageContent:
            'Lorem ipsum dolor sit amet, consecteturadipiscing elit.',
        messageType: MessageType.Sender,
        time: '9:22'),
    ChatMessage(
        messageContent:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
        messageType: MessageType.Receiver,
        time: '9:22'),
    ChatMessage(
        messageContent:
        'Lorem ipsum dolor sit amet, consecteturadipiscing elit.',
        messageType: MessageType.Receiver,
        time: '9:22'),
    ChatMessage(
        messageContent:
        'Lorem ipsum dolor sit amet, consecteturadipiscing elit.',
        messageType: MessageType.Sender,
        time: '9:22'),
    ChatMessage(
        messageContent:
        'Lorem ipsum dolor sit amet, consecteturadipiscing elit.',
        messageType: MessageType.Receiver,
        time: '9:22'),
    ChatMessage(
        messageContent:
        'Lorem ipsum dolor sit amet, consecteturadipiscing elit.',
        messageType: MessageType.Sender,
        time: '9:22'),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isSuccessPayment = true;
    isShowLoader = true;
    // _future = getChatDetails(id:widget.receiptId);

    // Timer(
    //   Duration(seconds: 1),
    //       () => _controller.jumpTo(_controller.position.maxScrollExtent),
    // );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return isSuccessPayment
        ? Scaffold(
          body: Container(
              color: Colors.white,
              height: MediaQuery.of(context).size.height,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 100,
                    color: Color(0xff006e6f),
                    child: SafeArea(
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Padding(
                          padding: const EdgeInsets.only(
                            top: 10,
                          ),
                          child: Row(
                            children: [
                              GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                child: AppCommon.kIconTrainerListTextView(
                                    fontName: AppStrings.kFontAwsSolid,
                                    content: ''),
                                onTap: () {
                                  print('tap on back');
                                  widget.isReturn(true);
                                },
                              ),
                              SizedBox(
                                width: 30,
                              ),
                              Text(
                                widget.userName,
                                style: GoogleFonts.roboto(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              Spacer(),
                              FaIcon(
                                FontAwesomeIcons.search,
                                color: Colors.white,
                                size: 18.5,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          flex:9,
                          child: Container(
                            color: Colors.white,
                            child: ListView.builder(
                              controller: _controller,
                              itemCount: messages.length,
                              shrinkWrap: true,
                              padding: EdgeInsets.only(top: 10, bottom: 10),
                              itemBuilder: (context, index) {
                                print('listview length ${messages.length}');
                                return Row(
                                  mainAxisSize: MainAxisSize.max,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: Visibility(
                                        visible: (messages[index].messageType ==
                                            MessageType.Receiver && index>0?messages[index-1].messageType!=MessageType.Receiver:messages[index].messageType ==
                                            MessageType.Receiver)?true: false,
                                        child: Padding(
                                          padding: const EdgeInsets.only(left: 15,top:10,),
                                          child: Container(
                                            child: CircleAvatar(
                                              backgroundImage: AssetImage(AppStrings.kImgGymMan,),
                                            ),
                                            width: 40,
                                            height: 40,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 9,
                                      child: Container(
                                        padding: EdgeInsets.only(
                                            left: 14, right: 14, top: 10, bottom: 10),
                                        child: Align(
                                          alignment: messages[index].messageType ==
                                                  MessageType.Receiver
                                              ? Alignment.topLeft
                                              : Alignment.topRight,
                                          child: Container(
                                            width: 250,
                                            decoration: BoxDecoration(
                                              border: messages[index].messageType ==
                                                      MessageType.Receiver
                                                  ? Border.all(
                                                      color: Color(0xfff0f0f0),
                                                    )
                                                  : Border.all(
                                                      color: Colors.grey,
                                                    ),
                                              borderRadius: messages[index]
                                                          .messageType ==
                                                      MessageType.Receiver
                                                  ? BorderRadius.only(
                                                      topRight: Radius.circular(10),
                                                      bottomLeft: Radius.circular(10),
                                                      bottomRight: Radius.circular(10),
                                                    )
                                                  : BorderRadius.only(
                                                      topLeft: Radius.circular(10),
                                                      bottomLeft: Radius.circular(10),
                                                      bottomRight: Radius.circular(10)),
                                              color: (messages[index].messageType ==
                                                      MessageType.Receiver
                                                  ? Color(0xfff0f0f0)
                                                  : Colors.white),
                                            ),
                                            padding: EdgeInsets.all(10),
                                            child: Column(
                                              children: [
                                                Text(
                                                  messages[index].messageContent,
                                                  style: GoogleFonts.roboto(
                                                    color: Color(0xff222222),
                                                    fontSize: 14,
                                                  ),
                                                  textAlign: TextAlign.left,
                                                ),
                                                Align(
                                                  alignment: Alignment.topRight,
                                                  child: Text(
                                                    messages[index].time,
                                                    style: GoogleFonts.roboto(
                                                      color: Color(0xff006e6f),
                                                      fontSize: 10,
                                                      fontWeight: FontWeight.w500,
                                                    ),
                                                    textAlign: TextAlign.right,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                );
                              },
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: ListView(
                            shrinkWrap: true,
                            children: [
                              SizedBox(height: 15,),
                              Divider(color: Colors.grey),
                              Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 10),
                                      child: TextField(
                                        decoration: InputDecoration(
                                            hintText: "Type a message ...",
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: Color(0xff66067173),),
                                              borderRadius: BorderRadius.circular(30.0,),
                                            ),
                                            focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: Color(0xff66067173),),
                                              borderRadius: BorderRadius.circular(30.0,),
                                            ),
                                            contentPadding: EdgeInsets.only(left: 10,top: 10,),
                                            prefixIcon: Icon(Icons.attach_file,color: Colors.grey,),
                                            suffixIcon: Icon(FontAwesomeIcons.smile,color: Colors.grey),
                                            hintStyle:
                                            TextStyle(color: Colors.black54),
                                            border: OutlineInputBorder()),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 15,
                                  ),
                                  FloatingActionButton(
                                    mini: true,
                                    onPressed: () {},
                                    child: Icon(
                                      Icons.send,
                                      color: Colors.white,
                                      size: 18,
                                    ),
                                    backgroundColor: Color(0xff067173),
                                    elevation: 0,
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
        )
        : SuccessPayment(scaffoldKey: widget.scaffoldKey,isReturn: (value){},);
  }
}
