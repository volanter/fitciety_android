import 'dart:math';
import 'package:flutter/material.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/trainer_ui/trainer_dashboard.dart';
import 'package:ficiety/ui/register.dart';
import 'package:ficiety/ui/user_dashboard.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SinglePage extends StatefulWidget {
  @override
  _SinglePageState createState() => _SinglePageState();
}

class _SinglePageState extends State<SinglePage> {

  late SharedPreferences preferences;
  late List<String> listImage = [];
  var _random = new Random();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    listImage.add(AppStrings.kImgSlide1);
    listImage.add(AppStrings.kImgSlide2);
    listImage.add(AppStrings.kImgSlide3);
    listImage.add(AppStrings.kImgSlide4);

    SharedPreferences.getInstance().then((value) => preferences = value);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: Container(
          child: LayoutBuilder(builder: (context, constraints){
            print('width is ${constraints.maxWidth}');
            return  Container(
              decoration:constraints.maxWidth>600?BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage(
                    listImage[_random.nextInt(listImage.length)],
                  ),
                ),
              ):BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage(
                    listImage[_random.nextInt(listImage.length)],
                  ),
                ),
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: 90,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 20,),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 20,),
                          child: Image.asset(
                            AppStrings.kImgLogo,
                            fit: BoxFit.fill,
                            width: 200,
                            height: 110,
                          ),
                        ),
                        Spacer(),
                        // Text(
                        //   AppStrings.kSkip,
                        //   style: GoogleFonts.roboto(
                        //     color: Colors.white,
                        //     fontWeight: FontWeight.w500,
                        //   ),
                        // ),
                        SizedBox(width: 5,),
                        // Icon(Icons.arrow_forward,color: Colors.white,size: 20,),
                      ],
                    ),
                  ),
                  Spacer(),
                  SizedBox(
                    height: 50,
                  ),
                  // Padding(
                  //   padding: const EdgeInsets.only(left: 20),
                  //   child: Align(
                  //     alignment: Alignment.centerLeft,
                  //     child: Text(AppStrings.kSliderText,
                  //         style: GoogleFonts.roboto(
                  //           color: Colors.white,
                  //           fontSize: 14,
                  //           letterSpacing: 1.0,
                  //           fontWeight: FontWeight.bold,
                  //         )),
                  //   ),
                  // ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(
                        bottom: 50,
                      ),
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () async{
                                print('is login ${ preferences.getString(AppStrings.kIsLogin)}');
                                if(preferences.getString(AppStrings.kPrefUserRoleKey)=='1'){
                                  preferences.getString(AppStrings.kIsLogin)==null?Navigator.of(context).pushNamed(AppStrings.kLoginRoute):Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => UserDashboard(
                                              userId: preferences.getString(AppStrings.kPrefUserIdKey).toString())));
                                }else{
                                  preferences.getString(AppStrings.kIsLogin)==null?Navigator.of(context).pushNamed(AppStrings.kLoginRoute):Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => TrainerDashboard()));
                                }

                              },
                              child: Container(
                                height: 35,
                                width: 143,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(70.0),
                                  border: Border.all(
                                    color: Colors.grey.shade200,
                                  ),
                                ),
                                child: Center(
                                  child: Text('Log In',
                                      style: GoogleFonts.roboto(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w900,
                                      )),
                                ),
                              ),
                            ),
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: (){
                                AwesomeDialog(
                                  context: context,
                                  dialogType: DialogType.INFO,
                                  animType: AnimType.BOTTOMSLIDE,
                                  title: 'Sign Up',
                                  desc: 'Who would you like to sign up as?',
                                  btnCancelText: 'Trainer',
                                  btnOkText: 'Client',
                                  btnCancelOnPress: () {
                                    Navigator.of(context).push(MaterialPageRoute(builder: (context)=>Register(userRole: '2',)));
                                  },
                                  btnOkOnPress: () {
                                    Navigator.of(context).push(MaterialPageRoute(builder: (context)=>Register(userRole: '1',)));
                                  },
                                )..show();
                                // Navigator.of(context).pushNamed(AppStrings.kRegisterRoute);
                              },
                              child: Container(
                                height: 35,
                                width: 143,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(70.0),
                                  color: Colors.white,
                                ),
                                child: Center(
                                  child: Text(
                                    'Sign Up',
                                    style: GoogleFonts.roboto(
                                      color: Color(0xff006e6f),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w900,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            );
          }),
        ),
      ),
    );
  }
}
