import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:ficiety/models/session_booking_model.dart';
import 'package:ficiety/ui/session_list.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'notification_page.dart';
import 'package:http/http.dart' as http;

class MySessionBooking extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  MySessionBooking({required this.scaffoldKey});

  @override
  _MySessionBookingState createState() => _MySessionBookingState();
}

enum ViewType {
  Notification,
  MySessionBooking,
  SessionList,
}

class _MySessionBookingState extends State<MySessionBooking> {
  int _crossAxisCount = 2;
  late bool isShowSessionBooking;
  late ViewType viewType;
  late bool isShowLoader;
  late Future<SessionBookingModel?> _future;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _future = getData();
    isShowLoader = true;
    viewType = ViewType.MySessionBooking;
    isShowSessionBooking = true;
  }

  Future<SessionBookingModel?> getData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    print(
        'userid id is ${preferences.getString(AppStrings.kPrefUserIdKey).toString()}');

    // try {
    //   String data =
    //       await DefaultAssetBundle.of(context).loadString('assets/auth.json');
    //   final jsonResult = jsonDecode(data);
    //   return AuthorisationRequestModel.fromJson(jsonResult);
    // } catch (exception) {
    //   print('exception $exception');
    // }

    try {
      final response = await http.post(
        Uri.parse(AppStrings.kGetMySessionBookingUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue,
        },
        body: jsonEncode(<String, String>{
          'user_id':
              preferences.getString(AppStrings.kPrefUserIdKey).toString(),
        }),
      );
      var responseData = jsonDecode(response.body);
      setState(() {
        isShowLoader = false;
      });
      print('response from auth list report $responseData');
      if (response.statusCode == 200) {
        if (responseData['status'] == 'success') {
          return SessionBookingModel.fromJson(responseData);
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              Navigator.of(context).pop();
            },
          )..show();
        }
      } else {
        throw Exception('Failed to load data');
      }
    } catch (exception) {
      print('exception $exception');
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    var size = MediaQuery.of(context).size;
    final double itemHeight = (size.height - 380) / 2;
    final double itemWidth = size.width / 2;
    double _crossAxisSpacing = 1.0,
        _mainAxisSpacing = 1.0,
        _aspectRatio = (itemWidth / itemHeight);
    return viewType == ViewType.MySessionBooking
        ? Container(
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SafeArea(
                  child: Container(
                    height: 100,
                    color: Color(0xff006e6f),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Padding(
                            padding: const EdgeInsets.only(
                              top: 10,
                            ),
                            child: Row(
                              children: [
                                GestureDetector(
                                  child: Image.asset(
                                    AppStrings.kImgMenuBar,
                                    width: 18,
                                    height: 13,
                                  ),
                                  onTap: () {
                                    if (widget.scaffoldKey.currentState!
                                        .isDrawerOpen) {
                                      Navigator.of(context).pop();
                                    } else {
                                      widget.scaffoldKey.currentState!
                                          .openDrawer();
                                    }
                                  },
                                ),
                                SizedBox(
                                  width: 30,
                                ),
                                Text(
                                  AppStrings.kMySessionBooking,
                                  style: GoogleFonts.roboto(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                Spacer(),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      viewType = ViewType.Notification;
                                    });
                                    // widget.isReturn(true);
                                  },
                                  child: Image.asset(
                                    AppStrings.kImgNotification,
                                    width: 17.5,
                                    height: 18.5,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.only(
                      left: 30,
                      right: 30,
                      top: 10,
                    ),
                    child: LayoutBuilder(builder: (context, constraint) {
                      print('max height ${constraint.maxHeight}');
                      return FutureBuilder<SessionBookingModel?>(
                          future: _future,
                          builder: (BuildContext context,
                              AsyncSnapshot<SessionBookingModel?> snapshot) {
                            if (!snapshot.hasData) {
                              return Center(
                                  child: Container(
                                      width: 50,
                                      height: 50,
                                      child: CircularProgressIndicator()));
                            } else {
                              return GridView.builder(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: snapshot.data!.data.length,
                                itemBuilder: (context, index) =>
                                    GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      viewType = ViewType.SessionList;
                                    });
                                    // setState(() {
                                    //   isShowSessionBooking = false;
                                    // });
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.all(5.0),
                                    child: Container(
                                      height:
                                          constraint.maxHeight > 270 ? 80 : 220,
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.circular(
                                            2.0,
                                          ),
                                          image: DecorationImage(
                                            image: NetworkImage(
                                              AppStrings.kImageUrl+snapshot.data!.data[index].image,
                                            ),
                                            fit: BoxFit.fill,
                                          ),
                                          border: Border.all(
                                            color: Color(
                                              0xff006e6f,
                                            ),
                                          )),
                                      child: Align(
                                        alignment: Alignment.bottomCenter,
                                        child: Container(
                                          height: 35.5,
                                          color: Color(0xff006e6f),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              Expanded(
                                                flex: 3,
                                                child: Center(
                                                  child: AutoSizeText(
                                                    snapshot.data!.data[index].fullname,
                                                    style: GoogleFonts.roboto(
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: 16,
                                                    ),
                                                    minFontSize: 12,
                                                    maxLines: 1,
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                flex: 1,
                                                child: CircleAvatar(
                                                  backgroundColor: Colors.white,
                                                  maxRadius: 7,
                                                  child: Text(
                                                    '5',
                                                    style: GoogleFonts.roboto(
                                                      color: Color(0xff006e6f),
                                                      fontSize: 8,
                                                    ),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: _crossAxisCount,
                                  crossAxisSpacing: _crossAxisSpacing,
                                  mainAxisSpacing: _mainAxisSpacing,
                                  childAspectRatio: _aspectRatio,
                                ),
                              );
                            }
                          });
                    })),
              ],
            ),
          )
        : viewType == ViewType.SessionList
            ? SessionList(
                scaffoldKey: widget.scaffoldKey,
                isReturn: (value) {
                  // setState(() {
                  //   isShowSessionBooking = value;
                  // });
                  setState(() {
                    viewType = ViewType.MySessionBooking;
                  });
                },
              )
            : NotificationPage(
                scaffoldKey: widget.scaffoldKey,
                isReturn: (value) {
                  setState(() {
                    viewType = ViewType.MySessionBooking;
                  });
                },
              );
  }
}
