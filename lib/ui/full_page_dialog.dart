import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';

class FullPageDialog extends StatefulWidget {
  final String image;

  FullPageDialog({required this.image});

  @override
  _FullPageDialogState createState() => _FullPageDialogState();
}

class _FullPageDialogState extends State<FullPageDialog> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          // physics: NeverScrollableScrollPhysics(),
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Align(
                alignment: Alignment.topRight,
                child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: AppCommon.kIconBottomSheetTextView(
                      fontName: AppStrings.kFontAwsSolid,
                      content: '',
                    )),
              ),
            ),
            SizedBox(
              height: 150,
            ),
            Image.network(
              AppStrings.kWorkOutUrl+widget.image,
              width: 720,
              height: 350,
            ),
          ],
        ),
      ),
    );
  }
}
