import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/models/trainer_details_model.dart';
import 'package:ficiety/ui/book_session_calender.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:ficiety/utils/progressdialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class TrainerDetails extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Function(bool) isReturn;
  final Function(bool) isBookSessionReturn;
  final String trainerId;

  TrainerDetails(
      {required this.scaffoldKey,
      required this.isReturn,
      required this.isBookSessionReturn,
      required this.trainerId});

  @override
  _TrainerDetailsState createState() => _TrainerDetailsState();
}

class _TrainerDetailsState extends State<TrainerDetails>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  late bool _isDetailsViewShow;
  late Future<TrainerDetailsModel?> _future;
  late ProgressDialog progressDialog;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print('trainer details call');
    progressDialog = ProgressDialog(context,ProgressDialogType.Normal);
    progressDialog.setMessage(AppStrings.kLoadingMsg);
    _future = getTrainerDetailsData(trainerId: widget.trainerId);
    _isDetailsViewShow = true;
    _tabController = new TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff024446),
    ));
    return WillPopScope(
        onWillPop: _onBackPress,
        child: /*_isDetailsViewShow
          ?*/
            Container(
          color: Colors.white,
          child: FutureBuilder<TrainerDetailsModel?>(
              future: _future,
              builder: (BuildContext context,
                  AsyncSnapshot<TrainerDetailsModel?> snapshot) {
                if (!snapshot.hasData) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        height: 300,
                        child: Stack(
                          clipBehavior: Clip.none,
                          fit: StackFit.loose,
                          alignment: Alignment.topLeft,
                          children: [
                            Container(
                              height: 190,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      fit: BoxFit.fill,
                                      image: NetworkImage(
                                        AppStrings.kImageUrl+snapshot.data!.data[0].backgroundImage,
                                      ))),
                              child: Column(
                                children: [
                                  SafeArea(
                                    child: Padding(
                                      padding: const EdgeInsets.all(20.0),
                                      child: Row(
                                        children: [
                                          GestureDetector(
                                            behavior:
                                                HitTestBehavior.translucent,
                                            child: Icon(
                                              Icons.arrow_back_sharp,
                                              color: Colors.white,
                                            ) /*Image.asset(
                                            AppStrings.kImgMenuBar,
                                            width: 18,
                                            height: 13,
                                          )*/
                                            ,
                                            onTap: () {
                                              if (_isDetailsViewShow) {
                                                widget.isReturn(true);
                                              } else {
                                                setState(() {
                                                  _isDetailsViewShow = true;
                                                });
                                              }

                                              // if (widget.scaffoldKey.currentState!
                                              //     .isDrawerOpen) {
                                              //   Navigator.of(context).pop();
                                              // } else {
                                              //   widget.scaffoldKey.currentState!
                                              //       .openDrawer();
                                              // }
                                            },
                                          ),
                                          SizedBox(
                                            width: 30,
                                          ),
                                          Text(
                                            AppStrings.kTrainerDetails,
                                            style: GoogleFonts.roboto(
                                              color: Colors.white,
                                              fontWeight: FontWeight.w700,
                                            ),
                                          ),
                                          Spacer(),
                                          Image.asset(
                                            AppStrings.kImgNotification,
                                            width: 17.5,
                                            height: 18.5,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Positioned(
                              top: 160,
                              left: 30,
                              child: Row(
                                children: [
                                  Container(
                                    width: 120,
                                    height: 120,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(
                                          5.0,
                                        ),
                                        border: Border.all(
                                          color: Colors.white,
                                        ),
                                        image: DecorationImage(
                                            fit: BoxFit.fill,
                                            image: NetworkImage(
                                              AppStrings.kImageUrl +
                                                  snapshot
                                                      .data!.data[0].profilePic,
                                            ))),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: 40,
                                      ),
                                      Text(
                                        snapshot.data!.data[0].trainerName,
                                        style: GoogleFonts.roboto(
                                          color: AppColors.kGreen,
                                          fontSize: 18,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                      SizedBox(height: 5),
                                      // Align(
                                      //   alignment: Alignment.topLeft,
                                      //   child: Row(
                                      //     children: [
                                      //       Icon(
                                      //         Icons.star,
                                      //         size: 15,
                                      //         color: Color(0xffffcc00),
                                      //       ),
                                      //       Icon(
                                      //         Icons.star,
                                      //         size: 15,
                                      //         color: Color(0xffffcc00),
                                      //       ),
                                      //       Icon(
                                      //         Icons.star,
                                      //         size: 15,
                                      //         color: Color(0xffffcc00),
                                      //       ),
                                      //       Icon(
                                      //         Icons.star,
                                      //         size: 15,
                                      //         color: Color(0xffffcc00),
                                      //       ),
                                      //       Icon(
                                      //         Icons.star,
                                      //         size: 15,
                                      //         color: Color(0xffffcc00),
                                      //       ),
                                      //     ],
                                      //   ),
                                      // ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      // Text(
                                      //   'Clients',
                                      //   style: GoogleFonts.roboto(
                                      //     color: AppColors.kBlack,
                                      //     fontWeight: FontWeight.w700,
                                      //   ),
                                      // ),
                                      // SizedBox(
                                      //   height: 2,
                                      // ),
                                      // Text(
                                      //   '245',
                                      //   style: GoogleFonts.roboto(
                                      //     color: AppColors.kGreen,
                                      //     fontWeight: FontWeight.w700,
                                      //   ),
                                      // ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 30,
                          right: 30,
                          bottom: 10,
                        ),
                        child: Text(
                          snapshot.data!.data[0].description,
                          softWrap: true,
                          overflow: TextOverflow.visible,
                          style: GoogleFonts.roboto(
                              color: AppColors.kBlack, fontSize: 12),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 30,
                          right: 30,
                          bottom: 10,
                          top: 10,
                        ),
                        child: Row(
                          children: [
                            FaIcon(
                              FontAwesomeIcons.mapMarkerAlt,
                              size: 15,
                              color: AppColors.kGreen,
                            ),
                            SizedBox(
                              width: 15,
                            ),
                            Text(
                              snapshot.data!.data[0].address ?? '',
                              softWrap: true,
                              overflow: TextOverflow.visible,
                              style: GoogleFonts.roboto(
                                color: AppColors.kBlack,
                                fontSize: 12,
                              ),
                              textAlign: TextAlign.left,
                            ),
                            Spacer(),
                            GestureDetector(
                              onTap: () {
                                launch(
                                    "tel://${snapshot.data!.data[0].mobile}");
                              },
                              child: Container(
                                width: 35,
                                height: 35,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                    color: Colors.grey.shade400,
                                  ),
                                ),
                                child: Align(
                                  child: Icon(
                                    Icons.call,
                                    color: AppColors.kGreen,
                                  ),
                                  alignment: Alignment.center,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            GestureDetector(
                              onTap: () {
                                String uri =
                                    'sms:+${snapshot.data!.data[0].mobile}';
                                launch(uri);
                              },
                              child: Container(
                                width: 35,
                                height: 35,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                    color: Colors.grey.shade400,
                                  ),
                                ),
                                child: Align(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Image.asset(
                                      AppStrings.kImgMsg,
                                    ),
                                  ),
                                  alignment: Alignment.center,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      // Padding(
                      //   padding: const EdgeInsets.only(
                      //     left: 30,
                      //     right: 30,
                      //   ),
                      //   child: Row(
                      //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      //     children: [
                      //       ElevatedButton(
                      //         onPressed: () {
                      //           setState(() {
                      //             _isDetailsViewShow = false;
                      //           });
                      //           // print('view profile clicked');
                      //           // widget.callback(AppStrings.kTrainerDetails);
                      //         },
                      //         style: ButtonStyle(
                      //             backgroundColor: MaterialStateProperty.all<Color>(
                      //                 AppColors.kGreen)),
                      //         child: Padding(
                      //           padding: const EdgeInsets.all(8.0),
                      //           child: Text(
                      //             AppStrings.kBookSession,
                      //             style: GoogleFonts.roboto(
                      //               color: Colors.white,
                      //               fontWeight: FontWeight.w500,
                      //             ),
                      //           ),
                      //         ),
                      //       ),
                      //       ElevatedButton(
                      //         onPressed: () {
                      //           print('button tap');
                      //         },
                      //         style: ButtonStyle(
                      //             backgroundColor: MaterialStateProperty.all<Color>(
                      //                 AppColors.kGreen)),
                      //         child: Padding(
                      //           padding: const EdgeInsets.all(8.0),
                      //           child: Text(
                      //             AppStrings.kConsultation,
                      //             style: GoogleFonts.roboto(
                      //               color: Colors.white,
                      //               fontWeight: FontWeight.w500,
                      //             ),
                      //           ),
                      //         ),
                      //       ),
                      //     ],
                      //   ),
                      // ),
                      // Padding(
                      //   padding: const EdgeInsets.only(
                      //     left: 20,
                      //     right: 20,
                      //   ),
                      //   child: TabBar(
                      //     unselectedLabelColor: Colors.grey,
                      //     labelColor: Colors.black,
                      //     tabs: [
                      //       Tab(
                      //         text: AppStrings.kSession,
                      //         // icon: Icon(Icons.people),
                      //       ),
                      //       Tab(
                      //         // icon: Icon(Icons.person),
                      //         text: AppStrings.kReviews,
                      //       )
                      //     ],
                      //     controller: _tabController,
                      //     indicatorSize: TabBarIndicatorSize.tab,
                      //     indicatorPadding: EdgeInsets.all(10),
                      //   ),
                      // ),
                      Divider(
                        color: Colors.grey,
                      ),
                      Expanded(child: _sessionView(snapshot)),
                      Divider(
                        color: Colors.grey,
                      ),
                    ],
                  );
                }
              }),
        )
        // : BookSessionCalender(
        //     scaffoldKey: widget.scaffoldKey,
        //   ),
        );
  }

  Widget _reviewView() {
    return ListView.separated(
      itemBuilder: (context, index) {
        return ListTile(
          leading: Container(
            width: 40,
            height: 40,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage(AppStrings.kImgSlide1),
                )),
          ),
          title: Column(
            children: [
              Align(
                child: Text(
                  'Kelly Jackson',
                  style: GoogleFonts.roboto(
                    color: Colors.black,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                alignment: Alignment.topLeft,
              ),
              Row(
                children: [
                  Icon(
                    Icons.star,
                    size: 10,
                    color: Color(0xffffcc00),
                  ),
                  Icon(
                    Icons.star,
                    size: 10,
                    color: Color(0xffffcc00),
                  ),
                  Icon(
                    Icons.star,
                    size: 10,
                    color: Color(0xffffcc00),
                  ),
                  Icon(
                    Icons.star,
                    size: 10,
                    color: Color(0xffffcc00),
                  ),
                  Icon(
                    Icons.star,
                    size: 10,
                    color: Color(0xffffcc00),
                  ),
                ],
              ),
            ],
          ),
          subtitle: Text(
            'The decade that brought us Star Trek and Doctor Who also resurrected Cicero—or at least what used to be Cicero—in an attempt to make the days before computerized design a little less painstaking.',
            style: GoogleFonts.roboto(
              color: Colors.black,
            ),
            softWrap: true,
          ),
          trailing: Text(
            '2 day ago',
            style: GoogleFonts.roboto(
              color: Color(0xffbbbbbb),
            ),
          ),
        );
      },
      separatorBuilder: (context, index) => Divider(
        color: Colors.grey,
      ),
      itemCount: 10,
    );
  }

  Widget _sessionView(AsyncSnapshot<TrainerDetailsModel?> snapshot) {
    return Padding(
      padding: const EdgeInsets.all(
        10.0,
      ),
      child: Padding(
        padding: const EdgeInsets.only(
          left: 15,
          right: 15,
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  AppStrings.kCategory,
                  style: GoogleFonts.roboto(
                    color: Color(0xff006e6f),
                    fontSize: 14,
                    fontWeight: FontWeight.w900,
                  ),
                ),
                Text(
                  AppStrings.kSession,
                  style: GoogleFonts.roboto(
                    color: Color(0xff006e6f),
                    fontSize: 14,
                    fontWeight: FontWeight.w900,
                  ),
                ),
                Text(
                  AppStrings.kPrice,
                  textAlign: TextAlign.start,
                  style: GoogleFonts.roboto(
                    color: Color(0xff006e6f),
                    fontSize: 14,
                    fontWeight: FontWeight.w900,
                  ),
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Divider(
              color: Colors.grey,
              height: 2,
            ),
            SizedBox(
              height: 5,
            ),
            ListView.separated(
              itemBuilder: (context, index) {
                return  ListTile(
                  contentPadding: EdgeInsets.zero,
                  dense: true,
                  leading: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Image.network(
                            AppStrings.kCategoryUrl+snapshot.data!.data[0].category[index].icon,
                            height: 20,
                            width: 20,),
                          Text(
                            snapshot.data!.data[0].category[index]
                                .categoryName ??
                                '',
                            style: GoogleFonts.roboto(
                              color: Colors.black,
                              fontSize: 12,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Text(
                            'Duration : '+snapshot.data!.data[0].category[index].duration,
                            style: GoogleFonts.roboto(
                              color: Colors.black,
                              fontSize: 10,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  title: Row(
                    children: [
                      Expanded(
                        child: Center(
                          child: Text(
                            '               '+snapshot.data!.data[0].category[index].sessionName,
                            style: GoogleFonts.roboto(
                              color: Colors.black,
                              fontSize: 12,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Row(
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: List.generate(3, (index1) {
                                print('index is $index1');
                                print('genth of list is ${snapshot.data!.data[0].category[index].sessions.length}');
                                return Column(
                                  children: [
                                    Text(
                                      index1==0?snapshot.data!.data[0].category[index].sessions[0].blockOf2:index1==1?snapshot.data!.data[0].category[index].sessions[0].blockOf3:snapshot.data!.data[0].category[index].sessions[0].blockOf4,
                                      style: GoogleFonts.roboto(
                                        color: Colors.black,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                  ],
                                );
                              }),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                FaIcon(
                                  FontAwesomeIcons.circle,
                                  size: 13,
                                  color: AppColors.kGreen,
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                FaIcon(
                                  FontAwesomeIcons.circle,
                                  size: 13,
                                  color: AppColors.kGreen,
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                FaIcon(
                                  FontAwesomeIcons.circle,
                                  size: 13,
                                  color: AppColors.kGreen,
                                ),
                              ],
                            ),
                          ],
                          mainAxisAlignment: MainAxisAlignment.end,
                        ),
                      ),
                    ],
                  ),
                );
                /*Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Wrap(
                            children: [
                              Image.network(
                                  AppStrings.kCategoryUrl+snapshot.data!.data[0].category[index].icon,
                              height: 35,
                              width: 35,),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                snapshot.data!.data[0].category[index]
                                        .categoryName ??
                                    '',
                                style: GoogleFonts.roboto(
                                  color: Colors.black,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            '                  '+snapshot.data!.data[0].category[index].duration,
                            style: GoogleFonts.roboto(
                              color: Colors.black,
                              fontSize: 10,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Center(
                      child: Text(
                        snapshot.data!.data[0].category[index].sessionName,
                        style: GoogleFonts.roboto(
                          color: Colors.black,
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: List.generate(3, (index1) {
                                print('index is $index1');
                                print('genth of list is ${snapshot.data!.data[0].category[index].sessions.length}');
                            return Column(
                              children: [
                                Text(
                                  index1==0?snapshot.data!.data[0].category[index].sessions[0].blockOf2:index1==1?snapshot.data!.data[0].category[index].sessions[0].blockOf3:snapshot.data!.data[0].category[index].sessions[0].blockOf4,
                                  style: GoogleFonts.roboto(
                                    color: Colors.black,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                              ],
                            );
                          }),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            FaIcon(
                              FontAwesomeIcons.circle,
                              size: 13,
                              color: AppColors.kGreen,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            FaIcon(
                              FontAwesomeIcons.circle,
                              size: 13,
                              color: AppColors.kGreen,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            FaIcon(
                              FontAwesomeIcons.circle,
                              size: 13,
                              color: AppColors.kGreen,
                            ),
                          ],
                        ),
                      ],
                      mainAxisAlignment: MainAxisAlignment.end,
                    ),
                  ],
                );*/
              },
              separatorBuilder: (context, index) => Divider(
                color: Colors.grey,
              ),
              shrinkWrap: true,
              itemCount: snapshot.data!.data[0].category.length,
              padding: EdgeInsets.zero,
            ),
            Divider(
              color: Colors.grey,
            ),
            Visibility(
              visible: _isDetailsViewShow ? false : true,
              child: Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: Container(
                          height: 60,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ElevatedButton(
                              onPressed: () {
                                widget.isBookSessionReturn(true);
                                // setState(() {
                                //   _isDetailsViewShow = false;
                                // });
                                // print('view profile clicked');
                                // widget.callback(AppStrings.kTrainerDetails);
                              },
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          AppColors.kGreen)),
                              child: Text(
                                AppStrings.kBookSession,
                                style: GoogleFonts.roboto(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: Container(
                          width: 140,
                          height: 60,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ElevatedButton(
                              onPressed: () {
                                widget.isBookSessionReturn(true);
                                // setState(() {
                                //   _isDetailsViewShow = false;
                                // });
                                // print('view profile clicked');
                                // widget.callback(AppStrings.kTrainerDetails);
                              },
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          AppColors.kGreen)),
                              child: Text(
                                AppStrings.kConsultation,
                                style: GoogleFonts.roboto(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Visibility(
              visible: _isDetailsViewShow ? true : false,
              child: Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: Container(
                          height: 60,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ElevatedButton(
                              onPressed: () {
                                progressDialog.show();
                                authorisationRequest();

                                // ScaffoldMessenger.of(context)
                                //     .showSnackBar(SnackBar(
                                //   content: Text(
                                //     'You only need to request autorisation once. This trainer will respond shortly',
                                //     style: TextStyle(color: Colors.white),
                                //     softWrap: true,
                                //   ),
                                //   backgroundColor: Colors.red,
                                // ));

                                // print('view profile clicked');
                                // widget.callback(AppStrings.kTrainerDetails);
                              },
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          AppColors.kGreen)),
                              child: Text(
                                /*AppStrings.kBookSession*/
                                AppStrings.kRequestAuthorisation,
                                style: GoogleFonts.roboto(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      // Container(
                      //   child: Container(
                      //     width: 140,
                      //     height: 100,
                      //     child: Padding(
                      //       padding: const EdgeInsets.all(8.0),
                      //       child: ElevatedButton(
                      //         onPressed: () {
                      //           // setState(() {
                      //           //   _isDetailsViewShow = false;
                      //           // });
                      //           // print('view profile clicked');
                      //           // widget.callback(AppStrings.kTrainerDetails);
                      //         },
                      //         style: ButtonStyle(
                      //             backgroundColor:
                      //             MaterialStateProperty.all<Color>(AppColors.kGreen)),
                      //         child: Text(
                      //           AppStrings.kConsultation,
                      //           style: GoogleFonts.roboto(
                      //             color: Colors.white,
                      //             fontWeight: FontWeight.w500,
                      //           ),
                      //         ),
                      //       ),
                      //     ),
                      //   ),
                      // ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<bool> _onBackPress() async {
    if (_isDetailsViewShow) {
    } else {
      setState(() {
        _isDetailsViewShow = true;
      });
    }
    print('back call $_isDetailsViewShow');
    return false;
  }

  static void LogPrint(Object object) async {
    int defaultPrintLength = 1020;
    if (object == null || object.toString().length <= defaultPrintLength) {
      print(object);
    } else {
      String log = object.toString();
      int start = 0;
      int endIndex = defaultPrintLength;
      int logLength = log.length;
      int tmpLogLength = log.length;
      while (endIndex < logLength) {
        print(log.substring(start, endIndex));
        endIndex += defaultPrintLength;
        start += defaultPrintLength;
        tmpLogLength -= defaultPrintLength;
      }
      if (tmpLogLength > 0) {
        print(log.substring(start, logLength));
      }
    }
  }

  Future<void> authorisationRequest() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();

    String jsonBody = jsonEncode(<String, String>{
      'trainer_id':widget.trainerId,
      'user_id': preferences.getString(AppStrings.kPrefUserIdKey).toString(),
    });

    print('json parms $jsonBody');


    try {
      final http.Response response = await http.post(
        Uri.parse(AppStrings.kSendAuthRequestUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue
        },
        body: jsonBody,);

      var _responseData = jsonDecode(response.body);
      print('response from service ${response.body}');

      Navigator.of(context).pop();

      if (response.statusCode == 200) {
        if (_responseData['status'] == 'success') {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            title: AppStrings.kSuccess,
            desc: _responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              setState(() {
                _isDetailsViewShow = false;
              });
            },
          )..show();
        } else {
          AppCommon.kErrorDialog(
              context: context,
              errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
        }
      } else {
        AppCommon.kErrorDialog(
            context: context,
            errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
      }
    } catch (exception) {
      print('exception is $exception');
      Navigator.of(context).pop();
      AppCommon.kErrorDialog(
          context: context, errorMsg: 'Some error occur.html response getting');
    }
  }


  Future<TrainerDetailsModel?> getTrainerDetailsData(
      {required String trainerId}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    print('trainer id ${widget.trainerId} and userid is ${preferences.getString(AppStrings.kPrefUserIdKey).toString()}');
    try {
      final response = await http.post(
        Uri.parse(AppStrings.kGetTrainerDetailsUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue,
        },
        body: jsonEncode(<String, String>{
          'user_id':
          preferences.getString(AppStrings.kPrefUserIdKey).toString(),
          'trainer_id' : widget.trainerId,
          // 'my_id': preferences.getString(AppStrings.kPrefUserIdKey).toString(),
          // 'device_token':
          // preferences.getString(AppStrings.kPrefDeviceToken).toString(),
          // 'device_type':
          // preferences.getString(AppStrings.kPrefDeviceType).toString(),
          // 'language': 'en',
        }),
      );
      var responseData = jsonDecode(response.body);
      LogPrint('response of larrge text ${responseData.toString()}');
      print('response from trainer details data  $responseData');
      if (response.statusCode == 200) {
        if (responseData['status'] == 'success') {
          return TrainerDetailsModel.fromJson(responseData);
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {},
          )..show();
        }
      } else {
        throw Exception('Failed to load data');
      }
    } catch (exception) {
      print('exception $exception');
    }
  }
}


class RadioListBuilder extends StatefulWidget {
  final List<String> price;

  const RadioListBuilder({required this.price});

  @override
  RadioListBuilderState createState() {
    return RadioListBuilderState();
  }
}

class RadioListBuilderState extends State<RadioListBuilder> {
  late String value;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) {
        return RadioListTile(
          value: index,
          groupValue: value,
          onChanged: (ind) => setState(() => value = ind.toString()),
          title: Text("Number $index"),
        );
      },
      itemCount: widget.price.length,
    );
  }
}
