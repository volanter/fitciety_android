import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/models/payment_history_model.dart';
import 'package:ficiety/ui/order_summary.dart';
import 'package:ficiety/ui/success_payment.dart';
import 'package:ficiety/ui/user_exercise_program.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class PaymentHistory extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Function(bool) isReturn;

  PaymentHistory({required this.scaffoldKey,required this.isReturn});

  @override
  _PaymentHistoryState createState() => _PaymentHistoryState();
}

class _PaymentHistoryState extends State<PaymentHistory> {
  late bool isSuccessPayment;
  late String selectedProgram;
  late Future<PaymentHistoryModel?> _future;
  late bool isShowLoader;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isShowLoader = true;
    _future = getPaymentHistory();
    isSuccessPayment = true;
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return isSuccessPayment?Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 120,
            color: Color(0xff006e6f),
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Padding(
                  padding: const EdgeInsets.only(
                    top: 10,
                  ),
                  child: Row(
                    children: [
                      GestureDetector(
                        child: Icon(Icons.arrow_back_sharp,color: Colors.white,)/*Image.asset(
                          AppStrings.kImgMenuBar,
                          width: 18,
                          height: 13,
                        )*/,
                        onTap: () {
                          widget.isReturn(true);
                          // if (widget.scaffoldKey.currentState!.isDrawerOpen) {
                          //   Navigator.of(context).pop();
                          // } else {
                          //   widget.scaffoldKey.currentState!.openDrawer();
                          // }
                        },
                      ),
                      // GestureDetector(
                      //   child: Image.asset(
                      //     AppStrings.kImgMenuBar,
                      //     width: 18,
                      //     height: 13,
                      //   ),
                      //   onTap: () {
                      //     if (widget.scaffoldKey.currentState!.isDrawerOpen) {
                      //       Navigator.of(context).pop();
                      //     } else {
                      //       widget.scaffoldKey.currentState!.openDrawer();
                      //     }
                      //   },
                      // ),
                      SizedBox(
                        width: 30,
                      ),
                      Text(
                        AppStrings.kPaymentHistory,
                        style: GoogleFonts.roboto(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      Spacer(),
                      Image.asset(
                        AppStrings.kImgNotification,
                        width: 17.5,
                        height: 18.5,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(
                left: 30,
                right: 30,
              ),
              child: FutureBuilder<PaymentHistoryModel?>(
                future: _future,
                builder: (BuildContext context, AsyncSnapshot<PaymentHistoryModel?>snapshot) {
                  if (!snapshot.hasData) {
                    return isShowLoader
                        ? Center(child: Container(width:50,height: 50,child: CircularProgressIndicator()))
                        : SizedBox();
                  } else {
                    return ListView.builder(
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () {
                            setState(() {
                              isSuccessPayment = false;
                            });
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(top: 15),
                            child: Container(
                              height: 120.5,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5.0,),
                                border: Border.all(color: Color(0xff66b5b5b5),),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(
                                  top: 20, left: 20, right: 20,),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Container(
                                          child: CircleAvatar(
                                            backgroundImage: AssetImage(
                                              AppStrings.kImgGymMan,),
                                          ),
                                          width: 45,
                                          height: 45,
                                        ),
                                        SizedBox(width: 20,),
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment
                                              .center,
                                          crossAxisAlignment: CrossAxisAlignment
                                              .start,
                                          children: [
                                            Text(snapshot.data!.data[0].walletHistory[index].name,
                                              style: GoogleFonts.roboto(
                                                color: Color(0xff333333),
                                                fontWeight: FontWeight.w900,
                                                fontSize: 14,
                                              ),),
                                            SizedBox(height: 3,),
                                            Text('${snapshot.data!.data[0].walletHistory[index].time} | ${snapshot.data!.data[0].walletHistory[index].date}',
                                              style: GoogleFonts.roboto(
                                                color: Color(0xff333333),
                                                fontWeight: FontWeight.w500,
                                                fontSize: 12,
                                              ),),
                                            SizedBox(height: 3,),
                                            Text(snapshot.data!.data[0].walletHistory[index].type,
                                              style: GoogleFonts.roboto(
                                                color: Color(0xff333333),
                                                fontWeight: FontWeight.w500,
                                                fontSize: 12,
                                              ),),
                                          ],
                                        ),
                                        Spacer(),
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment
                                              .center,
                                          crossAxisAlignment: CrossAxisAlignment
                                              .start,
                                          children: [
                                            Text(snapshot.data!.data[0].walletHistory[index].hoursRate,
                                              style: GoogleFonts.roboto(
                                                color: Color(0xff333333),
                                                fontWeight: FontWeight.w500,
                                                fontSize: 12,
                                              ),),
                                            SizedBox(height: 3,),
                                            Text(snapshot.data!.data[0].walletHistory[index].sesstionCount,
                                              style: GoogleFonts.roboto(
                                                color: Color(0xff333333),
                                                fontWeight: FontWeight.w500,
                                                fontSize: 12,
                                              ),),
                                            SizedBox(height: 3,),
                                            Text(
                                              '£${snapshot.data!.data[0].walletHistory[index].price}', style: GoogleFonts.roboto(
                                              color: AppColors.kGreen,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 12,
                                            ),),
                                          ],
                                        ),

                                      ],
                                    ),
                                    SizedBox(height: 10,),
                                    Divider(
                                      color: Colors.grey,
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment
                                          .start,
                                      crossAxisAlignment: CrossAxisAlignment
                                          .start,
                                      children: [
                                        Text('Payment Date & Time',
                                          style: GoogleFonts.roboto(
                                            color: Color(0xff333333),
                                            fontWeight: FontWeight.w900,
                                            fontSize: 12,
                                          ),),
                                        SizedBox(width: 5,),
                                        Text(':-', style: GoogleFonts.roboto(
                                          color: Color(0xff333333),
                                          fontWeight: FontWeight.w500,
                                          fontSize: 12,
                                        ),),
                                        SizedBox(width: 5,),
                                        Text('${snapshot.data!.data[0].walletHistory[index].time} | ${snapshot.data!.data[0].walletHistory[index].date}',
                                          style: GoogleFonts.roboto(
                                            color: Color(0xff333333),
                                            fontWeight: FontWeight.w500,
                                            fontSize: 12,
                                          ),),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                      itemCount: snapshot.data!.data[0].walletHistory.length,
                      shrinkWrap: true,
                      padding: EdgeInsets.zero,
                      scrollDirection: Axis.vertical,

                    );
                  }
                } ),
            ),
          )
        ],
      ),
    ):SuccessPayment(scaffoldKey: widget.scaffoldKey,isReturn: (value){
      setState(() {
        isSuccessPayment = value;
      });
    },);
  }

  Future<PaymentHistoryModel?> getPaymentHistory() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();

    try{
      final response = await http.post(
        Uri.parse(AppStrings.kPaymentHistoryUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue,
        },
        body: jsonEncode(<String, String>{
          'user_id': preferences.getString(AppStrings.kPrefUserIdKey).toString(),
        }),
      );
      var responseData = jsonDecode(response.body);
      setState(() {
        isShowLoader = false;
      });
      print('response from payment history $responseData');
      if (response.statusCode == 200) {
        if (responseData['status'] == 'success') {
          return PaymentHistoryModel.fromJson(responseData);
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              Navigator.of(context).pop();
            },
          )..show();
        }
      } else {
        throw Exception('Failed to load data');
      }
    }catch (exception){
      print('exception $exception');
    }
  }
  }
