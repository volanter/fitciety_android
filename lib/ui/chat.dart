import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/models/chat_list_model.dart';
import 'package:ficiety/ui/chat_details.dart';
import 'package:ficiety/ui/order_summary.dart';
import 'package:ficiety/ui/success_payment.dart';
import 'package:ficiety/ui/user_exercise_program.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:ficiety/utils/progressdialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Chat extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  Chat({required this.scaffoldKey});

  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  late bool isChatView;
  late String _userName;
  late String _receiptId;
  late ProgressDialog progressDialog;
  late SharedPreferences preferences;
  late Future<ChatListModel?> _future;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _future = getChatData();
    progressDialog = new ProgressDialog(context, ProgressDialogType.Normal);
    progressDialog.setMessage(AppStrings.kLoadingMsg);
    isChatView = true;
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return isChatView
        ? Container(
            color: Colors.white,
            height: MediaQuery.of(context).size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 120,
                  color: Color(0xff006e6f),
                  child: SafeArea(
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Padding(
                        padding: const EdgeInsets.only(
                          top: 10,
                        ),
                        child: Row(
                          children: [
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              child: Image.asset(
                                AppStrings.kImgMenuBar,
                                width: 20,
                                height: 20,
                              ),
                              onTap: () {
                                if (widget
                                    .scaffoldKey.currentState!.isDrawerOpen) {
                                  Navigator.of(context).pop();
                                } else {
                                  widget.scaffoldKey.currentState!.openDrawer();
                                }
                              },
                            ),
                            SizedBox(
                              width: 30,
                            ),
                            Text(
                              AppStrings.kMessage,
                              style: GoogleFonts.roboto(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            Spacer(),
                            FaIcon(
                              FontAwesomeIcons.search,
                              color: Colors.white,
                              size: 18.5,
                            ),

                            // Image.asset(
                            //   AppStrings.kImgNotification,
                            //   width: 17.5,
                            //   height: 18.5,
                            // ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: FutureBuilder<ChatListModel?>(
                      future: _future,
                      builder: (BuildContext context,
                          AsyncSnapshot<ChatListModel?> snapshot) {
                        if (!snapshot.hasData) {
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        } else {
                          return ListView.separated(
                            itemBuilder: (context, index) {
                              return Padding(
                                padding:
                                    const EdgeInsets.only(top: 5, bottom: 5),
                                child: ListTile(
                                  onTap: () {
                                    _userName = snapshot.data!.data[index].recipientName;
                                    _receiptId = snapshot.data!.data[index].recipientId;
                                    setState(() {
                                      isChatView = false;
                                    });
                                  },
                                  leading: Container(
                                    child: CircleAvatar(
                                      backgroundImage: NetworkImage(
                                        AppStrings.kImageUrl+snapshot.data!.data[index].recipientImage.toString(),
                                      ),
                                    ),
                                    width: 45,
                                    height: 45,
                                  ),
                                  title: Text(
                                    snapshot.data!.data[index].recipientName,
                                    style: GoogleFonts.roboto(
                                      color: Color(0xff333333),
                                      fontWeight: FontWeight.w900,
                                      fontSize: 14,
                                    ),
                                  ),
                                  subtitle: Text(
                                    snapshot.data!.data[index].message,
                                    style: GoogleFonts.roboto(
                                      color: Color(0xff444444),
                                      fontSize: 12,
                                    ),
                                  ),
                                  trailing: Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Text(
                                    snapshot.data!.data[index].time,
                                        style: GoogleFonts.roboto(
                                          color: Color(0xff71b6b4),
                                          fontSize: 10,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      snapshot.data!.data[index].unreadCount=='0'?SizedBox():CircleAvatar(
                                        maxRadius: 10,
                                        backgroundColor: Color(0xff9ccac8),
                                        child: Padding(
                                          padding: const EdgeInsets.all(5.0),
                                          child: Text(
                                            snapshot.data!.data[index].unreadCount,
                                            style: GoogleFonts.roboto(
                                              color: Color(0xff006667),
                                              fontSize: 9,
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              );
                            },
                            itemCount: snapshot.data!.data.length,
                            shrinkWrap: true,
                            padding: EdgeInsets.zero,
                            scrollDirection: Axis.vertical,
                            separatorBuilder:
                                (BuildContext context, int index) {
                              return Divider(
                                color: Colors.grey,
                              );
                            },
                          );
                        }
                      }),
                )
              ],
            ),
          )
        : ChatDetails(
            scaffoldKey: widget.scaffoldKey,
            receiptId: _receiptId,
            userName: _userName,
            isReturn: (value) {
              setState(() {
                isChatView = value;
              });
            },
          );
  }

  Future<ChatListModel?> getChatData() async {
    preferences = await SharedPreferences.getInstance();
    try {
      final response = await http.post(
        Uri.parse(AppStrings.kGetChatListUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue,
        },
        body: jsonEncode(<String, String>{
          'sender_id':
               preferences.getString(AppStrings.kPrefUserIdKey).toString(),
          // 'my_id': preferences.getString(AppStrings.kPrefUserIdKey).toString(),
          // 'device_token':
          // preferences.getString(AppStrings.kPrefDeviceToken).toString(),
          // 'device_type':
          // preferences.getString(AppStrings.kPrefDeviceType).toString(),
          // 'language': 'en',
        }),
      );

      var responseData = jsonDecode(response.body);
      print('response from chat data  $responseData');
      if (response.statusCode == 200) {
        if (responseData['status'] == 'success') {
          return ChatListModel.fromJson(responseData);
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {},
          )..show();
        }
      } else {
        throw Exception('Failed to load data');
      }
    } catch (exception) {
      print('exception $exception');
    }
  }
}
