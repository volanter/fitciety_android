import 'package:auto_size_text/auto_size_text.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:url_launcher/url_launcher.dart';

import 'notification_page.dart';

class PTProgram extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  PTProgram({required this.scaffoldKey});

  @override
  _PTProgramState createState() => _PTProgramState();
}

class _PTProgramState extends State<PTProgram> {
  int _crossAxisCount = 2;
  static const _url = 'https://www.bodycraft.com/pdfs/exercise/ExerciseBook.pdf';
  late bool isShowCurrentView;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isShowCurrentView = true;
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    print('max height ${MediaQuery.of(context).size.height}');
    print('max width ${MediaQuery.of(context).size.width}');
    var size = MediaQuery.of(context).size;
    double value = width>320&&height>625?420:220;
    print('value is $value');
    final double itemHeight = (size.height - value) / 2;
    final double itemWidth = size.width / 2;
    double _crossAxisSpacing = 5.0,
        _mainAxisSpacing = 5.0,
        _aspectRatio = (itemWidth / itemHeight);
    return isShowCurrentView?Container(
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Stack(
              fit: StackFit.loose,
              children: [
                Container(
                  height: 190,
                  color: Color(0xff006e6f),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SafeArea(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Padding(
                            padding: const EdgeInsets.only(
                              top: 10,
                            ),
                            child: Row(
                              children: [
                                GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  child: Image.asset(
                                    AppStrings.kImgMenuBar,
                                    width: 18,
                                    height: 13,
                                  ),
                                  onTap: () {
                                    if (widget.scaffoldKey.currentState!
                                        .isDrawerOpen) {
                                      Navigator.of(context).pop();
                                    } else {
                                      widget.scaffoldKey.currentState!
                                          .openDrawer();
                                    }
                                  },
                                ),
                                SizedBox(
                                  width: 30,
                                ),
                                Text(
                                  AppStrings.kPTProgrammes,
                                  style: GoogleFonts.roboto(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                Spacer(),
                                GestureDetector(
                                  onTap: (){
                                    setState(() {
                                      isShowCurrentView = false;
                                    });
                                  },
                                  behavior: HitTestBehavior.translucent,
                                  child: Image.asset(
                                    AppStrings.kImgNotification,
                                    width: 17.5,
                                    height: 18.5,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                    top: 100,
                    child: SizedBox(
                      height: MediaQuery.of(context).size.height - 200,
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: const EdgeInsets.only(
                          right: 30,
                          left: 30,
                        ),
                        child: GridView.builder(
                          shrinkWrap: true,
                          itemCount: 6,
                          itemBuilder: (context, index) => Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(
                                5.0,
                              ),
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(
                                      5.0,
                                    ),
                                    border: Border.all(
                                      color: Color(
                                        0xff006e6f,
                                      ),
                                    )),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Image.asset(
                                      AppStrings.kImgFitnessCenter,
                                      width: 75,
                                      height: 69,
                                      fit: BoxFit.fill,
                                    ),
                                    GestureDetector(
                                      behavior: HitTestBehavior.translucent,
                                      onTap: _launchURL,
                                      child: Container(
                                        decoration: BoxDecoration(
                                            // borderRadius: BorderRadius.only(
                                            //   bottomLeft: Radius.circular(5.0,),
                                            //   bottomRight: Radius.circular(5.0,),
                                            // ),
                                          color: Color(0xff2F8C8D),
                                        ),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            SizedBox(
                                              child: Padding(
                                                padding: const EdgeInsets.only(left: 10),
                                                child: AutoSizeText(
                                                  'Cable mchine rear deltoid row',
                                                  softWrap: true,
                                                  style: GoogleFonts.roboto(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 12,
                                                  ),
                                                ),
                                              ),
                                              width: 100,
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: Align(
                                                alignment: Alignment.bottomRight,
                                                child: CircleAvatar(
                                                  backgroundColor: Colors.white,
                                                  maxRadius: 10,
                                                  child: Icon(
                                                    Icons.download_rounded,
                                                    color: Color(
                                                      0xff006e6f,
                                                    ),
                                                    size: 10,
                                                  ),
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: _crossAxisCount,
                            crossAxisSpacing: _crossAxisSpacing,
                            mainAxisSpacing: _mainAxisSpacing,
                            childAspectRatio: _aspectRatio,
                          ),
                        ),
                      ),
                    ))
              ],
            ),
          ),
        ],
      ),
    ):NotificationPage(isReturn: (value){
      setState(() {
        isShowCurrentView = true;
      });
    },scaffoldKey: widget.scaffoldKey,);
  }

  void _launchURL() async =>  await canLaunch(_url) ? await launch(_url) : throw 'Could not launch $_url';
}
