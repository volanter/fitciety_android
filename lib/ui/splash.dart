import 'package:ficiety/ui/slider_screen.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 2),(){
      Navigator.of(context).pushReplacementNamed(AppStrings.kSinglePageRoute);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage(
              AppStrings.kImgBg,
            ),
          ),
        ),
        child: Center(
          child: Image.asset(
            AppStrings.kImgLogo,
            fit: BoxFit.fill,
            width: 149.5,
            height: 81,
          ),
        ),
      ),
    );
  }



}
