import 'package:dotted_line/dotted_line.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:group_radio_button/group_radio_button.dart';

class SuccessPayment extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Function(bool) isReturn;

  SuccessPayment({required this.scaffoldKey, required this.isReturn});

  @override
  _SuccessPaymentState createState() => _SuccessPaymentState();
}

class _SuccessPaymentState extends State<SuccessPayment> {


  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 120,
            color: Color(0xff006e6f),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SafeArea(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        top: 10,
                      ),
                      child: Row(
                        children: [
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: Icon(Icons.arrow_back_sharp,color: Colors.white,)/*Image.asset(
                          AppStrings.kImgMenuBar,
                          width: 18,
                          height: 13,
                        )*/,
                            onTap: () {
                              widget.isReturn(true);
                              // if (widget.scaffoldKey.currentState!.isDrawerOpen) {
                              //   Navigator.of(context).pop();
                              // } else {
                              //   widget.scaffoldKey.currentState!.openDrawer();
                              // }
                            },
                          ),
                          // GestureDetector(
                          //   child: Image.asset(
                          //     AppStrings.kImgMenuBar,
                          //     width: 18,
                          //     height: 13,
                          //   ),
                          //   onTap: () {
                          //     if (widget
                          //         .scaffoldKey.currentState!.isDrawerOpen) {
                          //       Navigator.of(context).pop();
                          //     } else {
                          //       widget.scaffoldKey.currentState!.openDrawer();
                          //     }
                          //   },
                          // ),
                          SizedBox(
                            width: 30,
                          ),
                          Text(
                            AppStrings.kSuccessPayment,
                            style: GoogleFonts.roboto(
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Spacer(),
                          Image.asset(
                            AppStrings.kImgNotification,
                            width: 17.5,
                            height: 18.5,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                child: Image.asset(
                  AppStrings.kImgPayment,
                  fit: BoxFit.fill,
                  height: 247,
                  alignment: Alignment.center,
                ),
              ),
              Container(
                width: 35,
                height: 35,
                decoration: BoxDecoration(color: Color(0xff3FAF47,),
                  shape: BoxShape.circle,
                ),
                child: Center(
                  child: FaIcon(
                    FontAwesomeIcons.check,
                    size: 20,
                    color: AppColors.kWhite,
                  ),
                ),
              ),
              SizedBox(height: 20,),
              Text(AppStrings.kPaymentSuccessful,style: GoogleFonts.roboto(
                color: Color(0xff006e6f),fontWeight: FontWeight.bold,
                fontSize: 18,
              ),),
              SizedBox(height: 20,),
              Text(AppStrings.kCongratulationMsg,style: GoogleFonts.roboto(
                color: Color(0xff666666),
                fontSize: 10,
              ),textAlign: TextAlign.center,),
              SizedBox(height: 20,),
              Container(
                width: 154,
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pushReplacementNamed(AppStrings.kUserDashboardRoute,);
                    // print('view profile clicked');
                    // widget.callback(AppStrings.kTrainerDetails);
                  },
                  style: ButtonStyle(
                      backgroundColor:
                      MaterialStateProperty.all<Color>(AppColors.kGreen)),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      AppStrings.kHomePage,
                      style: GoogleFonts.roboto(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
