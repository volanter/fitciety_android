import 'package:dotted_line/dotted_line.dart';
import 'package:ficiety/ui/payment_history.dart';
import 'package:ficiety/ui/success_payment.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_credit_card/credit_card_form.dart';
import 'package:flutter_credit_card/credit_card_model.dart';
import 'package:flutter_credit_card/credit_card_widget.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:group_radio_button/group_radio_button.dart';

class OrderSummary extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Function (bool) isReturn;

  OrderSummary({required this.scaffoldKey,required this.isReturn});

  @override
  _OrderSummaryState createState() => _OrderSummaryState();
}

class _OrderSummaryState extends State<OrderSummary> {
  String _bankDetailValue = 'Paypal';
  late bool _isShowHistory;

  List<String> _baneDetail = ['Direct Bank Transfer', 'Paypal','Cash','Add Bank Details'];

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  String cardNumber = '';
  String expiryDate = '';
  String cardHolderName = '';
  String cvvCode = '';
  bool isCvvFocused = false;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _isShowHistory = true;

  }


  @override
  Widget build(BuildContext context) {
    return _isShowHistory?WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        key: _scaffoldKey,
        body: Container(
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 120,
                color: Color(0xff006e6f),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SafeArea(
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Padding(
                          padding: const EdgeInsets.only(
                            top: 10,
                          ),
                          child: Row(
                            children: [
                              GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                child: Icon(Icons.arrow_back_sharp,color: Colors.white,)/*Image.asset(
                            AppStrings.kImgMenuBar,
                            width: 18,
                            height: 13,
                          )*/,
                                onTap: () {
                                  widget.isReturn(true);
                                  // if (widget.scaffoldKey.currentState!.isDrawerOpen) {
                                  //   Navigator.of(context).pop();
                                  // } else {
                                  //   widget.scaffoldKey.currentState!.openDrawer();
                                  // }
                                },
                              ),
/*                            GestureDetector(
                                child: Image.asset(
                                  AppStrings.kImgMenuBar,
                                  width: 18,
                                  height: 13,
                                ),
                                onTap: () {
                                  if (widget
                                      .scaffoldKey.currentState!.isDrawerOpen) {
                                    Navigator.of(context).pop();
                                  } else {
                                    widget.scaffoldKey.currentState!.openDrawer();
                                  }
                                },
                              )*/
                              SizedBox(
                                width: 30,
                              ),
                              Text(
                                'Payment',
                                style: GoogleFonts.roboto(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              Spacer(),
                              Image.asset(
                                AppStrings.kImgNotification,
                                width: 17.5,
                                height: 18.5,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  left: 30,
                  right: 30,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 30,
                    ),
                    Row(
                      children: [
                        Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              color: Colors.grey.shade400,
                            ),
                            image: DecorationImage(
                                fit: BoxFit.fill,
                                image: AssetImage(
                                    AppStrings.kImgGymMan,)),
                          ),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Text(
                          'Mark Hemmings',
                          style: GoogleFonts.roboto(
                            color: AppColors.kBlack,
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Session Time : 9 - 10 am',
                      style: GoogleFonts.roboto(
                        color: AppColors.kBlack,
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Session Date : 25 may,2021',
                      style: GoogleFonts.roboto(
                        color: AppColors.kBlack,
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Category : Online Training Session',
                      style: GoogleFonts.roboto(
                        color: AppColors.kBlack,
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Divider(color: Colors.grey,thickness: 1,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '£100 / 1 hrs ',
                              style: GoogleFonts.roboto(
                                color: AppColors.kBlack,
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            Text(
                              '5 Session ',
                              style: GoogleFonts.roboto(
                                color: AppColors.kBlack,
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ],
                        ),
                        Text(
                          '£500 ',
                          style: GoogleFonts.roboto(
                            color: AppColors.kGreen,
                            fontSize: 14,
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 50,
              ),
              DottedLine(dashColor: Colors.grey,),
              Padding(
                padding: const EdgeInsets.only(
                  left: 30,
                  right: 30,
                  top: 30,
                ),
                child: Container(
                  height: 210,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.0,),
                    border: Border.all(color: Color(0xffcae3e5),),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          AppStrings.kSelectPaymentMethod,
                          style: GoogleFonts.roboto(
                            color: AppColors.kGreen,
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: EdgeInsets.zero,
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: RadioGroup<String>.builder(
                            groupValue: _bankDetailValue,
                            onChanged: (value) {
                              print('value changed $value');
                              setState(() {
                                _bankDetailValue = value;
                              });
                              if(_bankDetailValue=='Add Bank Details'){
                                _scaffoldKey.currentState!.showBottomSheet((context) => showCardView());
                              }
                            },
                            items: _baneDetail,
                            itemBuilder: (item) => RadioButtonBuilder(
                              item,
                            ),
                          ),
                        ),
                      ),
                      Spacer(),
                      Container(
                        height: 32.5,
                        color: Color(0xfff0f6ff),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 30,
                  right: 30,
                  top: 10,),
                child: Container(
                  width: double.infinity,
                  height: 34.5,
                  child: ElevatedButton(
                    onPressed: () {
                      setState(() {
                        _isShowHistory = false;
                      });
                      // print('view profile clicked');
                      // widget.callback(AppStrings.kTrainerDetails);
                    },
                    style: ButtonStyle(
                        backgroundColor:
                        MaterialStateProperty.all<Color>(AppColors.kGreen)),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        AppStrings.kPayNow,
                        style: GoogleFonts.roboto(
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    ):SuccessPayment(scaffoldKey: widget.scaffoldKey,isReturn: (value){
      setState(() {
        _isShowHistory = true;
      });
    },);
  }

  Future<bool> _onBackPressed() async{
    widget.isReturn(true);
    return false;
  }

  Widget showCardView() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Container(
            child: Column(
              children: [
                SizedBox(height: 20,),
                CreditCardWidget(
                  cardNumber: cardNumber,
                  expiryDate: expiryDate,
                  cardHolderName: cardHolderName,
                  cvvCode: cvvCode,
                  showBackView: isCvvFocused,
                  obscureCardNumber: true,
                  obscureCardCvv: true,
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        CreditCardForm(
                          formKey: formKey,
                          obscureCvv: true,
                          obscureNumber: true,
                          cardNumber: cardNumber,
                          cvvCode: cvvCode,
                          cardHolderName: cardHolderName,
                          expiryDate: expiryDate,
                          themeColor: Colors.blue,
                          cardNumberDecoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Number',
                            hintText: 'XXXX XXXX XXXX XXXX',
                          ),
                          expiryDateDecoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Expired Date',
                            hintText: 'XX/XX',
                          ),
                          cvvCodeDecoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'CVV',
                            hintText: 'XXX',
                          ),
                          cardHolderDecoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Card Holder',
                          ),
                          onCreditCardModelChange: onCreditCardModelChange,
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                            primary: const Color(0xff1b447b),
                          ),
                          child: Container(
                            margin: const EdgeInsets.all(8),
                            child: const Text(
                              'Validate',
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'halter',
                                fontSize: 14,
                                package: 'flutter_credit_card',
                              ),
                            ),
                          ),
                          onPressed: () {
                            if (formKey.currentState!.validate()) {
                              print('valid!');
                            } else {
                              print('invalid!');
                            }
                          },
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );

  }

  void onCreditCardModelChange(CreditCardModel? creditCardModel) {
    setState(() {
      cardNumber = creditCardModel!.cardNumber;
      expiryDate = creditCardModel.expiryDate;
      cardHolderName = creditCardModel.cardHolderName;
      cvvCode = creditCardModel.cvvCode;
      isCvvFocused = creditCardModel.isCvvFocused;
    });
  }
}
