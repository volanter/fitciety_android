import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/graph_ui/measurement_graph.dart';
import 'package:ficiety/models/waist_hips_report_model.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:ficiety/utils/progressdialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'notification_page.dart';

class WaistHipsMeasurements extends StatefulWidget {

  final GlobalKey<ScaffoldState> scaffoldKey;
  final Function(bool) isReturn;
  WaistHipsMeasurements({required this.scaffoldKey, required this.isReturn});

  @override
  _WaistHipsMeasurementsState createState() => _WaistHipsMeasurementsState();
}

class _WaistHipsMeasurementsState extends State<WaistHipsMeasurements> {
  late bool isShowView;

  int _value = 1;

  TextEditingController _controllerWeight = TextEditingController(text: '50.00 LBS               ');
  TextEditingController _controllerHeight = TextEditingController(text: '120 CM   ');
  TextEditingController _controllerNeck = TextEditingController();
  TextEditingController _controllerAbdomen = TextEditingController();
  TextEditingController _controllerWaist = TextEditingController();
  TextEditingController _controllerHip = TextEditingController();

  FocusNode focusNodeWeight = FocusNode();
  FocusNode focusNodeHeight = FocusNode();
  FocusNode focusNodeNeck = FocusNode();
  FocusNode focusNodeAbdomen = FocusNode();
  FocusNode focusNodeWaist = FocusNode();
  FocusNode focusNodeHip = FocusNode();
  late bool isShowLoader;
  late Future<WaistHipsModel?> _future;
  late ProgressDialog progressDialog;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isShowView = true;
    progressDialog = ProgressDialog(context, ProgressDialogType.Normal);
    progressDialog.setMessage(AppStrings.kLoadingMsg);
    isShowLoader = true;
    _future = getReportData();
  }

  Future<void> saveData() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();

    String jsonBody = jsonEncode(<String, String>{
      'user_id':
      preferences.getString(AppStrings.kPrefUserIdKey).toString(),
      'gender_type': _value==1?'Male':'Female',
      'weight' : _controllerWeight.text,
      'height' : _controllerHeight.text,
      'neck' : _controllerNeck.text,
      'abdomen': _controllerAbdomen.text,
      'waist' : _controllerWaist.text,
      'hip' : _controllerHip.text,
    });

    print('json parms $jsonBody');

    try {
      final http.Response response = await http.post(
        Uri.parse(AppStrings.kPopSaveUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue
        },
        body: jsonBody,);

      var _responseData = jsonDecode(response.body);
      print('response from service ${response.body}');

      Navigator.of(context).pop();

      if (response.statusCode == 200) {
        if (_responseData['status'] == 'success') {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            title: AppStrings.kSuccess,
            desc: _responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              setState(() {
                _future = getReportData();
              });
            },
          )..show();
        } else {
          AppCommon.kErrorDialog(
              context: context,
              errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
        }

      } else {
        AppCommon.kErrorDialog(
            context: context,
            errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
      }
    } catch (exception) {
      print('exception is $exception');
      Navigator.of(context).pop();
      AppCommon.kErrorDialog(
          context: context, errorMsg: 'Some error occur.html response getting');
    }

  }


  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff057A87),
    ));
    return isShowView?Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _showViewDialog();
        },
        backgroundColor: Color(0xff006e6f),
        child: Icon(
          Icons.add,
          color: Colors.white,
          size: 30,
        ),
      ),
      body: Container(
        color: Colors.white,
        child: FutureBuilder<WaistHipsModel?>(
          future: _future,
          builder: (BuildContext context, AsyncSnapshot<WaistHipsModel?>snapshot){
            if(!snapshot.hasData){
              return Center(child: Container(width:50,height: 50,child: CircularProgressIndicator()));
            }else{
              print('legth of hips data ${snapshot.data!.data.waistHipsData.length}');
              return Column(
                children: [
                  Expanded(
                    flex: 1,
                    child: Stack(
                      children: [
                        Container(
                          height: 200.5,
                          color: Color(0xff057A87),
                          child: Column(
                            children: [
                              SafeArea(
                                child: Padding(
                                  padding: const EdgeInsets.all(20.0),
                                  child: Row(
                                    children: [
                                      GestureDetector(
                                        child: Icon(Icons.arrow_back_sharp,color: Colors.white,)/*Image.asset(
                            AppStrings.kImgMenuBar,
                            width: 18,
                            height: 13,
                          )*/,
                                        onTap: () {
                                          widget.isReturn(true);
                                          // if (widget.scaffoldKey.currentState!.isDrawerOpen) {
                                          //   Navigator.of(context).pop();
                                          // } else {
                                          //   widget.scaffoldKey.currentState!.openDrawer();
                                          // }
                                        },
                                      ),
                                      SizedBox(
                                        width: 30,
                                      ),
                                      Text(
                                        AppStrings.kMeasurementPr,
                                        style: GoogleFonts.roboto(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                      Spacer(),
                                      // IconButton(
                                      //   icon: Icon(
                                      //     Icons.wrap_text,
                                      //     color: Colors.white,
                                      //   ),
                                      //   onPressed: () => _openSearchTrainerDialog(),
                                      // ),
                                      GestureDetector(
                                        onTap: (){
                                          setState(() {
                                            isShowView = false;
                                          });
                                        },
                                        child: Image.asset(
                                          AppStrings.kImgNotification,
                                          width: 17.5,
                                          height: 18.5,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Positioned(
                          top: 70,
                          bottom: 0,
                          left: 0,
                          right: 0,
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                  left: 15,
                                  right: 15,
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: MeasurementGraph(),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20,right:20,bottom:5,top:10,),
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text(
                                  'Current Weight',
                                  style: GoogleFonts.roboto(
                                    color: Color(0xff222222),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                  ),
                                ),
                                Text(
                                  'Current Height',
                                  style: GoogleFonts.roboto(
                                    color: Color(0xff222222),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 10,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Text(
                                  '${snapshot.data!.data.currentWeight} KG',
                                  style: GoogleFonts.roboto(
                                    color: Color(0xff006e6f),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                  ),
                                ),
                                Text(
                                  '${snapshot.data!.data.currentHeight} ft',
                                  style: GoogleFonts.roboto(
                                    color: Color(0xff006e6f),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 10,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Text(
                                  snapshot.data!.data.tips[0]??'',
                                  style: GoogleFonts.roboto(
                                    color: Color(0xffaaaaaa),
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12,
                                  ),
                                ),
                                Text(
                                  snapshot.data!.data.tips[1]??'',
                                  style: GoogleFonts.roboto(
                                    color: Color(0xffaaaaaa),
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 30,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text(
                                  ' Date  ',
                                  style: GoogleFonts.roboto(
                                    color: Color(0xff006e6f),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                  ),
                                ),
                                Text(
                                  'Weight',
                                  style: GoogleFonts.roboto(
                                    color: Color(0xff006e6f),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                  ),
                                ),
                                Text(
                                  'Hips',
                                  style: GoogleFonts.roboto(
                                    color: Color(0xff006e6f),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 10,right:10,),
                              child: Divider(),
                            ),
                            ListView.separated(itemBuilder: (context, index){
                              return                             Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    snapshot.data!.data.waistHipsData[index].date,
                                    style: GoogleFonts.roboto(
                                      color: Color(0xff333333),
                                      fontWeight: FontWeight.w500,
                                      fontSize: 10,
                                    ),
                                  ),
                                  Text(
                                    snapshot.data!.data.waistHipsData[index].waist??'',
                                    style: GoogleFonts.roboto(
                                      color: Color(0xff333333),
                                      fontWeight: FontWeight.w500,
                                      fontSize: 10,
                                    ),
                                  ),
                                  Text(
                                    snapshot.data!.data.waistHipsData[index].hips??'',
                                    style: GoogleFonts.roboto(
                                      color: Color(0xff333333),
                                      fontWeight: FontWeight.w500,
                                      fontSize: 10,
                                    ),
                                  ),
                                ],
                              );
                            }, separatorBuilder: (context, index){
                              return Divider();
                            }, itemCount: snapshot.data!.data.waistHipsData.length, shrinkWrap: true,),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              );
            }
        },
        ),
      ),
    ):NotificationPage(scaffoldKey: widget.scaffoldKey,isReturn: (value){
      setState(() {
        isShowView = true;
      });
    },);
  }

   _showViewDialog() {
     return showDialog(
         context: context,
         builder: (context) {
           return StatefulBuilder(
             builder: (context, setState) {
               return Dialog(
                 insetPadding: EdgeInsets.symmetric(
                   horizontal: 2.0,
                   vertical: 2.0,
                 ),
                 // title: Text(
                 //   AppStrings.kSearchForTrainer,style: GoogleFonts.roboto(
                 //   color: AppColors.kGreen,fontWeight: FontWeight.w900,
                 // ),
                 // ),
                 shape: RoundedRectangleBorder(
                   borderRadius: BorderRadius.circular(
                     10.0,
                   ),
                 ),
                 child: SingleChildScrollView(
                   child: Container(
                     width: 380,
                     height: 540,
                     padding: EdgeInsets.all(
                       20.0,
                     ),
                     child: Column(
                       mainAxisAlignment: MainAxisAlignment.start,
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: [
                         Row(
                           children: [
                             Spacer(),
                             GestureDetector(
                               child: Icon(
                                 Icons.clear,
                                 color: AppColors.kGreen,
                               ),
                               onTap: () => Navigator.of(context).pop(),
                             ),
                           ],
                         ),
                         Text(
                           'Select Gender',
                           style: GoogleFonts.roboto(
                             color: Color(0xff006e6f),
                             fontWeight: FontWeight.w900,
                             fontSize: 18,
                           ),
                         ),
                         SizedBox(
                           height: 20,
                         ),
                         Row(
                           mainAxisAlignment: MainAxisAlignment.start,
                           crossAxisAlignment: CrossAxisAlignment.center,
                           children: [
                             Text(
                               'Gender' + ' :',
                               style: AppCommon.kConsultationFormTextStyle(),
                             ),
                             SizedBox(
                               width: 10,
                             ),
                             DropdownButton(
                                 value: _value,
                                 items: [
                                   DropdownMenuItem(
                                     child: Text("Male"),
                                     value: 1,
                                   ),
                                   DropdownMenuItem(
                                     child: Text("Female"),
                                     value: 2,
                                   ),
                                 ],
                                 onChanged: (value) {
                                   setState(() {
                                     _value = int.parse(value.toString());
                                   });
                                 })
                           ],
                         ),
                         SizedBox(
                           height: 20,
                         ),
                         Text(
                           'Select Weight And Height',
                           style: GoogleFonts.roboto(
                             color: Color(0xff006e6f),
                             fontWeight: FontWeight.w900,
                             fontSize: 18,
                           ),
                         ),
                         SizedBox(
                           height: 25,
                         ),
                         Row(
                           mainAxisAlignment: MainAxisAlignment.start,
                           crossAxisAlignment: CrossAxisAlignment.center,
                           children: [
                             Text(
                               'Weight :',
                               style: AppCommon.kConsultationFormTextStyle(),
                             ),
                             SizedBox(
                               width: 10,
                             ),
                             Container(
                               width: 170,
                               child: TextFormField(
                                 controller: _controllerWeight,
                                 focusNode: focusNodeWeight,
                                 maxLines: 1,
                                 textInputAction: TextInputAction.next,
                                 keyboardType: TextInputType.text,
                                 decoration: InputDecoration(
                                   contentPadding:
                                   EdgeInsets.symmetric(vertical: 2),
                                   isDense: true,
                                 ),
                                 onFieldSubmitted: (_) {
                                   AppCommon.kChangeFocusNode(
                                       currentFocusNode: focusNodeWeight,
                                       nextFocusNode: focusNodeHeight,
                                       context: context);
                                 },
                               ),
                             ),
                           ],
                         ),
                         SizedBox(
                           height: 25,
                         ),
                         Row(
                           mainAxisAlignment: MainAxisAlignment.start,
                           crossAxisAlignment: CrossAxisAlignment.center,
                           children: [
                             Text(
                               'Height : ',
                               style: AppCommon.kConsultationFormTextStyle(),
                             ),
                             SizedBox(
                               width: 10,
                             ),
                             Container(
                               width: 130,
                               child: TextFormField(
                                 controller: _controllerHeight,
                                 focusNode: focusNodeHeight,
                                 maxLines: 1,
                                 textInputAction: TextInputAction.next,
                                 keyboardType: TextInputType.text,
                                 decoration: InputDecoration(
                                   contentPadding:
                                   EdgeInsets.symmetric(vertical: 2),
                                   isDense: true,
                                 ),
                                 onFieldSubmitted: (_) {
                                   AppCommon.kChangeFocusNode(
                                       currentFocusNode: focusNodeHeight,
                                       nextFocusNode: focusNodeNeck,
                                       context: context);
                                 },
                               ),
                             ),
                           ],
                         ),
                     SizedBox(
                       height: 25,
                     ),
                     Row(
                       mainAxisAlignment: MainAxisAlignment.start,
                       crossAxisAlignment: CrossAxisAlignment.center,
                       children: [
                         Text(
                           'Neck :',
                           style: AppCommon.kConsultationFormTextStyle(),
                         ),
                         SizedBox(
                           width: 10,
                         ),
                         Container(
                           width: 150,
                           child: TextFormField(
                             controller: _controllerNeck,
                             focusNode: focusNodeNeck,
                             maxLines: 1,
                             textInputAction: TextInputAction.next,
                             keyboardType: TextInputType.text,
                             decoration: InputDecoration(
                               contentPadding:
                               EdgeInsets.symmetric(vertical: 2),
                               isDense: true,
                             ),
                             onFieldSubmitted: (_) {
                               AppCommon.kChangeFocusNode(
                                   currentFocusNode: focusNodeNeck,
                                   nextFocusNode: _value==1?focusNodeAbdomen:focusNodeWaist,
                                   context: context);
                             },
                           ),
                         ),
                       ],
                     ),
                         SizedBox(
                           height: 25,
                         ),
                         Visibility(
                           visible: _value == 2 ?true:false,
                           child: Row(
                             mainAxisAlignment: MainAxisAlignment.start,
                             crossAxisAlignment: CrossAxisAlignment.center,
                             children: [
                               Text(
                                 'Waist :',
                                 style: AppCommon.kConsultationFormTextStyle(),
                               ),
                               SizedBox(
                                 width: 10,
                               ),
                               Container(
                                 width: 150,
                                 child: TextFormField(
                                   controller: _controllerWaist,
                                   focusNode: focusNodeWaist,
                                   maxLines: 1,
                                   textInputAction: TextInputAction.next,
                                   keyboardType: TextInputType.text,
                                   decoration: InputDecoration(
                                     contentPadding:
                                     EdgeInsets.symmetric(vertical: 2),
                                     isDense: true,
                                   ),
                                   onFieldSubmitted: (_) {
                                     AppCommon.kChangeFocusNode(
                                         currentFocusNode: focusNodeWaist,
                                         nextFocusNode: focusNodeHip,
                                         context: context);
                                   },
                                 ),
                               ),
                             ],
                           ),
                         ),
                         // SizedBox(
                         //   height: 25,
                         // ),
                         Visibility(
                           visible: _value == 1 ? true:false,
                           child: Row(
                             mainAxisAlignment: MainAxisAlignment.start,
                             crossAxisAlignment: CrossAxisAlignment.center,
                             children: [
                               Text(
                                 'Abdomen :',
                                 style: AppCommon.kConsultationFormTextStyle(),
                               ),
                               SizedBox(
                                 width: 10,
                               ),
                               Container(
                                 width: 150,
                                 child: TextFormField(
                                   controller: _controllerAbdomen,
                                   focusNode: focusNodeAbdomen,
                                   maxLines: 1,
                                   textInputAction: TextInputAction.next,
                                   keyboardType: TextInputType.text,
                                   decoration: InputDecoration(
                                     contentPadding:
                                     EdgeInsets.symmetric(vertical: 2),
                                     isDense: true,
                                   ),
                                   onFieldSubmitted: (_) {
                                     AppCommon.kChangeFocusNode(
                                         currentFocusNode: focusNodeAbdomen,
                                         nextFocusNode: focusNodeWaist,
                                         context: context);
                                   },
                                 ),
                               ),
                             ],
                           ),
                         ),
                         SizedBox(
                           height: 25,
                         ),
                         Row(
                           mainAxisAlignment: MainAxisAlignment.start,
                           crossAxisAlignment: CrossAxisAlignment.center,
                           children: [
                             Text(
                               'Hip :',
                               style: AppCommon.kConsultationFormTextStyle(),
                             ),
                             SizedBox(
                               width: 10,
                             ),
                             Container(
                               width: 150,
                               child: TextFormField(
                                 controller: _controllerHip,
                                 focusNode: focusNodeHip,
                                 maxLines: 1,
                                 textInputAction: TextInputAction.done,
                                 keyboardType: TextInputType.text,
                                 decoration: InputDecoration(
                                   contentPadding:
                                   EdgeInsets.symmetric(vertical: 2),
                                   isDense: true,
                                 ),
                               ),
                             ),
                           ],
                         ),
                         SizedBox(
                           height: 40,
                         ),
                         GestureDetector(
                           onTap: (){
                             progressDialog.show();
                             saveData();
                           },
                           child: Center(
                             child: Container(
                               width: 130,
                               height: 40,
                               decoration: BoxDecoration(
                                 color: AppColors.kGreen,
                                 borderRadius: BorderRadius.circular(20),
                               ),
                               child: Center(
                                 child: Text(
                                   AppStrings.kSave,
                                   style: GoogleFonts.roboto(
                                     color: Colors.white,
                                     fontWeight: FontWeight.w500,
                                   ),
                                 ),
                               ),
                             ),
                           ),
                         ),
                       ],
                     ),
                   ),
                 ),
               );
             },
           );
         });
  }

  Future<WaistHipsModel?> getReportData() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();

    try{
      final response = await http.post(
        Uri.parse(AppStrings.kGetWaistHipsUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue,
        },
        body: jsonEncode(<String, String>{
          'user_id': preferences.getString(AppStrings.kPrefUserIdKey).toString(),
        }),
      );
      var responseData = jsonDecode(response.body);
      setState(() {
        isShowLoader = false;
      });
      print('response from waist hips report $responseData');
      if (response.statusCode == 200) {
        if (responseData['status'] == 'success') {
          return WaistHipsModel.fromJson(responseData);
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              Navigator.of(context).pop();
            },
          )..show();
        }
      } else {
        throw Exception('Failed to load data');
      }
    }catch (exception){
      print('exception $exception');
    }
  }

}

