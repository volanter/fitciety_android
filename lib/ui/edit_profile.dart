import 'dart:convert';
import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/models/my_profile_model.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:ficiety/utils/progressdialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:group_radio_button/group_radio_button.dart';
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:http/http.dart' as http;

class EditProfile extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Function(bool) isReturn;
  final bool isBackShow;

  EditProfile(
      {required this.scaffoldKey,
      required this.isReturn,
      required this.isBackShow});

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  final _formKey = GlobalKey<FormState>();
  late ProgressDialog progressDialog;
  late TextEditingController _controllerFirstName;
  late TextEditingController _controllerLastName;
  late TextEditingController _controllerEmailAddress;
  late TextEditingController _controllerPhoneNumber;
  late TextEditingController _controllerPassword;

  late TextEditingController _controllerGender;
  late TextEditingController _controllerDateOfBirth;
  List<String> _gender = ['Male', 'Female'];
  String _genderGroupValue = 'Male';

  late FocusNode focusNodeFirstName;
  late FocusNode focusNodeLastName;
  late FocusNode focusNodeEmailAddress;
  late FocusNode focusNodePhoneNumber;
  late FocusNode focusNodePassword;
  late FocusNode focusNodeGender;
  late FocusNode focusNodeDateOfBirth;
  late bool isPasswordShow;
  late Future<MyProfileModel?> _future;
  DateTime selectedDate = DateTime.now();
   PickedFile _imageFile = PickedFile('');
  final ImagePicker _picker = ImagePicker();

  late String image;

  void _pickImage() async {
    try {
      final pickedFile = await _picker.getImage(source: ImageSource.gallery);
      setState(() {
        _imageFile = pickedFile!;
      });
    } catch (e) {
      print("Image picker error " + e.toString());
    }
  }

  Future<StreamedResponse> uploadImage(filepath, url) async {
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.files.add(http.MultipartFile('image',
        File(filepath).readAsBytes().asStream(), File(filepath).lengthSync(),
        filename: filepath.split("/").last));
    Map<String,String> mapData;
    request.fields['type'] = 'profile';
    var res = await request.send();
    print('res $res');
    return res;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    progressDialog = new ProgressDialog(context, ProgressDialogType.Normal);
    progressDialog.setMessage(AppStrings.kLoadingMsg);
    image =
        AppStrings.kNoImageString;
    _future = _getProfileData();
    isPasswordShow = false;
    focusNodeFirstName = FocusNode();
    focusNodeLastName = FocusNode();
    focusNodeEmailAddress = FocusNode();
    focusNodePhoneNumber = FocusNode();
    focusNodePassword = FocusNode();
    focusNodeGender = FocusNode();
    focusNodeDateOfBirth = FocusNode();

    _controllerDateOfBirth = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            clipBehavior: Clip.none,
            fit: StackFit.loose,
            children: [
              Container(
                height: 190,
                color: Color(0xff006e6f),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SafeArea(
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Padding(
                          padding: const EdgeInsets.only(
                            top: 10,
                          ),
                          child: Row(
                            children: [
                              widget.isBackShow
                                  ? GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                      child: Icon(
                                        Icons.arrow_back_sharp,
                                        color: Colors.white,
                                      ) /*Image.asset(
                          AppStrings.kImgMenuBar,
                          width: 18,
                          height: 13,
                        )*/
                                      ,
                                      onTap: () {
                                        widget.isReturn(true);
                                        // if (widget.scaffoldKey.currentState!.isDrawerOpen) {
                                        //   Navigator.of(context).pop();
                                        // } else {
                                        //   widget.scaffoldKey.currentState!.openDrawer();
                                        // }
                                      },
                                    )
                                  : GestureDetector(
                                      child: Image.asset(
                                        AppStrings.kImgMenuBar,
                                        width: 18,
                                        height: 13,
                                      ),
                                      onTap: () {
                                        if (widget.scaffoldKey.currentState!
                                            .isDrawerOpen) {
                                          Navigator.of(context).pop();
                                        } else {
                                          widget.scaffoldKey.currentState!
                                              .openDrawer();
                                        }
                                      },
                                behavior: HitTestBehavior.translucent,
                                    ),
                              SizedBox(
                                width: 30,
                              ),
                              Text(
                                AppStrings.kEditProfile,
                                style: GoogleFonts.roboto(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              Spacer(),
                              GestureDetector(
                                onTap: () {
                                  print('save click');
                                  FocusScope.of(context).requestFocus(FocusNode());
                                  try{
                                    // if (_formKey.currentState!.validate()) {
                                    //   _formKey.currentState!.save();
                                    //
                                    // }
                                    if(_imageFile.path.isEmpty && image==AppStrings.kNoImageString){
                                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content:Text('Please upload images'),backgroundColor: Colors.redAccent,));
                                    }else{
                                      progressDialog.show();
                                      _updateProfileRequest();
                                    }

                                  }catch (exception){
                                    print('exception in save button $exception');
                                  }
                                },
                                child: Text(
                                  AppStrings.kSave,
                                  style: GoogleFonts.roboto(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                  top: 130,
                  left: 110,
                  child: Center(
                    child: GestureDetector(
                      onTap: (){
                        _pickImage();
                      },
                      child: pickImage(),
                    ),
                  )),
              Positioned(
                  top: 130,
                  right: 180,
                  child: GestureDetector(
                    onTap: (){
                      _pickImage();
                    },
                    child: Center(
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                        ),
                        child: Icon(
                          Icons.edit_sharp,
                          size: 12,
                          color: Colors.black,
                        ),
                        width: 20,
                        height: 20,
                      ),
                    ),
                  )),
            ],
          ),
          SizedBox(height: 70),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.all(10.0),
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: FutureBuilder<MyProfileModel?>(
                    future: _future,
                    builder: (BuildContext context,
                        AsyncSnapshot<MyProfileModel?> snapshot) {
                      if (!snapshot.hasData) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      } else {
                        print('data call');
                        return Form(
                          key: _formKey,
                          // autovalidateMode: AutovalidateMode.always,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: TextFormField(
                                      controller: _controllerFirstName,
                                      focusNode: focusNodeFirstName,
                                      maxLines: 1,
                                      textCapitalization: TextCapitalization.sentences,
                                      style: AppCommon.kSearchTextStyle(),
                                      textInputAction: TextInputAction.next,
                                      keyboardType: TextInputType.text,
                                      validator: (value) {
                                        return value!.isEmpty
                                            ? AppStrings.kErrorMSgFirstName
                                            : null;
                                      },
                                      decoration: InputDecoration(
                                        labelText: AppStrings.kFirstName,
                                        labelStyle:
                                            AppCommon.kProfileLabelTextStyle(),
                                        contentPadding:
                                            EdgeInsets.symmetric(vertical: 2),
                                        isDense: true,
                                        suffix: GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                _controllerFirstName.clear();
                                              });
                                              focusNodeFirstName.unfocus();
                                            },
                                            child: Icon(
                                              Icons.clear_outlined,
                                            )),
                                      ),
                                      onFieldSubmitted: (_) {
                                        AppCommon.kChangeFocusNode(
                                            currentFocusNode:
                                                focusNodeFirstName,
                                            nextFocusNode: focusNodeLastName,
                                            context: context);
                                      },
                                    ),
                                  ),
                                  SizedBox(
                                    width: 30,
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: TextFormField(
                                      controller: _controllerLastName,
                                      style: AppCommon.kSearchTextStyle(),
                                      focusNode: focusNodeLastName,
                                      textCapitalization: TextCapitalization.sentences,
                                      maxLines: 1,
                                      textInputAction: TextInputAction.next,
                                      keyboardType: TextInputType.text,
                                      validator: (value) {
                                        return value!.isEmpty
                                            ? AppStrings.kErrorMSgLastName
                                            : null;
                                      },
                                      decoration: InputDecoration(
                                        labelText: AppStrings.kLastName,
                                        labelStyle:
                                            AppCommon.kProfileLabelTextStyle(),
                                        contentPadding:
                                            EdgeInsets.symmetric(vertical: 2),
                                        isDense: true,
                                        suffix: GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                _controllerLastName.clear();
                                              });
                                              focusNodeLastName.unfocus();
                                            },
                                            child: Icon(
                                              Icons.clear_outlined,
                                            )),
                                      ),
                                      onFieldSubmitted: (_) {
                                        AppCommon.kChangeFocusNode(
                                            currentFocusNode: focusNodeLastName,
                                            nextFocusNode:
                                                focusNodeEmailAddress,
                                            context: context);
                                      },
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                controller: _controllerEmailAddress,
                                focusNode: focusNodeEmailAddress,
                                style: GoogleFonts.roboto(
                                  color: Colors.grey,
                                  fontWeight: FontWeight.w500,
                                ),
                                maxLines: 1,
                                enabled: false,
                                readOnly: true,
                                textInputAction: TextInputAction.next,
                                keyboardType: TextInputType.emailAddress,
                                validator: (value) {
                                  return value!.isEmpty
                                      ? AppStrings.kErrorMSgEmail
                                      : null;
                                },
                                decoration: InputDecoration(
                                  labelText: AppStrings.kEmail,
                                  labelStyle:
                                      AppCommon.kProfileLabelTextStyle(),
                                  contentPadding:
                                      EdgeInsets.symmetric(vertical: 2),
                                  // suffix: GestureDetector(
                                  //     onTap: () {
                                  //       setState(() {
                                  //         _controllerEmailAddress.clear();
                                  //       });
                                  //       focusNodeEmailAddress.unfocus();
                                  //     },
                                  //     child: Icon(
                                  //       Icons.clear_outlined,
                                  //     )),
                                ),
                                onFieldSubmitted: (_) {
                                  AppCommon.kChangeFocusNode(
                                      currentFocusNode: focusNodeEmailAddress,
                                      nextFocusNode: focusNodePhoneNumber,
                                      context: context);
                                },
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                controller: _controllerPhoneNumber,
                                focusNode: focusNodePhoneNumber,
                                maxLines: 1,
                                style: AppCommon.kSearchTextStyle(),
                                textInputAction: TextInputAction.next,
                                keyboardType: TextInputType.number,
                                validator: (value) {
                                  return value!.isEmpty
                                      ? AppStrings.kErrorMsgPhoneNo
                                      : null;
                                },
                                decoration: InputDecoration(
                                  labelText: AppStrings.kPhoneNumber,
                                  labelStyle:
                                      AppCommon.kProfileLabelTextStyle(),
                                  contentPadding:
                                      EdgeInsets.symmetric(vertical: 2),
                                  isDense: true,
                                  suffix: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          _controllerPhoneNumber.clear();
                                        });
                                        // focusNodePhoneNumber.unfocus();
                                      },
                                      child: Icon(
                                        Icons.clear_outlined,
                                      )),
                                ),
                                onFieldSubmitted: (_) {
                                  AppCommon.kChangeFocusNode(
                                      currentFocusNode: focusNodePhoneNumber,
                                      nextFocusNode: focusNodePassword,
                                      context: context);
                                },
                              ),
                              // SizedBox(
                              //   height: 10,
                              // ),
                              // TextFormField(
                              //   controller: _controllerPassword,
                              //   focusNode: focusNodePassword,
                              //   maxLines: 1,
                              //   style: AppCommon.kSearchTextStyle(),
                              //   textInputAction: TextInputAction.next,
                              //   keyboardType: TextInputType.text,
                              //   obscureText: isPasswordShow ? false : true,
                              //   validator: (value) {
                              //     return value!.isEmpty
                              //         ? AppStrings.kErrorMsgPassword
                              //         : null;
                              //   },
                              //   decoration: InputDecoration(
                              //     labelText: AppStrings.kPassword,
                              //     labelStyle:
                              //         AppCommon.kProfileLabelTextStyle(),
                              //     contentPadding:
                              //         EdgeInsets.symmetric(vertical: 2),
                              //     suffix: GestureDetector(
                              //         onTap: () {
                              //           setState(() {
                              //             isPasswordShow = !isPasswordShow;
                              //           });
                              //         },
                              //         child: Icon(
                              //           isPasswordShow
                              //               ? Icons.visibility
                              //               : Icons.visibility_off,
                              //         )),
                              //   ),
                              //   onFieldSubmitted: (_) {
                              //     AppCommon.kChangeFocusNode(
                              //         currentFocusNode: focusNodePassword,
                              //         nextFocusNode: focusNodeGender,
                              //         context: context);
                              //   },
                              // ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    AppStrings.kGender + ':',
                                    style:
                                        AppCommon.kConsultationFormTextStyle(),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  RadioGroup<String>.builder(
                                    direction: Axis.horizontal,
                                    groupValue: _genderGroupValue,
                                    onChanged: (value) {
                                      print('value changed $value');
                                      setState(() {
                                        _genderGroupValue = value;
                                      });
                                    },
                                    items: _gender,
                                    itemBuilder: (item) => RadioButtonBuilder(
                                      item,
                                    ),
                                  ),
                                ],
                              ),
                              // TextFormField(
                              //   controller: _controllerGender,
                              //   focusNode: focusNodeGender,
                              //   maxLines: 1,
                              //   style: AppCommon.kSearchTextStyle(),
                              //   textInputAction: TextInputAction.next,
                              //   keyboardType: TextInputType.text,
                              //   validator: (value) {
                              //     return value!.isEmpty
                              //         ? AppStrings.kErrorMsgGender
                              //         : null;
                              //   },
                              //   decoration: InputDecoration(
                              //     labelText: AppStrings.kGender,
                              //     labelStyle:
                              //         AppCommon.kProfileLabelTextStyle(),
                              //     contentPadding:
                              //         EdgeInsets.symmetric(vertical: 2),
                              //     isDense: true,
                              //     // suffix: GestureDetector(
                              //     //     onTap: () {
                              //     //     },
                              //     //     child: Icon(
                              //     //       isPasswordShow?Icons.visibility:Icons.visibility_off,
                              //     //     )),
                              //   ),
                              //   onFieldSubmitted: (_) {
                              //     AppCommon.kChangeFocusNode(
                              //         currentFocusNode: focusNodeGender,
                              //         nextFocusNode: focusNodeDateOfBirth,
                              //         context: context);
                              //   },
                              // ),
                              SizedBox(
                                height: 10,
                              ),
                              GestureDetector(
                                onTap: () {
                                  _selectDate(context);
                                },
                                child: TextFormField(
                                  controller: _controllerDateOfBirth,
                                  focusNode: focusNodeDateOfBirth,
                                  maxLines: 1,
                                  style: AppCommon.kSearchTextStyle(),
                                  textInputAction: TextInputAction.done,
                                  keyboardType: TextInputType.text,
                                  enabled: false,
                                  onTap: () {
                                    _selectDate(context);
                                  },
                                  validator: (value) {
                                    return value!.isEmpty
                                        ? AppStrings.kErrorMSgDob
                                        : null;
                                  },
                                  decoration: InputDecoration(
                                    labelText: AppStrings.kDateOfBirth,
                                    labelStyle:
                                        AppCommon.kProfileLabelTextStyle(),
                                    contentPadding:
                                        EdgeInsets.symmetric(vertical: 2),
                                    // suffix: GestureDetector(
                                    //     onTap: () {
                                    //       setState(() {
                                    //         _controllerDateOfBirth.clear();
                                    //       });
                                    //       focusNodeDateOfBirth.unfocus();
                                    //     },
                                    //     child: Icon(
                                    //       Icons.clear_outlined,
                                    //     )),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      }
                    },
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<MyProfileModel?> _getProfileData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    print('current user id ${preferences.getString(AppStrings.kPrefUserIdKey).toString()}');

    try {
      final response = await http.post(
        Uri.parse(AppStrings.kGetProfileUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue,
        },
        body: jsonEncode(<String, String>{
          'user_id':
              preferences.getString(AppStrings.kPrefUserIdKey).toString(),
          'my_id': preferences.getString(AppStrings.kPrefUserIdKey).toString(),
          'device_token':
              preferences.getString(AppStrings.kPrefDeviceToken).toString(),
          'device_type':
              preferences.getString(AppStrings.kPrefDeviceType).toString(),
          'language': 'en',
        }),
      );
      var responseData = jsonDecode(response.body);
      print('response from question $responseData');
      if (response.statusCode == 200) {
        if (responseData['status'] == 'success') {
          //     text: snapshot.data!.data[0].dob);

          setState(() {
            _controllerFirstName = TextEditingController(
                text: responseData['data'][0]['fname']??'');
            _controllerLastName = TextEditingController(
                text: responseData['data'][0]['lname']??'');
            _controllerEmailAddress = TextEditingController(
                text: responseData['data'][0]['email']??'');
            _controllerPhoneNumber = TextEditingController(
                text: responseData['data'][0]['mobile']??'');
            _controllerDateOfBirth = TextEditingController(
                text: responseData['data'][0]['dob']??'');
            // _controllerPassword =
            //     TextEditingController(text: '');
            _controllerGender = TextEditingController(
                text: responseData['data'][0]['gender']??'');
            if(responseData['data'][0]['image']!=null){
              image = AppStrings.kImageUrl+responseData['data'][0]['image'];
            }else{
              image = 'https://st4.depositphotos.com/14953852/24787/v/600/depositphotos_247872612-stock-illustration-no-image-available-icon-vector.jpg';
            }
          });


          return MyProfileModel.fromJson(responseData);
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {},
          )..show();
        }
      } else {
        throw Exception('Failed to load data');
      }
    } catch (exception) {
      print('exception $exception');
    }
  }

  _selectDate(BuildContext context) async {
    final DateTime picked = (await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(1800),
      lastDate: DateTime(2025),
    ))!;
    if (picked != selectedDate) selectedDate = picked;
    print('selected data $selectedDate');

    setState(() {
      _controllerDateOfBirth = TextEditingController(
          text: '${selectedDate.toLocal()}'.split(' ')[0]);
      print('set date ${_controllerDateOfBirth.text}');
    });
    // setState(() {
    // });
  }

  Future<void> _updateProfileRequest() async {
    String imageName = '';
    String image1 = '';
    if(image==AppStrings.kNoImageString){
      var res = await uploadImage(_imageFile.path,
          AppStrings.kImageUploadUrl);
      final respStr = await res.stream.bytesToString();
      print('upload image response ${respStr}');
      var imageResponse = jsonDecode(respStr);
      // print('image name ${imageResponse['data'][0]['image']}');
      imageName = imageResponse['data'][0]['image'];
    }




    SharedPreferences preferences = await SharedPreferences.getInstance();

    String jsonBody = jsonEncode(<String, String>{
      'user_id':
      preferences.getString(AppStrings.kPrefUserIdKey).toString(),
      'fname': _controllerFirstName.text,
      'lname': _controllerLastName.text,
      'mobile': _controllerPhoneNumber.text,
      'image': image==AppStrings.kNoImageString?imageName:image,
      // 'email' :_controllerEmailAddress.text,
      'device_token':
      preferences.getString(AppStrings.kPrefDeviceToken).toString(),
      'device_type':
      preferences.getString(AppStrings.kPrefDeviceType).toString(),
      'language': 'en',
      'dob': _controllerDateOfBirth.text,
      'gender': _genderGroupValue,
      'address': '',
      'about': '',
      'area_of_expertise': '',
      'cover_image': imageName,
      // preferences.getString(AppStrings.kPrefUserRoleKey).toString(),
    });

    print('json parms $jsonBody');

    try {
      final http.Response response = await http.post(
          Uri.parse(AppStrings.kSetProfileUrl),
          headers: <String, String>{
            AppStrings.kHeaderType: AppStrings.kHeaderValue
          },
          body: jsonBody,);

      var _responseData = jsonDecode(response.body);
      print('response from service ${response.body}');

      Navigator.of(context).pop();

      if (response.statusCode == 200) {
        if (_responseData['status'] == 'success') {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            title: AppStrings.kSuccess,
            desc: _responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {},
          )..show();
        } else {
          AppCommon.kErrorDialog(
              context: context,
              errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
        }
      } else {
        AppCommon.kErrorDialog(
            context: context,
            errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
      }
    } catch (exception) {
      print('exception is $exception');
      Navigator.of(context).pop();
      AppCommon.kErrorDialog(
          context: context, errorMsg: 'Some error occur.html response getting');
    }
  }

  Widget pickImage(){
    if(_imageFile.path==''){
      return Container(
        decoration: BoxDecoration(
            border: Border.all(
              color: Colors.white,
            ),
            shape: BoxShape.circle,
            image: DecorationImage(
              fit: BoxFit.cover,
              image: NetworkImage(image),
            )

        ),
        width: 150,
        height: 100,
      );
    }else{
      return Container(
        decoration: BoxDecoration(
            border: Border.all(
              color: Colors.white,
            ),
            shape: BoxShape.circle,
            image: DecorationImage(
              fit: BoxFit.fill,
              image: FileImage(File(_imageFile.path)),
            )

        ),
        width: 150,
        height: 100,
      );

    }
  }
}
