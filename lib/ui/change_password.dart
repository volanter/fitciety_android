import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:ficiety/utils/progressdialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class ChangePassword extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Function(bool) isReturn;

  ChangePassword({required this.scaffoldKey, required this.isReturn});

  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _controllerCurrentPassword = TextEditingController();
  TextEditingController _controllerNewPassword = TextEditingController();
  TextEditingController _controllerConfirmNewPassword = TextEditingController();
  late ProgressDialog progressDialog;
  late SharedPreferences preferences;

  late FocusNode focusNodeCurrentPassword;
  late FocusNode focusNodeNewPassword;
  late FocusNode focusNodeConfirmNewPassword;

  late bool isCurrentPasswordShow;
  late bool isNewPasswordShow;
  late bool isConfirmNewPasswordShow;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    progressDialog = new ProgressDialog(context, ProgressDialogType.Normal);
    progressDialog.setMessage(AppStrings.kLoadingMsg);
    isCurrentPasswordShow = false;
    isNewPasswordShow = false;
    isConfirmNewPasswordShow = false;
    focusNodeCurrentPassword = FocusNode();
    focusNodeNewPassword = FocusNode();
    focusNodeConfirmNewPassword = FocusNode();
    SharedPreferences.getInstance().then((value) => preferences = value);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: Scaffold.of(context).appBarMaxHeight,
            color: Color(0xff006e6f),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SafeArea(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        top: 10,
                      ),
                      child: Row(
                        children: [
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: Icon(
                              Icons.arrow_back_sharp,
                              color: Colors.white,
                            ) /*Image.asset(
                          AppStrings.kImgMenuBar,
                          width: 18,
                          height: 13,
                        )*/
                            ,
                            onTap: () {
                              widget.isReturn(true);
                              // if (widget.scaffoldKey.currentState!.isDrawerOpen) {
                              //   Navigator.of(context).pop();
                              // } else {
                              //   widget.scaffoldKey.currentState!.openDrawer();
                              // }
                            },
                          ),
                          // GestureDetector(
                          //   child: Image.asset(
                          //     AppStrings.kImgMenuBar,
                          //     width: 18,
                          //     height: 13,
                          //   ),
                          //   onTap: () {
                          //     if (widget
                          //         .scaffoldKey.currentState!.isDrawerOpen) {
                          //       Navigator.of(context).pop();
                          //     } else {
                          //       widget.scaffoldKey.currentState!.openDrawer();
                          //     }
                          //   },
                          // ),
                          SizedBox(
                            width: 30,
                          ),
                          Text(
                            AppStrings.kChangePassword,
                            style: GoogleFonts.roboto(
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Spacer(),
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: () {
                              if (_formKey.currentState!.validate()) {
                                _formKey.currentState!.save();
                                progressDialog.show();
                                _changePasswordRequest(
                                    oldPassword:
                                        _controllerCurrentPassword.text,
                                    newPassword: _controllerNewPassword.text);
                              }
                            },
                            child: Text(
                              AppStrings.kSave,
                              style: GoogleFonts.roboto(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 30),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.all(10.0),
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Form(
                    key: _formKey,
                    // autovalidateMode: AutovalidateMode.always,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextFormField(
                          controller: _controllerCurrentPassword,
                          focusNode: focusNodeCurrentPassword,
                          style: AppCommon.kSearchTextStyle(),
                          obscureText: isCurrentPasswordShow ? false : true,
                          maxLines: 1,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          validator: (value) {
                            return value!.isEmpty
                                ? AppStrings.kErrorMSgCurrentPassword
                                : null;
                          },
                          decoration: InputDecoration(
                            labelText: AppStrings.kCurrentPassword,
                            labelStyle: AppCommon.kProfileLabelTextStyle(),
                            contentPadding: EdgeInsets.symmetric(vertical: 2),
                            isDense: true,
                            suffix: GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                onTap: () {
                                  setState(() {
                                    isCurrentPasswordShow =
                                        !isCurrentPasswordShow;
                                  });
                                },
                                child: Icon(
                                  isCurrentPasswordShow
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                )),
                          ),
                          onFieldSubmitted: (_) {
                            AppCommon.kChangeFocusNode(
                                currentFocusNode: focusNodeCurrentPassword,
                                nextFocusNode: focusNodeNewPassword,
                                context: context);
                          },
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: _controllerNewPassword,
                          focusNode: focusNodeNewPassword,
                          style: AppCommon.kSearchTextStyle(),
                          obscureText: isNewPasswordShow ? false : true,
                          maxLines: 1,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          validator: (value) {
                            return value!.isEmpty
                                ? AppStrings.kErrorMSgNewPassword
                                : null;
                          },
                          decoration: InputDecoration(
                            labelText: AppStrings.kNewPassword,
                            labelStyle: AppCommon.kProfileLabelTextStyle(),
                            contentPadding: EdgeInsets.symmetric(vertical: 2),
                            isDense: true,
                            suffix: GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                onTap: () {
                                  setState(() {
                                    isNewPasswordShow = !isNewPasswordShow;
                                  });
                                },
                                child: Icon(
                                  isNewPasswordShow
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                )),
                          ),
                          onFieldSubmitted: (_) {
                            AppCommon.kChangeFocusNode(
                                currentFocusNode: focusNodeNewPassword,
                                nextFocusNode: focusNodeConfirmNewPassword,
                                context: context);
                          },
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: _controllerConfirmNewPassword,
                          focusNode: focusNodeConfirmNewPassword,
                          style: AppCommon.kSearchTextStyle(),
                          obscureText: isConfirmNewPasswordShow ? false : true,
                          maxLines: 1,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          validator: (value) {
                            return value!.isEmpty
                                ? AppStrings.kErrorMSgConfirmNewPassword
                                : null;
                          },
                          decoration: InputDecoration(
                            labelText: AppStrings.kConfirmNewPassword,
                            labelStyle: AppCommon.kProfileLabelTextStyle(),
                            contentPadding: EdgeInsets.symmetric(vertical: 2),
                            isDense: true,
                            suffix: GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                onTap: () {
                                  setState(() {
                                    isConfirmNewPasswordShow =
                                        !isConfirmNewPasswordShow;
                                  });
                                },
                                child: Icon(
                                  isConfirmNewPasswordShow
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                )),
                          ),
                          onFieldSubmitted: (_) {
                            focusNodeConfirmNewPassword.unfocus();
                          },
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 10,
                            right: 10,
                            top: 10,
                          ),
                          child: Container(
                            width: double.infinity,
                            height: 34.5,
                            child: ElevatedButton(
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  _formKey.currentState!.save();
                                  progressDialog.show();
                                  _changePasswordRequest(
                                      oldPassword:
                                      _controllerCurrentPassword.text,
                                      newPassword: _controllerNewPassword.text);
                                }

                                // print('view profile clicked');
                                // widget.callback(AppStrings.kTrainerDetails);
                              },
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          AppColors.kGreen)),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  AppStrings.kUpdatePassword,
                                  style: GoogleFonts.roboto(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _changePasswordRequest({required String oldPassword, required String newPassword}) async{
    try{
      final http.Response response = await http.post(
          Uri.parse(AppStrings.kChangePasswordUrl),
          headers: <String, String>{
            AppStrings.kHeaderType: AppStrings.kHeaderValue
          },
          body: jsonEncode(<String, String>{
            'user_id': preferences.getString(AppStrings.kPrefUserIdKey).toString(),
            'old_pass' : oldPassword,
            'new_pass' : newPassword,
            'device_token': preferences.getString(AppStrings.kPrefDeviceToken).toString(),
            'device_type': preferences.getString(AppStrings.kPrefDeviceType).toString(),
            'language': 'en',
          }));

      var _responseData = jsonDecode(response.body);
      print('response from service ${response.body}');

      Navigator.of(context).pop();

      if (response.statusCode == 200) {
        if (_responseData['status'] == 'success') {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            title: AppStrings.kSuccess,
            desc: _responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {},
          )..show();
        } else {
          AppCommon.kErrorDialog(
              context: context,
              errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
        }
      } else {
        AppCommon.kErrorDialog(
            context: context,
            errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
      }
    }catch(exception){
      print('exception is $exception');
      Navigator.of(context).pop();
      AppCommon.kErrorDialog(
          context: context,
          errorMsg: 'Some error occur.html response getting');
    }
  }
}
