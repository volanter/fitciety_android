import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/ui/view_summary.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:ficiety/utils/progressdialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:group_radio_button/group_radio_button.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class Consultation extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  Consultation({required this.scaffoldKey});

  @override
  _ConsultationState createState() => _ConsultationState();
}

class _ConsultationState extends State<Consultation> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _controllerFirstName = TextEditingController();
  TextEditingController _controllerLastName = TextEditingController();
  TextEditingController _controllerEmailAddress = TextEditingController();
  TextEditingController _controllerPhoneNumber = TextEditingController();
  TextEditingController _controllerDateOfBirth = TextEditingController();
  TextEditingController _controllerOccupation = TextEditingController();
  TextEditingController _controllerMyService = TextEditingController();
  TextEditingController _controllerMyService1 = TextEditingController();
  TextEditingController _controllerPrimaryGoal = TextEditingController();
  TextEditingController _controllerPrimaryGoal1 = TextEditingController();
   late ProgressDialog progressDialog;
   late SharedPreferences preferences;

  late FocusNode focusNodeFirstName;
  late FocusNode focusNodeLastName;
  late FocusNode focusNodeEmailAddress;
  late FocusNode focusNodePhoneNumber;
  late FocusNode focusNodeDateOfBirth ;
  late FocusNode focusNodeOccupation;
  late FocusNode focusNodeMyService ;
  late FocusNode focusNodeMyService1;
  late FocusNode focusNodePrimaryGoal;
  late FocusNode focusNodePrimaryGoal1;

  DateTime selectedDate = DateTime.now();

  List<String> _gender = ['Male', 'Female'];
  String _genderGroupValue = 'Male';
  String _physicalActivityGroupValue = 'Yes';
  String _recentSurgeryGroupValue = 'Yes';
  List<String> _physicalActivity = ['Yes', 'No'];
  List<String> _recentSurgery = ['Yes', 'No'];
  late bool isMorning;
  late bool isAfterNoon;
  late bool isEvening;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isMorning = true;
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      progressDialog = new ProgressDialog(context, ProgressDialogType.Normal);
      progressDialog.setMessage(AppStrings.kLoadingMsg);
      SharedPreferences.getInstance().then((value) => preferences = value);
    });

    isAfterNoon = false;
    isEvening = true;

    focusNodeFirstName = FocusNode();
    focusNodeLastName = FocusNode();
    focusNodeEmailAddress = FocusNode();
    focusNodePhoneNumber = FocusNode();
    focusNodeDateOfBirth = FocusNode();
    focusNodeOccupation = FocusNode();
    focusNodeMyService = FocusNode();
    focusNodeMyService1 = FocusNode();
    focusNodePrimaryGoal = FocusNode();
    focusNodePrimaryGoal1 = FocusNode();


  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: Scaffold.of(context).appBarMaxHeight,
            color: Color(0xff006e6f),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SafeArea(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        top: 10,
                      ),
                      child: Row(
                        children: [
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: Image.asset(
                              AppStrings.kImgMenuBar,
                              width: 20,
                              height: 20,
                            ),
                            onTap: () {
                              if (widget
                                  .scaffoldKey.currentState!.isDrawerOpen) {
                                Navigator.of(context).pop();
                              } else {
                                widget.scaffoldKey.currentState!.openDrawer();
                              }
                            },
                          ),
                          SizedBox(
                            width: 30,
                          ),
                          Text(
                            AppStrings.kConsultation,
                            style: GoogleFonts.roboto(
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Spacer(),
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: (){
                              if(_formKey.currentState!.validate()){
                                _formKey.currentState!.save();
                                progressDialog.show();
                                _submitRequest();
                              }
                            },
                            child: Text(
                              AppStrings.kSave,
                              style: GoogleFonts.roboto(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.all(10.0),
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Form(
                    key: _formKey,
                    // autovalidateMode: AutovalidateMode.always,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              AppStrings.kFirstName + ':',
                              style: AppCommon.kConsultationFormTextStyle(),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: TextFormField(
                                controller: _controllerFirstName,
                                textCapitalization: TextCapitalization.sentences,
                                focusNode: focusNodeFirstName,
                                maxLines: 1,
                                textInputAction: TextInputAction.next,
                                keyboardType: TextInputType.text,
                                validator: (value) {
                                  return value!.isEmpty
                                      ? AppStrings.kErrorMSgFirstName
                                      : null;
                                },
                                decoration: InputDecoration(
                                  contentPadding:
                                      EdgeInsets.symmetric(vertical: 2),
                                  isDense: true,
                                ),
                                onFieldSubmitted: (_) {
                                  AppCommon.kChangeFocusNode(
                                      currentFocusNode: focusNodeFirstName,
                                      nextFocusNode: focusNodeLastName,
                                      context: context);
                                },
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              AppStrings.kLastName + ':',
                              style: AppCommon.kConsultationFormTextStyle(),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: TextFormField(
                                controller: _controllerLastName,
                                textCapitalization: TextCapitalization.sentences,
                                focusNode: focusNodeLastName,
                                maxLines: 1,
                                textInputAction: TextInputAction.next,
                                keyboardType: TextInputType.text,
                                validator: (value) {
                                  return value!.isEmpty
                                      ? AppStrings.kErrorMSgLastName
                                      : null;
                                },
                                decoration: InputDecoration(
                                  contentPadding:
                                      EdgeInsets.symmetric(vertical: 2),
                                  isDense: true,
                                ),
                                onFieldSubmitted: (_) {
                                  AppCommon.kChangeFocusNode(
                                      currentFocusNode: focusNodeLastName,
                                      nextFocusNode: focusNodeEmailAddress,
                                      context: context);
                                },
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              AppStrings.kEmailAddress + ':',
                              style: AppCommon.kConsultationFormTextStyle(),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: TextFormField(
                                controller: _controllerEmailAddress,
                                focusNode: focusNodeEmailAddress,
                                maxLines: 1,
                                textInputAction: TextInputAction.next,
                                keyboardType: TextInputType.emailAddress,
                                validator: (value) {
                                  return value!.isEmpty
                                      ? AppStrings.kErrorMSgEmail
                                      : value.contains('@')
                                          ? null
                                          : AppStrings.kErrorMSgEmailValidate;
                                },
                                decoration: InputDecoration(
                                  contentPadding:
                                      EdgeInsets.symmetric(vertical: 2),
                                  isDense: true,
                                ),
                                onFieldSubmitted: (_) {
                                  AppCommon.kChangeFocusNode(
                                      currentFocusNode: focusNodeEmailAddress,
                                      nextFocusNode: focusNodePhoneNumber,
                                      context: context);
                                },
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              AppStrings.kPhoneNumber + ':',
                              style: AppCommon.kConsultationFormTextStyle(),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: TextFormField(
                                controller: _controllerPhoneNumber,
                                focusNode: focusNodePhoneNumber,
                                maxLines: 1,
                                textInputAction: TextInputAction.next,
                                keyboardType: TextInputType.number,
                                validator: (value) {
                                  return value!.isEmpty
                                      ? AppStrings.kErrorMsgPhoneNo
                                      : null;
                                },
                                decoration: InputDecoration(
                                  contentPadding:
                                      EdgeInsets.symmetric(vertical: 2),
                                  isDense: true,
                                ),
                                onFieldSubmitted: (_) {
                                  AppCommon.kChangeFocusNode(
                                      currentFocusNode: focusNodePhoneNumber,
                                      nextFocusNode: focusNodeOccupation,
                                      context: context);
                                },
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              AppStrings.kGender + ':',
                              style: AppCommon.kConsultationFormTextStyle(),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            RadioGroup<String>.builder(
                              direction: Axis.horizontal,
                              groupValue: _genderGroupValue,
                              onChanged: (value) {
                                print('value changed $value');
                                setState(() {
                                  _genderGroupValue = value;
                                });
                              },
                              items: _gender,
                              itemBuilder: (item) => RadioButtonBuilder(
                                item,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              AppStrings.kDateOfBirth + ':',
                              style: AppCommon.kConsultationFormTextStyle(),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                onTap: (){
                                  _selectDate(context);
                                },
                                child: TextFormField(
                                  controller: _controllerDateOfBirth,
                                  focusNode: focusNodeDateOfBirth,
                                  maxLines: 1,
                                  onTap: (){
                                    _selectDate(context);
                                  },
                                  textInputAction: TextInputAction.next,
                                  keyboardType: TextInputType.text,
                                  enabled: false,
                                  validator: (value) {
                                    return value!.isEmpty
                                        ? AppStrings.kErrorMSgDob
                                        : null;
                                  },
                                  decoration: InputDecoration(
                                    contentPadding:
                                    EdgeInsets.symmetric(vertical: 2),
                                    isDense: true,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              AppStrings.kOccupation + ':',
                              style: AppCommon.kConsultationFormTextStyle(),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: TextFormField(
                                controller: _controllerOccupation,
                                focusNode: focusNodeOccupation,
                                maxLines: 1,
                                textInputAction: TextInputAction.next,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  contentPadding:
                                  EdgeInsets.symmetric(vertical: 2),
                                  isDense: true,
                                ),
                                onFieldSubmitted: (_) {
                                  AppCommon.kChangeFocusNode(
                                      currentFocusNode: focusNodeOccupation,
                                      nextFocusNode: focusNodeMyService,
                                      context: context);
                                },
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 25,
                        ),
                        Text(
                          AppStrings.kMyServices + ':',
                          style: AppCommon.kConsultationFormTextStyle(),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        TextFormField(
                          controller: _controllerMyService,
                          focusNode: focusNodeMyService,
                          maxLines: 1,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            contentPadding:
                            EdgeInsets.symmetric(vertical: 2),
                            isDense: true,
                          ),
                          onFieldSubmitted: (_) {
                            AppCommon.kChangeFocusNode(
                                currentFocusNode: focusNodeMyService,
                                nextFocusNode: focusNodeMyService1,
                                context: context);
                          },
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: _controllerMyService1,
                          focusNode: focusNodeMyService1,
                          maxLines: 1,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            contentPadding:
                            EdgeInsets.symmetric(vertical: 2),
                            isDense: true,
                          ),
                          onFieldSubmitted: (_) {
                            AppCommon.kChangeFocusNode(
                                currentFocusNode: focusNodeMyService1,
                                nextFocusNode: focusNodePrimaryGoal,
                                context: context);
                          },
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          AppStrings.kPrimaryGoal + ':',
                          style: AppCommon.kConsultationFormTextStyle(),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        TextFormField(
                          controller: _controllerPrimaryGoal,
                          focusNode: focusNodePrimaryGoal,
                          maxLines: 1,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            contentPadding:
                            EdgeInsets.symmetric(vertical: 2),
                            isDense: true,
                          ),
                          onFieldSubmitted: (_) {
                            AppCommon.kChangeFocusNode(
                                currentFocusNode: focusNodePrimaryGoal,
                                nextFocusNode: focusNodePrimaryGoal1,
                                context: context);
                          },
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: _controllerPrimaryGoal1,
                          focusNode: focusNodePrimaryGoal1,
                          maxLines: 1,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            contentPadding:
                            EdgeInsets.symmetric(vertical: 2),
                            isDense: true,
                          ),
                          onFieldSubmitted: (_) {
                            focusNodePrimaryGoal1.unfocus();
                          },
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          AppStrings.kPhysicalActivity + ':',
                          style: AppCommon.kConsultationFormTextStyle(),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        RadioGroup<String>.builder(
                          direction: Axis.vertical,
                          groupValue: _physicalActivityGroupValue,
                          onChanged: (value) {
                            print('value changed $value');
                            setState(() {
                              _physicalActivityGroupValue = value;
                            });
                          },
                          items: _physicalActivity,
                          itemBuilder: (item) => RadioButtonBuilder(
                            item,
                            textPosition: RadioButtonTextPosition.right
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          AppStrings.kSurgery + ':',
                          style: AppCommon.kConsultationFormTextStyle(),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        RadioGroup<String>.builder(
                          direction: Axis.vertical,
                          groupValue: _recentSurgeryGroupValue,
                          onChanged: (value) {
                            print('value changed $value');
                            setState(() {
                              _recentSurgeryGroupValue = value;
                            });
                          },
                          items: _recentSurgery,
                          itemBuilder: (item) => RadioButtonBuilder(
                              item,
                              textPosition: RadioButtonTextPosition.right
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          AppStrings.kBestExercise + ':',
                          style: AppCommon.kConsultationFormTextStyle(),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        CheckboxListTile(
                          dense: true,
                          contentPadding: EdgeInsets.symmetric(vertical: 0,horizontal: 0),
                          title: Text(AppStrings.kMorning,style: AppCommon.kConsultationFormTextStyle().copyWith(fontSize: 14,),),
                          value: isMorning,
                          onChanged: (newValue) {
                            setState(() {
                              isMorning = newValue!;
                            });
                          },
                          controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                        ),
                        CheckboxListTile(
                          dense: true,
                          contentPadding: EdgeInsets.symmetric(vertical: 0,horizontal: 0),
                          title: Text(AppStrings.kAfterNoon,style: AppCommon.kConsultationFormTextStyle().copyWith(fontSize: 14,)),
                          value: isAfterNoon,
                          onChanged: (newValue) {
                            setState(() {
                              isAfterNoon = newValue!;
                            });
                          },
                          controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                        ),
                        CheckboxListTile(
                          dense: true,
                          contentPadding: EdgeInsets.symmetric(vertical: 0,horizontal: 0),
                          title: Text(AppStrings.kEvening,style: AppCommon.kConsultationFormTextStyle().copyWith(fontSize: 14,)),
                          value: isEvening,
                          onChanged: (newValue) {
                            setState(() {
                              isEvening = newValue!;
                            });
                          },
                          controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
  _selectDate(BuildContext context) async {
    final DateTime picked = (await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(1800),
      lastDate: DateTime(2025),
    ))!;
    if (picked != selectedDate)
      setState(() {
        selectedDate = picked;
        print('selected data $selectedDate');
        _controllerDateOfBirth = TextEditingController(text: '${selectedDate.toLocal()}'.split(' ')[0]);
      });
  }

  Future<void> _submitRequest() async{

    String bestTime = '';
    if(isMorning){
      bestTime = 'Morning';
    }
    if(isAfterNoon){
      bestTime = bestTime.length>0?bestTime:''+',AfterNoon';
    }
    if(isEvening){
      print('is evening call $bestTime');
      bestTime = bestTime.length>0?bestTime:''+',Evening';
      print('best time text $bestTime');
    }

    String jsonBody = jsonEncode(<String, String>{
      'user_id': preferences.getString(AppStrings.kPrefUserIdKey).toString(),
      'first_name' : _controllerFirstName.text,
      'last_name' : _controllerLastName.text,
      'email_address': _controllerEmailAddress.text,
      'phone_number': _controllerPhoneNumber.text,
      'gender': _genderGroupValue,
      'dob': _controllerDateOfBirth.text,
      'occupation': _controllerOccupation.text,
      'about_my_services': _controllerMyService.text+' '+_controllerMyService1.text,
      'primary_goal': _controllerPrimaryGoal.text+' '+_controllerPrimaryGoal1.text,
      'physical_activity':_physicalActivityGroupValue,
      'recent_surgery': _recentSurgeryGroupValue,
      'best_time_exercise' : bestTime
    });
    print('json body data $jsonBody');
    try{
      final http.Response response = await http.post(
          Uri.parse(AppStrings.kConsultationUrl),
          headers: <String, String>{
            AppStrings.kHeaderType: AppStrings.kHeaderValue
          },
          body: jsonBody);

      var _responseData = jsonDecode(response.body);
      print('response from service ${response.body}');

      Navigator.of(context).pop();

      if (response.statusCode == 200) {
        if (_responseData['status'] == 'success') {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            title: AppStrings.kSuccess,
            desc: _responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {},
          )..show();
        } else {
          AppCommon.kErrorDialog(
              context: context,
              errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
        }
      } else {
        AppCommon.kErrorDialog(
            context: context,
            errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
      }
    }catch(exception){
      print('exception is $exception');
      Navigator.of(context).pop();
      AppCommon.kErrorDialog(
          context: context,
          errorMsg: 'Some error occur.html response getting');
    }

  }

}

