import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/models/trainer_list_model.dart';
import 'package:ficiety/ui/book_session_calender.dart';
import 'package:ficiety/ui/trainer_details.dart';
import 'package:ficiety/ui/user_dashboard.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:group_radio_button/group_radio_button.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

typedef StringValue = String Function(String);

class TrainerListView extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  // StringValue callback;
  TrainerListView({required this.scaffoldKey /*,required this.callback*/
      });

  @override
  _TrainerListViewState createState() => _TrainerListViewState();
}

enum TrainerViewType {
  TrainerList,
  TrainerDetails,
  BookSession,
}

class _TrainerListViewState extends State<TrainerListView> {
  late TrainerViewType trainerViewType;

  String _genderGroupValue = 'Male';

  List<String> _gender = ['Male', 'Female'];

  String _specialistGroupValue = 'Personal Training';

  List<String> _specialist = [
    'Personal Training',
    'Calisthenics',
    'Boxing',
    'Yoga / Pilates',
    'Strength & Conditioning',
    'Rehabiliation'
  ];
  late Future<TrainerListModel?> _future;
  TextEditingController _controllerTrainerName = TextEditingController();
  TextEditingController _controllerCategory = TextEditingController();
  TextEditingController _controllerNearByMe = TextEditingController();
  TextEditingController _controllerCurrentLocation = TextEditingController();
  TextEditingController _controllerLatLang = TextEditingController();
  
  FocusNode _focusNodeTrainerName = FocusNode();
  FocusNode _focusNodeCategory = FocusNode();
  FocusNode _focusNodeNearByMe = FocusNode();
  FocusNode _focusNodeCurrentLocation = FocusNode();
  FocusNode _focusNodeLatLang = FocusNode();
  GlobalKey<FormState> _formKey = GlobalKey();
  
  
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _future = getTrainerListData(search: false);
    trainerViewType = TrainerViewType.TrainerList;
  }

  @override
  Widget build(BuildContext context) {
    return trainerViewType == TrainerViewType.TrainerList
        ? Container(
            color: Colors.white,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Expanded(
                  child: Stack(
                    children: [
                      Container(
                        height: 200.5,
                        color: Color(0xff057A87),
                        child: Column(
                          children: [
                            SafeArea(
                              child: Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Row(
                                  children: [
                                    GestureDetector(
                                      behavior: HitTestBehavior.translucent,
                                      child: Image.asset(
                                        AppStrings.kImgMenuBar,
                                        width: 20,
                                        height: 20,
                                      ),
                                      onTap: () {
                                        if (widget.scaffoldKey.currentState!
                                            .isDrawerOpen) {
                                          Navigator.of(context).pop();
                                        } else {
                                          widget.scaffoldKey.currentState!
                                              .openDrawer();
                                        }
                                      },
                                    ),
                                    SizedBox(
                                      width: 30,
                                    ),
                                    Text(
                                      AppStrings.kTrainerList,
                                      style: GoogleFonts.roboto(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                    Spacer(),
                                    GestureDetector(
                                      behavior: HitTestBehavior.translucent,
                                      child: AppCommon.kIconTrainerListTextView(
                                        content: '',
                                        fontName: AppStrings.kFontAwsSolid,
                                      ),
                                      onTap: () => _openSearchTrainerDialog(),
                                    ),
                                    // IconButton(
                                    //   icon: Icon(
                                    //     Icons.wrap_text,
                                    //     color: Colors.white,
                                    //   ),
                                    //   onPressed: () => _openSearchTrainerDialog(),
                                    // ),
                                    // Image.asset(
                                    //   AppStrings.kImgNotification,
                                    //   width: 17.5,
                                    //   height: 18.5,
                                    // ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Positioned(
                        top: 80,
                        bottom: 0,
                        left: 0,
                        right: 0,
                        child: Container(
                          height: 670,
                          child: Padding(
                            padding: const EdgeInsets.only(
                              left: 20,
                            ),
                            child: FutureBuilder<TrainerListModel?>(
                                future: _future,
                                builder: (BuildContext context,
                                    AsyncSnapshot<TrainerListModel?> snapshot) {
                                  if (!snapshot.hasData) {
                                    return Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  } else {
                                    return ListView.builder(
                                        itemCount: snapshot.data?.data.length,
                                        physics:
                                            AlwaysScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        scrollDirection: Axis.horizontal,
                                        itemBuilder: (context, index) {
                                          return GestureDetector(
                                            behavior: HitTestBehavior.translucent,
                                            onTap: () {
                                              // print('container click');
                                              // widget.callback(AppStrings.kTrainerDetails);
                                            },
                                            child: Container(
                                              width: 330,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(5.0),
                                                child: Card(
                                                  color: Colors.white,
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            10.0),
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      children: [
                                                        Image.network(
                                                          AppStrings
                                                              .kImageUrl+snapshot.data!.data[index].background_image,
                                                          width: 320,
                                                          height: 120,
                                                          fit: BoxFit.fill,
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .only(
                                                            top: 10,
                                                          ),
                                                          child: Row(
                                                            children: [
                                                              CircleAvatar(
                                                                backgroundImage: NetworkImage(AppStrings.kImageUrl+snapshot.data!.data[index].image),
                                                                radius: 40,
                                                              ),
                                                              // Image.network(
                                                              //   ,
                                                              //   width: 67.5,
                                                              //   height: 67.5,
                                                              //   fit:
                                                              //       BoxFit.fill,
                                                              // ),
                                                              SizedBox(
                                                                width: 20,
                                                              ),
                                                              Column(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .start,
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Text(
                                                                   '${snapshot.data?.data[index].fname} ${snapshot.data?.data[index].lname}',
                                                                    style: GoogleFonts
                                                                        .roboto(
                                                                      color: AppColors
                                                                          .kGreen,
                                                                      fontSize:
                                                                          18,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w700,
                                                                    ),
                                                                  ),
                                                                  SizedBox(
                                                                      height:
                                                                          5),
                                                                  // Align(
                                                                  //   alignment: Alignment.topLeft,
                                                                  //   child: Row(
                                                                  //     children: [
                                                                  //       Icon(
                                                                  //         Icons.star,
                                                                  //         size: 15,
                                                                  //         color:
                                                                  //         Color(0xffffcc00),
                                                                  //       ),
                                                                  //       Icon(
                                                                  //         Icons.star,
                                                                  //         size: 15,
                                                                  //         color:
                                                                  //         Color(0xffffcc00),
                                                                  //       ),
                                                                  //       Icon(
                                                                  //         Icons.star,
                                                                  //         size: 15,
                                                                  //         color:
                                                                  //         Color(0xffffcc00),
                                                                  //       ),
                                                                  //       Icon(
                                                                  //         Icons.star,
                                                                  //         size: 15,
                                                                  //         color:
                                                                  //         Color(0xffffcc00),
                                                                  //       ),
                                                                  //       Icon(
                                                                  //         Icons.star,
                                                                  //         size: 15,
                                                                  //         color:
                                                                  //         Color(0xffffcc00),
                                                                  //       ),
                                                                  //     ],
                                                                  //   ),
                                                                  // ),
                                                                ],
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .only(
                                                            top: 10,
                                                          ),
                                                          child: Text(
                                                            snapshot.data?.data[index].about??'',
                                                            softWrap: true,
                                                            overflow:
                                                                TextOverflow
                                                                    .visible,
                                                            style: GoogleFonts
                                                                .roboto(
                                                                    color: AppColors
                                                                        .kBlack,
                                                                    fontSize:
                                                                        12),
                                                            textAlign:
                                                                TextAlign.left,
                                                          ),
                                                        ),
                                                        SizedBox(height: 10),
                                                        Text(
                                                          AppStrings
                                                              .kAreaOfExpertise,
                                                          style: GoogleFonts
                                                              .roboto(
                                                            color: AppColors
                                                                .kGreen,
                                                            fontSize: 16,
                                                            fontWeight:
                                                                FontWeight.w700,
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 15,
                                                        ),
                                                        Container(
                                                          height: 100,
                                                          child: ListView(
                                                            shrinkWrap: true,
                                                            children: List.generate(snapshot.data!.data[index].areaOfExpertise.length, (index1){
                                                              return Column(
                                                                children: [
                                                                  Row(
                                                                    children: [
                                                                      FaIcon(
                                                                        FontAwesomeIcons
                                                                            .checkCircle,
                                                                        size: 15,
                                                                        color: AppColors
                                                                            .kGreen,
                                                                      ),
                                                                      SizedBox(
                                                                        width: 15,
                                                                      ),
                                                                      Text(
                                                                        snapshot.data!.data[index].areaOfExpertise[index1],
                                                                        style: GoogleFonts
                                                                            .roboto(
                                                                          color: AppColors
                                                                              .kBlack,
                                                                        ),
                                                                        textAlign:
                                                                        TextAlign
                                                                            .left,
                                                                      ),
                                                                    ],
                                                                  ),
                                                                  SizedBox(
                                                                    height: 10,
                                                                  ),
                                                                ],
                                                              );
                                                            }),
                                                          ),
                                                        ),
                                                        // Column(
                                                        //   mainAxisSize: MainAxisSize.min,
                                                        //
                                                        //   children: List.generate(snapshot.data!.data[index].areaOfExpertise.length, (index1){
                                                        //     return Column(
                                                        //       children: [
                                                        //         Row(
                                                        //           children: [
                                                        //             FaIcon(
                                                        //               FontAwesomeIcons
                                                        //                   .checkCircle,
                                                        //               size: 15,
                                                        //               color: AppColors
                                                        //                   .kGreen,
                                                        //             ),
                                                        //             SizedBox(
                                                        //               width: 15,
                                                        //             ),
                                                        //             Text(
                                                        //               snapshot.data!.data[index].areaOfExpertise[index1],
                                                        //               style: GoogleFonts
                                                        //                   .roboto(
                                                        //                 color: AppColors
                                                        //                     .kBlack,
                                                        //               ),
                                                        //               textAlign:
                                                        //               TextAlign
                                                        //                   .left,
                                                        //             ),
                                                        //           ],
                                                        //         ),
                                                        //         SizedBox(
                                                        //           height: 10,
                                                        //         ),
                                                        //       ],
                                                        //     );
                                                        //   }),
                                                        // ),
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        Row(
                                                          children: [
                                                            FaIcon(
                                                              FontAwesomeIcons
                                                                  .mapMarkerAlt,
                                                              size: 15,
                                                              color: AppColors
                                                                  .kGreen,
                                                            ),
                                                            SizedBox(
                                                              width: 15,
                                                            ),
                                                            Expanded(
                                                              child: Text(
                                                                snapshot.data?.data[index].address??'',
                                                                softWrap: true,
                                                                overflow:
                                                                    TextOverflow
                                                                        .visible,
                                                                style:
                                                                    GoogleFonts
                                                                        .roboto(
                                                                  color: AppColors
                                                                      .kBlack,
                                                                ),
                                                                textAlign:
                                                                    TextAlign
                                                                        .left,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        Divider(
                                                          color: Colors.grey,
                                                        ),
                                                        // Row(
                                                        //   mainAxisAlignment:
                                                        //   MainAxisAlignment.spaceEvenly,
                                                        //   children: [
                                                        //     Column(
                                                        //       children: [
                                                        //         // AppCommon.kIconTrainerListView(content: '', fontName: AppStrings.kFontAwsSolid,),
                                                        //         // SizedBox(
                                                        //         //   height: 2,
                                                        //         // ),
                                                        //         Text(
                                                        //           '100',
                                                        //           style: GoogleFonts.roboto(
                                                        //             color: AppColors.kBlack,
                                                        //           ),
                                                        //           textAlign: TextAlign.left,
                                                        //         )
                                                        //       ],
                                                        //     ),
                                                        //     Column(
                                                        //       children: [
                                                        //         // AppCommon.kIconTrainerListView(content: '', fontName: AppStrings.kFontAwsSolid,),
                                                        //         // SizedBox(
                                                        //         //   height: 2,
                                                        //         // ),
                                                        //         Text(
                                                        //           '50',
                                                        //           style: GoogleFonts.roboto(
                                                        //             color: AppColors.kBlack,
                                                        //           ),
                                                        //           textAlign: TextAlign.left,
                                                        //         )
                                                        //       ],
                                                        //     ),
                                                        //     Column(
                                                        //       children: [
                                                        //         // AppCommon.kIconTrainerListView(content: '', fontName: AppStrings.kFontAwsSolid,),
                                                        //         // SizedBox(
                                                        //         //   height: 2,
                                                        //         // ),
                                                        //         Text(
                                                        //           '25',
                                                        //           style: GoogleFonts.roboto(
                                                        //             color: AppColors.kBlack,
                                                        //           ),
                                                        //           textAlign: TextAlign.left,
                                                        //         )
                                                        //       ],
                                                        //     ),
                                                        //   ],
                                                        // ),
                                                        Spacer(),
                                                        Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceEvenly,
                                                          children: [
                                                            ElevatedButton(
                                                              onPressed: () {
                                                                print(
                                                                    'view profile clicked');
                                                                setState(() {
                                                                  trainerViewType =
                                                                      TrainerViewType
                                                                          .TrainerDetails;
                                                                });
                                                                // widget.callback(AppStrings.kTrainerDetails);
                                                              },
                                                              style: ButtonStyle(
                                                                  backgroundColor: MaterialStateProperty.all<
                                                                          Color>(
                                                                      AppColors
                                                                          .kGreen)),
                                                              child: Text(
                                                                'View Profile',
                                                                style:
                                                                    GoogleFonts
                                                                        .roboto(
                                                                  color: Colors
                                                                      .white,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500,
                                                                ),
                                                              ),
                                                            ),
                                                            // ElevatedButton(
                                                            //   onPressed: () {
                                                            //     print(
                                                            //         'button tap');
                                                            //   },
                                                            //   style: ButtonStyle(
                                                            //       backgroundColor: MaterialStateProperty.all<
                                                            //               Color>(
                                                            //           AppColors
                                                            //               .kGreen)),
                                                            //   child: Text(
                                                            //     'Contact Trainer',
                                                            //     style:
                                                            //         GoogleFonts
                                                            //             .roboto(
                                                            //       color: Colors
                                                            //           .white,
                                                            //       fontWeight:
                                                            //           FontWeight
                                                            //               .w500,
                                                            //     ),
                                                            //   ),
                                                            // ),
                                                          ],
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          );
                                        });
                                  }
                                }),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        : trainerViewType == TrainerViewType.TrainerDetails
            ? TrainerDetails(
                scaffoldKey: widget.scaffoldKey,
                isReturn: (value) {
                  setState(() {
                    trainerViewType = TrainerViewType.TrainerList;
                  });
                },
                isBookSessionReturn: (value) {
                  setState(() {
                    trainerViewType = TrainerViewType.BookSession;
                  });
                }, trainerId: '1',
              )
            : BookSessionCalender(
                scaffoldKey: widget.scaffoldKey,
                isReturn: (value) {
                  setState(() {
                    trainerViewType = TrainerViewType.TrainerDetails;
                  });
                },
              );
  }

  _openSearchTrainerDialog() {
    return showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, setState) {
              return Center(
                child: SingleChildScrollView(
                  child: Dialog(
                    insetPadding: EdgeInsets.symmetric(
                      horizontal: 2.0,
                      vertical: 2.0,
                    ),
                    // title: Text(
                    //   AppStrings.kSearchForTrainer,style: GoogleFonts.roboto(
                    //   color: AppColors.kGreen,fontWeight: FontWeight.w900,
                    // ),
                    // ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(
                        10.0,
                      ),
                    ),
                    child: Container(
                      width: 380,
                      height: 630,
                      padding: EdgeInsets.all(
                        20.0,
                      ),
                      child: SingleChildScrollView(
                        child: Form(
                          key: _formKey,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Text(
                                    AppStrings.kSearchForTrainer,
                                    style: GoogleFonts.roboto(
                                      color: AppColors.kGreen,
                                      fontWeight: FontWeight.w900,
                                      fontSize: 18,
                                    ),
                                  ),
                                  Spacer(),
                                  GestureDetector(
                                    behavior: HitTestBehavior.translucent,
                                    child: Icon(
                                      Icons.cancel_outlined,
                                      color: AppColors.kGreen,
                                    ),
                                    onTap: () => Navigator.of(context).pop(),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                style: AppCommon.kSearchTextStyle(),
                                focusNode: _focusNodeTrainerName,
                                textInputAction: TextInputAction.next,
                                onFieldSubmitted: (_){
                                  AppCommon.kChangeFocusNode(currentFocusNode: _focusNodeTrainerName, nextFocusNode: _focusNodeCategory
                                      , context: context);
                                },
                                controller: _controllerTrainerName,
                                validator: (value){
                                  if(value!.isEmpty){
                                    return 'Please enter trainer name';
                                  }else{
                                    return null;
                                  }
                              },
                                decoration: InputDecoration(
                                  hintText: AppStrings.kSearchTrainerName,
                                  hintStyle: AppCommon.kSearchTextStyle()
                                      .copyWith(color: Color(0xff414040)),
                                  suffixIconConstraints: BoxConstraints(maxHeight: 30),
                                  suffixIcon: Padding(
                                    padding: const EdgeInsets.only(
                                      right: 10.0,
                                    ),
                                    child: AppCommon.kIconTrainerSearchTextView(
                                      fontName: AppStrings.kFontAwsSolid,
                                      content: '',
                                    ),
                                  ),
                                ),
                              ),
                              TextFormField(
                                focusNode: _focusNodeCategory,
                                textInputAction: TextInputAction.next,
                                onFieldSubmitted: (_){
                                  AppCommon.kChangeFocusNode(currentFocusNode: _focusNodeCategory, nextFocusNode: _focusNodeNearByMe
                                      , context: context);
                                },
                                controller: _controllerCategory,
                                validator: (value){
                                  if(value!.isEmpty){
                                    return 'Please enter category';
                                  }else{
                                    return null;
                                  }
                                },
                                style: AppCommon.kSearchTextStyle(),
                                decoration: InputDecoration(
                                  hintText: AppStrings.kSelectCategory,
                                  hintStyle: AppCommon.kSearchTextStyle()
                                      .copyWith(color: Color(0xff414040)),
                                  suffixIconConstraints: BoxConstraints(maxHeight: 30),
                                  suffixIcon: Padding(
                                    padding: const EdgeInsets.only(
                                      right: 10.0,
                                    ),
                                    child: FaIcon(
                                      FontAwesomeIcons.arrowDown,
                                      size: 15,
                                      color: AppColors.kGreen,
                                    ),
                                  ),
                                ),
                              ),
                              TextFormField(
                                focusNode: _focusNodeNearByMe,
                                textInputAction: TextInputAction.next,
                                controller: _controllerNearByMe,
                                  onFieldSubmitted: (_){
                                    AppCommon.kChangeFocusNode(currentFocusNode: _focusNodeNearByMe, nextFocusNode: _focusNodeCurrentLocation
                                        , context: context);
                                  },
                                validator: (value){
                                  if(value!.isEmpty){
                                    return 'Please enter near by me';
                                  }else{
                                    return null;
                                  }
                                },
                                style: AppCommon.kSearchTextStyle(),
                                decoration: InputDecoration(
                                  hintText: AppStrings.kSearchNearByMe,
                                  hintStyle: AppCommon.kSearchTextStyle()
                                      .copyWith(color: Color(0xff414040)),
                                  suffixIconConstraints: BoxConstraints(maxHeight: 30),
                                  suffixIcon: Padding(
                                    padding: const EdgeInsets.only(
                                      right: 10.0,
                                    ),
                                    child: AppCommon.kIconTrainerSearchTextView(
                                      fontName: AppStrings.kFontAwsSolid,
                                      content: '',
                                    ) /*FaIcon(
                                      FontAwesomeIcons.mapMarkerAlt,
                                      size: 15,
                                      color: AppColors.kGreen,
                                    )*/
                                    ,
                                  ),
                                ),
                              ),
                              TextFormField(
                                focusNode: _focusNodeCurrentLocation,
                                textInputAction: TextInputAction.next,
                                controller: _controllerCurrentLocation,
                                onFieldSubmitted: (_){
                                  AppCommon.kChangeFocusNode(currentFocusNode: _focusNodeCurrentLocation, nextFocusNode: _focusNodeLatLang
                                      , context: context);
                                },
                                // validator: (value){
                                //   if(value!.isEmpty){
                                //     return 'Please enter near by me';
                                //   }else{
                                //     return null;
                                //   }
                                // },
                                style: AppCommon.kSearchTextStyle(),
                                decoration: InputDecoration(
                                  hintText: AppStrings.kCurrentLocation,
                                  hintStyle: AppCommon.kSearchTextStyle()
                                      .copyWith(color: Color(0xff414040)),
                                  suffixIconConstraints: BoxConstraints(maxHeight: 30),
                                  suffixIcon: Padding(
                                    padding: const EdgeInsets.only(
                                      right: 10.0,
                                    ),
                                    child: AppCommon.kIconTrainerSearchTextView(
                                      fontName: AppStrings.kFontAwsSolid,
                                      content: '',
                                    ) /*FaIcon(
                                      FontAwesomeIcons.mapMarkerAlt,
                                      size: 15,
                                      color: AppColors.kGreen,
                                    )*/
                                    ,
                                  ),
                                ),
                              ),
                              TextFormField(
                                focusNode: _focusNodeLatLang,
                                textInputAction: TextInputAction.done,
                                controller: _controllerLatLang,
                                // validator: (value){
                                //   if(value!.isEmpty){
                                //     return 'Please enter near by me';
                                //   }else{
                                //     return null;
                                //   }
                                // },
                                style: AppCommon.kSearchTextStyle(),
                                decoration: InputDecoration(
                                  hintText: AppStrings.kLatLang,
                                  hintStyle: AppCommon.kSearchTextStyle()
                                      .copyWith(color: Color(0xff414040)),
                                  suffixIconConstraints: BoxConstraints(maxHeight: 30),
                                  suffixIcon: Padding(
                                    padding: const EdgeInsets.only(
                                      right: 10.0,
                                    ),
                                    child: AppCommon.kIconTrainerSearchTextView(
                                      fontName: AppStrings.kFontAwsSolid,
                                      content: '',
                                    ) /*FaIcon(
                                      FontAwesomeIcons.mapMarkerAlt,
                                      size: 15,
                                      color: AppColors.kGreen,
                                    )*/
                                    ,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                AppStrings.kSelectGender,
                                style: AppCommon.kSearchTextStyle().copyWith(
                                  color: Color(0xff414040),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              RadioGroup<String>.builder(
                                groupValue: _genderGroupValue,
                                onChanged: (value) {
                                  print('value changed $value');
                                  setState(() {
                                    _genderGroupValue = value;
                                  });
                                },
                                items: _gender,
                                itemBuilder: (item) => RadioButtonBuilder(
                                  item,
                                ),
                              ),
                              Divider(
                                color: Colors.grey,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                'Select Specialist',
                                style: AppCommon.kSearchTextStyle().copyWith(
                                  color: Color(0xff414040),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              RadioGroup<String>.builder(
                                groupValue: _specialistGroupValue,
                                onChanged: (value) => setState(() {
                                  _specialistGroupValue = value;
                                }),
                                items: _specialist,
                                itemBuilder: (item) => RadioButtonBuilder(
                                  item,
                                ),
                              ),
                              Divider(
                                color: Colors.grey,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Center(
                                child: Container(
                                  width: 130,
                                  height: 40,
                                  child: ElevatedButton(
                                    onPressed: () {
                                      if(_formKey.currentState!.validate()){
                                        _formKey.currentState!.save();
                                        Navigator.of(context).pop();
                                        setState(() {
                                          _future = getTrainerListData(search: true);
                                        });
                                      }
                                    },
                                    style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all<Color>(
                                                AppColors.kGreen)),
                                    child: Text(
                                      'Search',
                                      style: GoogleFonts.roboto(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              );
            },
          );
        });
  }

  Future<TrainerListModel?> getTrainerListData({required bool search}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    print('user id ${preferences.getString(AppStrings.kPrefUserIdKey).toString()}');
    late var response;
    try {
      if(search == false){
        response = await http.post(
          Uri.parse(AppStrings.kGetTrainerListUrl),
          headers: <String, String>{
            AppStrings.kHeaderType: AppStrings.kHeaderValue,
          },
          body: jsonEncode(<String, String>{
            'user_id':
            preferences.getString(AppStrings.kPrefUserIdKey).toString(),
            // 'my_id': preferences.getString(AppStrings.kPrefUserIdKey).toString(),
            // 'device_token':
            // preferences.getString(AppStrings.kPrefDeviceToken).toString(),
            // 'device_type':
            // preferences.getString(AppStrings.kPrefDeviceType).toString(),
            // 'language': 'en',
          }),
        );
      }else{
        response = await http.post(
          Uri.parse(AppStrings.kSearchForTrainerUrl),
          headers: <String, String>{
            AppStrings.kHeaderType: AppStrings.kHeaderValue,
          },
          body: jsonEncode(<String, String>{
            'user_id':
            preferences.getString(AppStrings.kPrefUserIdKey).toString(),
            'trainer_name_company_name': _controllerTrainerName.text,
            'category': _controllerCategory.text,
            'near_by_me': _controllerNearByMe.text,
            'gender': _genderGroupValue,
            'specialist': _specialistGroupValue,
          }),
        );
      }

      var responseData = jsonDecode(response.body);
      print('response from serch trainer data  $responseData');
      if (response.statusCode == 200) {
        if (responseData['status'] == 'success') {
          return TrainerListModel.fromJson(responseData);
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {},
          )..show();
        }
      } else {
        throw Exception('Failed to load data');
      }
    } catch (exception) {
      print('exception $exception');
    }
  }
}
