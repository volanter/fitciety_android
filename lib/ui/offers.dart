import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/models/offers_model.dart';
import 'package:ficiety/ui/chat_details.dart';
import 'package:ficiety/ui/order_summary.dart';
import 'package:ficiety/ui/success_payment.dart';
import 'package:ficiety/ui/user_exercise_program.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'notification_page.dart';

class Offers extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  Offers({required this.scaffoldKey});

  @override
  _OffersState createState() => _OffersState();
}

class _OffersState extends State<Offers> {

  List<String> _linkImage = [];
  List<String> _linkName = [];
  List<String> _linkOffer = [];
  List<String> _linkAvailable = [];
  List<String> _linkExpiry = [];
  late bool isOfferShow;
  late Future<OffersModel?> _future;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _future = getOfferData();
    isOfferShow = true;

    _linkImage.add('assets/images/hnm_9.png');
    _linkImage.add('assets/images/layer_15.png');
    _linkImage.add('assets/images/levis.png');
    _linkImage.add('assets/images/lime.png');
    _linkImage.add('assets/images/layer_15.png');

    _linkAvailable.add('Available Online  Only');
    _linkAvailable.add('Available Online  Only');
    _linkAvailable.add('Available Online  Only');
    _linkAvailable.add('Available Online  Only');
    _linkAvailable.add('Available Online  Only');

    _linkExpiry.add('Expiry 20/04/021');
    _linkExpiry.add('Expiry 20/04/021');
    _linkExpiry.add('Expiry 20/04/021');
    _linkExpiry.add('Expiry 20/04/021');
    _linkExpiry.add('Expiry 20/04/021');

    _linkName.add('Limeroad');
    _linkName.add('Fila');
    _linkName.add('H&M');
    _linkName.add("Levi's");
    _linkName.add('Adidas');

    _linkOffer.add('Pay On  Limeroad  10% Back');
    _linkOffer.add('GAKOR W Training & Gym Shoes For Women');
    _linkOffer.add('New User Offer Get 20%  Off');
    _linkOffer.add('Tracksuits Flat 40% Off');
    _linkOffer.add('Sports Shoes Flat 50% Off');

  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return isOfferShow?Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 120,
            color: Color(0xff006e6f),
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Padding(
                  padding: const EdgeInsets.only(
                    top: 10,
                  ),
                  child: Row(
                    children: [
                      GestureDetector(
                        child: Image.asset(
                          AppStrings.kImgMenuBar,
                          width: 18,
                          height: 13,
                        ),
                        onTap: () {
                          if (widget.scaffoldKey.currentState!.isDrawerOpen) {
                            Navigator.of(context).pop();
                          } else {
                            widget.scaffoldKey.currentState!.openDrawer();
                          }
                        },
                      ),
                      SizedBox(
                        width: 30,
                      ),
                      Text(
                        AppStrings.kOffers,
                        style: GoogleFonts.roboto(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      Spacer(),
                      GestureDetector(
                        onTap: (){
                          setState(() {
                            isOfferShow = false;
                          });
                        },
                        child: Image.asset(
                          AppStrings.kImgNotification,
                          width: 17.5,
                          height: 18.5,
                        ),
                      ),

                      // Image.asset(
                      //   AppStrings.kImgNotification,
                      //   width: 17.5,
                      //   height: 18.5,
                      // ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: FutureBuilder<OffersModel?>(
              future: _future,
              builder: (BuildContext context, AsyncSnapshot<OffersModel?>snapshot){
                if (!snapshot.hasData) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  return ListView.separated(
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.only(top:5,bottom: 5),
                        child: ListTile(
                          leading: Image.network(AppStrings.kImageUrl+snapshot.data!.data[index].image,width: 85,fit: BoxFit.fill,alignment: Alignment.center,),
                          title: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(snapshot.data!.data[index].companyName,style: GoogleFonts.roboto(
                                color: Color(0xffadadad),fontWeight: FontWeight.w500,
                                fontSize: 14,
                              ),),
                              SizedBox(height: 10,),
                              Text(snapshot.data!.data[index].offer,style: GoogleFonts.roboto(
                                color: Color(0xff333333),fontWeight: FontWeight.bold,
                                fontSize: 14,
                              ),),
                              SizedBox(height: 10,),
                              Text(snapshot.data!.data[index].description,style: GoogleFonts.roboto(
                                color: Color(0xffadadad),fontWeight: FontWeight.w500,
                                fontSize: 14,
                              ),),
                              SizedBox(height: 10,),
                              Text(snapshot.data!.data[index].expire,style: GoogleFonts.roboto(
                                color: Color(0xffadadad),fontWeight: FontWeight.w500,
                                fontSize: 14,
                              ),),
                            ],
                          ),
                        ),
                      );
                    },
                    itemCount: snapshot.data!.data.length,
                    shrinkWrap: true,
                    padding:EdgeInsets.zero,
                    scrollDirection: Axis.vertical, separatorBuilder: (BuildContext context, int index) {
                    return Divider(color: Colors.grey,);
                  },
                  );

                }
              },
            ),
          )
        ],
      ),
    ):NotificationPage(isReturn: (value){
      setState(() {
        isOfferShow = value;
      });
    }, scaffoldKey: widget.scaffoldKey,);
  }

  Future<OffersModel?> getOfferData() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    print('user id ${preferences.getString(AppStrings.kPrefUserIdKey).toString()}');

    try{
      final response = await http.post(
        Uri.parse(AppStrings.kGetOffersUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue,
        },
        body: jsonEncode(<String, String>{
          'user_id': preferences.getString(AppStrings.kPrefUserIdKey).toString(),
          // 'my_id': preferences.getString(AppStrings.kPrefUserIdKey).toString(),
          // 'device_token':
          // preferences.getString(AppStrings.kPrefDeviceToken).toString(),
          // 'device_type':
          // preferences.getString(AppStrings.kPrefDeviceType).toString(),
          // 'language': 'en',
        }),
      );
      var responseData = jsonDecode(response.body);
      print('response from offers $responseData');
      if (response.statusCode == 200) {
        if (responseData['status'] == 'success') {
          return OffersModel.fromJson(responseData);
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              Navigator.of(context).pop();
            },
          )..show();
        }
      } else {
        throw Exception('Failed to load data');
      }
    }catch (exception){
      print('exception $exception');
    }
  }
}
