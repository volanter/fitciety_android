import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/trainer_model/session_details_model.dart';
import 'package:ficiety/ui/book_session_calender.dart';
import 'package:ficiety/ui/notification_page.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:ficiety/utils/progressdialog.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SessionDetails extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Function(bool) isReturn;
  final String sessionId;

  SessionDetails(
      {required this.scaffoldKey,
      required this.isReturn,
      required this.sessionId});

  @override
  _SessionDetailsState createState() => _SessionDetailsState();
}

class _SessionDetailsState extends State<SessionDetails>
    with SingleTickerProviderStateMixin {
  late bool _isDetailsViewShow;
  late ProgressDialog progressDialog;
  late SharedPreferences preferences;
  late Future<SessionDetailsModel?> _future;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _future = getDetailsData();
    progressDialog = new ProgressDialog(context, ProgressDialogType.Normal);
    progressDialog.setMessage(AppStrings.kLoadingMsg);
    _isDetailsViewShow = true;
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff024446),
    ));
    return _isDetailsViewShow
        ? Container(
            color: Colors.white,
            child: FutureBuilder<SessionDetailsModel?>(
              future: _future,
              builder: (BuildContext context,
                  AsyncSnapshot<SessionDetailsModel?> snapshot) {
                if (!snapshot.hasData) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SafeArea(
                        child: Container(
                          height: 100,
                          color: Color(0xff006e6f),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    top: 10,
                                  ),
                                  child: Row(
                                    children: [
                                      GestureDetector(
                                        behavior: HitTestBehavior.translucent,
                                        child: Icon(
                                          Icons.arrow_back_sharp,
                                          color: Colors.white,
                                        ) /*Image.asset(
                                            AppStrings.kImgMenuBar,
                                            width: 18,
                                            height: 13,
                                          )*/
                                        ,
                                        onTap: () {
                                          widget.isReturn(true);
                                          // if (widget.scaffoldKey.currentState!
                                          //     .isDrawerOpen) {
                                          //   Navigator.of(context).pop();
                                          // } else {
                                          //   widget.scaffoldKey.currentState!
                                          //       .openDrawer();
                                          // }
                                        },
                                      ),
                                      SizedBox(
                                        width: 30,
                                      ),
                                      Text(
                                        AppStrings.kSessionDetails,
                                        style: GoogleFonts.roboto(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                      Spacer(),
                                      GestureDetector(
                                        behavior: HitTestBehavior.translucent,
                                        onTap: () {
                                          setState(() {
                                            _isDetailsViewShow = false;
                                          });
                                          // widget.isReturn(true);
                                        },
                                        child: Image.asset(
                                          AppStrings.kImgNotification,
                                          width: 17.5,
                                          height: 18.5,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 40,
                          left: 30,
                        ),
                        child: Row(
                          children: [
                            Container(
                              width: 120,
                              height: 120,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(
                                    5.0,
                                  ),
                                  border: Border.all(
                                    color: Colors.white,
                                  ),
                                  image: DecorationImage(
                                      fit: BoxFit.fill,
                                      image: NetworkImage(
                                        AppStrings.kImageUrl+snapshot.data!.data[0].userImage,
                                      ))),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  snapshot.data!.data[0].fullname,
                                  style: GoogleFonts.roboto(
                                    color: AppColors.kGreen,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        FaIcon(
                                          FontAwesomeIcons.clock,
                                          color: AppColors.kGreen,
                                          size: 14,
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        FaIcon(
                                          FontAwesomeIcons.calendar,
                                          color: AppColors.kGreen,
                                          size: 14,
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        FaIcon(
                                          FontAwesomeIcons.th,
                                          color: AppColors.kGreen,
                                          size: 14,
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        FaIcon(
                                          FontAwesomeIcons.handshake,
                                          color: AppColors.kGreen,
                                          size: 14,
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          snapshot.data!.data[0].time,
                                          style: GoogleFonts.roboto(
                                              color: AppColors.kBlack,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          snapshot.data!.data[0].date,
                                          style: GoogleFonts.roboto(
                                              color: AppColors.kBlack,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          snapshot.data!.data[0].category,
                                          style: GoogleFonts.roboto(
                                              color: AppColors.kBlack,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          '£${snapshot.data!.data[0].price}',
                                          style: GoogleFonts.roboto(
                                              color: AppColors.kBlack,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          snapshot.data!.data[0].desciption,
                          style: GoogleFonts.roboto(
                              color: AppColors.kBlack,
                              fontSize: 12,),
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Expanded(
                        flex: 4,
                        child: Image.asset(
                          'assets/images/session_detail.png',
                          fit: BoxFit.fill,
                        ),
                      ),
                      Expanded(
                          flex: 1,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                width: 160,
                                height: 40,
                                decoration: BoxDecoration(
                                  color: AppColors.kGreen,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: Center(
                                  child: Text(
                                    'Reschedule',
                                    style: GoogleFonts.roboto(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 30,
                              ),
                              Container(
                                width: 160,
                                height: 40,
                                decoration: BoxDecoration(
                                  color: AppColors.kGreen,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: Center(
                                  child: Text(
                                    'Cancel',
                                    style: GoogleFonts.roboto(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ))
                    ],
                  );
                }
              },
            ),
          )
        : NotificationPage(
            scaffoldKey: widget.scaffoldKey,
            isReturn: (value) {
              setState(() {
                _isDetailsViewShow = value;
              });
            });
  }

  Future<bool> _onBackPress() async {
    if (_isDetailsViewShow) {
    } else {
      setState(() {
        _isDetailsViewShow = true;
      });
    }
    print('back call $_isDetailsViewShow');
    return false;
  }

  Future<SessionDetailsModel?> getDetailsData() async {
    preferences = await SharedPreferences.getInstance();
    try {
      final response = await http.post(
        Uri.parse(AppStrings.GetSessionDetailsUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue,
        },
        body: jsonEncode(<String, String>{
          'user_id':
              '3' /*preferences.getString(AppStrings.kPrefUserIdKey).toString()*/,
          'session_id': '1' /*widget.sessionId*/,
          // 'my_id': preferences.getString(AppStrings.kPrefUserIdKey).toString(),
          // 'device_token':
          // preferences.getString(AppStrings.kPrefDeviceToken).toString(),
          // 'device_type':
          // preferences.getString(AppStrings.kPrefDeviceType).toString(),
          // 'language': 'en',
        }),
      );

      var responseData = jsonDecode(response.body);
      print('response from chat data  $responseData');
      if (response.statusCode == 200) {
        if (responseData['status'] == 'success') {
          return SessionDetailsModel.fromJson(responseData);
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {},
          )..show();
        }
      } else {
        throw Exception('Failed to load data');
      }
    } catch (exception) {
      print('exception $exception');
    }
  }
}
