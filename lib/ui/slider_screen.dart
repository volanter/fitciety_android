import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';

import 'login.dart';

class SliderScreen extends StatefulWidget {
  @override
  _SliderScreenState createState() => _SliderScreenState();
}

class _SliderScreenState extends State<SliderScreen> {
  List<Slide> slides = [];
  late Function goToTab;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    slides.add(
      new Slide(
        // title: "ERASER",
        // description: "Allow miles wound place the leave had. To sitting subject no improve studied limited",
        backgroundImage: AppStrings.kImgSlide1,
        backgroundImageFit: BoxFit.cover,
        // backgroundColor: Color(0xfff5a623),
      ),
    );
    slides.add(
      new Slide(
        // title: "PENCIL",
        // description: "Ye indulgence unreserved connection alteration appearance",
        // pathImage: "images/photo_pencil.png",
        // backgroundColor: Color(0xff203152),
        backgroundImage: AppStrings.kImgSlide2,
        backgroundImageFit: BoxFit.cover,
      ),
    );
    slides.add(
      new Slide(
        // title: "RULER",
        // description:
        // "Much evil soon high in hope do view. Out may few northward believing attempted. Yet timed being songs marry one defer men our. Although finished blessing do of",
        // pathImage: "images/photo_ruler.png",
        // backgroundColor: Color(0xff9932CC),
        backgroundImage: AppStrings.kImgSlide3,
        backgroundImageFit: BoxFit.cover,
      ),
    );
    slides.add(
      new Slide(
        // title: "RULER",
        // description:
        // "Much evil soon high in hope do view. Out may few northward believing attempted. Yet timed being songs marry one defer men our. Although finished blessing do of",
        // pathImage: "images/photo_ruler.png",
        // backgroundColor: Color(0xff9932CC),
        backgroundImage: AppStrings.kImgSlide4,
        backgroundImageFit: BoxFit.cover,
      ),
    );
  }

  void onDonePress() {
    this.goToTab(0);
//    // Do what you want
  }

  void onTabChangeCompleted(index) {
    // Index of current tab is focused
  }

  List<Widget> renderListCustomTabs() {
    List<Widget> tabs = [];
    for (int i = 0; i < slides.length; i++) {
      Slide currentSlide = slides[i];
      tabs.add(Container(
        width: double.infinity,
        height: double.infinity,
        child: Container(
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage(
                  currentSlide.backgroundImage!,
                ),
              ),
            ),
            child: Column(
              children: [
                SizedBox(
                  height: 90,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 20,),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 20,),
                        child: Image.asset(
                          AppStrings.kImgLogo,
                          fit: BoxFit.fill,
                          width: 79.5,
                          height: 47,
                        ),
                      ),
                      Spacer(),
                      // Text(
                      //   AppStrings.kSkip,
                      //   style: GoogleFonts.roboto(
                      //     color: Colors.white,
                      //     fontWeight: FontWeight.w500,
                      //   ),
                      // ),
                      SizedBox(width: 5,),
                      // Icon(Icons.arrow_forward,color: Colors.white,size: 20,),
                    ],
                  ),
                ),
                Spacer(),
                SizedBox(
                  height: 50,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(AppStrings.kSliderText,
                        style: GoogleFonts.roboto(
                          color: Colors.white,
                          fontSize: 14,
                          letterSpacing: 1.0,
                          fontWeight: FontWeight.bold,
                        )),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(
                      bottom: 100,
                    ),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          GestureDetector(
                            onTap: (){
                              Navigator.of(context).pushNamed(AppStrings.kLoginRoute);
                            },
                            child: Container(
                              height: 35,
                              width: 143,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(70.0),
                                border: Border.all(
                                  color: Colors.grey.shade200,
                                ),
                              ),
                              child: Center(
                                child: Text('Log In',
                                    style: GoogleFonts.roboto(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w900,
                                    )),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: (){
                              Navigator.of(context).pushNamed(AppStrings.kRegisterRoute);
                            },
                            child: Container(
                              height: 35,
                              width: 143,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(70.0),
                                color: Colors.white,
                              ),
                              child: Center(
                                child: Text(
                                  'Sign Up',
                                  style: GoogleFonts.roboto(
                                    color: Color(0xff006e6f),
                                    fontWeight: FontWeight.w900,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ));
    }
    return tabs;
  }

  @override
  Widget build(BuildContext context) {
    return IntroSlider(
      onDonePress: onDonePress,
      colorActiveDot: Colors.white,
      colorDot: Colors.grey,
      scrollPhysics: BouncingScrollPhysics(),
      isScrollable: true,
      listCustomTabs: renderListCustomTabs(),
      onSkipPress: (){
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=>Login()));
      },
    );
  }
}
