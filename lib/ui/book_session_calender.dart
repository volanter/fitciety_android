import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/models/book_session_calender_model.dart';
import 'package:ficiety/ui/view_summary.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:group_radio_button/group_radio_button.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:http/http.dart' as http;

class BookSessionCalender extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Function (bool) isReturn;
  final String sessionId;

  BookSessionCalender({required this.scaffoldKey,required this.isReturn,this.sessionId=''});

  @override
  _BookSessionCalenderState createState() => _BookSessionCalenderState();
}

class _BookSessionCalenderState extends State<BookSessionCalender> {

  late bool _isCalenderView;
  late String timeText;
  late Future<BookSessionCalenderModel?> _future;
  late bool _isShowDialog;



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _isShowDialog = false;
    _future = getCalenderData();

    timeText = '6 - 7 am';
    _isCalenderView = true;
  }
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: _isCalenderView?Container(
        color: Colors.white,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: Scaffold.of(context).appBarMaxHeight,
              color: Color(0xff006e6f),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SafeArea(
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Padding(
                        padding: const EdgeInsets.only(
                          top: 10,
                        ),
                        child: Row(
                          children: [
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              child: Icon(Icons.arrow_back_sharp,color: Colors.white,)/*Image.asset(
                                          AppStrings.kImgMenuBar,
                                          width: 18,
                                          height: 13,
                                        )*/,
                              onTap: () {
                                widget.isReturn(true);
                                // if (widget.scaffoldKey.currentState!
                                //     .isDrawerOpen) {
                                //   Navigator.of(context).pop();
                                // } else {
                                //   widget.scaffoldKey.currentState!
                                //       .openDrawer();
                                // }
                              },
                            ),
                            // GestureDetector(
                            //   child: Image.asset(
                            //     AppStrings.kImgMenuBar,
                            //     width: 18,
                            //     height: 13,
                            //   ),
                            //   onTap: () {
                            //     if (widget
                            //         .scaffoldKey.currentState!.isDrawerOpen) {
                            //       Navigator.of(context).pop();
                            //     } else {
                            //       widget.scaffoldKey.currentState!.openDrawer();
                            //     }
                            //   },
                            // ),
                            SizedBox(
                              width: 30,
                            ),
                            Text(
                              AppStrings.kBookSession,
                              style: GoogleFonts.roboto(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 7,
              child: SfCalendar(
                view: CalendarView.workWeek,
                todayHighlightColor: Color(0xff07595f),
                showNavigationArrow: true,
                // showCurrentTimeIndicator: true,
                // allowViewNavigation: true,
                // specialRegions: _getTimeRegions(),
                dataSource: MeetingDataSource(_getDataSource()),
                onTap: (CalendarTapDetails details){
                  // print('calender tap details ${details.appointments!.first}');
                  if(details.appointments!=null){
                    _showViewDialog();
                  }

                },
                // by default the month appointment display mode set as Indicator, we can
                // change the display mode as appointment using the appointment display
                // mode property
              //   monthViewSettings: MonthViewSettings(
              //     showAgenda: true,
              //       appointmentDisplayMode:
              //           MonthAppointmentDisplayMode.appointment),
              ),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.only(left: 20,top: 20,right: 20,bottom: 10,),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Expanded(
                          child: GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: (){
                              setState(() {
                                timeText = '6 - 7 am';
                              });
                            },
                            child: Container(
                              height: 35.5,
                              decoration: BoxDecoration(
                                color: timeText == '6 - 7 am'?Color(0xff07595f):Color(0xffffffff),
                                borderRadius: BorderRadius.circular(5.0),
                                border: Border.all(
                                  width: 2.0,
                                  color: Color(0xffECECEC),
                                ),
                              ),
                              child: Center(
                                child: Text(
                                  '6 - 7 am',
                                  style: GoogleFonts.roboto(
                                    color: timeText == '6 - 7 am'?Colors.white:Color(0xff07595f),
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(width: 5,),
                        Expanded(
                          child: GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: (){
                              setState(() {
                                timeText = '7 - 8 am';
                              });
                            },
                            child: Container(
                              // width: 80,
                              height: 35.5,
                              decoration: BoxDecoration(
                                color: timeText == '7 - 8 am'?Color(0xff07595f):Color(0xffffffff),
                                borderRadius: BorderRadius.circular(5.0),
                                border: Border.all(
                                  width: 2.0,
                                  color: Color(0xffECECEC),
                                ),
                              ),
                              child: Center(
                                child: Text(
                                  '7 - 8 am',
                                  style: GoogleFonts.roboto(
                                    color: timeText == '7 - 8 am'?Colors.white:Color(0xff07595f),
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(width: 5,),
                        Expanded(
                          child: GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: (){
                              setState(() {
                                timeText = '8 - 9 am';
                              });
                            },
                            child: Container(
                              // width: 80,
                              height: 35.5,
                              decoration: BoxDecoration(
                                color: timeText == '8 - 9 am'?Color(0xff07595f):Color(0xffffffff),
                                borderRadius: BorderRadius.circular(5.0),
                                border: Border.all(
                                  width: 2.0,
                                  color: Color(0xffECECEC),
                                ),
                              ),
                              child: Center(
                                child: Text(
                                  '8 - 9 am',
                                  style: GoogleFonts.roboto(
                                    color: timeText == '8 - 9 am'?Colors.white:Color(0xff07595f),
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(width: 5,),
                        Expanded(
                          child: GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: (){
                              setState(() {
                                timeText = '9 - 10 am';
                              });
                            },
                            child: Container(
                              // width: 80,
                              height: 35.5,
                              decoration: BoxDecoration(
                                color: timeText == '9 - 10 am'?Color(0xff07595f):Color(0xffffffff),
                                borderRadius: BorderRadius.circular(5.0),
                                border: Border.all(
                                  width: 2.0,
                                  color: Color(0xffECECEC),
                                ),
                              ),
                              child: Center(
                                child: Text(
                                  '9 - 10 am',
                                  style: GoogleFonts.roboto(
                                    color: timeText == '9 - 10 am'?Colors.white:Color(0xff07595f),
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 15,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Expanded(
                          child: GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: (){
                              setState(() {
                                timeText = '5 - 6 pm';
                              });
                            },
                            child: Container(
                              // width: 80,
                              height: 35.5,
                              decoration: BoxDecoration(
                                color: timeText == '5 - 6 pm'?Color(0xff07595f):Color(0xffffffff),
                                borderRadius: BorderRadius.circular(5.0),
                                border: Border.all(
                                  width: 2.0,
                                  color: Color(0xffECECEC),
                                ),
                              ),
                              child: Center(
                                child: Text(
                                  '5 - 6 pm',
                                  style: GoogleFonts.roboto(
                                    color: timeText == '5 - 6 pm'?Colors.white:Color(0xff07595f),
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(width: 5,),
                        Expanded(
                          child: GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: (){
                              setState(() {
                                timeText = '6 - 7 pm';
                              });
                            },
                            child: Container(
                              // width: 80,
                              height: 35.5,
                              decoration: BoxDecoration(
                                color: timeText == '6 - 7 pm'?Color(0xff07595f):Color(0xffffffff),
                                borderRadius: BorderRadius.circular(5.0),
                                border: Border.all(
                                  width: 2.0,
                                  color: Color(0xffECECEC),
                                ),
                              ),
                              child: Center(
                                child: Text(
                                  '6 - 7 pm',
                                  style: GoogleFonts.roboto(
                                    color: timeText == '6 - 7 pm'?Colors.white:Color(0xff07595f),
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(width: 5,),
                        Expanded(
                          child: GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: (){
                              setState(() {
                                timeText = '7 - 8 pm';
                              });
                            },
                            child: Container(
                              // width: 80,
                              height: 35.5,
                              decoration: BoxDecoration(
                                color: timeText == '7 - 8 pm'?Color(0xff07595f):Color(0xffffffff),
                                borderRadius: BorderRadius.circular(5.0),
                                border: Border.all(
                                  width: 2.0,
                                  color: Color(0xffECECEC),
                                ),
                              ),
                              child: Center(
                                child: Text(
                                  '7 - 8 pm',
                                  style: GoogleFonts.roboto(
                                    color: timeText == '7 - 8 pm'?Colors.white:Color(0xff07595f),
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(width: 5,),
                        Expanded(
                          child: GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: (){
                              setState(() {
                                timeText = '8 - 9 pm';
                              });
                            },
                            child: Container(
                              // width: 80,
                              height: 35.5,
                              decoration: BoxDecoration(
                                color: timeText == '8 - 9 pm'?Color(0xff07595f):Color(0xffffffff),
                                borderRadius: BorderRadius.circular(5.0),
                                border: Border.all(
                                  width: 2.0,
                                  color: Color(0xffECECEC),
                                ),
                              ),
                              child: Center(
                                child: Text(
                                  '8 - 9 pm',
                                  style: GoogleFonts.roboto(
                                    color: timeText == '8 - 9 pm'?Colors.white:Color(0xff07595f),
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    // SizedBox(width: 15,),
                    // SizedBox(width: 15,),
                    // Padding(
                    //   padding: const EdgeInsets.only(top: 15,),
                    //   child: GestureDetector(
                    //     onTap: (){
                    //       setState(() {
                    //         timeText = '5 - 6 pm';
                    //       });
                    //     },
                    //     child: Container(
                    //       width: 80,
                    //       height: 35.5,
                    //       decoration: BoxDecoration(
                    //         color: timeText == '5 - 6 pm'?Color(0xff07595f):Color(0xffffffff),
                    //         borderRadius: BorderRadius.circular(5.0),
                    //         border: Border.all(
                    //           width: 2.0,
                    //           color: Color(0xffECECEC),
                    //         ),
                    //       ),
                    //       child: Center(
                    //         child: Text(
                    //           '5 - 6 pm',
                    //           style: GoogleFonts.roboto(
                    //             color: timeText == '5 - 6 pm'?Colors.white:Color(0xff07595f),
                    //             fontSize: 12,
                    //             fontWeight: FontWeight.bold,
                    //           ),
                    //         ),
                    //       ),
                    //     ),
                    //   ),
                    // ),
                    // SizedBox(width: 15,),
                    // Padding(
                    //   padding: const EdgeInsets.only(top: 15,),
                    //   child: GestureDetector(
                    //     onTap: (){
                    //       setState(() {
                    //         timeText = '6 - 7 pm';
                    //       });
                    //     },
                    //     child: Container(
                    //       width: 80,
                    //       height: 35.5,
                    //       decoration: BoxDecoration(
                    //         color: timeText == '6 - 7 pm'?Color(0xff07595f):Color(0xffffffff),
                    //         borderRadius: BorderRadius.circular(5.0),
                    //         border: Border.all(
                    //           width: 2.0,
                    //           color: Color(0xffECECEC),
                    //         ),
                    //       ),
                    //       child: Center(
                    //         child: Text(
                    //           '6 - 7 pm',
                    //           style: GoogleFonts.roboto(
                    //             color: timeText == '6 - 7 pm'?Colors.white:Color(0xff07595f),
                    //             fontSize: 12,
                    //             fontWeight: FontWeight.bold,
                    //           ),
                    //         ),
                    //       ),
                    //     ),
                    //   ),
                    // ),
                    // SizedBox(width: 15,),
                    // Padding(
                    //   padding: const EdgeInsets.only(top: 15,),
                    //   child: GestureDetector(
                    //     onTap: (){
                    //       setState(() {
                    //         timeText = '7 - 8 pm';
                    //       });
                    //     },
                    //     child: Container(
                    //       width: 80,
                    //       // height: 35.5,
                    //       decoration: BoxDecoration(
                    //         color: timeText == '7 - 8 pm'?Color(0xff07595f):Color(0xffffffff),
                    //         borderRadius: BorderRadius.circular(5.0),
                    //         border: Border.all(
                    //           width: 2.0,
                    //           color: Color(0xffECECEC),
                    //         ),
                    //       ),
                    //       child: Center(
                    //         child: Text(
                    //           '7 - 8 pm',
                    //           style: GoogleFonts.roboto(
                    //             color: timeText == '7 - 8 pm'?Colors.white:Color(0xff07595f),
                    //             fontSize: 12,
                    //             fontWeight: FontWeight.bold,
                    //           ),
                    //         ),
                    //       ),
                    //     ),
                    //   ),
                    // ),

                    // SizedBox(width: 15,),
                    Padding(
                      padding: const EdgeInsets.only(top: 15,),
                      // child: GestureDetector(
                      //   onTap: (){
                      //     setState(() {
                      //       timeText = '8 - 9 pm';
                      //     });
                      //   },
                      //   child: Container(
                      //     width: 80,
                      //     // height: 35.5,
                      //     decoration: BoxDecoration(
                      //       color: timeText == '8 - 9 pm'?Color(0xff07595f):Color(0xffffffff),
                      //       borderRadius: BorderRadius.circular(5.0),
                      //       border: Border.all(
                      //         width: 2.0,
                      //         color: Color(0xffECECEC),
                      //       ),
                      //     ),
                      //     child: Center(
                      //       child: Text(
                      //         '8 - 9 pm',
                      //         style: GoogleFonts.roboto(
                      //           color: timeText == '8 - 9 pm'?Colors.white:Color(0xff07595f),
                      //           fontSize: 12,
                      //           fontWeight: FontWeight.bold,
                      //         ),
                      //       ),
                      //     ),
                      //   ),
                      // ),
                    ),

                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Container(
                    width: 215,
                    height: 200.5,
                    child: ElevatedButton(
                      onPressed: () {
                        setState(() {
                          _isCalenderView = false;
                        });
                        // print('view profile clicked');
                        // widget.callback(AppStrings.kTrainerDetails);
                      },
                      style: ButtonStyle(
                          backgroundColor:
                          MaterialStateProperty.all<Color>(Color(0xff138c95))),
                      child: Text(
                        AppStrings.kContinue,
                        style: GoogleFonts.roboto(
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ):ViewSummary(scaffoldKey: widget.scaffoldKey,isReturn: (value){
        print('void callback call');
        setState(() {
          _isCalenderView = value;
        });
      },),
    );
  }

  List<Meeting> _getDataSource() {
    final List<Meeting> meetings = <Meeting>[];
    final DateTime today = DateTime.now();
    final DateTime startTime =
        DateTime(today.year, today.month, today.day, 9, 0, 0);
    final DateTime endTime = startTime.add(const Duration(hours: 1));
    meetings.add(Meeting(
        '3 User \n View User', startTime, endTime, const Color(0xFF07595f), false));
    meetings.add(Meeting(
        '4 User\n View User',  DateTime(today.year, today.month, today.day, 15, 0, 0), endTime, const Color(0xFF07595f), false));
    meetings.add(Meeting(
        '5 User\n View User',  DateTime(today.year, today.month, today.day, 12, 0, 0), endTime, const Color(0xFF07595f), false));
    return meetings;
  }

  Future<bool> _onBackPressed() async{
    if(_isCalenderView){

    }else{
      setState(() {
        _isCalenderView = true;
      });
    }
    return true;
  }

  List<TimeRegion> _getTimeRegions() {
    final List<TimeRegion> regions = <TimeRegion>[];
    regions.add(TimeRegion(
        startTime: DateTime.now(),
        endTime: DateTime.now().add(Duration(hours: 1)),
        enablePointerInteraction: false,
        color: Colors.grey.withOpacity(0.2),
        text: 'Break'));

    return regions;
  }

  _showViewDialog() {
    print('call dialog');
  return showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, setState) {
              return Dialog(
                insetPadding: EdgeInsets.symmetric(
                  horizontal: 2.0,
                  vertical: 2.0,
                ),
                // title: Text(
                //   AppStrings.kSearchForTrainer,style: GoogleFonts.roboto(
                //   color: AppColors.kGreen,fontWeight: FontWeight.w900,
                // ),
                // ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                    10.0,
                  ),
                ),
                child: Container(
                  width: 380,
                  height: 430,
                  padding: EdgeInsets.all(
                    20.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.topRight,
                        child: GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          child: Icon(
                            Icons.cancel_outlined,
                            color: AppColors.kGreen,
                          ),
                          onTap: () => Navigator.of(context).pop(),
                        ),
                      ),
                      Center(
                        child: Text(
                          AppStrings.kAllUser,
                          style: GoogleFonts.roboto(
                            color: Color(0xff07595f),
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Expanded(
                        child: ListView.builder(
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Container(
                                height: 60,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(
                                    5.0,
                                  ),
                                  border: Border.all(
                                    color: Color(
                                      0xff66b5b5b5,
                                    ),
                                  ),
                                ),
                                child: Row(
                                  children: [
                                    Image.asset(AppStrings.kImgGymMan,height: 60,width: 80,fit: BoxFit.fill,),
                                    SizedBox(width: 20,),
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text('Mark Hemmings',style: GoogleFonts.roboto(color: Color(0xff333333),fontSize: 14,fontWeight: FontWeight.bold),),
                                        SizedBox(height: 10,),
                                        Text('Online Training',style: GoogleFonts.roboto(color: Color(0xff333333),fontSize: 10,),),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                          itemCount: 5,
                          padding: EdgeInsets.zero,
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        });
  }

  Future<BookSessionCalenderModel?> getCalenderData() async{
    SharedPreferences  preferences = await SharedPreferences.getInstance();
    try {
      final response = await http.post(
        Uri.parse(AppStrings.kGetBookSessionCalenderUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue,
        },
        body: jsonEncode(<String, String>{
          'trainer_id':
          preferences.getString(AppStrings.kPrefUserIdKey).toString(),
          'selected_date' : '',
          'session_id' : ''
        }),
      );
      var responseData = jsonDecode(response.body);
      setState(() {
        _isShowDialog = false;
      });
      print('response from auth list report $responseData');
      if (response.statusCode == 200) {
        if (responseData['status'] == 'success') {
          return BookSessionCalenderModel.fromJson(responseData);
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              Navigator.of(context).pop();
            },
          )..show();
        }
      } else {
        throw Exception('Failed to load data');
      }
    } catch (exception) {
      print('exception $exception');
    }
  }
}

class Meeting {
  /// Creates a meeting class with required details.
  Meeting(this.eventName, this.from, this.to, this.background, this.isAllDay);

  /// Event name which is equivalent to subject property of [Appointment].
  String eventName;

  /// From which is equivalent to start time property of [Appointment].
  DateTime from;

  /// To which is equivalent to end time property of [Appointment].
  DateTime to;

  /// Background which is equivalent to color property of [Appointment].
  Color background;

  /// IsAllDay which is equivalent to isAllDay property of [Appointment].
  bool isAllDay;
}

class MeetingDataSource extends CalendarDataSource {
  /// Creates a meeting data source, which used to set the appointment
  /// collection to the calendar
  MeetingDataSource(List<Meeting> source) {
    appointments = source;
  }

  @override
  DateTime getStartTime(int index) {
    return appointments![index].from;
  }

  @override
  DateTime getEndTime(int index) {
    return appointments![index].to;
  }

  @override
  String getSubject(int index) {
    return appointments![index].eventName;
  }

  @override
  Color getColor(int index) {
    return appointments![index].background;
  }

  @override
  bool isAllDay(int index) {
    return appointments![index].isAllDay;
  }
}
