import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/models/exercise_details_model.dart';
import 'package:ficiety/ui/full_page_dialog.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'notification_page.dart';

class UserExerciseProgram extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final String programName;
  final Function(bool) isReturn;
  final String exerciseId;
  final String day;

  UserExerciseProgram(
      {required this.scaffoldKey,
      required this.programName,
      required this.isReturn,
      required this.exerciseId,
      required this.day});

  @override
  _UserExerciseProgramState createState() => _UserExerciseProgramState();
}

class _UserExerciseProgramState extends State<UserExerciseProgram> {
  List<String> programName = [
    'Jumping Jacks',
    'Abdominal Crunches',
    'Russian Twist',
    'Mountain Climber',
    'Heel Touch',
    'Leg Raises',
    'Plank'
  ];
  List<String> imagesName = [
    AppStrings.kImgGif2,
    AppStrings.kImgGif2,
    AppStrings.kImgGif2,
    AppStrings.kImgGif2,
    AppStrings.kImgGif2,
    AppStrings.kImgGif2,
    AppStrings.kImgGif2
  ];
  List<String> time = [
    '00 :20',
    '00 :20',
    '00 :20',
    '00 :20',
    '00 :20',
    '00 :20',
    '00 :20'
  ];
  late bool isCurrentView;
  late bool isShowLoader;
  late Future<ExerciseDetailsModel?> _future;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isShowLoader = true;
    _future = getExerciseDetailsData();
    isCurrentView = true;
    print('selected day is ${widget.day} and selected id ${widget.exerciseId}');
  }

  void showFullScreenDialog({String? image}) {
    showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel: '',
        // barrierLabel: MaterialLocalizations.of(context)
        //     .modalBarrierDismissLabel,
        // barrierColor: Colors.black45,
        transitionDuration: const Duration(milliseconds: 300),
        pageBuilder: (BuildContext buildContext, Animation animation,
            Animation secondaryAnimation) {
          return Column(
            children: [
              Expanded(
                child: Container(
                  width: double.infinity,
                  height: double.infinity,
                  child: Center(
                    child: Material(
                      child: Container(
                        color: Colors.black.withOpacity(animation.value),
                        child: Text("I am a variable"),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
          // return SizedBox.expand(
          //   child: Column(
          //     children: [
          //       Expanded(flex: 10,child: Image.asset(image!,fit: BoxFit.fill,)),
          //       Expanded(
          //         flex: 1,
          //         child: SizedBox.expand(
          //           child: ElevatedButton(
          //             style: ElevatedButton.styleFrom(
          //               primary: Color(0xff087D8D)
          //             ),
          //             onPressed: () => Navigator.pop(context),
          //             child: Text(
          //               'Dismiss',
          //               style: TextStyle(fontSize: 20),
          //             ),
          //           ),
          //         ),
          //       ),
          //     ],
          //   ),
          // );
        });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return isCurrentView
        ? Container(
            color: Colors.white,
            height: MediaQuery.of(context).size.height,
            child: FutureBuilder<ExerciseDetailsModel?>(
              future: _future,
              builder: (BuildContext context, AsyncSnapshot<ExerciseDetailsModel?> snapshot){
                if(!snapshot.hasData){
                  return isShowLoader?Center(child: Container(width:50,height:50,child: CircularProgressIndicator())):SizedBox();
                }else{
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 140,
                        color: Color(0xff006e6f),
                        child: SafeArea(
                          child: Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: Padding(
                              padding: const EdgeInsets.only(
                                top: 10,
                              ),
                              child: Row(
                                children: [
                                  GestureDetector(
                                    behavior: HitTestBehavior.translucent,
                                    child: AppCommon.kIconTrainerListTextView(
                                        fontName: AppStrings.kFontAwsSolid,
                                        content: ''),
                                    onTap: () {
                                      print('tap on back');
                                      widget.isReturn(true);
                                    },
                                  ),
                                  SizedBox(
                                    width: 30,
                                  ),
                                  Text(
                                    snapshot.data!.data[0].categoryTitle,
                                    style: GoogleFonts.roboto(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  Spacer(),
                                  GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        isCurrentView = false;
                                      });
                                    },
                                    child: Image.asset(
                                      AppStrings.kImgNotification,
                                      width: 17.5,
                                      height: 18.5,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 30,
                          right: 30,
                          top: 20,
                        ),
                        child: Text(snapshot.data!.data[0].categoryTitle,
                            style: GoogleFonts.roboto(
                              color: Color(0xff006e6f),
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 30,
                          right: 30,
                          top: 10,
                        ),
                        child: Text(snapshot.data!.data[0].workoutsCount.toString()+' Workout',
                            style: GoogleFonts.roboto(
                              color: Color(0xff666666),
                              fontSize: 10,
                            )),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(
                            left: 30,
                            right: 30,
                          ),
                          child: ListView.builder(
                            itemBuilder: (context, index) {
                              return GestureDetector(
                                onTap: () {
                                  showDialog(
                                    context: context,
                                    builder: (context) => StatefulBuilder(
                                      builder: (context, setState) {
                                        return AlertDialog(
                                          contentPadding: EdgeInsets.zero,
                                          content: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              Row(
                                                children: [
                                                  Padding(
                                                    padding:
                                                    const EdgeInsets.all(10.0),
                                                    child: Align(
                                                      alignment: Alignment.topRight,
                                                      child: GestureDetector(
                                                          onTap: () {
                                                            Navigator.of(context)
                                                                .pop();
                                                          },
                                                          child: AppCommon
                                                              .kIconBottomSheetTextView(
                                                            fontName: AppStrings
                                                                .kFontAwsSolid,
                                                            content: '',
                                                          )),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 5,
                                                  ),
                                                  Padding(
                                                    padding:
                                                    const EdgeInsets.all(10.0),
                                                    child: Align(
                                                      alignment: Alignment.topRight,
                                                      child: GestureDetector(
                                                        onTap: () {
                                                          Navigator.of(context).pop();
                                                          Navigator.push(context, MaterialPageRoute(builder: (context)=>FullPageDialog(image: snapshot.data!.data[0].workouts[index].programGif)));
                                                        },
                                                        child: Icon(
                                                          Icons.zoom_out_map_outlined,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                                mainAxisAlignment:
                                                MainAxisAlignment.end,
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Image.network(
                                                AppStrings.kWorkOutUrl+snapshot.data!.data[0].workouts[index].programGif,
                                                height: 300,
                                                fit: BoxFit.fill,
                                                width: 250,
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Container(
                                                decoration: BoxDecoration(
                                                    boxShadow: <BoxShadow>[
                                                      BoxShadow(
                                                          color: Colors.black54,
                                                          blurRadius: 15.0,
                                                          offset: Offset(0, 0.75)),
                                                    ],
                                                    color: Colors.white),
                                                child: Column(
                                                  children: [
                                                    Padding(
                                                      padding:
                                                      const EdgeInsets.all(10.0),
                                                      child: Align(
                                                        alignment: Alignment.topLeft,
                                                        child: Text(
                                                            snapshot.data!.data[0].workouts[index].programName,
                                                            style: GoogleFonts.roboto(
                                                              color:
                                                              Color(0xff006e6f),
                                                              fontSize: 16,
                                                              fontWeight:
                                                              FontWeight.bold,
                                                            )),
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: 5,
                                                    ),
                                                    Padding(
                                                      padding:
                                                      const EdgeInsets.all(10.0),
                                                      child: Align(
                                                        alignment: Alignment.topLeft,
                                                        child: Text(
                                                            snapshot.data!.data[0].workouts[index].programDescription,
                                                            style: GoogleFonts.roboto(
                                                              color:
                                                              Color(0xff666666),
                                                              fontSize: 12,
                                                            )),
                                                      ),
                                                    ),
                                                    // SizedBox(
                                                    //   height: 5,
                                                    // ),
                                                    // Padding(
                                                    //   padding:
                                                    //   const EdgeInsets.all(10.0),
                                                    //   child: Align(
                                                    //     alignment: Alignment.topLeft,
                                                    //     child: Text(
                                                    //         'Return to the start Position then do the next rep this exercise provides a full body workout and works all your large muscle groups.',
                                                    //         style: GoogleFonts.roboto(
                                                    //           color:
                                                    //           Color(0xff666666),
                                                    //           fontSize: 12,
                                                    //         )),
                                                    //   ),
                                                    // ),
                                                    // SizedBox(
                                                    //   height: 5,
                                                    // ),
                                                    // Padding(
                                                    //   padding: const EdgeInsets.only(
                                                    //       left: 10, bottom: 5),
                                                    //   child: Align(
                                                    //     alignment: Alignment.topLeft,
                                                    //     child: Text('Reps: 10-12',
                                                    //         style: GoogleFonts.roboto(
                                                    //           color:
                                                    //           Color(0xff666666),
                                                    //           fontSize: 12,
                                                    //         )),
                                                    //   ),
                                                    // ),
                                                    // Padding(
                                                    //   padding: const EdgeInsets.only(
                                                    //       left: 10, bottom: 5),
                                                    //   child: Align(
                                                    //     alignment: Alignment.topLeft,
                                                    //     child: Text('Cycles: 3-4',
                                                    //         style: GoogleFonts.roboto(
                                                    //           color:
                                                    //           Color(0xff666666),
                                                    //           fontSize: 12,
                                                    //         )),
                                                    //   ),
                                                    // ),
                                                    // Padding(
                                                    //   padding: const EdgeInsets.only(
                                                    //       left: 10, bottom: 5),
                                                    //   child: Align(
                                                    //     alignment: Alignment.topLeft,
                                                    //     child: Text(
                                                    //         'Rest: 60 seconds',
                                                    //         style: GoogleFonts.roboto(
                                                    //           color:
                                                    //           Color(0xff666666),
                                                    //           fontSize: 12,
                                                    //         )),
                                                    //   ),
                                                    // ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      },
                                    ),
                                  );
                                },
                                child: Container(
                                  height: 100,
                                  child: Card(
                                    color: Colors.white,
                                    elevation: 5.0,
                                    margin: EdgeInsets.all(10.0),
                                    shadowColor: Colors.grey,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          width: 30,
                                        ),
                                        Image.network(
                                          AppStrings.kWorkOutUrl+snapshot.data!.data[0].workouts[index].programGif,
                                          height: 62,
                                          fit: BoxFit.fill,
                                          width: 62,
                                        ),
                                        SizedBox(
                                          width: 30,
                                        ),
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Text(snapshot.data!.data[0].workouts[index].programName,
                                                style: GoogleFonts.roboto(
                                                  color: Color(0xff006e6f),
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w500,
                                                )),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Text(snapshot.data!.data[0].workouts[index].programDuration,
                                                style: GoogleFonts.roboto(
                                                  color: Color(0xff006e6f),
                                                  fontSize: 12,
                                                ))
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                            itemCount: snapshot.data!.data[0].workouts.length,
                            shrinkWrap: true,
                            padding: EdgeInsets.zero,
                            scrollDirection: Axis.vertical,
                          ),
                        ),
                      )
                    ],
                  );
                }
            },
            ),
          )
        : NotificationPage(
            scaffoldKey: widget.scaffoldKey,
            isReturn: (value) {
              setState(() {
                isCurrentView = value;
              });
            },
          );
  }

  Future<ExerciseDetailsModel?> getExerciseDetailsData() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();

    try{
      final response = await http.post(
        Uri.parse(AppStrings.kGetExerciseDetailsUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue,
        },
        body: jsonEncode(<String, String>{
          'user_id': '7'/*preferences.getString(AppStrings.kPrefUserIdKey).toString()*/,
          'day': widget.day,
          'exercise_id': widget.exerciseId,
        }),
      );
      var responseData = jsonDecode(response.body);
      setState(() {
        isShowLoader = false;
      });
      print('response from offers $responseData');
      if (response.statusCode == 200) {
        if (responseData['status'] == 'success') {
          return ExerciseDetailsModel.fromJson(responseData);
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              Navigator.of(context).pop();
            },
          )..show();
        }
      } else {
        throw Exception('Failed to load data');
      }
    }catch (exception){
      print('exception $exception');
    }
  }
}
