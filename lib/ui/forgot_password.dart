import 'dart:convert';
import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:device_info/device_info.dart';
import 'package:ficiety/ui/slider_screen.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:ficiety/utils/progressdialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  final _formKey = GlobalKey<FormState>();
  late ProgressDialog progressDialog;
  late SharedPreferences preferences;

  TextEditingController _controllerEmail = TextEditingController();
  late String _token;
  late String _deviceType;
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    progressDialog = new ProgressDialog(context, ProgressDialogType.Normal);
    progressDialog.setMessage(AppStrings.kLoadingMsg);

    SharedPreferences.getInstance().then((value) => preferences = value);
    getDeviceInformation();
  }

  void getDeviceInformation() async {
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      print('Running on ${androidInfo.model}');
      _deviceType = 'android';
      _token = androidInfo.androidId;
      print('token is $_token');
    } else {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      print('Running on ${iosInfo.utsname.machine}');
      _deviceType = 'ios';
      _token = iosInfo.identifierForVendor;
      print('token is _token');
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.fill,
            image: AssetImage(
              AppStrings.kImgLoginBg,
            ),
          ),
        ),
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Theme(
              data: ThemeData(
                  primaryColor: AppColors.kOffGrey,
                  accentColor: AppColors.kOffGrey,
                  dividerColor: AppColors.kOffGrey,
                  accentTextTheme: TextTheme(
                    bodyText1: TextStyle(
                      color: AppColors.kOffGrey,
                    ),
                  ),
                  hintColor: AppColors.kOffGrey,
                  primaryTextTheme: TextTheme(
                    bodyText1: TextStyle(
                      color: AppColors.kOffGrey,
                    ),
                  )),
              child: Column(
                children: [
                  GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: (){
                      Navigator.of(context).pop();
                    },
                    child: SafeArea(
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Icon(
                          Icons.arrow_back,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 130,
                  ),
                  Image.asset(
                    AppStrings.kImgLogo,
                    fit: BoxFit.fill,
                    width: 117.5,
                    height: 63.5,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 80,
                    ),
                    child: Text(
                      AppStrings.kForgotPassword,
                      style: GoogleFonts.roboto(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontSize: 18,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      AppStrings.kEmailEnterMsg,
                      textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(
                        color: Colors.white.withOpacity(0.8),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 60,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 25,
                      right: 25,
                    ),
                    child: TextFormField(
                      controller: _controllerEmail,
                      style: TextStyle(
                        color: AppColors.kOffGrey,
                      ),
                      decoration: InputDecoration(
                        hintText: AppStrings.kEnterEmailAddress,
                        border: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: AppColors.kOffGrey,
                          ),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.kOffGrey),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.kOffGrey),
                        ),
                        prefixIconConstraints: BoxConstraints(maxHeight: 30),
                        prefixIcon: Padding(
                          padding: const EdgeInsets.only(
                            right: 15,
                          ),
                          child: FaIcon(
                            FontAwesomeIcons.envelope,
                            size: 15,
                            color: AppColors.kOffGrey,
                          ),
                        ),
                        prefixStyle: TextStyle(
                            color: AppColors.kOffGrey, letterSpacing: 0.0),
                      ),
                      validator: (value){
                        if(value!.isEmpty){
                          return 'Please enter email address';
                        }else{
                          return null;
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: () {
                      progressDialog.show();
                      _saveRequest(email: _controllerEmail.text);
                    },
                    child: AppCommon.kCommonButton(
                      btnText: AppStrings.kSend,
                    ),
                  ),
                  Spacer(),
                  GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: () {},
                    child: Padding(
                      padding: const EdgeInsets.only(
                        right: 25,
                        top: 10,
                      ),
                      child: RichText(
                        text: TextSpan(
                          text: AppStrings.kStillLoginIn, style: GoogleFonts.roboto(
                          color: Colors.white,
                        ),
                          children: <TextSpan>[
                            TextSpan(
                              text: AppStrings.kHelp, style: GoogleFonts.roboto(
                              color: Color(0xff46B7CD),),),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _saveRequest({required String email}) async{
    try{
      final http.Response response = await http.post(
          Uri.parse(AppStrings.kForgotUrl),
          headers: <String, String>{
            AppStrings.kHeaderType: AppStrings.kHeaderValue
          },
          body: jsonEncode(<String, String>{
            'email': email,
            'device_token': _token,
            'device_type': _deviceType,
            'language': 'en',
          }));

      var _responseData = jsonDecode(response.body);
      print('response from service ${response.body}');

      Navigator.of(context).pop();

      if (response.statusCode == 200) {
        if (_responseData['status'] == 'success') {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            title: AppStrings.kSuccess,
            desc: _responseData['message'],
            btnCancelOnPress: () {
              Navigator.of(context).pop();
            },
            btnOkOnPress: () {
              Navigator.of(context).pop();
            },
          )..show();
        } else {
          AppCommon.kErrorDialog(
              context: context,
              errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
        }
      } else {
        AppCommon.kErrorDialog(
            context: context,
            errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
      }
    }catch(exception){
      print('exception is $exception');
      Navigator.of(context).pop();
      AppCommon.kErrorDialog(
          context: context,
          errorMsg: 'Some error occur.html response getting');
    }



  }
}
