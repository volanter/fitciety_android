import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/models/diet_plan_model.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:ficiety/utils/progressdialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:group_radio_button/group_radio_button.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:http/http.dart' as http;

import 'notification_page.dart';

class DietPlanning extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  DietPlanning({required this.scaffoldKey});

  @override
  _DietPlanningState createState() => _DietPlanningState();
}

class _DietPlanningState extends State<DietPlanning> {
  TextEditingController _controllerAddFood = TextEditingController();
  TextEditingController _controllerChoice = TextEditingController();
  TextEditingController _controllerComment = TextEditingController();
  late ProgressDialog progressDialog;
  late bool isShowLoader;
  late Future<DietPlanModel?> _future;

  FocusNode focusNodeAddFood = FocusNode();
  FocusNode focusNodeCalories = FocusNode();
  FocusNode focusNodeComment = FocusNode();
  late bool isShowCurrentView;
  String _snacksGroupValue = 'BreakFast';
  late String selectedDay;
  List<String> _foodList = [];

  List<String> _snacks = [
    'BreakFast',
    'Snack',
    'Lunch',
    'Dinner',
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    progressDialog = ProgressDialog(context, ProgressDialogType.Normal);
    progressDialog.setMessage(AppStrings.kLoadingMsg);
    isShowLoader = true;
    _future = getDietPlanningData();
    isShowCurrentView = true;
    selectedDay = 'W';
  }

  _showViewDialog() {
    print('call dialog');
    return showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, setState) {
              return Dialog(
                insetPadding: EdgeInsets.symmetric(
                  horizontal: 2.0,
                  vertical: 2.0,
                ),
                // title: Text(
                //   AppStrings.kSearchForTrainer,style: GoogleFonts.roboto(
                //   color: AppColors.kGreen,fontWeight: FontWeight.w900,
                // ),
                // ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                    10.0,
                  ),
                ),
                child: SingleChildScrollView(
                  child: Container(
                    width: 380,
                    height: 550,
                    padding: EdgeInsets.all(
                      20.0,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Align(
                          alignment: Alignment.topRight,
                          child: GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: Icon(
                              Icons.clear_outlined,
                              color: Color(0xff999999),
                            ),
                            onTap: () => Navigator.of(context).pop(),
                          ),
                        ),
                        SizedBox(height: 5),
                        Row(
                          children: [
                            Text(
                              'Add Diet Plan',
                              style: GoogleFonts.roboto(
                                color: AppColors.kBlack,
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                              ),
                            ),
                            Spacer(),
                            // GestureDetector(
                            //   child: Icon(
                            //     Icons.clear_outlined,
                            //     color: Color(0xff999999),
                            //   ),
                            //   onTap: () => Navigator.of(context).pop(),
                            // ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () {
                                setState(() {
                                  selectedDay = "S";
                                });
                              },
                              child: Container(
                                height: 25,
                                width: 25,
                                decoration: BoxDecoration(
                                  color: selectedDay == 'S'
                                      ? Color(0xff0a545e)
                                      : Colors.white,
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                      color: selectedDay == 'S'
                                          ? Color(0xff08535b)
                                          : Colors.black,
                                      width: 2),
                                ),
                                child: Center(
                                  child: Text(
                                    'S',
                                    style: GoogleFonts.roboto(
                                      color: selectedDay == 'S'
                                          ? AppColors.kWhite
                                          : AppColors.kBlack,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () {
                                setState(() {
                                  selectedDay = "M";
                                });
                              },
                              child: Container(
                                height: 25,
                                width: 25,
                                decoration: BoxDecoration(
                                  color: selectedDay == 'M'
                                      ? Color(0xff0a545e)
                                      : Colors.white,
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                      color: selectedDay == 'M'
                                          ? Color(0xff08535b)
                                          : Colors.black,
                                      width: 2),
                                ),
                                child: Center(
                                  child: Text(
                                    'M',
                                    style: GoogleFonts.roboto(
                                      color: selectedDay == 'M'
                                          ? AppColors.kWhite
                                          : AppColors.kBlack,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () {
                                setState(() {
                                  selectedDay = "T";
                                });
                              },
                              child: Container(
                                height: 25,
                                width: 25,
                                decoration: BoxDecoration(
                                  color: selectedDay == 'T'
                                      ? Color(0xff0a545e)
                                      : Colors.white,
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                      color: selectedDay == 'T'
                                          ? Color(0xff08535b)
                                          : Colors.black,
                                      width: 2),
                                ),
                                child: Center(
                                  child: Text(
                                    'T',
                                    style: GoogleFonts.roboto(
                                      color: selectedDay == 'T'
                                          ? AppColors.kWhite
                                          : AppColors.kBlack,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () {
                                setState(() {
                                  selectedDay = "W";
                                });
                              },
                              child: Container(
                                height: 25,
                                width: 25,
                                decoration: BoxDecoration(
                                  color: selectedDay == 'W'
                                      ? Color(0xff0a545e)
                                      : Colors.white,
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                      color: selectedDay == 'W'
                                          ? Color(0xff08535b)
                                          : Colors.black,
                                      width: 2),
                                ),
                                child: Center(
                                  child: Text(
                                    'W',
                                    style: GoogleFonts.roboto(
                                      color: selectedDay == 'W'
                                          ? AppColors.kWhite
                                          : AppColors.kBlack,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () {
                                setState(() {
                                  selectedDay = "TU";
                                });
                              },
                              child: Container(
                                height: 25,
                                width: 25,
                                decoration: BoxDecoration(
                                  color: selectedDay == 'TU'
                                      ? Color(0xff0a545e)
                                      : Colors.white,
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                      color: selectedDay == 'TU'
                                          ? Color(0xff08535b)
                                          : Colors.black,
                                      width: 2),
                                ),
                                child: Center(
                                  child: Text(
                                    'T',
                                    style: GoogleFonts.roboto(
                                      color: selectedDay == 'TU'
                                          ? AppColors.kWhite
                                          : AppColors.kBlack,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () {
                                setState(() {
                                  selectedDay = "F";
                                });
                              },
                              child: Container(
                                height: 25,
                                width: 25,
                                decoration: BoxDecoration(
                                  color: selectedDay == 'F'
                                      ? Color(0xff0a545e)
                                      : Colors.white,
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                      color: selectedDay == 'F'
                                          ? Color(0xff08535b)
                                          : Colors.black,
                                      width: 2),
                                ),
                                child: Center(
                                  child: Text(
                                    'F',
                                    style: GoogleFonts.roboto(
                                      color: selectedDay == 'F'
                                          ? AppColors.kWhite
                                          : AppColors.kBlack,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () {
                                setState(() {
                                  selectedDay = "SU";
                                });
                              },
                              child: Container(
                                height: 25,
                                width: 25,
                                decoration: BoxDecoration(
                                  color: selectedDay == 'SU'
                                      ? Color(0xff0a545e)
                                      : Colors.white,
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                      color: selectedDay == 'SU'
                                          ? Color(0xff08535b)
                                          : Colors.black,
                                      width: 2),
                                ),
                                child: Center(
                                  child: Text(
                                    'S',
                                    style: GoogleFonts.roboto(
                                      color: selectedDay == 'SU'
                                          ? AppColors.kWhite
                                          : AppColors.kBlack,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        RadioGroup<String>.builder(
                          groupValue: _snacksGroupValue,
                          onChanged: (value) => setState(() {
                            _snacksGroupValue = value;
                          }),
                          items: _snacks,
                          itemBuilder: (item) => RadioButtonBuilder(
                            item,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              flex: 4,
                              child: TextFormField(
                                controller: _controllerAddFood,
                                focusNode: focusNodeAddFood,
                                maxLines: 1,
                                textInputAction: TextInputAction.next,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(vertical: 2),
                                  isDense: true,
                                  hintText: 'Add Your Food..',
                                ),
                                onFieldSubmitted: (_) {
                                  AppCommon.kChangeFocusNode(
                                      currentFocusNode: focusNodeAddFood,
                                      nextFocusNode: focusNodeCalories,
                                      context: context);
                                },
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: IconButton(
                                onPressed: () {
                                  setState(() {
                                    _foodList.add(_controllerAddFood.text);
                                    _controllerAddFood.clear();
                                  });


                                },
                                icon: Icon(
                                  Icons.add_circle_outline,
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Expanded(
                          child: ListView(
                            scrollDirection: Axis.horizontal,
                            shrinkWrap: true,
                            children: List.generate(_foodList.length, (index) => Row(
                              children: [
                                getChipView(content: _foodList[index]),
                                SizedBox(
                                  width: 10,
                                ),
                              ],
                            )),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        TextFormField(
                          controller: _controllerChoice,
                          focusNode: focusNodeCalories,
                          maxLines: 1,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(vertical: 2),
                            isDense: true,
                            hintText: 'Enter Your Calories..',
                          ),
                          onFieldSubmitted: (_) {
                            AppCommon.kChangeFocusNode(
                                currentFocusNode: focusNodeCalories,
                                nextFocusNode: focusNodeComment,
                                context: context);
                          },
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        TextFormField(
                          controller: _controllerComment,
                          focusNode: focusNodeComment,
                          maxLines: 1,
                          textInputAction: TextInputAction.done,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(vertical: 2),
                            isDense: true,
                            hintText: 'Comments',
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        GestureDetector(
                          onTap: () {
                            progressDialog.show();
                            addDietPlan();
                          },
                          child: Align(
                            alignment: Alignment.topRight,
                            child: Container(
                              width: 130,
                              height: 40,
                              decoration: BoxDecoration(
                                color: AppColors.kGreen,
                                borderRadius: BorderRadius.circular(20),
                              ),
                              child: Center(
                                child: Text(
                                  'ADD',
                                  style: GoogleFonts.roboto(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          );
        });
  }

  Chip getChipView({String? content}) {
    return Chip(
      label: Text(
        content!,
        style: GoogleFonts.roboto(
          fontSize: 10,
          color: Colors.black,
          fontWeight: FontWeight.w500,
        ),
      ),
      deleteIconColor: Colors.black,
      onDeleted: () {
        setState(() {
          _foodList.remove(content);
        });

      },
      deleteIcon: GestureDetector(
        onTap: (){
          setState(() {
            _foodList.remove(content);
          });
        },
        child: Icon(
          Icons.clear_rounded,
          color: Colors.black,
          size: 16,
        ),
      ),
      useDeleteButtonTooltip: true,
      backgroundColor: Color(0xfff2f2f2),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            30,
          ),
          side: BorderSide(
            color: Color(0xffdddddd),
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return isShowCurrentView
        ? Scaffold(
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                _showViewDialog();
              },
              backgroundColor: Color(0xff006e6f),
              child: Icon(
                Icons.add,
                color: Colors.white,
                size: 30,
              ),
            ),
            body: Container(
              color: Colors.white,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 120,
                    color: Color(0xff006e6f),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SafeArea(
                          child: Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: Padding(
                              padding: const EdgeInsets.only(
                                top: 10,
                              ),
                              child: Row(
                                children: [
                                  GestureDetector(
                                    behavior: HitTestBehavior.translucent,
                                    child: Image.asset(
                                      AppStrings.kImgMenuBar,
                                      width: 20,
                                      height: 20,
                                    ),
                                    onTap: () {
                                      if (widget.scaffoldKey.currentState!
                                          .isDrawerOpen) {
                                        Navigator.of(context).pop();
                                      } else {
                                        widget.scaffoldKey.currentState!
                                            .openDrawer();
                                      }
                                    },
                                  ),
                                  SizedBox(
                                    width: 30,
                                  ),
                                  Text(
                                    'Diet Planning',
                                    style: GoogleFonts.roboto(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  Spacer(),
                                  GestureDetector(
                                    behavior: HitTestBehavior.translucent,
                                    onTap: () {
                                      setState(() {
                                        isShowCurrentView = false;
                                      });
                                    },
                                    child: Image.asset(
                                      AppStrings.kImgNotification,
                                      width: 17.5,
                                      height: 18.5,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: SfCalendar(
                      view: CalendarView.schedule,
                      showDatePickerButton: true,
                      showNavigationArrow: true,
                      allowViewNavigation: true,
                      todayHighlightColor: Colors.blue,
                      dataSource: MeetingDataSource(_getDataSource()),
                      // onTap: (CalendarTapDetails details){
                      //   _showViewDialog();
                      // },
                      // by default the month appointment display mode set as Indicator, we can
                      // change the display mode as appointment using the appointment display
                      // mode property

                      monthViewSettings: MonthViewSettings(
                          appointmentDisplayMode:
                              MonthAppointmentDisplayMode.appointment),
                    ),
                  ),
                  SizedBox(height: 30),
                ],
              ),
            ),
          )
        : NotificationPage(
            isReturn: (value) {
              setState(() {
                isShowCurrentView = value;
              });
            },
            scaffoldKey: widget.scaffoldKey,
          );
  }

  Future<DietPlanModel?> getDietPlanningData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    try {
      final response = await http.post(
        Uri.parse(AppStrings.kGetDietPlanDataUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue,
        },
        body: jsonEncode(<String, String>{
          'user_id':
              preferences.getString(AppStrings.kPrefUserIdKey).toString(),
        }),
      );
      var responseData = jsonDecode(response.body);
      setState(() {
        isShowLoader = false;
      });
      print('response from diet plan data $responseData');
      if (response.statusCode == 200) {
        if (responseData['status'] == 'success') {
          return DietPlanModel.fromJson(responseData);
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              // Navigator.of(context).pop();
            },
          )..show();
        }
      } else {
        throw Exception('Failed to load data');
      }
    } catch (exception) {
      print('exception $exception');
    }
  }

  Future<void> addDietPlan() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    String jsonBody = jsonEncode(<String, String>{
      'user_id': preferences.getString(AppStrings.kPrefUserIdKey).toString(),
      'day': selectedDay,
      'meal': '',
      'add_food': '',
      'enter_calories': _controllerChoice.text,
      'comments': _controllerComment.text,
      'date': '',
      'start_time': '',
      'end_time': ''
    });

    print('json parms $jsonBody');

    try {
      final http.Response response = await http.post(
        Uri.parse(AppStrings.kAddDietPlanUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue
        },
        body: jsonBody,
      );

      var _responseData = jsonDecode(response.body);
      print('response from service ${response.body}');

      Navigator.of(context).pop();

      if (response.statusCode == 200) {
        if (_responseData['status'] == 'success') {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            title: AppStrings.kSuccess,
            desc: _responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              setState(() {
                _future = getDietPlanningData();
              });
            },
          )..show();
        } else {
          AppCommon.kErrorDialog(
              context: context,
              errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
        }
      } else {
        AppCommon.kErrorDialog(
            context: context,
            errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
      }
    } catch (exception) {
      print('exception is $exception');
      Navigator.of(context).pop();
      AppCommon.kErrorDialog(
          context: context, errorMsg: 'Some error occur.html response getting');
    }
  }
}

List<Meeting> _getDataSource() {
  final List<Meeting> meetings = <Meeting>[];
  final DateTime today = DateTime.now();
  final DateTime startTime =
      DateTime(today.year, today.month, today.day, 9, 0, 0);
  final DateTime endTime = startTime.add(const Duration(hours: 1));
  meetings.add(Meeting(' Brekfast\nBacon, egg and cheese sandwich.', startTime,
      endTime, const Color(0xFF12a5a5), false));
  meetings.add(Meeting(
      'Snack\nChocolate covered potato chips',
      DateTime(today.year, today.month, today.day, 20, 0, 0),
      endTime,
      const Color(0xFF1587d0),
      false));
  return meetings;
}

class MeetingDataSource extends CalendarDataSource {
  /// Creates a meeting data source, which used to set the appointment
  /// collection to the calendar
  MeetingDataSource(List<Meeting> source) {
    appointments = source;
  }

  @override
  DateTime getStartTime(int index) {
    return appointments![index].from;
  }

  @override
  DateTime getEndTime(int index) {
    return appointments![index].to;
  }

  @override
  String getSubject(int index) {
    return appointments![index].eventName;
  }

  @override
  Color getColor(int index) {
    return appointments![index].background;
  }

  @override
  bool isAllDay(int index) {
    return appointments![index].isAllDay;
  }
}

class Meeting {
  /// Creates a meeting class with required details.
  Meeting(this.eventName, this.from, this.to, this.background, this.isAllDay);

  /// Event name which is equivalent to subject property of [Appointment].
  String eventName;

  /// From which is equivalent to start time property of [Appointment].
  DateTime from;

  /// To which is equivalent to end time property of [Appointment].
  DateTime to;

  /// Background which is equivalent to color property of [Appointment].
  Color background;

  /// IsAllDay which is equivalent to isAllDay property of [Appointment].
  bool isAllDay;
}
