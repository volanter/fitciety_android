import 'package:ficiety/ui/edit_profile.dart';
import 'package:ficiety/ui/payment_history.dart';
import 'package:ficiety/ui/privacy_policy.dart';
import 'package:ficiety/ui/term_condition.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

import 'change_password.dart';
import 'notification_page.dart';

class Settings extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  // final Function(String) name;

  Settings({required this.scaffoldKey/*,required this.name*/});


  @override
  _SettingsState createState() => _SettingsState();
}

enum SettingViewType{
  Setting,
  Profile,
  PaymentHistory,
  Notification,
  ChangePassword,
  PrivacyPolicy,
  TermsCondition,
}

class _SettingsState extends State<Settings> {

  List<String> listName = [];
  List<String> listContent = [];
  int _crossAxisCount = 2;
  late SettingViewType settingViewType;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    settingViewType = SettingViewType.Setting;
    listName.add('Profile');
    listName.add('Payment\nHistory');
    listName.add('Notification');
    listName.add('Change\nPassword');
    listName.add('Terms\nconditions');
    listName.add('Privacy and\npolicy');

    listContent.add('');
    listContent.add('');
    listContent.add('');
    listContent.add('');
    listContent.add('');
    listContent.add('');
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return settingViewType==SettingViewType.Setting?Container(
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: Scaffold.of(context).appBarMaxHeight,
            color: Color(0xff006e6f),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SafeArea(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        top: 10,
                      ),
                      child: Row(
                        children: [
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: Image.asset(
                              AppStrings.kImgMenuBar,
                              width: 18,
                              height: 13,
                            ),
                            onTap: () {
                              if (widget
                                  .scaffoldKey.currentState!.isDrawerOpen) {
                                Navigator.of(context).pop();
                              } else {
                                widget.scaffoldKey.currentState!.openDrawer();
                              }
                            },
                          ),
                          SizedBox(
                            width: 30,
                          ),
                          Text(
                            AppStrings.kSetting,
                            style: GoogleFonts.roboto(
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height:10),
          Expanded(
            child: Padding(
                padding: const EdgeInsets.only(
                  left: 30,
                  right: 30,
                ),
                child: GridView.builder(
                  shrinkWrap: true,
                  itemCount: listName.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () {
                        switch(index){
                          case 0:
                            setState(() {
                              settingViewType=SettingViewType.Profile;
                            });
                            break;
                          case 1:
                            setState(() {
                              settingViewType=SettingViewType.PaymentHistory;
                            });
                            break;
                          case 2:
                            setState(() {
                              settingViewType=SettingViewType.Notification;
                            });
                            break;
                          case 3:
                            setState(() {
                              settingViewType=SettingViewType.ChangePassword;
                            });
                            break;
                          case 4:
                            setState(() {
                              settingViewType=SettingViewType.TermsCondition;
                            });
                            break;
                          case 5:
                            setState(() {
                              settingViewType=SettingViewType.PrivacyPolicy;
                            });
                            break;



                        }
                        // widget.name(listName[index]);
                        // setState(() {
                        //   isShowClientList = false;
                        // });
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Container(
                          // height: 131,
                          // width: 142.5,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                5.0,
                              ),
                              border: Border.all(
                                color: Color(0xff006E6F),
                              )),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              AppCommon.kIconDashboardTextView(
                                content: listContent[index],
                                fontName: AppStrings.kFontAwsSolid,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                listName[index],
                                // AppStrings.kPTProgrammesTrainer,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.roboto(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: _crossAxisCount,
                    crossAxisSpacing: 5.0,
                    mainAxisSpacing: 2.0,
                    childAspectRatio: (MediaQuery.of(context).size.height / 2.5) /
                        (MediaQuery.of(context).size.height - 480),
                  ),
                )),
          ),
        ],
      ),
    ):settingViewType==SettingViewType.Profile?EditProfile(scaffoldKey: widget.scaffoldKey,isReturn: (bool ) {
      setState(() {
        settingViewType=SettingViewType.Setting;
      });
    },isBackShow: true,):settingViewType==SettingViewType.PaymentHistory?PaymentHistory(scaffoldKey: widget.scaffoldKey, isReturn: (bool ) {
      setState(() {
        settingViewType=SettingViewType.Setting;
      });
    },):settingViewType==SettingViewType.Notification?NotificationPage(scaffoldKey: widget.scaffoldKey, isReturn: (bool ) {
      setState(() {
        settingViewType=SettingViewType.Setting;
      });
    },):settingViewType==SettingViewType.TermsCondition?TermCondition(scaffoldKey: widget.scaffoldKey, isReturn: (bool){
      setState(() {
        settingViewType=SettingViewType.Setting;
      });
    }):settingViewType==SettingViewType.PrivacyPolicy?PrivacyPolicy(scaffoldKey: widget.scaffoldKey, isReturn: (bool){
      setState(() {
        settingViewType=SettingViewType.Setting;
      });
    }):ChangePassword(scaffoldKey: widget.scaffoldKey,isReturn: (bool ) {
      setState(() {
        settingViewType=SettingViewType.Setting;
      });
    });
  }
}
