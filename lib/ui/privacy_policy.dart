
import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:ficiety/utils/progressdialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class PrivacyPolicy extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Function(bool) isReturn;

  PrivacyPolicy({required this.scaffoldKey, required this.isReturn});

  @override
  _PrivacyPolicyState createState() => _PrivacyPolicyState();
}

class _PrivacyPolicyState extends State<PrivacyPolicy> {

  late ProgressDialog progressDialog;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    progressDialog = new ProgressDialog(context, ProgressDialogType.Normal);
    progressDialog.setMessage(AppStrings.kLoadingMsg);
    progressDialog.show();
    getPrivacyPolicyData();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 120,
            color: Color(0xff006e6f),
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Padding(
                  padding: const EdgeInsets.only(
                    top: 10,
                  ),
                  child: Row(
                    children: [
                      GestureDetector(
                        child: Icon(Icons.arrow_back_sharp,color: Colors.white,)/*Image.asset(
                          AppStrings.kImgMenuBar,
                          width: 18,
                          height: 13,
                        )*/,
                        onTap: () {
                          widget.isReturn(true);
                          // if (widget.scaffoldKey.currentState!.isDrawerOpen) {
                          //   Navigator.of(context).pop();
                          // } else {
                          //   widget.scaffoldKey.currentState!.openDrawer();
                          // }
                        },
                      ),
                      SizedBox(
                        width: 30,
                      ),
                      Text(
                        AppStrings.kPrivacyPolicy,
                        style: GoogleFonts.roboto(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(
                left: 30,
                right: 30,
              ),
            ),
          )
        ],
      ),
    );
  }


  Future<void> getPrivacyPolicyData() async{

    try{
      final response = await http.get(
        Uri.parse(AppStrings.kPrivacyPolicyUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue,
        },
      );
      var responseData = jsonDecode(response.body);
      Navigator.of(context).pop();
      print('response from privacy policy  data  $responseData');
      if (response.statusCode == 200) {
        if (responseData['status'] == 'success') {

        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              // Navigator.of(context).pop();
            },
          )..show();
        }
      } else {
        throw Exception('Failed to load data');
      }
    }catch (exception){
      print('exception $exception');
    }
  }
}
