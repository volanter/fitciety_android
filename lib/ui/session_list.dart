import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/models/session_list_model.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:ficiety/utils/progressdialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'notification_page.dart';

class SessionList extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Function(bool) isReturn;

  SessionList({required this.scaffoldKey, required this.isReturn});

  @override
  _SessionListState createState() => _SessionListState();
}

class _SessionListState extends State<SessionList>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  late bool _isCancel;
  late bool isSessionView;
  late bool isShowLoader;
  late Future<SessionListModel?> _future;
  late ProgressDialog progressDialog;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    progressDialog = ProgressDialog(context, ProgressDialogType.Normal);
    progressDialog.setMessage(AppStrings.kLoadingMsg);
    isSessionView = true;
    _future = getSessionListData();
    isShowLoader = true;
    _isCancel = true;
    _tabController = new TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff014344),
    ));
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: isSessionView?Container(
        color: Colors.white,
        child: FutureBuilder<SessionListModel?>(
          future: _future,
          builder: (BuildContext context, AsyncSnapshot<SessionListModel?>snapshot){
            if(!snapshot.hasData){
              return Center(child: Container(width:50,height:50,child: CircularProgressIndicator()));
            }else{
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    height: 300,
                    child: Stack(
                      clipBehavior: Clip.none,
                      fit: StackFit.loose,
                      alignment: Alignment.topLeft,
                      children: [
                        Container(
                          height: 190,
                          // color: Color(0xff057A87),
                          decoration: BoxDecoration(
                            image: DecorationImage(
                                fit: BoxFit.fill,
                                image: AssetImage(
                                  AppStrings.kImgTrainerBg,
                                )),),
                          child: Column(
                            children: [
                              SafeArea(
                                child: Padding(
                                  padding: const EdgeInsets.all(20.0),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      top: 10,
                                      left: 15,
                                    ),
                                    child: Row(
                                      children: [
                                        GestureDetector(
                                          child: AppCommon.kIconTrainerListTextView(
                                              fontName: AppStrings.kFontAwsSolid,
                                              content: ''),
                                          onTap: () {
                                            print('tap on back');
                                            widget.isReturn(true);
                                          },
                                          behavior: HitTestBehavior.translucent,
                                        ),
                                        SizedBox(
                                          width: 30,
                                        ),
                                        Text(
                                          AppStrings.kSessionList,
                                          style: GoogleFonts.roboto(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                        Spacer(),
                                        GestureDetector(
                                          behavior: HitTestBehavior.translucent,
                                          onTap: (){
                                            setState(() {
                                              isSessionView = false;
                                            });
                                          },
                                          child: Image.asset(
                                            AppStrings.kImgNotification,
                                            width: 17.5,
                                            height: 18.5,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Positioned(
                          top: 160,
                          left: 30,
                          child: Row(
                            children: [
                              Container(
                                width: 120,
                                height: 120,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(
                                      5.0,
                                    ),
                                    border: Border.all(
                                      color: Colors.white,
                                    ),
                                    image: DecorationImage(
                                        fit: BoxFit.fill,
                                        image: NetworkImage(
                                          AppStrings.kImageUrl+snapshot.data!.data[0].image,
                                        ))),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: 40,
                                  ),
                                  Text(
                                    snapshot.data!.data[0].trainerName,
                                    style: GoogleFonts.roboto(
                                      color: AppColors.kGreen,
                                      fontSize: 18,
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  SizedBox(height: 5),
                                  // Align(
                                  //   alignment: Alignment.topLeft,
                                  //   child: Row(
                                  //     children: [
                                  //       Icon(
                                  //         Icons.star,
                                  //         size: 15,
                                  //         color: Color(0xffffcc00),
                                  //       ),
                                  //       Icon(
                                  //         Icons.star,
                                  //         size: 15,
                                  //         color: Color(0xffffcc00),
                                  //       ),
                                  //       Icon(
                                  //         Icons.star,
                                  //         size: 15,
                                  //         color: Color(0xffffcc00),
                                  //       ),
                                  //       Icon(
                                  //         Icons.star,
                                  //         size: 15,
                                  //         color: Color(0xffffcc00),
                                  //       ),
                                  //       Icon(
                                  //         Icons.star,
                                  //         size: 15,
                                  //         color: Color(0xffffcc00),
                                  //       ),
                                  //     ],
                                  //   ),
                                  // ),
                                  Text(
                                    snapshot.data!.data[0].companyName,
                                    softWrap: true,
                                    overflow: TextOverflow.visible,
                                    style: GoogleFonts.roboto(
                                      color: AppColors.kBlack,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 20,
                      right: 20,
                    ),
                    child: TabBar(
                      unselectedLabelColor: Colors.grey,
                      labelColor: Color(0xff047176),
                      tabs: [
                        Tab(
                          text: AppStrings.kMyBooking,
                          // icon: Icon(Icons.people),
                        ),
                        Tab(
                          // icon: Icon(Icons.person),
                          text: AppStrings.kCompleted,
                        )
                      ],
                      controller: _tabController,
                      indicatorSize: TabBarIndicatorSize.tab,
                      indicatorPadding: EdgeInsets.all(10),
                    ),
                  ),
                  Divider(
                    color: Colors.grey,
                  ),
                  Expanded(
                    child: TabBarView(
                      children: [_myBookingView(snapshot.data!.data[0].myBooking,AppStrings.kImageUrl+snapshot.data!.data[0].image), _completedView(snapshot.data!.data[0].completed,AppStrings.kImageUrl+snapshot.data!.data[0].image)],
                      controller: _tabController,
                    ),
                  ),
                ],
              );
            }
          },
        ),
      ):NotificationPage(scaffoldKey: widget.scaffoldKey,isReturn: (value){
        setState(() {
          isSessionView = value;
        });
      },),
    );
  }

  Widget _completedView(List<Completed> completed, String image) {
    return Padding(
      padding: const EdgeInsets.only(
        left:10.0,right: 10.0,bottom:10.0,
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 15, right: 15,),
        child: ListView.builder(
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                height: 100,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(
                    5.0,
                  ),
                  border: Border.all(
                    color: Color(
                      0xff66b5b5b5,
                    ),
                  ),
                ),
                child: Row(
                  children: [
                    Expanded(child: Image.network(image,height: 100,width: 102,fit: BoxFit.fill,),flex: 3,),
                    Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          FaIcon(FontAwesomeIcons.userTie,color: AppColors.kGreen,size: 12,),
                          FaIcon(FontAwesomeIcons.clock,color: AppColors.kGreen,size: 12,),
                          FaIcon(FontAwesomeIcons.calendar,color: AppColors.kGreen,size: 12,),
                          FaIcon(FontAwesomeIcons.th,color: AppColors.kGreen,size: 12,),
                          // FaIcon(FontAwesomeIcons.handshake,color: AppColors.kGreen,size: 12,),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(completed[index].fullname,style: GoogleFonts.roboto(color: AppColors.kBlack,fontSize: 12,fontWeight: FontWeight.w500),),
                          Text(completed[index].time,style: GoogleFonts.roboto(color: AppColors.kBlack,fontSize: 12,fontWeight: FontWeight.w500),),
                          Text(completed[index].date,style: GoogleFonts.roboto(color: AppColors.kBlack,fontSize: 12,fontWeight: FontWeight.w500),),
                          Text(completed[index].category,style: GoogleFonts.roboto(color: AppColors.kBlack,fontSize: 12,fontWeight: FontWeight.w500),),
                          // Text('4 Session Left',style: GoogleFonts.roboto(color: AppColors.kBlack,fontSize: 12,fontWeight: FontWeight.w500),),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            height: 14.5,
                            width: 70,
                            decoration: BoxDecoration(color: Color(0xffefb861),
                              borderRadius: BorderRadius.circular(10.0),),
                            child: Center(child: Text(completed[index].paymentStatus,style: GoogleFonts.roboto(color: AppColors.kWhite,fontSize: 6,fontWeight: FontWeight.w500,),)),
                          ),
                          // Container(
                          //     height: 20,
                          //     width: 20,
                          //     decoration: BoxDecoration(color: Color(0xff006e6f),
                          //       borderRadius: BorderRadius.circular(2.0),),
                          //     child: Center(child: FaIcon(FontAwesomeIcons.pencilAlt,color: AppColors.kWhite,size: 12,),)),
                          // GestureDetector(
                          //   onTap: (){
                          //     showDialog(
                          //       context: context,
                          //       builder: (context) => StatefulBuilder(
                          //         builder: (context,setState){
                          //           return AlertDialog(
                          //             content: Column(
                          //               mainAxisSize: MainAxisSize.min,
                          //               children: [
                          //                 Text("Are you sure want to\nremove your Session?",style: GoogleFonts.roboto(color: Color(0xff333333),fontSize: 17,fontWeight: FontWeight.bold)),
                          //                 SizedBox(
                          //                   height: 30,
                          //                 ),
                          //                 Row(
                          //                   mainAxisAlignment: MainAxisAlignment.spaceAround,
                          //                   children: [
                          //                     Padding(
                          //                       padding: const EdgeInsets.all(2.0),
                          //                       child: Container(
                          //                         width: 96.5,
                          //                         height: 30,
                          //                         decoration: BoxDecoration(
                          //                           borderRadius: BorderRadius.circular(
                          //                             15.0,
                          //                           ),
                          //                           color: _isCancel
                          //                               ? AppColors.kWhite
                          //                               : AppColors.kGreen,
                          //                           border: Border.all(color: AppColors.kGreen,),
                          //                         ),
                          //                         child: GestureDetector(
                          //                           onTap: () {
                          //                             setState(() {
                          //                               _isCancel = !_isCancel;
                          //                             });
                          //                           },
                          //                           child: Center(
                          //                             child: Text(
                          //                               AppStrings.kYes,
                          //                               style: GoogleFonts.roboto(
                          //                                 color: _isCancel
                          //                                     ? AppColors.kGreen
                          //                                     : AppColors.kWhite,
                          //                                 fontWeight: FontWeight.bold,
                          //                               ),
                          //                               textAlign: TextAlign.center,
                          //                             ),
                          //                           ),
                          //                         ),
                          //                       ),
                          //                     ),
                          //                     Padding(
                          //                       padding: const EdgeInsets.all(2.0),
                          //                       child: Container(
                          //                         width: 96.5,
                          //                         height: 30,
                          //                         decoration: BoxDecoration(
                          //                           borderRadius: BorderRadius.circular(
                          //                             15.0,
                          //                           ),
                          //                           color: _isCancel
                          //                               ? AppColors.kGreen
                          //                               : AppColors.kWhite,
                          //                           border: Border.all(color: AppColors.kGreen,),
                          //                         ),
                          //                         child: GestureDetector(
                          //                           onTap: () {
                          //                             setState(() {
                          //                               _isCancel = !_isCancel;
                          //                             });
                          //                           },
                          //                           child: Center(
                          //                             child: Text(
                          //                               AppStrings.kCancel,
                          //                               style: GoogleFonts.roboto(
                          //                                 color: _isCancel
                          //                                     ? AppColors.kWhite
                          //                                     : AppColors.kGreen,
                          //                                 fontWeight: FontWeight.bold,
                          //                               ),
                          //                               textAlign: TextAlign.center,
                          //                             ),
                          //                           ),
                          //                         ),
                          //                       ),
                          //                     ),
                          //                   ],
                          //                 ),
                          //               ],
                          //             ),
                          //           );
                          //         },
                          //       ),
                          //     );
                          //   },
                          //   child: Container(
                          //       height: 20,
                          //       width: 20,
                          //       decoration: BoxDecoration(color: Color(0xff006e6f),
                          //         borderRadius: BorderRadius.circular(2.0),),
                          //       child: Center(child: FaIcon(FontAwesomeIcons.trashAlt,color: AppColors.kWhite,size: 12,),)),
                          // ),
                          Container(
                              height: 20,
                              width: 20,
                              decoration: BoxDecoration(color: Color(0xff006e6f),
                                borderRadius: BorderRadius.circular(2.0),),
                              child: Center(child: FaIcon(FontAwesomeIcons.history,color: AppColors.kWhite,size: 12,),))
                        ],
                      ),
                    ),
                    SizedBox(width: 10,),
                  ],
                ),
              ),
            );
          },
          itemCount: completed.length,
          padding: EdgeInsets.zero,
        ),
      ),
    );
  }

  Widget _myBookingView(List<MyBooking> myBooking,String image) {
    return Padding(
      padding: const EdgeInsets.only(
        left:10.0,right: 10.0,bottom:10.0,
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 15, right: 15,),
        child: ListView.builder(
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                height: 100,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(
                      5.0,
                    ),
                    border: Border.all(
                      color: Color(
                        0xff66b5b5b5,
                      ),
                    ),
                ),
                child: Row(
                  children: [
                    Expanded(child: Image.network(image,height: 100,width: 102,fit: BoxFit.fill,),flex: 3,),
                    Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          FaIcon(FontAwesomeIcons.userTie,color: AppColors.kGreen,size: 12,),
                          FaIcon(FontAwesomeIcons.clock,color: AppColors.kGreen,size: 12,),
                          FaIcon(FontAwesomeIcons.calendar,color: AppColors.kGreen,size: 12,),
                          FaIcon(FontAwesomeIcons.th,color: AppColors.kGreen,size: 12,),
                          FaIcon(FontAwesomeIcons.handshake,color: AppColors.kGreen,size: 12,),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(myBooking[index].fullname,style: GoogleFonts.roboto(color: AppColors.kBlack,fontSize: 12,fontWeight: FontWeight.w500),),
                          Text(myBooking[index].time,style: GoogleFonts.roboto(color: AppColors.kBlack,fontSize: 12,fontWeight: FontWeight.w500),),
                          Text(myBooking[index].date,style: GoogleFonts.roboto(color: AppColors.kBlack,fontSize: 12,fontWeight: FontWeight.w500),),
                          Text(myBooking[index].category,style: GoogleFonts.roboto(color: AppColors.kBlack,fontSize: 12,fontWeight: FontWeight.w500),),
                          Text(myBooking[index].sessionLeft,style: GoogleFonts.roboto(color: AppColors.kBlack,fontSize: 12,fontWeight: FontWeight.w500),),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Container(
                            height: 14.5,
                            width: 70,
                            decoration: BoxDecoration(color: Color(0xffefb861),
                            borderRadius: BorderRadius.circular(10.0),),
                            child: Center(child: Text(myBooking[index].paymentStatus,style: GoogleFonts.roboto(color: AppColors.kWhite,fontSize: 6,fontWeight: FontWeight.w500,),)),
                          ),
                          Container(
                            height: 20,
                            width: 20,
                            decoration: BoxDecoration(color: Color(0xff006e6f),
                              borderRadius: BorderRadius.circular(2.0),),
                            child: Center(child: FaIcon(FontAwesomeIcons.pencilAlt,color: AppColors.kWhite,size: 12,),)),
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: (){
                              showDialog(
                                context: context,
                                builder: (context) => StatefulBuilder(
                                  builder: (context,setState){
                                    return AlertDialog(
                                      content: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Text("Are you sure want to\nremove your Session?",style: GoogleFonts.roboto(color: Color(0xff333333),fontSize: 17,fontWeight: FontWeight.bold)),
                                          SizedBox(
                                            height: 30,
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.all(2.0),
                                                child: Container(
                                                  width: 96.5,
                                                  height: 30,
                                                  decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(
                                                      15.0,
                                                    ),
                                                    color: _isCancel
                                                        ? AppColors.kWhite
                                                        : AppColors.kGreen,
                                                    border: Border.all(color: AppColors.kGreen,),
                                                  ),
                                                  child: GestureDetector(
                                                    behavior: HitTestBehavior.translucent,
                                                    onTap: () {
                                                      setState(() {
                                                        _isCancel = !_isCancel;
                                                      });
                                                      Navigator.of(context).pop();
                                                      progressDialog.show();
                                                      removeSession(sessionId:myBooking[index].sessionId);
                                                    },
                                                    child: Center(
                                                      child: Text(
                                                        AppStrings.kYes,
                                                        style: GoogleFonts.roboto(
                                                          color: _isCancel
                                                              ? AppColors.kGreen
                                                              : AppColors.kWhite,
                                                          fontWeight: FontWeight.bold,
                                                        ),
                                                        textAlign: TextAlign.center,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.all(2.0),
                                                child: Container(
                                                  width: 96.5,
                                                  height: 30,
                                                  decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(
                                                      15.0,
                                                    ),
                                                    color: _isCancel
                                                        ? AppColors.kGreen
                                                        : AppColors.kWhite,
                                                    border: Border.all(color: AppColors.kGreen,),
                                                  ),
                                                  child: GestureDetector(
                                                    behavior: HitTestBehavior.translucent,
                                                    onTap: () {
                                                      setState(() {
                                                        _isCancel = !_isCancel;
                                                      });
                                                      Navigator.of(context).pop();
                                                    },
                                                    child: Center(
                                                      child: Text(
                                                        AppStrings.kCancel,
                                                        style: GoogleFonts.roboto(
                                                          color: _isCancel
                                                              ? AppColors.kWhite
                                                              : AppColors.kGreen,
                                                          fontWeight: FontWeight.bold,
                                                        ),
                                                        textAlign: TextAlign.center,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                ),
                              );
                            },
                            child: Container(
                                height: 20,
                                width: 20,
                                decoration: BoxDecoration(color: Color(0xff006e6f),
                                  borderRadius: BorderRadius.circular(2.0),),
                                child: Center(child: FaIcon(FontAwesomeIcons.trashAlt,color: AppColors.kWhite,size: 12,),)),
                          ),
                          // Container(
                          //     height: 20,
                          //     width: 20,
                          //     decoration: BoxDecoration(color: Color(0xff006e6f),
                          //       borderRadius: BorderRadius.circular(2.0),),
                          //     child: Center(child: FaIcon(FontAwesomeIcons.history,color: AppColors.kWhite,size: 12,),))
                        ],
                      ),
                    ),
                    SizedBox(width: 10,),
                  ],
                ),
              ),
            );
          },
          itemCount: myBooking.length,
          padding: EdgeInsets.zero,
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() async {
    widget.isReturn(true);
    return false;
  }

  Future<SessionListModel?> getSessionListData() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();

    try{
      final response = await http.post(
        Uri.parse(AppStrings.kGetSessionListUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue,
        },
        body: jsonEncode(<String, String>{
          'user_id': preferences.getString(AppStrings.kPrefUserIdKey).toString(),
        }),
      );
      var responseData = jsonDecode(response.body);
      setState(() {
        isShowLoader = false;
      });
      print('response from session list $responseData');
      if (response.statusCode == 200) {
        if (responseData['status'] == 'success') {
          return SessionListModel.fromJson(responseData);
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              Navigator.of(context).pop();
            },
          )..show();
        }
      } else {
        throw Exception('Failed to load data');
      }
    }catch (exception){
      print('exception $exception');
    }
  }

  Future<void> removeSession({required int sessionId}) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();

    String jsonBody = jsonEncode(<String, String>{
      'user_id':
      preferences.getString(AppStrings.kPrefUserIdKey).toString(),
      'session_id':sessionId.toString(),
    });

    print('json parms $jsonBody');

    try {
      final http.Response response = await http.post(
        Uri.parse(AppStrings.kRemoveSesionUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue
        },
        body: jsonBody,);

      var _responseData = jsonDecode(response.body);
      print('response from service ${response.body}');

      Navigator.of(context).pop();

      if (response.statusCode == 200) {
        if (_responseData['status'] == 'success') {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            title: AppStrings.kSuccess,
            desc: _responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              setState(() {
                _future = getSessionListData();
              });
            },
          )..show();
        } else {
          AppCommon.kErrorDialog(
              context: context,
              errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
        }
      } else {
        AppCommon.kErrorDialog(
            context: context,
            errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
      }
    } catch (exception) {
      print('exception is $exception');
      Navigator.of(context).pop();
      AppCommon.kErrorDialog(
          context: context, errorMsg: 'Some error occur.html response getting');
    }

  }

}
