import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/models/exercise_model.dart';
import 'package:ficiety/ui/user_exercise_program.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'notification_page.dart';

class UserExercise extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  UserExercise({required this.scaffoldKey});

  @override
  _UserExerciseState createState() => _UserExerciseState();
}

enum UserExerciseType {
  UserExercise,
  Notification,
  UserExerciseProgram,
}

class _UserExerciseState extends State<UserExercise> {
  List<String> dayName = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  List<String> exerciseName = [
    'ABS Beginner',
    'Chest Beginner',
    'Arm Beginner',
    'Leg Beginner',
    'Shoulder & Back Beginner',
    'ABS Beginner'
  ];
  List<String> imagesName = [
    AppStrings.kImgAbs,
    AppStrings.kImgAbs,
    AppStrings.kImgBack,
    AppStrings.kImgAbs,
    AppStrings.kImgAbs,
    AppStrings.kImgBack,
  ];
  late bool isShowUserExercise;
  late int _choiceIndex;
  String selectedDay = 'Mon';
  late String selectedId;
  late String selectedProgram;
  late UserExerciseType userExerciseType;
  late bool isShowLoader;
  late Future<ExerciseModel?> _future;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _future = getExercise(dayName: 'mon');
    isShowLoader = true;
    userExerciseType = UserExerciseType.UserExercise;
    isShowUserExercise = true;
    _choiceIndex = 0;
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return userExerciseType == UserExerciseType.UserExercise
        ? Container(
            color: Colors.white,
            height: MediaQuery.of(context).size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 140,
                  color: Color(0xff006e6f),
                  child: SafeArea(
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Padding(
                        padding: const EdgeInsets.only(
                          top: 10,
                        ),
                        child: Row(
                          children: [
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              child: Image.asset(
                                AppStrings.kImgMenuBar,
                                width: 18,
                                height: 13,
                              ),
                              onTap: () {
                                if (widget
                                    .scaffoldKey.currentState!.isDrawerOpen) {
                                  Navigator.of(context).pop();
                                } else {
                                  widget.scaffoldKey.currentState!.openDrawer();
                                }
                              },
                            ),
                            SizedBox(
                              width: 30,
                            ),
                            Text(
                              AppStrings.kUserExercise,
                              style: GoogleFonts.roboto(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            Spacer(),
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () {
                                setState(() {
                                  userExerciseType =
                                      UserExerciseType.Notification;
                                });
                              },
                              child: Image.asset(
                                AppStrings.kImgNotification,
                                width: 17.5,
                                height: 18.5,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 30,
                    right: 30,
                    top: 20,
                  ),
                  child: Text(AppStrings.kDays,
                      style: GoogleFonts.roboto(
                        color: Color(0xff006e6f),
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 30,
                    right: 30,
                  ),
                  child: Container(
                    height: 60,
                    child: ListView.builder(
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.only(
                            left: 5,
                            right: 5,
                          ),
                          child: ChoiceChip(
                            label: Text(
                              dayName[index],
                              style: GoogleFonts.roboto(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            selected: _choiceIndex == index,
                            selectedColor: Color(0xff006e6f),
                            onSelected: (bool selected) {
                              setState(() {
                                print('click on day ');
                                selectedDay = dayName[index];
                                _choiceIndex = selected ? index : 0;
                                isShowLoader = true;
                                _future = getExercise(dayName: dayName[index]);
                              });
                            },
                            backgroundColor: Color(0xff0d454a),
                            labelStyle: TextStyle(color: Colors.white),
                          ),
                        );
                      },
                      itemCount: dayName.length,
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      padding: EdgeInsets.zero,
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(
                      left: 30,
                      right: 30,
                    ),
                    child: FutureBuilder<ExerciseModel?>(
                      future: _future,
                      builder: (BuildContext context,
                          AsyncSnapshot<ExerciseModel?> snapshot) {
                        if (!snapshot.hasData) {
                          return isShowLoader
                              ? Center(
                                  child: Container(
                                      width: 50,
                                      height: 50,
                                      child: CircularProgressIndicator()))
                              : SizedBox();
                        } else {
                          return ListView.builder(
                            itemBuilder: (context, index) {
                              return GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                onTap: () {
                                  setState(() {
                                    selectedProgram =
                                        snapshot.data!.data[index].exerciseName;
                                    selectedId = snapshot.data!.data[index].levelId;
                                    // isShowUserExercise = false;
                                    userExerciseType =
                                        UserExerciseType.UserExerciseProgram;
                                  });
                                },
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 15),
                                  child: Container(
                                    height: 110.5,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(
                                        5.0,
                                      ),
                                      // border: Border.all(
                                      //   color: Color(0xff80000000),
                                      // ),
                                      // image: DecorationImage(
                                      //   image: NetworkImage('https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/home-abs-1559644660.jpg?crop=1.00xw:0.752xh;0,0.111xh&resize=980:*'),
                                      //   fit: BoxFit.fill,
                                      // )
                                    ),
                                    child: Stack(
                                      children: [
                                        Image.network(
                                          AppStrings.kExerciseImageUrl +
                                              snapshot.data!.data[index]
                                                  .exerciseImage,
                                          fit: BoxFit.fill,
                                          width: double.infinity,
                                          height: double.infinity,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            left: 30,
                                            top: 20,
                                          ),
                                          child: Text(
                                              snapshot.data!.data[index]
                                                  .exerciseName,
                                              style: GoogleFonts.roboto(
                                                color: Colors.white,
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500,
                                              )),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                            itemCount: snapshot.data!.data.length,
                            shrinkWrap: true,
                            padding: EdgeInsets.zero,
                            scrollDirection: Axis.vertical,
                          );
                        }
                      },
                    ),
                  ),
                )
              ],
            ),
          )
        : userExerciseType == UserExerciseType.UserExerciseProgram
            ? UserExerciseProgram(
                scaffoldKey: widget.scaffoldKey,
                programName: selectedProgram,
                day: selectedDay,
                exerciseId: selectedId,

                isReturn: (value) {
                  setState(() {
                    // isShowUserExercise = value;
                    userExerciseType = UserExerciseType.UserExercise;
                  });
                },
              )
            : NotificationPage(
                scaffoldKey: widget.scaffoldKey,
                isReturn: (value) {
                  setState(() {
                    userExerciseType = UserExerciseType.UserExercise;
                  });
                },
              );
  }

  Future<ExerciseModel?> getExercise({required String dayName}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    try {
      final response = await http.post(Uri.parse(AppStrings.kGetExerciseUrl),
          headers: <String, String>{
            AppStrings.kHeaderType: AppStrings.kHeaderValue,
          },
          body: jsonEncode(<String, String>{
            'user_id':
                '7' /*preferences.getString(AppStrings.kPrefUserIdKey).toString()*/,
            'day': dayName,
          }));
      var responseData = jsonDecode(response.body);
      setState(() {
        isShowLoader = false;
      });
      print('response from exercise $responseData');
      if (response.statusCode == 200) {
        if (responseData['status'] == 'success') {
          return ExerciseModel.fromJson(responseData);
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              Navigator.of(context).pop();
            },
          )..show();
        }
      } else {
        throw Exception('Failed to load data');
      }
    } catch (exception) {
      print('exception $exception');
    }
  }
}
