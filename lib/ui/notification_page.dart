
import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/models/notification_model.dart';
import 'package:ficiety/ui/order_summary.dart';
import 'package:ficiety/ui/success_payment.dart';
import 'package:ficiety/ui/user_exercise_program.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class NotificationPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Function(bool) isReturn;

  NotificationPage({required this.scaffoldKey, required this.isReturn});

  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  late bool isSuccessPayment;
  late String selectedProgram;
  late Future<NotificationModel?> _future;
  late bool isShowLoader;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isShowLoader = true;
    _future = getNotificationData();
    isSuccessPayment = true;
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 120,
            color: Color(0xff006e6f),
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Padding(
                  padding: const EdgeInsets.only(
                    top: 10,
                  ),
                  child: Row(
                    children: [
                      GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: Icon(Icons.arrow_back_sharp,color: Colors.white,)/*Image.asset(
                          AppStrings.kImgMenuBar,
                          width: 18,
                          height: 13,
                        )*/,
                        onTap: () {
                          widget.isReturn(true);
                          // if (widget.scaffoldKey.currentState!.isDrawerOpen) {
                          //   Navigator.of(context).pop();
                          // } else {
                          //   widget.scaffoldKey.currentState!.openDrawer();
                          // }
                        },
                      ),
                      SizedBox(
                        width: 30,
                      ),
                      Text(
                        AppStrings.kNotification,
                        style: GoogleFonts.roboto(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(
                left: 30,
                right: 30,
              ),
              child: FutureBuilder<NotificationModel?>(
               future: _future,
                builder: (BuildContext context, AsyncSnapshot<NotificationModel?> snapshot){
                 if(!snapshot.hasData){
                   return Center(
                     child: isShowLoader?CircularProgressIndicator():Text('No Data Found',style: TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.w900,),),
                   );
                 }else{
                   return ListView.builder(
                     itemBuilder: (context, index) {
                       return GestureDetector(
                         behavior: HitTestBehavior.translucent,
                         onTap: () {
                           setState(() {
                             isSuccessPayment = false;
                           });
                         },
                         child: Padding(
                           padding: const EdgeInsets.only(top: 15),
                           child: Container(
                             decoration: BoxDecoration(
                               borderRadius: BorderRadius.circular(
                                 5.0,
                               ),
                               gradient: new LinearGradient(colors: [Color(0xff006e6f),Colors.white],stops: [0.02, 0.02],),
                               border: Border.all(
                                 color: Color(0xff66b5b5b5),
                               ),
                             ),
                             child: ListTile(
                               leading: Container(
                                 child: CircleAvatar(
                                   backgroundImage: NetworkImage(
                                     AppStrings.kImageUrl+snapshot.data!.data[index].userImage
                                   ),
                                 ),
                                 width: 45,
                                 height: 45,
                               ),
                               title: RichText(
                                 text: TextSpan(
                                     text: snapshot.data!.data[index].title+' ',
                                     style: GoogleFonts.roboto(
                                       color: Color(0xff333333),
                                       fontWeight: FontWeight.bold,
                                       fontSize: 10,
                                     ),
                                     children: <TextSpan>[
                                       TextSpan(
                                         text: snapshot.data!.data[index].message,
                                         style: GoogleFonts.roboto(
                                           color: Color(0xff333333),
                                           fontWeight: FontWeight.normal,
                                           fontSize: 10,
                                         ),
                                       ),
                                     ]
                                 ),
                               ),
                               // Text(
                               //   'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.',
                               //   softWrap: true,
                               //   overflow: TextOverflow.visible,
                               //   style: GoogleFonts.roboto(
                               //     color: Color(0xff333333),
                               //     fontWeight: FontWeight.w900,
                               //     fontSize: 10,
                               //   ),
                               // ),
                               trailing: Padding(
                                 padding: EdgeInsets.zero,
                                 child: Icon(Icons.clear_outlined,size: 15,color: Colors.grey,),
                               ),
                               subtitle: Text(
                                 snapshot.data!.data[index].date+ ' '+snapshot.data!.data[index].time,
                                 style: GoogleFonts.roboto(
                                   color: Color(0xffaaaaaa),
                                   fontWeight: FontWeight.w500,
                                   fontSize: 12,
                                 ),
                               ),
                             ),
                           ),
                         ),
                       );
                     },
                     itemCount: snapshot.data!.data.length,
                     shrinkWrap: true,
                     padding: EdgeInsets.zero,
                     scrollDirection: Axis.vertical,
                   );
                 }
                },

              ),
            ),
          )
        ],
      ),
    );
  }

  Future<NotificationModel?> getNotificationData() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();

    try{
      final response = await http.post(
        Uri.parse(AppStrings.kGetNotificationUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue,
        },
        body: jsonEncode(<String, String>{
          'user_id': preferences.getString(AppStrings.kPrefUserIdKey).toString(),
          // 'my_id': preferences.getString(AppStrings.kPrefUserIdKey).toString(),
          // 'device_token':
          // preferences.getString(AppStrings.kPrefDeviceToken).toString(),
          // 'device_type':
          // preferences.getString(AppStrings.kPrefDeviceType).toString(),
          // 'language': 'en',
        }),
      );
      var responseData = jsonDecode(response.body);
      print('response from notification data  $responseData');
      if (response.statusCode == 200) {
        if (responseData['status'] == 'success') {
          return NotificationModel.fromJson(responseData);
        } else {
          setState(() {
            isShowLoader = false;
          });
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              // Navigator.of(context).pop();
            },
          )..show();
        }
      } else {
        throw Exception('Failed to load data');
      }
    }catch (exception){
      print('exception $exception');
    }
  }
}
