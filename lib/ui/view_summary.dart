import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/models/view_summary_model.dart';
import 'package:ficiety/ui/order_summary.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';


class ViewSummary extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Function(bool) isReturn;

  ViewSummary({required this.scaffoldKey, required this.isReturn});

  @override
  _ViewSummaryState createState() => _ViewSummaryState();
}

class _ViewSummaryState extends State<ViewSummary> {
  late bool isShowViewSummary;
  late Future<ViewSummaryModel?> _future;
  late bool isShowLoader;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isShowLoader = true;
    _future = getViewSummaryData();
    isShowViewSummary = true;
  }
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return isShowViewSummary?Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 120,
            color: Color(0xff006e6f),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SafeArea(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        top: 10,
                        // left: 20,
                      ),
                      child: Row(
                        children: [
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child:Icon(Icons.arrow_back_sharp,color: Colors.white,) /*AppCommon.kIconTrainerListTextView(
                                fontName: AppStrings.kFontAwsSolid,
                                content: '')*/,
                            onTap: () {
                              print('tap on back');
                              widget.isReturn(true);
                            },
                          ),
                          SizedBox(
                            width: 30,
                          ),
                          Text(
                            AppStrings.kViewSummary,
                            style: GoogleFonts.roboto(
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Spacer(),
                          AppCommon.kIconTextView(
                              fontName: AppStrings.kFontAwsSolid, content: ''),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20,right: 20,top: 30),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Center(
                        child: Text(
                          AppStrings.kAppointmentDate,
                          style: GoogleFonts.roboto(
                            color: Color(0xff006e6f),
                            fontSize: 14,
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Center(
                        child: Text(
                          AppStrings.kType,
                          style: GoogleFonts.roboto(
                            color: Color(0xff006e6f),
                            fontSize: 14,
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                      ),
                    ),Expanded(
                      child: Center(
                        child: Text(
                          AppStrings.kAmount,
                          style: GoogleFonts.roboto(
                            color: Color(0xff006e6f),
                            fontSize: 14,
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Divider(
                  color: Colors.grey,
                  height: 2,
                ),
                Container(
                  height: MediaQuery.of(context).size.height-400,
                  child: FutureBuilder<ViewSummaryModel?>(
                    future: _future,
                    builder: (BuildContext context, AsyncSnapshot<ViewSummaryModel?>snapshot){
                      if(!snapshot.hasData){
                        return isShowLoader?Center(child: Container(width:50,height: 50,child: CircularProgressIndicator())):SizedBox();
                      }else{
                        return ListView.builder(itemBuilder: (context,index){
                          return Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 10,bottom: 10,),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          snapshot.data!.data[index].date,
                                          style: GoogleFonts.roboto(
                                            color: Colors.black,
                                            fontSize: 12,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                        SizedBox(height: 5,),
                                        Text(
                                          snapshot.data!.data[index].time,
                                          style: GoogleFonts.roboto(
                                            color: Colors.black,
                                            fontSize: 10,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                        SizedBox(height: 5,),
                                        Text(
                                          snapshot.data!.data[index].time,
                                          style: GoogleFonts.roboto(
                                            color: Colors.black,
                                            fontSize: 10,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 10,bottom: 10,),
                                    child: Text(
                                      /*snapshot.data!.data[index].type*/'',
                                      style: GoogleFonts.roboto(
                                        color: Colors.black,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 10,bottom: 10,),
                                    child: Text(
                                      /*'£${snapshot.data!.data[index].amount}'*/'',
                                      style: GoogleFonts.roboto(
                                        color: Colors.black,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          );
                        },  itemCount: snapshot.data!.data.length,padding: EdgeInsets.zero,);
                      }
                    },
                  ),
                ),
                SizedBox(height: 50,),
                Container(
                  height: 35,
                  width: 181,
                  child: ElevatedButton(
                    onPressed: () {
                      setState(() {
                        isShowViewSummary = false;
                      });
                      // print('view profile clicked');
                      // widget.callback(AppStrings.kTrainerDetails);
                    },
                    style: ButtonStyle(
                        backgroundColor:
                        MaterialStateProperty.all<Color>(Color(0xff147e91))),
                    child: Text(
                      AppStrings.kPayNow,
                      style: GoogleFonts.roboto(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    ):OrderSummary(scaffoldKey: widget.scaffoldKey,isReturn: (value){
      setState(() {
        isShowViewSummary = value;
      });
    },);
  }

  Future<ViewSummaryModel?> getViewSummaryData() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();

    try{
      final response = await http.post(
        Uri.parse(AppStrings.kGetViewSummaryUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue,
        },
        body: jsonEncode(<String, String>{
          'user_id': preferences.getString(AppStrings.kPrefUserIdKey).toString(),
        }),
      );
      var responseData = jsonDecode(response.body);
      setState(() {
        isShowLoader = false;
      });
      print('response from view summary $responseData');
      if (response.statusCode == 200) {
        if (responseData['status'] == 'success') {
          return ViewSummaryModel.fromJson(responseData);
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              Navigator.of(context).pop();
            },
          )..show();
        }
      } else {
        throw Exception('Failed to load data');
      }
    }catch (exception){
      print('exception $exception');
    }
  }

}

