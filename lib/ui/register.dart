import 'dart:convert';
import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:device_info/device_info.dart';
import 'package:ficiety/trainer_ui/trainer_dashboard.dart';
import 'package:ficiety/ui/user_dashboard.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:ficiety/utils/progressdialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:group_radio_button/group_radio_button.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

enum GenderText { Male, Female }

class Register extends StatefulWidget {
  final String userRole;

  Register({required this.userRole});

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final _formKey = GlobalKey<FormState>();
  late bool _isTrainerView;
  late ProgressDialog progressDialog;

  TextEditingController _controllerFirstName = TextEditingController();
  TextEditingController _controllerLastName = TextEditingController();
  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerMobile = TextEditingController();
  TextEditingController _controllerPassword = TextEditingController();

  FocusNode _focusNodeFirstName = FocusNode();
  FocusNode _focusNodeLastName = FocusNode();
  FocusNode _focusNodeEmail = FocusNode();
  FocusNode _focusNodeMobile = FocusNode();
  FocusNode _focusNodePassword = FocusNode();
  FocusNode _focusNodeConfirmPassword = FocusNode();

  late String _token;
  late String _deviceType;
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  late SharedPreferences preferences;
  late bool isAgree;

  var _url = 'https://www.google.com';
  String _genderValue = 'Male';


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isAgree = true;
    progressDialog = new ProgressDialog(context, ProgressDialogType.Normal);
    progressDialog.setMessage('Loading...');
    _isTrainerView = false;

    SharedPreferences.getInstance().then((value) => preferences = value);
    getDeviceInformation();
  }

  void getDeviceInformation() async {
    print('gender value is $_genderValue');
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      print('Running on ${androidInfo.model}');
      _deviceType = 'android';
      _token = androidInfo.androidId;
      print('token is $_token');
    } else {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      print('Running on ${iosInfo.utsname.machine}');
      _deviceType = 'ios';
      _token = iosInfo.identifierForVendor;
      print('token is _token');
    }
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    print('height of screen $height');
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.fill,
            image: AssetImage(
              AppStrings.kImgLoginBg,
            ),
          ),
        ),
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Theme(
              data: ThemeData(
                  primaryColor: AppColors.kOffGrey,
                  accentColor: AppColors.kOffGrey,
                  dividerColor: AppColors.kOffGrey,
                  accentTextTheme: TextTheme(
                    bodyText1: TextStyle(
                      color: AppColors.kOffGrey,
                    ),
                  ),
                  hintColor: AppColors.kOffGrey,
                  primaryTextTheme: TextTheme(
                    bodyText1: TextStyle(
                      color: AppColors.kOffGrey,
                    ),
                  )),
              child: ListView(
                children: [
                  GestureDetector(
                    onTap: () => Navigator.of(context).pop(),
                    child: SafeArea(
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Icon(
                          Icons.arrow_back,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  // SafeArea(
                  //   child: Padding(
                  //     padding: const EdgeInsets.only(
                  //       top: 20,
                  //     ),
                  //     child: Center(
                  //       child: Container(
                  //         width: 150.5,
                  //         height: 30,
                  //         decoration: BoxDecoration(
                  //           borderRadius: BorderRadius.circular(
                  //             20.0,
                  //           ),
                  //           color: Colors.white,
                  //         ),
                  //         child: Row(
                  //           children: [
                  //             Expanded(
                  //               flex: 1,
                  //               child: Padding(
                  //                 padding: const EdgeInsets.all(2.0),
                  //                 child: Container(
                  //                   width: double.infinity,
                  //                   height: double.infinity,
                  //                   decoration: BoxDecoration(
                  //                     borderRadius: BorderRadius.circular(
                  //                       15.0,
                  //                     ),
                  //                     color: _isTrainerView
                  //                         ? AppColors.kBlue
                  //                         : AppColors.kWhite,
                  //                   ),
                  //                   child: GestureDetector(
                  //                     onTap: () {
                  //                       setState(() {
                  //                         _isTrainerView = !_isTrainerView;
                  //                       });
                  //                     },
                  //                     child: Center(
                  //                       child: Text(
                  //                         AppStrings.kTrainer,
                  //                         style: GoogleFonts.roboto(
                  //                           color: _isTrainerView
                  //                               ? AppColors.kWhite
                  //                               : AppColors.kBlue,
                  //                           fontWeight: FontWeight.bold,
                  //                         ),
                  //                         textAlign: TextAlign.center,
                  //                       ),
                  //                     ),
                  //                   ),
                  //                 ),
                  //               ),
                  //             ),
                  //             Expanded(
                  //               flex: 1,
                  //               child: Padding(
                  //                 padding: const EdgeInsets.all(2.0),
                  //                 child: Container(
                  //                   width: double.infinity,
                  //                   height: double.infinity,
                  //                   decoration: BoxDecoration(
                  //                     borderRadius: BorderRadius.circular(
                  //                       15.0,
                  //                     ),
                  //                     color: _isTrainerView
                  //                         ? AppColors.kWhite
                  //                         : AppColors.kBlue,
                  //                   ),
                  //                   child: GestureDetector(
                  //                     onTap: () {
                  //                       setState(() {
                  //                         _isTrainerView = !_isTrainerView;
                  //                       });
                  //                     },
                  //                     child: Center(
                  //                       child: Text(
                  //                         /*AppStrings.kUser*/'Client',
                  //                         style: GoogleFonts.roboto(
                  //                           color: _isTrainerView
                  //                               ? AppColors.kBlue
                  //                               : AppColors.kWhite,
                  //                           fontWeight: FontWeight.bold,
                  //                         ),
                  //                         textAlign: TextAlign.center,
                  //                       ),
                  //                     ),
                  //                   ),
                  //                 ),
                  //               ),
                  //             ),
                  //           ],
                  //         ),
                  //       ),
                  //     ),
                  //   ),
                  // ),
                  SizedBox(
                    height: height / 14,
                  ),
                  // Image.asset(
                  //   AppStrings.kImgProfilePic,
                  //   height: 90,
                  //   width: 90,
                  // ),
                  SizedBox(
                    height: 50,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 25,
                      right: 25,
                    ),
                    child: Row(
                      children: [
                        Expanded(
                          child: TextFormField(
                            controller: _controllerFirstName,
                            focusNode: _focusNodeFirstName,
                            textCapitalization: TextCapitalization.sentences,
                            style: AppCommon.kEditTextStyle(),
                            textInputAction: TextInputAction.next,
                            decoration: InputDecoration(
                              hintText: AppStrings.kFirstName,
                              border: AppCommon.kUnderlineBorder(),
                              enabledBorder: AppCommon.kUnderlineBorder(),
                              focusedBorder: AppCommon.kUnderlineBorder(),
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return AppStrings.kErrorMSgFirstName;
                              } else {
                                return null;
                              }
                            },
                            onFieldSubmitted: (_) {
                              AppCommon.kChangeFocusNode(
                                  currentFocusNode: _focusNodeFirstName,
                                  nextFocusNode: _focusNodeLastName,
                                  context: context);
                            },
                          ),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Expanded(
                          child: TextFormField(
                              controller: _controllerLastName,
                              focusNode: _focusNodeLastName,
                              style: AppCommon.kEditTextStyle(),
                              textCapitalization: TextCapitalization.sentences,
                              textInputAction: TextInputAction.next,
                              decoration: InputDecoration(
                                hintText: AppStrings.kLastName,
                                border: AppCommon.kUnderlineBorder(),
                                enabledBorder: AppCommon.kUnderlineBorder(),
                                focusedBorder: AppCommon.kUnderlineBorder(),
                              ),
                              onFieldSubmitted: (_) {
                                AppCommon.kChangeFocusNode(
                                    currentFocusNode: _focusNodeLastName,
                                    nextFocusNode: _focusNodeEmail,
                                    context: context);
                              },
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return AppStrings.kErrorMSgLastName;
                                } else {
                                  return null;
                                }
                              }),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 25,
                      right: 25,
                    ),
                    child: TextFormField(
                        style: AppCommon.kEditTextStyle(),
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.emailAddress,
                        controller: _controllerEmail,
                        focusNode: _focusNodeEmail,
                        decoration: InputDecoration(
                          hintText: AppStrings.kEmail,
                          border: AppCommon.kUnderlineBorder(),
                          enabledBorder: AppCommon.kUnderlineBorder(),
                          focusedBorder: AppCommon.kUnderlineBorder(),
                        ),
                        onFieldSubmitted: (_) {
                          AppCommon.kChangeFocusNode(
                              currentFocusNode: _focusNodeEmail,
                              nextFocusNode: _focusNodeMobile,
                              context: context);
                        },
                        validator: (value) {
                          if (value!.isEmpty) {
                            return AppStrings.kErrorMSgEmail;
                          } else if (!(value.contains('@'))) {
                            return AppStrings.kErrorMSgEmailValidate;
                          } else {
                            return null;
                          }
                        }),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 25,
                      right: 25,
                    ),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: TextFormField(
                            keyboardType: TextInputType.number,
                            style: AppCommon.kEditTextStyle(),
                            maxLength: 3,
                            decoration: InputDecoration(
                              hintText: '91',
                              counterText: '',
                              suffix: Image.asset(
                                AppStrings.kImgExpandMore,
                                width: 20,
                                height: 20,
                              ),
                              border: AppCommon.kUnderlineBorder(),
                              enabledBorder: AppCommon.kUnderlineBorder(),
                              focusedBorder: AppCommon.kUnderlineBorder(),
                            ),
                            /*validator: (value) {
                                if (value!.isEmpty) {
                                  return AppStrings.kErrorMSgCountry;
                                } else {
                                  return null;
                                }
                              }*/
                          ),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Expanded(
                          flex: 4,
                          child: TextFormField(
                              style: AppCommon.kEditTextStyle(),
                              controller: _controllerMobile,
                              maxLength: 10,
                              focusNode: _focusNodeMobile,
                              textInputAction: TextInputAction.next,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                counterText: '',
                                hintText: AppStrings.kMobileNo,
                                border: AppCommon.kUnderlineBorder(),
                                enabledBorder: AppCommon.kUnderlineBorder(),
                                focusedBorder: AppCommon.kUnderlineBorder(),
                              ),
                              onFieldSubmitted: (_) {
                                AppCommon.kChangeFocusNode(
                                    currentFocusNode: _focusNodeMobile,
                                    nextFocusNode: _focusNodePassword,
                                    context: context);
                              },
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return AppStrings.kErrorMSgMobile;
                                } else {
                                  return null;
                                }
                              }),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Theme(
                    data: Theme.of(context)
                        .copyWith(unselectedWidgetColor: AppColors.kOffGrey),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Expanded(
                          child: RadioListTile<String>(
                            title: const Text(
                              'Male',
                              style: TextStyle(color: AppColors.kOffGrey),
                            ),
                            value: 'Male',
                            groupValue: _genderValue,
                            onChanged: (value) {
                              setState(() {
                                _genderValue = value.toString();
                              });
                            },
                          ),
                        ),
                        Expanded(
                          child: RadioListTile<String>(
                            title: const Text(
                              'Female',
                              style: TextStyle(color: AppColors.kOffGrey),
                            ),
                            value: 'Female',
                            groupValue: _genderValue,
                            onChanged: (value) {
                              setState(() {
                                _genderValue = value.toString();
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 25,
                      right: 25,
                    ),
                    child: TextFormField(
                        style: AppCommon.kEditTextStyle(),
                        obscureText: true,
                        controller: _controllerPassword,
                        textCapitalization: TextCapitalization.sentences,
                        focusNode: _focusNodePassword,
                        textInputAction: TextInputAction.next,
                        decoration: InputDecoration(
                          hintText: AppStrings.kPassword,
                          border: AppCommon.kUnderlineBorder(),
                          enabledBorder: AppCommon.kUnderlineBorder(),
                          focusedBorder: AppCommon.kUnderlineBorder(),
                        ),
                        onFieldSubmitted: (_) {
                          AppCommon.kChangeFocusNode(
                              currentFocusNode: _focusNodePassword,
                              nextFocusNode: _focusNodeConfirmPassword,
                              context: context);
                        },
                        validator: (value) {
                          if (value!.isEmpty) {
                            return AppStrings.kErrorMSgPassword;
                          } else {
                            return null;
                          }
                        }),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 25,
                      right: 25,
                    ),
                    child: TextFormField(
                        focusNode: _focusNodeConfirmPassword,
                        style: AppCommon.kEditTextStyle(),
                        obscureText: true,
                        textCapitalization: TextCapitalization.sentences,
                        textInputAction: TextInputAction.done,
                        decoration: InputDecoration(
                          hintText: AppStrings.kConfirmPassword,
                          border: AppCommon.kUnderlineBorder(),
                          enabledBorder: AppCommon.kUnderlineBorder(),
                          focusedBorder: AppCommon.kUnderlineBorder(),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return AppStrings.kErrorMSgConfirmPassword;
                          } else if (value != _controllerPassword.text) {
                            return 'Password does not match';
                          } else {
                            return null;
                          }
                        }),
                  ),
/*                  Padding(
                    padding: const EdgeInsets.only(
                      left: 25,
                      right: 25,
                    ),
                    child: TextFormField(
                        style: AppCommon.kEditTextStyle(),
                        decoration: InputDecoration(
                          hintText: AppStrings.kGender,
                          border: AppCommon.kUnderlineBorder(),
                          suffixIcon: Image.asset(
                            AppStrings.kImgExpandMore,
                            width: 20,
                            height: 20,
                          ),
                          enabledBorder: AppCommon.kUnderlineBorder(),
                          focusedBorder: AppCommon.kUnderlineBorder(),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return AppStrings.kErrorMSgGender;
                          } else {
                            return null;
                          }
                        }),
                  )*/
                  // Padding(
                  //   padding: const EdgeInsets.only(
                  //     left: 25,
                  //     right: 25,
                  //   ),
                  //   child: TextFormField(
                  //       style: AppCommon.kEditTextStyle(),
                  //       decoration: InputDecoration(
                  //         hintText: AppStrings.kDOB,
                  //         border: AppCommon.kUnderlineBorder(),
                  //         enabledBorder: AppCommon.kUnderlineBorder(),
                  //         focusedBorder: AppCommon.kUnderlineBorder(),
                  //       ),
                  //       validator: (value) {
                  //         if (value!.isEmpty) {
                  //           return AppStrings.kErrorMSgDob;
                  //         } else {
                  //           return null;
                  //         }
                  //       }),
                  // ),
                  Padding(
                    padding: const EdgeInsets.only(
                      right: 25,
                      top: 10,
                    ),
                    child: Theme(
                      data: ThemeData(
                        unselectedWidgetColor: Colors.white,
                      ),
                      child: CheckboxListTile(
                        value: isAgree,
                        controlAffinity: ListTileControlAffinity.leading,
                        checkColor: Colors.white,
                        title: RichText(
                          text: TextSpan(
                            text: AppStrings.kAgreeText,
                            style: GoogleFonts.roboto(
                              color: Colors.white,
                            ),
                            children: <TextSpan>[
                              TextSpan(
                                text: AppStrings.kTermConditions,
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () => _launchURL(),
                                style: GoogleFonts.roboto(
                                  color: Color(0xff46B7CD),
                                ),
                              ),
                            ],
                          ),
                        ),
                        onChanged: (bool? value) {
                          setState(() {
                            isAgree = !isAgree;
                          });
                        },
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  GestureDetector(
                    onTap: () {
                      FocusScope.of(context).requestFocus(FocusNode());
                      if (_formKey.currentState!.validate()) {
                        _formKey.currentState!.save();
                        if (isAgree) {
                          progressDialog.show();
                          _registrationRequest(
                              fName: _controllerFirstName.text,
                              lName: _controllerLastName.text,
                              mobile: _controllerMobile.text,
                              email: _controllerEmail.text,
                              password: _controllerPassword.text,
                              image: '',
                              gender: _genderValue.toString(),
                              dob: '',
                              deviceToken: _token,
                              deviceType: _deviceType,
                              language: 'en');
                        } else {
                          AppCommon.kErrorDialog(
                              context: context,
                              errorMsg: 'Please Agree Terms & Condition');
                        }
                      }
                    },
                    // onTap: () => Navigator.of(context).pushReplacementNamed(
                    //   AppStrings.kForgotPasswordScreen,
                    // ),
                    child: AppCommon.kCommonButton(
                      btnText: AppStrings.kSignUp,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: const EdgeInsets.only(
                        right: 25,
                        top: 10,
                      ),
                      child: Center(
                        child: RichText(
                          text: TextSpan(
                            text: 'Already have an account? ',
                            style: GoogleFonts.roboto(
                              color: Colors.white,
                            ),
                            children: <TextSpan>[
                              TextSpan(
                                text: 'Sign In',
                                recognizer: TapGestureRecognizer()
                                  ..onTap =
                                      () => Navigator.of(context).pushNamed(
                                            AppStrings.kLoginRoute,
                                          ),
                                style: GoogleFonts.roboto(
                                  color: AppColors.kBlue,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _registrationRequest(
      {required String fName,
      required String lName,
      required String mobile,
      required String email,
      required String password,
      required String image,
      required String gender,
      required String dob,
      required String deviceToken,
      required String deviceType,
      required String language}) async {
    print('user role is ${widget.userRole} and gender is $gender');

    String jsonParms = jsonEncode(<String, dynamic>{
      'fname': fName,
      'lname': lName,
      'mobile': int.parse(mobile),
      'email': email,
      'password': password,
      'confirm_password': password,
      // 'image':
      //     'https://blog.logrocket.com/wp-content/uploads/2021/05/intro-dart-flutter-feature.png',
      'gender': gender,
      'dob': '',
      'device_token': deviceToken,
      'device_type': deviceType,
      'language': language,
      'user_role': widget.userRole,
    });

    print('json parms data is $jsonParms');

    try {
      final http.Response response = await http.post(
          Uri.parse(AppStrings.kSignUpUrl),
          headers: <String, String>{
            AppStrings.kHeaderType: AppStrings.kHeaderValue
          },
          body: jsonParms);

      var _responseData = jsonDecode(response.body);
      print('response from service ${response.body}');

      Navigator.of(context).pop();

      if (response.statusCode == 200) {
        if (_responseData['status'] == 'success') {
          var _usersData = _responseData['data'];
          print('use data ${_usersData.toString()}');
          await preferences.setString(AppStrings.kIsLogin, 'true');
          await preferences.setString(AppStrings.kPrefUserIdKey,
              _usersData[0][AppStrings.kPrefUserIdKey].toString());
          await preferences.setString(AppStrings.kPrefFirstNameKey,
              _usersData[0][AppStrings.kPrefFirstNameKey]);
          await preferences.setString(AppStrings.kPrefLastNameKey,
              _usersData[0][AppStrings.kPrefLastNameKey]);
          await preferences.setString(
              AppStrings.kPrefUserRoleKey, widget.userRole);
          AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            title: AppStrings.kSuccess,
            desc: 'Register Successfully',
            btnCancelOnPress: () {
              Navigator.of(context).pop();
            },
            btnOkOnPress: () {
              if (widget.userRole == '1') {
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => UserDashboard(
                              userId: _usersData[0][AppStrings.kPrefUserIdKey]
                                  .toString(),
                              firstRegister: true,
                            )));
              } else {
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => TrainerDashboard()));
              }
            },
          )..show();
        } else {
          AppCommon.kErrorDialog(
              context: context,
              errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
        }
      } else {
        AppCommon.kErrorDialog(
            context: context,
            errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
      }
    } catch (exception) {
      print('exception is $exception');
      Navigator.of(context).pop();
      AppCommon.kErrorDialog(
          context: context, errorMsg: 'Some error occur.html response getting');
    }
  }

  void _launchURL() async {
    await canLaunch(_url) ? await launch(_url) : throw 'Could not launch $_url';
  }
}
