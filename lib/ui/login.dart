import 'dart:convert';
import 'dart:io' show Platform;
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:device_info/device_info.dart';
import 'package:ficiety/trainer_ui/trainer_dashboard.dart';
import 'package:ficiety/ui/register.dart';
import 'package:ficiety/ui/slider_screen.dart';
import 'package:ficiety/ui/user_dashboard.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:ficiety/utils/progressdialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  late ProgressDialog progressDialog;
  final _formKey = GlobalKey<FormState>();
  TextEditingController _controllerUserName = TextEditingController();
  TextEditingController _controllerPassword = TextEditingController();
  FocusNode _focusNodeUserName = FocusNode();
  FocusNode _focusNodePassword = FocusNode();

  late String _token;
  late String _deviceType;
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  late SharedPreferences preferences;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    progressDialog = new ProgressDialog(context, ProgressDialogType.Normal);
    progressDialog.setMessage('Logging in...');

    SharedPreferences.getInstance().then((value) => preferences = value);
    getDeviceInformation();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    print('height of screen $height');
    print('test for gitlab');
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.fill,
            image: AssetImage(
              AppStrings.kImgLoginBg,
            ),
          ),
        ),
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Theme(
              data: ThemeData(
                  primaryColor: AppColors.kOffGrey,
                  accentColor: AppColors.kOffGrey,
                  dividerColor: AppColors.kOffGrey,
                  accentTextTheme: TextTheme(
                    bodyText1: TextStyle(
                      color: AppColors.kOffGrey,
                    ),
                  ),
                  hintColor: AppColors.kOffGrey,
                  primaryTextTheme: TextTheme(
                    bodyText1: TextStyle(
                      color: AppColors.kOffGrey,
                    ),
                  )),
              child: Column(
                children: [
                  GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: () => Navigator.of(context).pop(),
                    child: SafeArea(
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Icon(
                          Icons.arrow_back,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: height / 7.7,
                  ),
                  Image.asset(
                    AppStrings.kImgLogo,
                    fit: BoxFit.fill,
                    width: 300.5,
                    height: 190.5,
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 25,
                      right: 25,
                    ),
                    child: TextFormField(
                      controller: _controllerUserName,
                      focusNode: _focusNodeUserName,
                      keyboardType: TextInputType.emailAddress,
                      textInputAction: TextInputAction.next,
                      style: TextStyle(
                        color: AppColors.kWhite,
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Please enter email';
                        } else if (!(value.contains('@'))) {
                          return 'Please enter valid email';
                        } else {
                          return null;
                        }
                      },
                      onFieldSubmitted: (_){
                        AppCommon.kChangeFocusNode(currentFocusNode: _focusNodeUserName, nextFocusNode: _focusNodePassword, context: context);
                      },
                      decoration: InputDecoration(
                        hintText: AppStrings.kEmail,
                        border: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: AppColors.kOffGrey,
                          ),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.kOffGrey),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.kOffGrey),
                        ),
                        prefixIconConstraints: BoxConstraints(maxHeight: 30),
                        prefixIcon: Padding(
                          padding: const EdgeInsets.only(
                            right: 15,
                          ),
                          child: FaIcon(
                            FontAwesomeIcons.envelope,
                            size: 15,
                            color: AppColors.kOffGrey,
                          ),
                        ),
                        prefixStyle: TextStyle(
                            color: AppColors.kOffGrey, letterSpacing: 0.0),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 25,
                      right: 25,
                    ),
                    child: TextFormField(
                      controller: _controllerPassword,
                      focusNode: _focusNodePassword,
                      textInputAction: TextInputAction.done,
                      style: TextStyle(
                        color: AppColors.kWhite,
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Please enter password';
                        } else {
                          return null;
                        }
                      },
                      obscureText: true,
                      decoration: InputDecoration(
                        hintText: AppStrings.kPassword,
                        border: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: AppColors.kOffGrey,
                          ),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.kOffGrey),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.kOffGrey),
                        ),
                        prefixIconConstraints: BoxConstraints(maxHeight: 30),
                        prefixIcon: Padding(
                          padding: const EdgeInsets.only(
                            right: 15,
                          ),
                          child: Icon(
                            Icons.lock,
                            size: 20,
                            color: AppColors.kOffGrey,
                          ),
                        ),
                        prefixStyle: TextStyle(
                            color: AppColors.kOffGrey, letterSpacing: 0.0),
                      ),
                    ),
                  ),
                  GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: () {
                      Navigator.pushNamed(
                          context, AppStrings.kForgotPasswordRoute);
                      // Navigator.pushNamed(
                      //     context, AppStrings.kForgotPasswordRoute);
                      // Navigator.pushNamed(
                      //     context, AppStrings.kTrainerDashboardRouter);
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(
                        right: 25,
                        top: 10,
                      ),
                      child: Align(
                          alignment: Alignment.topRight,
                          child: Text(
                            '${AppStrings.kForgotPassword}?',
                            style: GoogleFonts.roboto(
                              color: Colors.white,
                            ),
                          )),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: () {
                      if (_formKey.currentState!.validate()) {
                        _formKey.currentState!.save();
                        progressDialog.show();
                        _loginRequest(
                            username: _controllerUserName.text,
                            password: _controllerPassword.text);
                      }

                      // if(_controllerUserName.text=='user@gmail.com'){
                      //   Navigator.of(context).pushReplacementNamed(
                      //     AppStrings.kUserDashboardRoute,
                      //   );
                      // }else if(_controllerUserName.text=='trainer@gmail.com'){
                      //   _openDialog();
                      // }else{
                      //   ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Please enter valid login details',style: GoogleFonts.roboto(color: Colors.red,fontWeight: FontWeight.w900,),),),);
                      // }
                    },
                    child: AppCommon.kCommonButton(
                      btnText: AppStrings.kLoginIn,
                    ),
                  ),
                  Spacer(),
                  GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: () {
                      Navigator.pushNamed(
                          context, AppStrings.kForgotPasswordRoute);
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(
                        right: 25,
                        top: 10,
                      ),
                      child: RichText(
                        text: TextSpan(
                          text: AppStrings.kDontHaveAccount,
                          style: GoogleFonts.roboto(
                            color: Colors.white,
                          ),
                          children: <TextSpan>[
                            TextSpan(
                              text: AppStrings.kSignUp,
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  AwesomeDialog(
                                    context: context,
                                    dialogType: DialogType.INFO,
                                    animType: AnimType.BOTTOMSLIDE,
                                    title: 'Sign Up',
                                    desc: 'Who would you like to sign up as?',
                                    btnCancelText: 'Trainer',
                                    btnOkText: 'Client',
                                    btnCancelOnPress: () {
                                      Navigator.of(context).push(MaterialPageRoute(builder: (context)=>Register(userRole: '2',)));
                                    },
                                    btnOkOnPress: () {
                                      Navigator.of(context).push(MaterialPageRoute(builder: (context)=>Register(userRole: '1',)));
                                    },
                                  )..show();
                                },
                              style: GoogleFonts.roboto(
                                color: Color(0xff46B7CD),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _openDialog() {
    return showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, setState) {
              return Dialog(
                insetPadding: EdgeInsets.symmetric(
                  horizontal: 2.0,
                  vertical: 2.0,
                ),
                // title: Text(
                //   AppStrings.kSearchForTrainer,style: GoogleFonts.roboto(
                //   color: AppColors.kGreen,fontWeight: FontWeight.w900,
                // ),
                // ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                    10.0,
                  ),
                ),
                child: Container(
                  width: 380,
                  height: 500,
                  padding: EdgeInsets.all(
                    20.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Container(
                          height: 150,
                          width: 150,
                          decoration: BoxDecoration(
                            color: Color(0xff074748),
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                'Trainer',
                                style: GoogleFonts.roboto(
                                  color: Colors.white,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Text(
                                '£ 8.99',
                                style: GoogleFonts.roboto(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                'Per Month',
                                style: GoogleFonts.roboto(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        'Trainer',
                        style: GoogleFonts.roboto(
                          color: Color(0xff026f72),
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          AppCommon.kIconTrainerSearchTextView(
                              fontName: AppStrings.kFontAwsRegular,
                              content: ''),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Text(
                              'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                              softWrap: true,
                              overflow: TextOverflow.visible,
                              style: GoogleFonts.roboto(
                                color: Color(0xff333333),
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          AppCommon.kIconTrainerSearchTextView(
                              fontName: AppStrings.kFontAwsRegular,
                              content: ''),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Text(
                              'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. ',
                              softWrap: true,
                              overflow: TextOverflow.visible,
                              style: GoogleFonts.roboto(
                                color: Color(0xff333333),
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          AppCommon.kIconTrainerSearchTextView(
                              fontName: AppStrings.kFontAwsRegular,
                              content: ''),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Text(
                              'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.',
                              softWrap: true,
                              overflow: TextOverflow.visible,
                              style: GoogleFonts.roboto(
                                color: Color(0xff333333),
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Center(
                        child: Container(
                          width: 130,
                          height: 40,
                          child: ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).pushReplacementNamed(
                                AppStrings.kTrainerDashboardRouter,
                              );
                            },
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        AppColors.kGreen)),
                            child: Text(
                              'GO PREMIUM',
                              style: GoogleFonts.roboto(
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        });
  }

  Future<void> _loginRequest(
      {required String username, required String password}) async {
    print('user name is $username and password is $password');

    try{
      final http.Response response = await http.post(
          Uri.parse(AppStrings.kSignInUrl),
          headers: <String, String>{
            AppStrings.kHeaderType: AppStrings.kHeaderValue
          },
          body: jsonEncode(<String, String>{
            'email': username,
            'password': password,
            'device_token': _token,
            'device_type': _deviceType,
            'language': 'en',
          }));

      var _responseData = jsonDecode(response.body);
      print('response from service ${response.body}');


      progressDialog.hide();
      if (response.statusCode == 200) {
        if (_responseData['status'] == 'success') {

          try{
            var _usersData = _responseData['data'];
            print('use data ${_usersData.toString()}');
            print('print 1');
            await preferences.setString(AppStrings.kIsLogin, 'true');
            print('print 2');
            await preferences.setString(
                AppStrings.kPrefUserIdKey, _usersData[0][AppStrings.kPrefUserIdKey].toString());
            print('print 3');
            await preferences.setString(AppStrings.kPrefFirstNameKey,
                _usersData[0][AppStrings.kPrefFirstNameKey]);
            print('print 4');
            await preferences.setString(AppStrings.kPrefLastNameKey,
                _usersData[0][AppStrings.kPrefLastNameKey]);
            print('print 5');
            await preferences.setString(AppStrings.kPrefUserRoleKey,
                _usersData[0][AppStrings.kPrefUserRoleKey]);
            print('print 6');
            if(_usersData[0][AppStrings.kPrefUserRoleKey]=='1'){
              print('print user role 1 call');
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => UserDashboard(
                          userId: _usersData[0][AppStrings.kPrefUserIdKey].toString())));
            }else{
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => TrainerDashboard()));
            }
          }catch (exception){
            print('exception of redirection $exception');
          }

          // Navigator.pushReplacement(
          //     context,
          //     MaterialPageRoute(
          //         builder: (context) => UserDashboard(
          //             userId: _usersData[0][AppStrings.kPrefUserIdKey].toString())));
        } else {
          AppCommon.kErrorDialog(
              context: context,
              errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
        }
      } else {
        AppCommon.kErrorDialog(
            context: context,
            errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
      }
    }catch(exception){
      print('exception is $exception');
      Navigator.of(context).pop();
      AppCommon.kErrorDialog(
          context: context,
          errorMsg: 'Some error occur.html response getting');
    }



    // progressDialog.hide();


  }

  void getDeviceInformation() async {
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      print('Running on ${androidInfo.model}');
      _deviceType = 'android';
      _token = androidInfo.androidId;
      print('token is $_token');
      await preferences.setString(AppStrings.kPrefDeviceToken, _token);
      await preferences.setString(AppStrings.kPrefDeviceType, _deviceType);
    } else {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      print('Running on ${iosInfo.utsname.machine}');
      _deviceType = 'ios';
      _token = iosInfo.identifierForVendor;
      await preferences.setString(AppStrings.kPrefDeviceToken, _token);
      await preferences.setString(AppStrings.kPrefDeviceType, _deviceType);
      print('token is _token');
    }
  }
}
