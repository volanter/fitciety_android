//@dart=2.9

class WaistHipsModel {
  String message;
  String status;
  Data data;

  WaistHipsModel({this.message, this.status, this.data});

  WaistHipsModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String userId;
  String currentWeight;
  String currentHeight;
  List<String> tips;
  List<WaistHipsData> waistHipsData;

  Data(
      {this.userId,
        this.currentWeight,
        this.currentHeight,
        this.tips,
        this.waistHipsData});

  Data.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    currentWeight = json['current_weight'];
    currentHeight = json['current_height'];
    tips = json['tips'].cast<String>();
    if (json['waist_hips_data'] != null) {
      waistHipsData = new List<WaistHipsData>();
      json['waist_hips_data'].forEach((v) {
        waistHipsData.add(new WaistHipsData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['current_weight'] = this.currentWeight;
    data['current_height'] = this.currentHeight;
    data['tips'] = this.tips;
    if (this.waistHipsData != null) {
      data['waist_hips_data'] =
          this.waistHipsData.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class WaistHipsData {
  String date;
  String waist;
  String hips;

  WaistHipsData({this.date, this.waist, this.hips});

  WaistHipsData.fromJson(Map<String, dynamic> json) {
    date = json['date'];
    waist = json['waist'];
    hips = json['hips'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date;
    data['waist'] = this.waist;
    data['hips'] = this.hips;
    return data;
  }
}