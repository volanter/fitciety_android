//@dart=2.9
class SessionListModel {
  String message;
  String status;
  List<Data> data;

  SessionListModel({this.message, this.status, this.data});

  SessionListModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int trainerId;
  String trainerName;
  String email;
  String mobile;
  String image;
  String companyName;
  List<MyBooking> myBooking;
  List<Completed> completed;

  Data(
      {this.trainerId,
        this.trainerName,
        this.email,
        this.mobile,
        this.image,
        this.companyName,
        this.myBooking,
        this.completed});

  Data.fromJson(Map<String, dynamic> json) {
    trainerId = json['trainer_id'];
    trainerName = json['trainer_name'];
    email = json['email'];
    mobile = json['mobile'];
    image = json['image'];
    companyName = json['company_name'];
    if (json['my_booking'] != null) {
      myBooking = new List<MyBooking>();
      json['my_booking'].forEach((v) {
        myBooking.add(new MyBooking.fromJson(v));
      });
    }
    if (json['completed'] != null) {
      completed = new List<Completed>();
      json['completed'].forEach((v) {
        completed.add(new Completed.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['trainer_id'] = this.trainerId;
    data['trainer_name'] = this.trainerName;
    data['email'] = this.email;
    data['mobile'] = this.mobile;
    data['image'] = this.image;
    data['company_name'] = this.companyName;
    if (this.myBooking != null) {
      data['my_booking'] = this.myBooking.map((v) => v.toJson()).toList();
    }
    if (this.completed != null) {
      data['completed'] = this.completed.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Completed {
  int sessionId;
  String fullname;
  String icon;
  String date;
  String time;
  String category;
  String sessionLeft;
  String paymentStatus;

  Completed(
      {this.sessionId,
        this.fullname,
        this.icon,
        this.date,
        this.time,
        this.category,
        this.sessionLeft,
        this.paymentStatus});

  Completed.fromJson(Map<String, dynamic> json) {
    sessionId = json['session_id'];
    fullname = json['fullname'];
    icon = json['icon'];
    date = json['date'];
    time = json['time'];
    category = json['category'];
    sessionLeft = json['session_left'];
    paymentStatus = json['payment_status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['session_id'] = this.sessionId;
    data['fullname'] = this.fullname;
    data['icon'] = this.icon;
    data['date'] = this.date;
    data['time'] = this.time;
    data['category'] = this.category;
    data['session_left'] = this.sessionLeft;
    data['payment_status'] = this.paymentStatus;
    return data;
  }
}

class MyBooking {
  int sessionId;
  String fullname;
  String icon;
  String date;
  String time;
  String category;
  String sessionLeft;
  String paymentStatus;

  MyBooking(
      {this.sessionId,
        this.fullname,
        this.icon,
        this.date,
        this.time,
        this.category,
        this.sessionLeft,
        this.paymentStatus});

  MyBooking.fromJson(Map<String, dynamic> json) {
    sessionId = json['session_id'];
    fullname = json['fullname'];
    icon = json['icon'];
    date = json['date'];
    time = json['time'];
    category = json['category'];
    sessionLeft = json['session_left'];
    paymentStatus = json['payment_status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['session_id'] = this.sessionId;
    data['fullname'] = this.fullname;
    data['icon'] = this.icon;
    data['date'] = this.date;
    data['time'] = this.time;
    data['category'] = this.category;
    data['session_left'] = this.sessionLeft;
    data['payment_status'] = this.paymentStatus;
    return data;
  }
}