// @dart=2.9
class NotificationModel {
  String message;
  String status;
  List<Data> data;

  NotificationModel({this.message, this.status, this.data});

  NotificationModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String id;
  String type;
  String title;
  String message;
  String userImage;
  String date;
  String time;

  Data(
      {this.id,
        this.type,
        this.title,
        this.message,
        this.userImage,
        this.date,
        this.time});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    title = json['title'];
    message = json['message'];
    userImage = json['user_image'];
    date = json['date'];
    time = json['time'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['type'] = this.type;
    data['title'] = this.title;
    data['message'] = this.message;
    data['user_image'] = this.userImage;
    data['date'] = this.date;
    data['time'] = this.time;
    return data;
  }
}