// @dart=2.9
class TrainerDetailsModel {
  String message;
  String status;
  List<Data> data;

  TrainerDetailsModel({this.message, this.status, this.data});

  TrainerDetailsModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int userId;
  String trainerName;
  String email;
  String mobile;
  String profilePic;
  String backgroundImage;
  String address;
  String description;
  List<Category> category;
  String isRequest;

  Data(
      {this.userId,
        this.trainerName,
        this.email,
        this.mobile,
        this.profilePic,
        this.backgroundImage,
        this.address,
        this.description,
        this.category,
        this.isRequest});

  Data.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    trainerName = json['trainer_name'];
    email = json['email'];
    mobile = json['mobile'];
    profilePic = json['profile_pic'];
    backgroundImage = json['background_image'];
    address = json['address'];
    description = json['description'];
    if (json['category'] != null) {
      category = new List<Category>();
      json['category'].forEach((v) {
        category.add(new Category.fromJson(v));
      });
    }
    isRequest = json['is_request'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['trainer_name'] = this.trainerName;
    data['email'] = this.email;
    data['mobile'] = this.mobile;
    data['profile_pic'] = this.profilePic;
    data['background_image'] = this.backgroundImage;
    data['address'] = this.address;
    data['description'] = this.description;
    if (this.category != null) {
      data['category'] = this.category.map((v) => v.toJson()).toList();
    }
    data['is_request'] = this.isRequest;
    return data;
  }
}

class Category {
  String icon;
  String categoryName;
  String sessionName;
  String duration;
  String location;
  String sessionLat;
  String sessionLng;
  int categoryId;
  String sessionPrice;
  List<Sessions> sessions;

  Category(
      {this.icon,
        this.categoryName,
        this.sessionName,
        this.duration,
        this.location,
        this.sessionLat,
        this.sessionLng,
        this.categoryId,
        this.sessionPrice,
        this.sessions});

  Category.fromJson(Map<String, dynamic> json) {
    icon = json['icon'];
    categoryName = json['category_name'];
    sessionName = json['session_name'];
    duration = json['duration'];
    location = json['location'];
    sessionLat = json['session_lat'];
    sessionLng = json['session_lng'];
    categoryId = json['category_id'];
    sessionPrice = json['session_price'];
    if (json['sessions'] != null) {
      sessions = new List<Sessions>();
      json['sessions'].forEach((v) {
        sessions.add(new Sessions.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['icon'] = this.icon;
    data['category_name'] = this.categoryName;
    data['session_name'] = this.sessionName;
    data['duration'] = this.duration;
    data['location'] = this.location;
    data['session_lat'] = this.sessionLat;
    data['session_lng'] = this.sessionLng;
    data['category_id'] = this.categoryId;
    data['session_price'] = this.sessionPrice;
    if (this.sessions != null) {
      data['sessions'] = this.sessions.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Sessions {
  String blockOf2;
  String blockOf3;
  String blockOf4;

  Sessions({this.blockOf2, this.blockOf3, this.blockOf4});

  Sessions.fromJson(Map<String, dynamic> json) {
    blockOf2 = json['Block Of 2'];
    blockOf3 = json['Block Of 3'];
    blockOf4 = json['Block of 4'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Block Of 2'] = this.blockOf2;
    data['Block Of 3'] = this.blockOf3;
    data['Block of 4'] = this.blockOf4;
    return data;
  }
}