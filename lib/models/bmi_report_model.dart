//@dart=2.9
class BMIReportModel {
  String message;
  String status;
  Data data;

  BMIReportModel({this.message, this.status, this.data});

  BMIReportModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String userId;
  String currentWeight;
  String currentHeight;
  String bmi;
  List<String> tips;
  List<BmiData> bmiData;

  Data(
      {this.userId,
        this.currentWeight,
        this.currentHeight,
        this.bmi,
        this.tips,
        this.bmiData});

  Data.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    currentWeight = json['current_weight'];
    currentHeight = json['current_height'];
    bmi = json['bmi'];
    tips = json['tips'].cast<String>();
    if (json['bmi_data'] != null) {
      bmiData = new List<BmiData>();
      json['bmi_data'].forEach((v) {
        bmiData.add(new BmiData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['current_weight'] = this.currentWeight;
    data['current_height'] = this.currentHeight;
    data['bmi'] = this.bmi;
    data['tips'] = this.tips;
    if (this.bmiData != null) {
      data['bmi_data'] = this.bmiData.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class BmiData {
  String date;
  String weight;
  String height;
  String bmi;

  BmiData({this.date, this.weight, this.height, this.bmi});

  BmiData.fromJson(Map<String, dynamic> json) {
    date = json['date'];
    weight = json['weight'];
    height = json['height'];
    bmi = json['bmi'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date;
    data['weight'] = this.weight;
    data['height'] = this.height;
    data['bmi'] = this.bmi;
    return data;
  }
}