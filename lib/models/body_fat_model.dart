//@dart=2.9
class BodyFatModel {
  String message;
  String status;
  Data data;

  BodyFatModel({this.message, this.status, this.data});

  BodyFatModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String userId;
  String currentWeight;
  String currentHeight;
  String bfp;
  List<String> tips;
  List<BfpData> bfpData;

  Data(
      {this.userId,
        this.currentWeight,
        this.currentHeight,
        this.bfp,
        this.tips,
        this.bfpData});

  Data.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    currentWeight = json['current_weight'];
    currentHeight = json['current_height'];
    bfp = json['bfp'];
    tips = json['tips'].cast<String>();
    if (json['bfp_data'] != null) {
      bfpData = new List<BfpData>();
      json['bfp_data'].forEach((v) {
        bfpData.add(new BfpData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['current_weight'] = this.currentWeight;
    data['current_height'] = this.currentHeight;
    data['bfp'] = this.bfp;
    data['tips'] = this.tips;
    if (this.bfpData != null) {
      data['bfp_data'] = this.bfpData.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class BfpData {
  String date;
  String weight;
  String height;
  String bfp;

  BfpData({this.date, this.weight, this.height, this.bfp});

  BfpData.fromJson(Map<String, dynamic> json) {
    date = json['date'];
    weight = json['weight'];
    height = json['height'];
    bfp = json['bfp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date;
    data['weight'] = this.weight;
    data['height'] = this.height;
    data['bfp'] = this.bfp;
    return data;
  }
}