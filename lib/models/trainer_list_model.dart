// @dart=2.9
class TrainerListModel {
  String message;
  String status;
  List<Data> data;

  TrainerListModel({this.message, this.status, this.data});

  TrainerListModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String fname;
  String lname;
  String image;
  String about;
  List<String> areaOfExpertise;
  String address;
  String background_image;

  Data(
      {this.fname,
        this.lname,
        this.image,
        this.about,
        this.areaOfExpertise,
        this.address,
      this.background_image});

  Data.fromJson(Map<String, dynamic> json) {
    fname = json['fname'];
    lname = json['lname'];
    image = json['image'];
    about = json['about'];
    areaOfExpertise = json['area_of_expertise'].cast<String>();
    address = json['address'];
    background_image = json['background_image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fname'] = this.fname;
    data['lname'] = this.lname;
    data['image'] = this.image;
    data['about'] = this.about;
    data['area_of_expertise'] = this.areaOfExpertise;
    data['address'] = this.address;
    data['background_image'] = this.background_image;
    return data;
  }
}