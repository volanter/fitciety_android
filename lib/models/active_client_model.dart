// // @dart=2.9
// class ClientListModel {
//   String message;
//   String status;
//   List<Data> data;
//
//   ClientListModel({this.message, this.status, this.data});
//
//   ClientListModel.fromJson(Map<String, dynamic> json) {
//     message = json['message'];
//     status = json['status'];
//     if (json['data'] != null) {
//       data = new List<Data>();
//       json['data'].forEach((v) {
//         data.add(new Data.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['message'] = this.message;
//     data['status'] = this.status;
//     if (this.data != null) {
//       data['data'] = this.data.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class Data {
//   List<Active> active;
//   List<All> all;
//
//   Data({this.active, this.all});
//
//   Data.fromJson(Map<String, dynamic> json) {
//     if (json['active'] != null) {
//       active = new List<Active>();
//       json['active'].forEach((v) {
//         active.add(new Active.fromJson(v));
//       });
//     }
//     if (json['all'] != null) {
//       all = new List<All>();
//       json['all'].forEach((v) {
//         all.add(new All.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     if (this.active != null) {
//       data['active'] = this.active.map((v) => v.toJson()).toList();
//     }
//     if (this.all != null) {
//       data['all'] = this.all.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class Active {
//   String userId;
//   String userName;
//   String userImage;
//   String about;
//   String location;
//   String mobileNumber;
//
//   Active(
//       {this.userId,
//         this.userName,
//         this.userImage,
//         this.about,
//         this.location,
//         this.mobileNumber});
//
//   Active.fromJson(Map<String, dynamic> json) {
//     userId = json['user_id'];
//     userName = json['user_name'];
//     userImage = json['user_image'];
//     about = json['about'];
//     location = json['location'];
//     mobileNumber = json['mobile_number'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['user_id'] = this.userId;
//     data['user_name'] = this.userName;
//     data['user_image'] = this.userImage;
//     data['about'] = this.about;
//     data['location'] = this.location;
//     data['mobile_number'] = this.mobileNumber;
//     return data;
//   }
// }