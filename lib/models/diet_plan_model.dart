//@dart=2.9

class DietPlanModel {
  String message;
  String status;
  List<Data> data;

  DietPlanModel({this.message, this.status, this.data});

  DietPlanModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String userId;
  String date;
  List<DietData> dietData;

  Data({this.userId, this.date, this.dietData});

  Data.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    date = json['date'];
    if (json['diet_data'] != null) {
      dietData = new List<DietData>();
      json['diet_data'].forEach((v) {
        dietData.add(new DietData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['date'] = this.date;
    if (this.dietData != null) {
      data['diet_data'] = this.dietData.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DietData {
  String day;
  String time;
  String type;
  String description;

  DietData({this.day, this.time, this.type, this.description});

  DietData.fromJson(Map<String, dynamic> json) {
    day = json['day'];
    time = json['time'];
    type = json['type'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['day'] = this.day;
    data['time'] = this.time;
    data['type'] = this.type;
    data['description'] = this.description;
    return data;
  }
}