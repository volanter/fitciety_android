// @dart=2.9
class ChatListModel {
  String message;
  String status;
  List<Data> data;

  ChatListModel({this.message, this.status, this.data});

  ChatListModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String chatId;
  String senderId;
  String recipientId;
  String message;
  String type;
  String recipientName;
  String recipientImage;
  String unreadCount;
  String conversationId;
  String date;
  String time;

  Data(
      {this.chatId,
        this.senderId,
        this.recipientId,
        this.message,
        this.type,
        this.recipientName,
        this.recipientImage,
        this.unreadCount,
        this.conversationId,
        this.date,
        this.time});

  Data.fromJson(Map<String, dynamic> json) {
    chatId = json['chat_id'];
    senderId = json['sender_id'];
    recipientId = json['recipient_id'];
    message = json['message'];
    type = json['type'];
    recipientName = json['recipient_name'];
    recipientImage = json['recipient_image'];
    unreadCount = json['unread_count'];
    conversationId = json['conversation_id'];
    date = json['date'];
    time = json['time'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['chat_id'] = this.chatId;
    data['sender_id'] = this.senderId;
    data['recipient_id'] = this.recipientId;
    data['message'] = this.message;
    data['type'] = this.type;
    data['recipient_name'] = this.recipientName;
    data['recipient_image'] = this.recipientImage;
    data['unread_count'] = this.unreadCount;
    data['conversation_id'] = this.conversationId;
    data['date'] = this.date;
    data['time'] = this.time;
    return data;
  }
}