//@dart=2.9
class AuthorisationRequestModel {
  String sTATUS;
  String message;
  List<Response> response;

  AuthorisationRequestModel({this.sTATUS, this.message, this.response});

  AuthorisationRequestModel.fromJson(Map<String, dynamic> json) {
    sTATUS = json['STATUS'];
    message = json['message'];
    if (json['response'] != null) {
      response = new List<Response>();
      json['response'].forEach((v) {
        response.add(new Response.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['STATUS'] = this.sTATUS;
    data['message'] = this.message;
    if (this.response != null) {
      data['response'] = this.response.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Response {
  int userId;
  String fname;
  String lname;
  String email;
  String password;
  String confirmPassword;
  String mobile;
  String image;
  String coverImage;
  String dob;
  String gender;
  String userRole;
  Null address;
  Null about;
  Null areaOfExpertise;
  String createdAt;
  String updatedAt;
  String message;

  Response(
      {this.userId,
        this.fname,
        this.lname,
        this.email,
        this.password,
        this.confirmPassword,
        this.mobile,
        this.image,
        this.coverImage,
        this.dob,
        this.gender,
        this.userRole,
        this.address,
        this.about,
        this.areaOfExpertise,
        this.createdAt,
        this.updatedAt,
        this.message});

  Response.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    fname = json['fname'];
    lname = json['lname'];
    email = json['email'];
    password = json['password'];
    confirmPassword = json['confirm_password'];
    mobile = json['mobile'];
    image = json['image'];
    coverImage = json['cover_image'];
    dob = json['dob'];
    gender = json['gender'];
    userRole = json['user_role'];
    address = json['address'];
    about = json['about'];
    areaOfExpertise = json['area_of_expertise'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['fname'] = this.fname;
    data['lname'] = this.lname;
    data['email'] = this.email;
    data['password'] = this.password;
    data['confirm_password'] = this.confirmPassword;
    data['mobile'] = this.mobile;
    data['image'] = this.image;
    data['cover_image'] = this.coverImage;
    data['dob'] = this.dob;
    data['gender'] = this.gender;
    data['user_role'] = this.userRole;
    data['address'] = this.address;
    data['about'] = this.about;
    data['area_of_expertise'] = this.areaOfExpertise;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['message'] = this.message;
    return data;
  }
}