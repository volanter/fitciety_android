// @dart=2.9
class ExerciseDetailsModel {
  String message;
  String status;
  List<Data> data;

  ExerciseDetailsModel({this.message, this.status, this.data});

  ExerciseDetailsModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String categoryTitle;
  int workoutsCount;
  List<Workouts> workouts;

  Data({this.categoryTitle, this.workoutsCount, this.workouts});

  Data.fromJson(Map<String, dynamic> json) {
    categoryTitle = json['category_title'];
    workoutsCount = json['workouts_count'];
    if (json['workouts'] != null) {
      workouts = new List<Workouts>();
      json['workouts'].forEach((v) {
        workouts.add(new Workouts.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['category_title'] = this.categoryTitle;
    data['workouts_count'] = this.workoutsCount;
    if (this.workouts != null) {
      data['workouts'] = this.workouts.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Workouts {
  String userId;
  String programName;
  String programGif;
  String programDuration;
  String programDescription;

  Workouts(
      {this.userId,
        this.programName,
        this.programGif,
        this.programDuration,
        this.programDescription});

  Workouts.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    programName = json['program_name'];
    programGif = json['program_gif'];
    programDuration = json['program_duration'];
    programDescription = json['program_description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['program_name'] = this.programName;
    data['program_gif'] = this.programGif;
    data['program_duration'] = this.programDuration;
    data['program_description'] = this.programDescription;
    return data;
  }
}