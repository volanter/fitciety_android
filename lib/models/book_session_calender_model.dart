/// message : "Session Calender Data"
/// status : "success"
/// data : [{"available_slot":[{"available_time":"3AM-4AM"},{"available_time":"3AM-4AM"},{"available_time":"3AM-4AM"}],"calender_slot":[{"user_count":5,"time_slot":"3AM-4AM","user_details":[{"user_id":5,"user_name":5,"user_image":5,"user_category":5},{"user_id":5,"user_name":5,"user_image":5,"user_category":5},{"user_id":5,"user_name":5,"user_image":5,"user_category":5}]}]}]

class BookSessionCalenderModel {
  String? _message;
  String? _status;
  List<Data>? _data;

  String? get message => _message;
  String? get status => _status;
  List<Data>? get data => _data;

  BookSessionCalenderModel({
      String? message, 
      String? status, 
      List<Data>? data}){
    _message = message;
    _status = status;
    _data = data;
}

  BookSessionCalenderModel.fromJson(dynamic json) {
    _message = json["message"];
    _status = json["status"];
    if (json["data"] != null) {
      _data = [];
      json["data"].forEach((v) {
        _data?.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["message"] = _message;
    map["status"] = _status;
    if (_data != null) {
      map["data"] = _data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// available_slot : [{"available_time":"3AM-4AM"},{"available_time":"3AM-4AM"},{"available_time":"3AM-4AM"}]
/// calender_slot : [{"user_count":5,"time_slot":"3AM-4AM","user_details":[{"user_id":5,"user_name":5,"user_image":5,"user_category":5},{"user_id":5,"user_name":5,"user_image":5,"user_category":5},{"user_id":5,"user_name":5,"user_image":5,"user_category":5}]}]

class Data {
  List<Available_slot>? _availableSlot;
  List<Calender_slot>? _calenderSlot;

  List<Available_slot>? get availableSlot => _availableSlot;
  List<Calender_slot>? get calenderSlot => _calenderSlot;

  Data({
      List<Available_slot>? availableSlot, 
      List<Calender_slot>? calenderSlot}){
    _availableSlot = availableSlot;
    _calenderSlot = calenderSlot;
}

  Data.fromJson(dynamic json) {
    if (json["available_slot"] != null) {
      _availableSlot = [];
      json["available_slot"].forEach((v) {
        _availableSlot?.add(Available_slot.fromJson(v));
      });
    }
    if (json["calender_slot"] != null) {
      _calenderSlot = [];
      json["calender_slot"].forEach((v) {
        _calenderSlot?.add(Calender_slot.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_availableSlot != null) {
      map["available_slot"] = _availableSlot?.map((v) => v.toJson()).toList();
    }
    if (_calenderSlot != null) {
      map["calender_slot"] = _calenderSlot?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// user_count : 5
/// time_slot : "3AM-4AM"
/// user_details : [{"user_id":5,"user_name":5,"user_image":5,"user_category":5},{"user_id":5,"user_name":5,"user_image":5,"user_category":5},{"user_id":5,"user_name":5,"user_image":5,"user_category":5}]

class Calender_slot {
  int? _userCount;
  String? _timeSlot;
  List<User_details>? _userDetails;

  int? get userCount => _userCount;
  String? get timeSlot => _timeSlot;
  List<User_details>? get userDetails => _userDetails;

  Calender_slot({
      int? userCount, 
      String? timeSlot, 
      List<User_details>? userDetails}){
    _userCount = userCount;
    _timeSlot = timeSlot;
    _userDetails = userDetails;
}

  Calender_slot.fromJson(dynamic json) {
    _userCount = json["user_count"];
    _timeSlot = json["time_slot"];
    if (json["user_details"] != null) {
      _userDetails = [];
      json["user_details"].forEach((v) {
        _userDetails?.add(User_details.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["user_count"] = _userCount;
    map["time_slot"] = _timeSlot;
    if (_userDetails != null) {
      map["user_details"] = _userDetails?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// user_id : 5
/// user_name : 5
/// user_image : 5
/// user_category : 5

class User_details {
  int? _userId;
  int? _userName;
  int? _userImage;
  int? _userCategory;

  int? get userId => _userId;
  int? get userName => _userName;
  int? get userImage => _userImage;
  int? get userCategory => _userCategory;

  User_details({
      int? userId, 
      int? userName, 
      int? userImage, 
      int? userCategory}){
    _userId = userId;
    _userName = userName;
    _userImage = userImage;
    _userCategory = userCategory;
}

  User_details.fromJson(dynamic json) {
    _userId = json["user_id"];
    _userName = json["user_name"];
    _userImage = json["user_image"];
    _userCategory = json["user_category"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["user_id"] = _userId;
    map["user_name"] = _userName;
    map["user_image"] = _userImage;
    map["user_category"] = _userCategory;
    return map;
  }

}

/// available_time : "3AM-4AM"

class Available_slot {
  String? _availableTime;

  String? get availableTime => _availableTime;

  Available_slot({
      String? availableTime}){
    _availableTime = availableTime;
}

  Available_slot.fromJson(dynamic json) {
    _availableTime = json["available_time"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["available_time"] = _availableTime;
    return map;
  }

}