import 'package:ficiety/ui/chat_details.dart';
import 'package:flutter/material.dart';

class ChatMessage{
  String messageContent;
  MessageType messageType;
  String time;
  ChatMessage({required this.messageContent, required this.messageType,required this.time});
}