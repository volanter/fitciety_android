// @dart=2.9
class ViewSummaryModel {
  String message;
  String status;
  List<Data> data;

  ViewSummaryModel({this.message, this.status, this.data});

  ViewSummaryModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  int sessionId;
  int userId;
  String time;
  String createdAt;
  String updatedAt;
  String date;

  Data(
      {this.id,
        this.sessionId,
        this.userId,
        this.time,
        this.createdAt,
        this.updatedAt,
        this.date});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    sessionId = json['session_id'];
    userId = json['user_id'];
    time = json['time'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    date = json['date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['session_id'] = this.sessionId;
    data['user_id'] = this.userId;
    data['time'] = this.time;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['date'] = this.date;
    return data;
  }
}