// @dart=2.9
class PaymentHistoryModel {
  String message;
  String status;
  List<Data> data;

  PaymentHistoryModel({this.message, this.status, this.data});

  PaymentHistoryModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String walletBalance;
  List<WalletHistory> walletHistory;

  Data({this.walletBalance, this.walletHistory});

  Data.fromJson(Map<String, dynamic> json) {
    walletBalance = json['wallet_balance'];
    if (json['wallet_history'] != null) {
      walletHistory = new List<WalletHistory>();
      json['wallet_history'].forEach((v) {
        walletHistory.add(new WalletHistory.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['wallet_balance'] = this.walletBalance;
    if (this.walletHistory != null) {
      data['wallet_history'] =
          this.walletHistory.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class WalletHistory {
  String name;
  String time;
  String date;
  String type;
  String sesstionCount;
  String price;
  String hoursRate;

  WalletHistory(
      {this.name,
        this.time,
        this.date,
        this.type,
        this.sesstionCount,
        this.price,
        this.hoursRate});

  WalletHistory.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    time = json['time'];
    date = json['date'];
    type = json['type'];
    sesstionCount = json['sesstion_count'];
    price = json['price'];
    hoursRate = json['hours_rate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['time'] = this.time;
    data['date'] = this.date;
    data['type'] = this.type;
    data['sesstion_count'] = this.sesstionCount;
    data['price'] = this.price;
    data['hours_rate'] = this.hoursRate;
    return data;
  }
}