// @dart=2.9
class ExerciseModel {
  String message;
  String status;
  List<Data> data;

  ExerciseModel({this.message, this.status, this.data});

  ExerciseModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String userId;
  String levelId;
  String days;
  String exerciseName;
  String exerciseImage;

  Data(
      {this.userId,
        this.levelId,
        this.days,
        this.exerciseName,
        this.exerciseImage});

  Data.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    levelId = json['level_id'];
    days = json['days'];
    exerciseName = json['exercise_name'];
    exerciseImage = json['exercise_image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['level_id'] = this.levelId;
    data['days'] = this.days;
    data['exercise_name'] = this.exerciseName;
    data['exercise_image'] = this.exerciseImage;
    return data;
  }
}