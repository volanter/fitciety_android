// @dart=2.9
class OffersModel {
  String message;
  String status;
  List<Data> data;

  OffersModel({this.message, this.status, this.data});

  OffersModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  String companyName;
  String offer;
  String description;
  String expire;
  String image;

  Data(
      {this.id,
        this.companyName,
        this.offer,
        this.description,
        this.expire,
        this.image});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    companyName = json['company_name'];
    offer = json['offer'];
    description = json['description'];
    expire = json['expire'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['company_name'] = this.companyName;
    data['offer'] = this.offer;
    data['description'] = this.description;
    data['expire'] = this.expire;
    data['image'] = this.image;
    return data;
  }
}