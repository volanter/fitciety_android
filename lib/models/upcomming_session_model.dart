// @dart=2.9
class UpCommingSessionModel {
  String message;
  String status;
  List<Data> data;

  UpCommingSessionModel({this.message, this.status, this.data});

  UpCommingSessionModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int bookSessionId;
  String fullname;
  String categoryName;
  String startDate;
  String startTime;
  String coverImage;

  Data(
      {this.bookSessionId,
        this.fullname,
        this.categoryName,
        this.startDate,
        this.startTime,
        this.coverImage});

  Data.fromJson(Map<String, dynamic> json) {
    bookSessionId = json['book_session_id'];
    fullname = json['fullname'];
    categoryName = json['category_name'];
    startDate = json['start_date'];
    startTime = json['start_time'];
    coverImage = json['cover_image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['book_session_id'] = this.bookSessionId;
    data['fullname'] = this.fullname;
    data['category_name'] = this.categoryName;
    data['start_date'] = this.startDate;
    data['start_time'] = this.startTime;
    data['cover_image'] = this.coverImage;
    return data;
  }
}