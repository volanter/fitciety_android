// @dart=2.9
class MyProfileModel {
  String message;
  String status;
  List<Data> data;

  MyProfileModel({this.message, this.status, this.data});

  MyProfileModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int userId;
  String fname;
  String lname;
  String email;
  String mobile;
  String image;
  String dob;
  String gender;
  String address;
  String about;
  List<String> areaOfExpertise;

  Data(
      {this.userId,
        this.fname,
        this.lname,
        this.email,
        this.mobile,
        this.image,
        this.dob,
        this.gender,
        this.address,
        this.about,
        this.areaOfExpertise});

  Data.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    fname = json['fname'];
    lname = json['lname'];
    email = json['email'];
    mobile = json['mobile'];
    image = json['image'];
    dob = json['dob'];
    gender = json['gender'];
    address = json['address'];
    about = json['about'];
    areaOfExpertise = json['area_of_expertise'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['fname'] = this.fname;
    data['lname'] = this.lname;
    data['email'] = this.email;
    data['mobile'] = this.mobile;
    data['image'] = this.image;
    data['dob'] = this.dob;
    data['gender'] = this.gender;
    data['address'] = this.address;
    data['about'] = this.about;
    data['area_of_expertise'] = this.areaOfExpertise;
    return data;
  }
}