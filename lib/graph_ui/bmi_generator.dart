import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/models/bmi_report_model.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'indicator.dart';

class BMIGenerator extends StatefulWidget {
  @override
  _BMIGeneratorState createState() => _BMIGeneratorState();
}

class _BMIGeneratorState extends State<BMIGenerator> {
  int touchedIndex=0;
  late bool isShowLoader;
  late Future<BMIReportModel?> _future;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isShowLoader = true;
    _future = getBmiReportData();
  }
  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1.4,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Card(
            shape: RoundedRectangleBorder(
              side: BorderSide(color: Color(0xff006e6f),),
              borderRadius: BorderRadius.circular(10.0),
            ),
            color: Colors.white,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 15,right: 15,top: 10),
                  child: Container(
                    height: 30,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: Color(0xff21a0a1),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: Center(child: Text('BMI GENERATOR',style: GoogleFonts.roboto(
                      color: Colors.white,fontSize: 15,fontWeight: FontWeight.bold,
                    ),)),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: <Widget>[
                    const SizedBox(
                      height: 18,
                    ),
                    Expanded(
                      child: AspectRatio(
                        aspectRatio: 1,
                        child: PieChart(
                          PieChartData(
                              pieTouchData: PieTouchData(touchCallback: (pieTouchResponse) {
                                setState(() {
                                  final desiredTouch = pieTouchResponse.touchInput is! PointerExitEvent &&
                                      pieTouchResponse.touchInput is! PointerUpEvent;
                                  if (desiredTouch && pieTouchResponse.touchedSection != null) {
                                    touchedIndex = pieTouchResponse.touchedSection!.touchedSectionIndex;
                                  } else {
                                    touchedIndex = -1;
                                  }
                                });
                              }),
                              borderData: FlBorderData(
                                show: false,
                              ),
                              sectionsSpace: 0,
                              centerSpaceRadius: 20,
                              sections: showingSections()),
                        ),
                      ),
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const <Widget>[
                        Indicator(
                          color: Color(0xff0293ee),
                          text: 'Underweight',
                          isSquare: true,
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        Indicator(
                          color: Color(0xfff8b250),
                          text: 'Normal',
                          isSquare: true,
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        Indicator(
                          color: Color(0xff845bef),
                          text: 'Overweight',
                          isSquare: true,
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        Indicator(
                          color: Color(0xff13d38e),
                          text: 'Severely obese',
                          isSquare: true,
                        ),
                        SizedBox(
                          height: 18,
                        ),
                      ],
                    ),
                    const SizedBox(
                      width: 28,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  List<PieChartSectionData> showingSections() {
    return List.generate(4, (i) {
      final isTouched = i == touchedIndex;
      final double fontSize = isTouched ? 10 : 10;
      final double radius = isTouched ? 50 : 50;
      switch (i) {
        case 0:
          return PieChartSectionData(
            color: const Color(0xff0293ee),
            value: 25,
            title: '16-18',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: 10, fontWeight: FontWeight.bold, color: const Color(0xffffffff)),
          );
        case 1:
          return PieChartSectionData(
            color: const Color(0xfff8b250),
            value: 25,
            title: '18.5-25',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: 10, fontWeight: FontWeight.bold, color: const Color(0xffffffff)),
          );
        case 2:
          return PieChartSectionData(
            color: const Color(0xff845bef),
            value: 25,
            title: '30-35',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: 10, fontWeight: FontWeight.bold, color: const Color(0xffffffff)),
          );
        case 3:
          return PieChartSectionData(
            color: const Color(0xff13d38e),
            value: 25,
            title: '35-40',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: 10, fontWeight: FontWeight.bold, color: const Color(0xffffffff)),
          );
        default:
          return PieChartSectionData(
            color: const Color(0xff13d38e),
            value: 15,
            title: '15%',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize, fontWeight: FontWeight.bold, color: const Color(0xffffffff)),
          );
      }
    });
  }

  Future<BMIReportModel?> getBmiReportData() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();

    try{
      final response = await http.post(
        Uri.parse(AppStrings.kGetBmiReportDataUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue,
        },
        body: jsonEncode(<String, String>{
          'user_id': preferences.getString(AppStrings.kPrefUserIdKey).toString(),
        }),
      );
      var responseData = jsonDecode(response.body);
      setState(() {
        isShowLoader = false;
      });
      print('response from view summary $responseData');
      if (response.statusCode == 200) {
        if (responseData['status'] == 'success') {
          return BMIReportModel.fromJson(responseData);
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              Navigator.of(context).pop();
            },
          )..show();
        }
      } else {
        throw Exception('Failed to load data');
      }
    }catch (exception){
      print('exception $exception');
    }
  }
}


