// @dart=2.9
class SessionDetailsModel {
  String message;
  String status;
  List<Data> data;

  SessionDetailsModel({this.message, this.status, this.data});

  SessionDetailsModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int sessionId;
  String fullname;
  String userImage;
  String date;
  String time;
  String category;
  String price;
  String desciption;

  Data(
      {this.sessionId,
        this.fullname,
        this.userImage,
        this.date,
        this.time,
        this.category,
        this.price,
        this.desciption});

  Data.fromJson(Map<String, dynamic> json) {
    sessionId = json['session_id'];
    fullname = json['fullname'];
    userImage = json['user_image'];
    date = json['date'];
    time = json['time'];
    category = json['category'];
    price = json['price'];
    desciption = json['desciption'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['session_id'] = this.sessionId;
    data['fullname'] = this.fullname;
    data['user_image'] = this.userImage;
    data['date'] = this.date;
    data['time'] = this.time;
    data['category'] = this.category;
    data['price'] = this.price;
    data['desciption'] = this.desciption;
    return data;
  }
}