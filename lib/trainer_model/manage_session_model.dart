// @dart=2.9
class ManageSessionModel {
  String message;
  String status;
  List<Data> data;

  ManageSessionModel({this.message, this.status, this.data});

  ManageSessionModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int categoryId;
  String categoryName;
  String categoryIcon;
  String time;
  String price;

  Data(
      {this.categoryId,
        this.categoryName,
        this.categoryIcon,
        this.time,
        this.price});

  Data.fromJson(Map<String, dynamic> json) {
    categoryId = json['category_id'];
    categoryName = json['category_name'];
    categoryIcon = json['category_icon'];
    time = json['time'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['category_id'] = this.categoryId;
    data['category_name'] = this.categoryName;
    data['category_icon'] = this.categoryIcon;
    data['time'] = this.time;
    data['price'] = this.price;
    return data;
  }
}