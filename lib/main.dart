// @dart=2.9
import 'package:ficiety/trainer_ui/trainer_dashboard.dart';
import 'package:ficiety/ui/forgot_password.dart';
import 'package:ficiety/ui/login.dart';
import 'package:ficiety/ui/register.dart';
import 'package:ficiety/ui/single_page_ui.dart';
import 'package:ficiety/ui/slider_screen.dart';
import 'package:ficiety/ui/user_dashboard.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'ui/splash.dart';

void main() {
  runApp(MyApp());
}



class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: AppStrings.kSinglePageRoute,
      debugShowCheckedModeBanner: false,
      title: AppStrings.kAppName,
      theme: ThemeData(
        primarySwatch: Colors.green,
        iconTheme: IconThemeData(
          color: AppColors.kGreen,
        ),
        accentColor: AppColors.kGreen,
      ),
      routes: {
        AppStrings.kSplashRoute: (context) => Splash(),
        AppStrings.kSliderRoute: (context) => SliderScreen(),
        AppStrings.kLoginRoute: (context) => Login(),
        AppStrings.kForgotPasswordRoute: (context) => ForgotPassword(),
        AppStrings.kRegisterRoute: (context) => Register(),
        AppStrings.kUserDashboardRoute: (context) => UserDashboard(),
        AppStrings.kTrainerDashboardRouter: (context) => TrainerDashboard(),
        AppStrings.kSinglePageRoute: (context) => SinglePage(),
      },
    );
  }
}
