import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/ui/notification_page.dart';
import 'package:ficiety/ui/order_summary.dart';
import 'package:ficiety/ui/success_payment.dart';
import 'package:ficiety/ui/user_exercise_program.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:ficiety/utils/progressdialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class BookingRequestTrainer extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  // final Function(bool) isReturn;

  BookingRequestTrainer({required this.scaffoldKey/*,required this.isReturn*/});

  @override
  _BookingRequestTrainerState createState() => _BookingRequestTrainerState();
}

class _BookingRequestTrainerState extends State<BookingRequestTrainer> {
  late bool isCurrentView;
  late String selectedProgram;
  late bool isShowLoader;
  late Future _future;
  late ProgressDialog progressDialog;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    progressDialog = ProgressDialog(context,ProgressDialogType.Normal);
    progressDialog.setMessage(AppStrings.kLoadingMsg);
    isCurrentView = true;
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return isCurrentView?Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 120,
            color: Color(0xff006e6f),
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Padding(
                  padding: const EdgeInsets.only(
                    top: 10,
                  ),
                  child: Row(
                    children: [
                      // GestureDetector(
                      //   child: Icon(Icons.arrow_back_sharp,color: Colors.white,)/*Image.asset(
                      //     AppStrings.kImgMenuBar,
                      //     width: 18,
                      //     height: 13,
                      //   )*/,
                      //   onTap: () {
                      //     widget.isReturn(true);
                      //     // if (widget.scaffoldKey.currentState!.isDrawerOpen) {
                      //     //   Navigator.of(context).pop();
                      //     // } else {
                      //     //   widget.scaffoldKey.currentState!.openDrawer();
                      //     // }
                      //   },
                      // ),
                      GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: Image.asset(
                          AppStrings.kImgMenuBar,
                          width: 20,
                          height: 20,
                        ),
                        onTap: () {
                          if (widget.scaffoldKey.currentState!.isDrawerOpen) {
                            Navigator.of(context).pop();
                          } else {
                            widget.scaffoldKey.currentState!.openDrawer();
                          }
                        },
                      ),
                      SizedBox(
                        width: 30,
                      ),
                      Text(
                        AppStrings.kBookingRequest,
                        style: GoogleFonts.roboto(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      Spacer(),
                      GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onTap: (){
                          setState(() {
                            isCurrentView = false;
                          });
                        },
                        child: Image.asset(
                          AppStrings.kImgNotification,
                          width: 20,
                          height: 20,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(
                left: 20,
                right: 20,
                bottom: 20,

              ),
              child: ListView.builder(
                itemBuilder: (context, index) {
                  return GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: (){
                      // setState(() {
                      //   isCurrentView = false;
                      // });
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: Container(
                        height: 100.5,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5.0,),
                          border: Border.all(color: Color(0xff66b5b5b5),),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(top: 20,left: 20,right: 20,),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Container(
                                    child: CircleAvatar(
                                      backgroundImage: AssetImage(AppStrings.kImgGymMan,),
                                    ),
                                    width: 45,
                                    height: 45,
                                  ),
                                  SizedBox(width: 20,),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text('Mark Hemming',style: GoogleFonts.roboto(
                                        color: Color(0xff333333),fontWeight: FontWeight.w900,
                                        fontSize: 14,
                                      ),),
                                      SizedBox(height: 3,),
                                      Text('9 - 10 am | 25 may,2020',style: GoogleFonts.roboto(
                                        color: Color(0xff333333),fontWeight: FontWeight.w500,
                                        fontSize: 12,
                                      ),),
                                      SizedBox(height: 3,),
                                      Text('Online Training Session',style: GoogleFonts.roboto(
                                        color: Color(0xff333333),fontWeight: FontWeight.w500,
                                        fontSize: 12,
                                      ),),
                                      SizedBox(height: 3,),
                                      Text('£ 100',style: GoogleFonts.roboto(
                                        color: Color(0xff333333),fontWeight: FontWeight.w500,
                                        fontSize: 12,
                                      ),),
                                    ],
                                  ),
                                  Spacer(),
                                  InkWell(
                                    onTap: (){
                                      print('click');
                                      progressDialog.show();
                                      authorisationRequest();
                                    },
                                    child: Ink(
                                      color: AppColors.kGreen,
                                      child: Center(
                                        child: Container(
                                          width: 90,
                                          height: 35,
                                          decoration: BoxDecoration(
                                            color: AppColors.kGreen,
                                            borderRadius: BorderRadius.circular(5),
                                          ),
                                          child: Center(
                                            child: Text(
                                              'Confirm',
                                              style: GoogleFonts.roboto(
                                                color: Colors.white,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
                itemCount: 10,
                shrinkWrap: true,
                padding: EdgeInsets.zero,
                scrollDirection: Axis.vertical,

              ),
            ),
          )
        ],
      ),
    ):NotificationPage(scaffoldKey: widget.scaffoldKey,isReturn: (value){
      setState(() {
        isCurrentView = value;
      });
    },);
  }

  Future<void> authorisationRequest() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();

    String jsonBody = jsonEncode(<String, String>{
      // 'trainer_id':'7'
      /*preferences.getString(AppStrings.kPrefUserIdKey).toString()*/
      'user_id': '4',
      'time' : '10-2 am',
      'session_id' : '1',
      'date' : '25-12-2021'
    });

    print('json parms $jsonBody');


    try {
      // final http.Response response = await http.post(
      //   Uri.parse(AppStrings.kSendAuthRequestUrl),
      final http.Response response = await http.post(
        Uri.parse(AppStrings.kSendBookingRequest),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue
        },
        body: jsonBody,);

      var _responseData = jsonDecode(response.body);
      print('response from service ${response.body}');

      Navigator.of(context).pop();

      if (response.statusCode == 200) {
        if (_responseData['status'] == true) {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            title: AppStrings.kSuccess,
            desc: _responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {},
          )..show();
        } else {
          AppCommon.kErrorDialog(
              context: context,
              errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
        }
      } else {
        AppCommon.kErrorDialog(
            context: context,
            errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
      }
    } catch (exception) {
      print('exception is $exception');
      Navigator.of(context).pop();
      AppCommon.kErrorDialog(
          context: context, errorMsg: 'Some error occur.html response getting');
    }
  }
}
