import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/models/client_list_model.dart';
import 'package:ficiety/trainer_ui/trainer_profile.dart';
import 'package:ficiety/trainer_ui/trainer_user_dashboard.dart';
import 'package:ficiety/ui/notification_page.dart';
import 'package:ficiety/ui/session_list.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class ClientList extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  ClientList({required this.scaffoldKey});

  @override
  _ClientListState createState() => _ClientListState();
}

class _ClientListState extends State<ClientList>
    with SingleTickerProviderStateMixin {
  int _crossAxisCount = 3;
  late TabController _tabController;
  late bool isShowClientList;
  late ClientViewType clientViewType;
  late Future<ClientListModel?> _future;
  late bool isShowLoader;
  String name = '';
  String about = '';
  String image = '';
  String address = '';
  String mobile = '';
  String id = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isShowLoader = true;
    _future = getClientList();
    clientViewType = ClientViewType.ClientList;
    isShowClientList = true;
    _tabController = new TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return clientViewType == ClientViewType.ClientList
        ? Container(
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 160,
                  color: Color(0xff006e6f),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SafeArea(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Padding(
                            padding: const EdgeInsets.only(
                              top: 10,
                            ),
                            child: Row(
                              children: [
                                GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  child: Image.asset(
                                    AppStrings.kImgMenuBar,
                                    width: 20,
                                    height: 20,
                                  ),
                                  onTap: () {
                                    if (widget.scaffoldKey.currentState!
                                        .isDrawerOpen) {
                                      Navigator.of(context).pop();
                                    } else {
                                      widget.scaffoldKey.currentState!
                                          .openDrawer();
                                    }
                                  },
                                ),
                                SizedBox(
                                  width: 30,
                                ),
                                Text(
                                  AppStrings.kClientList,
                                  style: GoogleFonts.roboto(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                Spacer(),
                                GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  onTap: (){
                                    setState(() {
                                      clientViewType = ClientViewType.Notification;
                                    });
                                  },
                                  child: Image.asset(
                                    AppStrings.kImgNotification,
                                    width: 20,
                                    height: 20,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(
                            bottom: 0.5,
                          ),
                          child: TabBar(
                            unselectedLabelColor: Colors.white70,
                            labelColor: Colors.white,
                            indicatorColor: Colors.white,
                            indicatorSize: TabBarIndicatorSize.tab,
                            tabs: [
                              Tab(
                                text: AppStrings.kActiveClients,
                              ),
                              Tab(
                                text: AppStrings.kAllClients,
                              )
                            ],
                            controller: _tabController,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: FutureBuilder<ClientListModel?>(
                    future: _future,
                    builder: (BuildContext context, AsyncSnapshot<ClientListModel?>snapshot){
                      if(!snapshot.hasData){
                        return Center(child: Container(width:50,height:50,child: CircularProgressIndicator()));
                      }else{
                        return TabBarView(
                          children: [_allClientsView(snapshot.data!.data[0].active), _allView(snapshot.data!.data[0].all)],
                          controller: _tabController,
                        );
                      }
                    },
                  ),
                ),
              ],
            ),
          )
        : clientViewType == ClientViewType.Dashboard?TrainerUserDashboard(scaffoldKey: widget.scaffoldKey,isReturn: (value){
          setState(() {
            clientViewType = ClientViewType.ClientList;
          });
    }, image: image, address: address, about: about, mobile: mobile, name: name,id: id):NotificationPage(scaffoldKey: widget.scaffoldKey,isReturn: (value){
      setState(() {
        clientViewType = ClientViewType.ClientList;
      });
    },);
  }

  _allView(List<All> all) {
    return Padding(
        padding: const EdgeInsets.only(
          left: 30,
          right: 30,
        ),
        child: GridView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: all.length,
          itemBuilder: (context, index) {
            return GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () {
                setState(() {
                  name = all[index].userName;
                  mobile = all[index].mobileNumber;
                  address = all[index].location;
                  about = all[index].about;
                  id = all[index].userId;
                  image = AppStrings.kImageUrl+all[index].userImage;
                  clientViewType = ClientViewType.Dashboard;
                });
                // setState(() {
                //   isShowClientList = false;
                // });
              },
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Container(
                  width: 100,
                  height: 80,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(
                        5.0,
                      ),
                      border: Border.all(
                        color: Color(
                          0xffeeeeee,
                        ),
                      )),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 56,
                        height: 56,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            image: NetworkImage(
                              AppStrings.kImageUrl+all[index].userImage,
                            ),
                            fit: BoxFit.fill,
                          ),),
                      ),
                      SizedBox(height: 10,),
                      Text(
                        all[index].userName,
                        style: GoogleFonts.roboto(
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 12,
                        ),
                      ),
                      SizedBox(height: 2,),
                      Text(
                        'View Dashboard',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 8,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: _crossAxisCount,
            crossAxisSpacing: 1.0,
            mainAxisSpacing: 1.0,
            childAspectRatio: (MediaQuery.of(context).size.height / 2) /
                (MediaQuery.of(context).size.height - 320),
          ),
        ));
  }


  _allClientsView(List<Active> active) {
    return Padding(
        padding: const EdgeInsets.only(
          left: 30,
          right: 30,
        ),
        child: GridView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: active.length,
          itemBuilder: (context, index) {
            return GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () {
                setState(() {
                  name = active[index].userName;
                  mobile = active[index].mobileNumber;
                  address = active[index].location;
                  about = active[index].about;
                  id = active[index].userId;
                  image = AppStrings.kImageUrl+active[index].userImage;
                  clientViewType = ClientViewType.Dashboard;
                });
                // setState(() {
                //   isShowClientList = false;
                // });
              },
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Container(
                  width: 100,
                  height: 80,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(
                        5.0,
                      ),
                      border: Border.all(
                        color: Color(
                          0xffeeeeee,
                        ),
                      )),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 56,
                        height: 56,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: NetworkImage(
                                AppStrings.kImageUrl+active[index].userImage,
                              ),
                              fit: BoxFit.fill,
                            ),),
                      ),
                      SizedBox(height: 10,),
                      Text(
                        active[index].userName,
                        style: GoogleFonts.roboto(
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 12,
                        ),
                      ),
                      SizedBox(height: 2,),
                      Text(
                        'View Dashboard',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 8,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: _crossAxisCount,
            crossAxisSpacing: 1.0,
            mainAxisSpacing: 1.0,
            childAspectRatio: (MediaQuery.of(context).size.height / 2) /
                (MediaQuery.of(context).size.height - 320),
          ),
        ));
  }

  Future<ClientListModel?> getClientList() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();

    try{
      final response = await http.post(
        Uri.parse(AppStrings.kGetClientReportDataUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue,
        },
        body: jsonEncode(<String, String>{
          'trainer_id': preferences.getString(AppStrings.kPrefUserIdKey).toString(),
        }),
      );
      var responseData = jsonDecode(response.body);
      setState(() {
        isShowLoader = false;
      });
      print('response from client list report $responseData');
      if (response.statusCode == 200) {
        if (responseData['status'] == 'success') {
          return ClientListModel.fromJson(responseData);
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              Navigator.of(context).pop();
            },
          )..show();
        }
      } else {
        throw Exception('Failed to load data');
      }
    }catch (exception){
      print('exception $exception');
    }
  }

}

enum ClientViewType{
  Notification,
  Dashboard,
  ClientList,
}