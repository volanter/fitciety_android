import 'package:ficiety/trainer_ui/trainer_payment_history.dart';
import 'package:ficiety/trainer_ui/trainer_profile.dart';
import 'package:ficiety/ui/notification_page.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:url_launcher/url_launcher.dart';


class TrainerUserDashboard extends StatefulWidget {

  final GlobalKey<ScaffoldState> scaffoldKey;
  final Function (bool) isReturn;
  final String name;
  final String image;
  final String about;
  final String address;
  final String mobile;
  final String id;

  TrainerUserDashboard({required this.scaffoldKey,required this.name,required this.id,required this.image,required this.about, required this.address, required this.mobile, required this.isReturn});


  @override
  _TrainerUserDashboardState createState() => _TrainerUserDashboardState();
}

class _TrainerUserDashboardState extends State<TrainerUserDashboard> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  late bool _isShowView;

  String _genderGroupValue = 'Male';

  List<String> _gender = ['Male', 'Female'];

  String _specialistGroupValue = 'Personal Training';

  List<String> _specialist = [
    'Personal Training',
    'Calisthenics',
    'Boxing',
    'Yoga / Pilates',
    'Strength & Conditioning',
    'Rehabiliation'
  ];

  late TrainerDashboardView trainerDashboardView;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print('image is ${widget.image}');
    _isShowView = true;
    trainerDashboardView= TrainerDashboardView.Dashboard;

  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff057A87),
    ));
    return Scaffold(
      key: _scaffoldKey,
      body: _homeView(),
    );
  }


  Widget _homeView() {
    return trainerDashboardView== TrainerDashboardView.Dashboard?Container(
      color: Colors.white,
      child: ListView(
        children: [
          Container(
            height: 250,
            child: Stack(
              clipBehavior: Clip.none,
              children: [
                Container(
                  height: 150.5,
                  color: Color(0xff057A87),
                  child: Column(
                    children: [
                      SafeArea(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Row(
                            children: [
                              GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                child: Icon(Icons.arrow_back_sharp,color: Colors.white,)/*Image.asset(
                          AppStrings.kImgMenuBar,
                          width: 18,
                          height: 13,
                        )*/,
                                onTap: () {
                                  widget.isReturn(true);
                                  // if (widget.scaffoldKey.currentState!.isDrawerOpen) {
                                  //   Navigator.of(context).pop();
                                  // } else {
                                  //   widget.scaffoldKey.currentState!.openDrawer();
                                  // }
                                },
                              ),
                              // GestureDetector(
                              //   child: Image.asset(
                              //     AppStrings.kImgMenuBar,
                              //     width: 18,
                              //     height: 13,
                              //   ),
                              //   onTap: () {
                              //     if (_scaffoldKey.currentState!.isDrawerOpen) {
                              //       Navigator.of(context).pop();
                              //     } else {
                              //       _scaffoldKey.currentState!.openDrawer();
                              //     }
                              //   },
                              // ),
                              SizedBox(
                                width: 30,
                              ),
                              Text(
                                AppStrings.kUserDashboard,
                                style: GoogleFonts.roboto(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              Spacer(),
                              GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                onTap: (){
                                  setState(() {
                                    trainerDashboardView=TrainerDashboardView.Notification;
                                  });
                                },
                                child: Image.asset(
                                  AppStrings.kImgNotification,
                                  width: 20,
                                  height: 20,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: 100,
                  left: 30,
                  child: Row(
                    children: [
                      Container(
                        width: 120,
                        height: 120,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(
                              5.0,
                            ),
                            border: Border.all(
                              color: Colors.white,
                            ),
                            image: DecorationImage(
                                fit: BoxFit.fill,
                                image: NetworkImage(
                                  widget.image,
                                ))),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment:
                        CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 40,
                          ),
                          Text(
                            widget.name,
                            style: GoogleFonts.roboto(
                              color: AppColors.kGreen,
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          SizedBox(height: 5),
                          // Align(
                          //   alignment: Alignment.topLeft,
                          //   child: Row(
                          //     children: [
                          //       Icon(
                          //         Icons.star,
                          //         size: 15,
                          //         color: Color(0xffffcc00),
                          //       ),
                          //       Icon(
                          //         Icons.star,
                          //         size: 15,
                          //         color: Color(0xffffcc00),
                          //       ),
                          //       Icon(
                          //         Icons.star,
                          //         size: 15,
                          //         color: Color(0xffffcc00),
                          //       ),
                          //       Icon(
                          //         Icons.star,
                          //         size: 15,
                          //         color: Color(0xffffcc00),
                          //       ),
                          //       Icon(
                          //         Icons.star,
                          //         size: 15,
                          //         color: Color(0xffffcc00),
                          //       ),
                          //     ],
                          //   ),
                          // ),
                          SizedBox(
                            height: 10,
                          ),
                          // Text(
                          //   'Clients',
                          //   style: GoogleFonts.roboto(
                          //     color: AppColors.kBlack,
                          //     fontWeight: FontWeight.w700,
                          //   ),
                          // ),
                          // SizedBox(
                          //   height: 2,
                          // ),
                          // Text(
                          //   '245',
                          //   style: GoogleFonts.roboto(
                          //     color: AppColors.kGreen,
                          //     fontWeight: FontWeight.w700,
                          //   ),
                          // ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 30,
              right: 30,
            ),
            child: Text(
              widget.about,
              softWrap: true,
              overflow: TextOverflow.visible,
              style: GoogleFonts.roboto(
                  color: AppColors.kBlack, fontSize: 12),
              textAlign: TextAlign.left,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 30,
              right: 30,
              bottom: 10,
              top: 10,
            ),
            child: Row(
              children: [
                FaIcon(
                  FontAwesomeIcons.mapMarkerAlt,
                  size: 15,
                  color: AppColors.kGreen,
                ),
                SizedBox(
                  width: 15,
                ),
                Expanded(
                  flex: 6,
                  child: Text(
                    widget.address,
                    softWrap: true,
                    overflow: TextOverflow.visible,
                    style: GoogleFonts.roboto(
                      color: AppColors.kBlack,
                      fontSize: 12,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                GestureDetector(
                  onTap: (){
                    launch(
                        "tel://${widget.mobile}");
                  },
                  child: Expanded(
                    flex: 1,
                    child: Container(
                      width: 35,
                      height: 35,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(
                          color: Colors.grey.shade400,
                        ),
                      ),
                      child: Align(
                        child: Icon(
                          Icons.call,
                          color: AppColors.kGreen,
                        ),
                        alignment: Alignment.center,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                GestureDetector(
                  onTap: (){
                    String uri =
                        'sms:+${widget.mobile}';
                    launch(uri);
                  },
                  child: Expanded(
                    flex: 1,
                    child: Container(
                      width: 35,
                      height: 35,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(
                          color: Colors.grey.shade400,
                        ),
                      ),
                      child: Align(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Image.asset(
                            AppStrings.kImgMsg,
                          ),
                        ),
                        alignment: Alignment.center,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 30,
              right: 50,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    setState(() {
                      // selectedText = AppStrings.kViewDiaryCalender;
                    });
                  },
                  child: Container(
                    height: 131,
                    width: 142.5,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(
                          5.0,
                        ),
                        border: Border.all(
                          color: Color(0xff006E6F),
                        )),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // AppCommon.kIconDashboardTextView(
                        //   content: '',
                        //   fontName: AppStrings.kFontAwsSolid,
                        // ),
                        Image.asset(
                          AppStrings.kImgViewCalender,
                          width: 51,
                          height: 51,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          AppStrings.kViewDiaryCalender,
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                            color: Colors.black,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Spacer(),
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    setState(() {
                      // selectedText = AppStrings.kPTProgrammesTrainer;
                    });
                  },
                  child: Container(
                    height: 131,
                    width: 142.5,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(
                          5.0,
                        ),
                        border: Border.all(
                          color: Color(0xff006E6F),
                        )),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // AppCommon.kIconDashboardTextView(
                        //   content: '',
                        //   fontName: AppStrings.kFontAwsSolid,
                        // ),
                        Image.asset(
                          AppStrings.kImgPtProgram,
                          width: 51,
                          height: 51,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          AppStrings.kPTProgrammesTrainer,
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                            color: Colors.black,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 30,
              right: 50,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  child: Container(
                    height: 131,
                    width: 142.5,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(
                          5.0,
                        ),
                        border: Border.all(
                          color: Color(0xff006E6F),
                        )),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // AppCommon.kIconDashboardTextView(
                        //   content: '',
                        //   fontName: AppStrings.kFontAwsSolid,
                        // ),
                        Image.asset(
                          AppStrings.kImgMeasurements,
                          width: 51,
                          height: 51,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          AppStrings.kMeasurementsProgressReport,
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                            color: Colors.black,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                  ),
                  onTap: () {
                    setState(() {
                      trainerDashboardView=TrainerDashboardView.Measurements;
                    });
                    // _scaffoldKey.currentState!.showBottomSheet(
                    //   (context) {
                    //     return Container(
                    //       height: 235.5,
                    //       child: Column(
                    //         children: [
                    //           GestureDetector(
                    //               onTap: () {
                    //                 setState(() {
                    //                   _isShowDialog = false;
                    //                 });
                    //                 Navigator.of(context).pop();
                    //               },
                    //               child: Padding(
                    //                 padding: const EdgeInsets.all(10.0),
                    //                 child: Align(
                    //                     alignment: Alignment.topRight,
                    //                     child:
                    //                         AppCommon.kIconBottomSheetTextView(
                    //                       content: '',
                    //                       fontName: AppStrings.kFontAwsRegular,
                    //                     ) /*Image.asset(
                    //                       AppStrings.kImgClose,
                    //                       width: 15,
                    //                       height: 15.5,
                    //                     )*/
                    //                     ),
                    //               )),
                    //           Text(
                    //             AppStrings.kMeasurementPr,
                    //             textAlign: TextAlign.center,
                    //             style: GoogleFonts.roboto(
                    //               color: AppColors.kGreen,
                    //               fontWeight: FontWeight.w900,
                    //               fontSize: 18,
                    //             ),
                    //           ),
                    //           SizedBox(
                    //             height: 15,
                    //           ),
                    //           GestureDetector(
                    //             child: Container(
                    //               height: 40,
                    //               width: 270,
                    //               decoration: BoxDecoration(
                    //                 borderRadius: BorderRadius.circular(
                    //                   10.0,
                    //                 ),
                    //                 border: Border.all(color: Colors.grey),
                    //               ),
                    //               child: Row(
                    //                 children: [
                    //                   SizedBox(
                    //                     width: 20,
                    //                   ),
                    //                   AppCommon.kIconBottomSheetItemTextView(
                    //                     content: '',
                    //                     fontName: AppStrings.kFontAwsSolid,
                    //                   ),
                    //                   // Image.asset(
                    //                   //   AppStrings.kImgBmi,
                    //                   //   width: 16,
                    //                   //   height: 13.5,
                    //                   // ),
                    //                   SizedBox(
                    //                     width: 20,
                    //                   ),
                    //                   Text(
                    //                     AppStrings.kBMIMeasurements,
                    //                     textAlign: TextAlign.center,
                    //                     style: GoogleFonts.roboto(
                    //                       color: AppColors.kBlack,
                    //                       fontWeight: FontWeight.w500,
                    //                     ),
                    //                   )
                    //                 ],
                    //               ),
                    //             ),
                    //             onTap: () {
                    //               setState(() {
                    //                 _isShowDialog = false;
                    //                 // selectedText = AppStrings.kBMIMeasurements;
                    //               });
                    //               Navigator.of(context).pop();
                    //             },
                    //           ),
                    //           SizedBox(
                    //             height: 15,
                    //           ),
                    //           GestureDetector(
                    //             child: Container(
                    //               height: 40,
                    //               width: 270,
                    //               decoration: BoxDecoration(
                    //                 borderRadius: BorderRadius.circular(
                    //                   10.0,
                    //                 ),
                    //                 border: Border.all(color: Colors.grey),
                    //               ),
                    //               child: Row(
                    //                 children: [
                    //                   SizedBox(
                    //                     width: 20,
                    //                   ),
                    //                   AppCommon.kIconBottomSheetItemTextView(
                    //                       fontName: AppStrings.kFontAwsSolid,
                    //                       content: ''),
                    //                   // Image.asset(
                    //                   //   AppStrings.kImgFat,
                    //                   //   width: 16,
                    //                   //   height: 13.5,
                    //                   // ),
                    //                   SizedBox(
                    //                     width: 20,
                    //                   ),
                    //                   Text(
                    //                     AppStrings.kFATMeasurements,
                    //                     textAlign: TextAlign.center,
                    //                     style: GoogleFonts.roboto(
                    //                       color: AppColors.kBlack,
                    //                       fontWeight: FontWeight.w500,
                    //                     ),
                    //                   )
                    //                 ],
                    //               ),
                    //             ),
                    //             onTap: () {
                    //               setState(() {
                    //                 _isShowDialog = false;
                    //                 // selectedText = AppStrings.kFATMeasurements;
                    //               });
                    //               Navigator.of(context).pop();
                    //             },
                    //           ),
                    //           SizedBox(
                    //             height: 15,
                    //           ),
                    //           GestureDetector(
                    //             child: Container(
                    //               height: 40,
                    //               width: 270,
                    //               decoration: BoxDecoration(
                    //                 borderRadius: BorderRadius.circular(
                    //                   10.0,
                    //                 ),
                    //                 border: Border.all(color: Colors.grey),
                    //               ),
                    //               child: Row(
                    //                 children: [
                    //                   SizedBox(
                    //                     width: 20,
                    //                   ),
                    //                   AppCommon.kIconBottomSheetItemTextView(
                    //                       fontName: AppStrings.kFontAwsSolid,
                    //                       content: ''),
                    //                   // Image.asset(
                    //                   //   AppStrings.kImgWaist,
                    //                   //   width: 16,
                    //                   //   height: 13.5,
                    //                   // ),
                    //                   SizedBox(
                    //                     width: 20,
                    //                   ),
                    //                   Text(
                    //                     AppStrings.kWaistMeasurements,
                    //                     textAlign: TextAlign.center,
                    //                     style: GoogleFonts.roboto(
                    //                       color: AppColors.kBlack,
                    //                       fontWeight: FontWeight.w500,
                    //                     ),
                    //                   )
                    //                 ],
                    //               ),
                    //             ),
                    //             onTap: () {
                    //               setState(() {
                    //                 _isShowDialog = false;
                    //                 // selectedText =
                    //                     AppStrings.kWaistMeasurements;
                    //               });
                    //               Navigator.of(context).pop();
                    //             },
                    //           ),
                    //         ],
                    //       ),
                    //     );
                    //   },
                    //   shape: RoundedRectangleBorder(
                    //       borderRadius: BorderRadius.only(
                    //         topLeft: Radius.circular(20.0),
                    //         topRight: Radius.circular(20.0),
                    //       ),
                    //       side: BorderSide(
                    //         color: Colors.grey,
                    //       )),
                    // );
                  },
                ),
                Spacer(),
                Container(
                  height: 131,
                  width: 142.5,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(
                        5.0,
                      ),
                      border: Border.all(
                        color: Color(0xff006E6F),
                      )),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      // AppCommon.kIconDashboardTextView(
                      //     fontName: AppStrings.kFontAwsSolid, content: ''),
                      Image.asset(
                        AppStrings.kImgDietaryLog,
                        width: 51,
                        height: 51,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        AppStrings.kDietaryLogTrainer,
                        textAlign: TextAlign.center,
                        style: GoogleFonts.roboto(
                          color: Colors.black,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 30,
              right: 50,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    setState(() {
                      trainerDashboardView = TrainerDashboardView.PaymentHistory;
                    });
                  },
                  child: Container(
                    height: 131,
                    width: 142.5,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(
                          5.0,
                        ),
                        border: Border.all(
                          color: Color(0xff006E6F),
                        )),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // AppCommon.kIconDashboardTextView(
                        //   content: '',
                        //   fontName: AppStrings.kFontAwsSolid,
                        // ),
                        Image.asset(
                          AppStrings.kImgPayment,
                          width: 51,
                          height: 51,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          'Financials',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                            color: Colors.black,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Spacer(),
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    setState(() {
                      // selectedText = AppStrings.kPTProgrammesTrainer;
                    });
                  },
                  child: Container(
                    height: 131,
                    width: 142.5,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(
                          5.0,
                        ),
                        border: Border.all(
                          color: Color(0xff006E6F),
                        )),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // AppCommon.kIconDashboardTextView(
                        //   content: '',
                        //   fontName: AppStrings.kFontAwsSolid,
                        // ),
                        Image.asset(
                          AppStrings.kImgFitnessCenter,
                          width: 51,
                          height: 51,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          'Consulation',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                            color: Colors.black,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 30,
          ),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: (){
              setState(() {

              });
            },
            child: Padding(
              padding: const EdgeInsets.only(
                left: 30,
                right: 50,
              ),
              child: Container(
                height: 44.5,
                width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(
                      5.0,
                    ),
                    border: Border.all(
                      color: Color(0xff006E6F),
                    )),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Stack(
                      children: [
                        AppCommon.kIconMessageTextView(
                          content: '',
                          fontName: AppStrings.kFontAwsSolid,
                        ),
                        // Image.asset(
                        //   AppStrings.kImgMsg,
                        //   width: 39.5,
                        //   height: 31,
                        // ),
                        // Positioned(
                        //   top:10,
                        //   left: 10,
                        //   bottom: 2,
                        //   child: Container(
                        //     width: 30,
                        //     height: 30,
                        //     child: CircleAvatar(
                        //       backgroundColor: Colors.white,
                        //       child: CircleAvatar(
                        //         backgroundColor: Color(0xffFECF33),
                        //         child: Text('1',style: GoogleFonts.roboto(color: Colors.white,),),
                        //       ),
                        //     ),
                        //   ),
                        // )
                      ],
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Text(
                      'Excercise',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(
                        color: Colors.black,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          // GestureDetector(
          //   onTap: () {
          //     setState(() {
          //       // selectedText = AppStrings.kPTProgrammesTrainer;
          //     });
          //   },
          //   child: Container(
          //     height: 131,
          //     width: 50,
          //     decoration: BoxDecoration(
          //         borderRadius: BorderRadius.circular(
          //           5.0,
          //         ),
          //         border: Border.all(
          //           color: Color(0xff006E6F),
          //         )),
          //     child: Column(
          //       mainAxisAlignment: MainAxisAlignment.center,
          //       children: [
          //         // AppCommon.kIconDashboardTextView(
          //         //   content: '',
          //         //   fontName: AppStrings.kFontAwsSolid,
          //         // ),
          //         Image.asset(
          //           AppStrings.kImgFitnessCenter,
          //           width: 51,
          //           height: 51,
          //         ),
          //         SizedBox(
          //           height: 10,
          //         ),
          //         Text(
          //           'Excercise',
          //           textAlign: TextAlign.center,
          //           style: GoogleFonts.roboto(
          //             color: Colors.black,
          //             fontWeight: FontWeight.w700,
          //           ),
          //         ),
          //       ],
          //     ),
          //   ),
          // ),
          // Padding(
          //   padding: const EdgeInsets.only(
          //     left: 30,
          //     right: 50,
          //   ),
          //   child: Container(
          //     height: 44.5,
          //     width: double.infinity,
          //     decoration: BoxDecoration(
          //         borderRadius: BorderRadius.circular(
          //           5.0,
          //         ),
          //         border: Border.all(
          //           color: Color(0xff006E6F),
          //         )),
          //     child: Row(
          //       mainAxisAlignment: MainAxisAlignment.center,
          //       children: [
          //         Stack(
          //           children: [
          //             AppCommon.kIconMessageTextView(
          //               content: '',
          //               fontName: AppStrings.kFontAwsSolid,
          //             ),
          //             // Image.asset(
          //             //   AppStrings.kImgMsg,
          //             //   width: 39.5,
          //             //   height: 31,
          //             // ),
          //             // Positioned(
          //             //   top:10,
          //             //   left: 10,
          //             //   bottom: 2,
          //             //   child: Container(
          //             //     width: 30,
          //             //     height: 30,
          //             //     child: CircleAvatar(
          //             //       backgroundColor: Colors.white,
          //             //       child: CircleAvatar(
          //             //         backgroundColor: Color(0xffFECF33),
          //             //         child: Text('1',style: GoogleFonts.roboto(color: Colors.white,),),
          //             //       ),
          //             //     ),
          //             //   ),
          //             // )
          //           ],
          //         ),
          //         SizedBox(
          //           width: 15,
          //         ),
          //         Text(
          //           AppStrings.kMessageYourClient,
          //           textAlign: TextAlign.center,
          //           style: GoogleFonts.roboto(
          //             color: Colors.black,
          //             fontWeight: FontWeight.w700,
          //           ),
          //         ),
          //       ],
          //     ),
          //   ),
          // ),
        ],
      ),
    ):trainerDashboardView== TrainerDashboardView.Measurements?TrainerProfile(scaffoldKey: widget.scaffoldKey,isReturn: (value){
      setState(() {
        trainerDashboardView=TrainerDashboardView.Dashboard;
      });
    },):trainerDashboardView== TrainerDashboardView.Notification?NotificationPage(scaffoldKey: widget.scaffoldKey,isReturn: (value){
      setState(() {
        trainerDashboardView=TrainerDashboardView.Dashboard;
      });
    },):trainerDashboardView== TrainerDashboardView.PaymentHistory?TrainerPaymentHistory(scaffoldKey: widget.scaffoldKey):Container();
  }

}

enum TrainerDashboardView{
  ViewCalender,
  PTProgrammes,
  Measurements,
  DietaryLog,
  Notification,
  Dashboard,
  PaymentHistory,
}
