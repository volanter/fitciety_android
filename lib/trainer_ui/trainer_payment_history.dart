import 'package:ficiety/graph_ui/transition__history_graph.dart';
import 'package:ficiety/ui/order_summary.dart';
import 'package:ficiety/ui/success_payment.dart';
import 'package:ficiety/ui/user_exercise_program.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

class TrainerPaymentHistory extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  TrainerPaymentHistory({required this.scaffoldKey});

  @override
  _TrainerPaymentHistoryState createState() => _TrainerPaymentHistoryState();
}

class _TrainerPaymentHistoryState extends State<TrainerPaymentHistory> {
  late bool isSuccessPayment;
  late String selectedProgram;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isSuccessPayment = true;
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return isSuccessPayment?Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 230,
            color: Color(0xff006e6f),
            child: SafeArea(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        top: 10,
                      ),
                      child: Row(
                        children: [
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: Image.asset(
                              AppStrings.kImgMenuBar,
                              width: 20,
                              height: 20,
                            ),
                            onTap: () {
                              if (widget.scaffoldKey.currentState!.isDrawerOpen) {
                                Navigator.of(context).pop();
                              } else {
                                widget.scaffoldKey.currentState!.openDrawer();
                              }
                            },
                          ),
                          SizedBox(
                            width: 30,
                          ),
                          Text(
                            AppStrings.kPaymentHistory,
                            style: GoogleFonts.roboto(
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Spacer(),
                          Image.asset(
                            AppStrings.kImgNotification,
                            width: 17.5,
                            height: 18.5,
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Text(
                    '£7900',
                    style: GoogleFonts.roboto(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    'Total Balance',
                    style: GoogleFonts.roboto(
                      color: Colors.white,
                      fontSize: 9,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),

            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 20,
              left: 30,
              right: 30,
            ),
            child: Text(
              'Transaction History',
              style: GoogleFonts.roboto(
                color: Colors.black,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Expanded(
            child: ListView(
              shrinkWrap: true,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 20,right: 20,bottom: 10,top: 10,),
                  child: TransitionHistoryGraph(),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 30,
                    right: 30,
                  ),
                  child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onTap: (){
                          setState(() {
                            isSuccessPayment = false;
                          });
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(top: 15),
                          child: Container(
                            height: 120.5,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5.0,),
                              border: Border.all(color: Color(0xff66b5b5b5),),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.only(top: 20,left: 20,right: 20,),
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Container(
                                        child: CircleAvatar(
                                          backgroundImage: AssetImage(AppStrings.kImgGymMan,),
                                        ),
                                        width: 45,
                                        height: 45,
                                      ),
                                      SizedBox(width: 20,),
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('Mark Hemming',style: GoogleFonts.roboto(
                                            color: Color(0xff333333),fontWeight: FontWeight.w900,
                                            fontSize: 14,
                                          ),),
                                          SizedBox(height: 3,),
                                          Text('9 - 10 am | 25 may,2020',style: GoogleFonts.roboto(
                                            color: Color(0xff333333),fontWeight: FontWeight.w500,
                                            fontSize: 12,
                                          ),),
                                          SizedBox(height: 3,),
                                          Text('Online Training Session',style: GoogleFonts.roboto(
                                            color: Color(0xff333333),fontWeight: FontWeight.w500,
                                            fontSize: 12,
                                          ),),
                                        ],
                                      ),
                                      Spacer(),
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('£100 / 1 hrs',style: GoogleFonts.roboto(
                                            color: Color(0xff333333),fontWeight: FontWeight.w500,
                                            fontSize: 12,
                                          ),),
                                          SizedBox(height: 3,),
                                          Text('5 Session',style: GoogleFonts.roboto(
                                            color: Color(0xff333333),fontWeight: FontWeight.w500,
                                            fontSize: 12,
                                          ),),
                                          SizedBox(height: 3,),
                                          Text('£500',style: GoogleFonts.roboto(
                                            color: AppColors.kGreen,fontWeight: FontWeight.w500,
                                            fontSize: 12,
                                          ),),
                                        ],
                                      ),

                                    ],
                                  ),
                                  SizedBox(height: 10,),
                                  Divider(
                                    color: Colors.grey,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text('Payment Date & Time',style: GoogleFonts.roboto(
                                        color: Color(0xff333333),fontWeight: FontWeight.w900,
                                        fontSize: 12,
                                      ),),
                                      SizedBox(width: 5,),
                                      Text(':-',style: GoogleFonts.roboto(
                                        color: Color(0xff333333),fontWeight: FontWeight.w500,
                                        fontSize: 12,
                                      ),),
                                      SizedBox(width: 5,),
                                      Text('9 - 10 am | 25 may, 2020',style: GoogleFonts.roboto(
                                        color: Color(0xff333333),fontWeight: FontWeight.w500,
                                        fontSize: 12,
                                      ),),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                    itemCount: 10,
                    shrinkWrap: true,
                    padding: EdgeInsets.zero,
                    scrollDirection: Axis.vertical,

                  ),
                ),
              ],
            ),
          ),
          // Padding(
          //   padding: const EdgeInsets.all(20.0),
          //   child: TransitionHistoryGraph(),
          // ),
        ],
      ),
    ):SuccessPayment(scaffoldKey: widget.scaffoldKey,isReturn: (value){},);
  }
}
