import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/models/category_model.dart';
import 'package:ficiety/trainer_model/manage_session_model.dart';
import 'package:ficiety/ui/order_summary.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:ficiety/utils/progressdialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:date_format/date_format.dart';
import 'package:intl/intl.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class ManageSessionTrainer extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  // final Function(bool) isReturn;

  ManageSessionTrainer({required this.scaffoldKey /*, required this.isReturn*/
      });

  @override
  _ManageSessionTrainerState createState() => _ManageSessionTrainerState();
}

class _ManageSessionTrainerState extends State<ManageSessionTrainer> {
  late bool isShowViewSummary;
  TextEditingController _controllerCategory = TextEditingController();
  TextEditingController _controllerAvailableTime = TextEditingController();
  TextEditingController _controllerAvailableTimeTo = TextEditingController();
  TextEditingController _controllerPrice = TextEditingController();
  TextEditingController _controllerClassLocation = TextEditingController();
  TextEditingController _controllerDiscription = TextEditingController();
  List<String> listTime = [];

  late ProgressDialog progressDialog;
  late SharedPreferences preferences;

  late String _setTime, _setDate;

  late String _hour, _minute, _time;

  late String _hourTo, _minuteTo, _timeTo;

  late String dateTime;

  late String dateTimeTo;

  DateTime selectedDate = DateTime.now();

  DateTime selectedDateTo = DateTime.now();

  TimeOfDay selectedTime = TimeOfDay(hour: 00, minute: 00);

  TimeOfDay toSelectedTime = TimeOfDay(hour: 00, minute: 00);

  late Future<ManageSessionModel?> _future;

  int _value = 1;
  String _categoryValue = '';
  String _classValue = '';
  FocusNode focusNodeCategory = FocusNode();
  FocusNode focusNodeAvailableTime = FocusNode();
  FocusNode focusNodeAvailableTimeTo = FocusNode();
  FocusNode focusNodePrice = FocusNode();
  FocusNode focusNodeClassLocation = FocusNode();
  FocusNode focusNodeDiscription = FocusNode();
  int sessionBookValue = 1;
  bool monday = true;
  bool tuesday = false;
  bool wednesday = false;
  bool thursday = true;
  bool friday = true;
  bool saturday = false;
  bool sunday = true;
  final _formKey = GlobalKey<FormState>();
  late bool showProgress;
  late Future _futureCategory;
  late Future _futureClass;
  List<String> listCategoryName = [];
  List<int> listCategoryId = [];
  List<dynamic> listCategoryData = [];

  List<String> listClassName = [];
  List<int> listClassId = [];
  List<dynamic> listClassData = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    showProgress = true;
    _categoryValue = 'Select Category';
    _classValue = 'Select Class';
    listCategoryName.add('Select Category');
    listCategoryId.add(0);

    listClassId.add(0);
    listClassName.add('Select Class');

    _future = getManageSessionData();
    _futureCategory = getCategoryData();
    _futureClass = getClass();
    progressDialog = new ProgressDialog(context, ProgressDialogType.Normal);
    progressDialog.setMessage(AppStrings.kLoadingMsg);
    isShowViewSummary = true;
    SharedPreferences.getInstance().then((value) => preferences = value);
  }

  @override
  Widget build(BuildContext context) {
    dateTime = DateFormat.yMd().format(DateTime.now());
    dateTimeTo = DateFormat.yMd().format(DateTime.now());
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return isShowViewSummary
        ? Container(
            color: Colors.white,
            child: Scaffold(
              floatingActionButton: FloatingActionButton(
                onPressed: () {
                  _showViewDialog();
                },
                backgroundColor: Color(0xff006e6f),
                child: Icon(
                  Icons.add,
                  color: Colors.white,
                  size: 30,
                ),
              ),
              body: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 120,
                    color: Color(0xff006e6f),
                    child: SafeArea(
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Padding(
                          padding: const EdgeInsets.only(
                            top: 10,
                          ),
                          child: Row(
                            children: [
                              GestureDetector(
                                child: Image.asset(
                                  AppStrings.kImgMenuBar,
                                  width: 18,
                                  height: 13,
                                ),
                                onTap: () {
                                  if (widget
                                      .scaffoldKey.currentState!.isDrawerOpen) {
                                    Navigator.of(context).pop();
                                  } else {
                                    widget.scaffoldKey.currentState!
                                        .openDrawer();
                                  }
                                },
                              ),
                              SizedBox(
                                width: 30,
                              ),
                              Text(
                                AppStrings.kManageSession,
                                style: GoogleFonts.roboto(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              Spacer(),
                              Image.asset(
                                AppStrings.kImgNotification,
                                width: 17.5,
                                height: 18.5,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, top: 30),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: Center(
                                child: Text(
                                  AppStrings.kCategory,
                                  style: GoogleFonts.roboto(
                                    color: Color(0xff006e6f),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Center(
                                child: Text(
                                  AppStrings.kTime,
                                  style: GoogleFonts.roboto(
                                    color: Color(0xff006e6f),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Center(
                                child: Text(
                                  AppStrings.kPrice,
                                  style: GoogleFonts.roboto(
                                    color: Color(0xff006e6f),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Divider(
                          color: Colors.grey,
                          height: 2,
                        ),
                        Container(
                          child: FutureBuilder<ManageSessionModel?>(
                              future: _future,
                              builder: (BuildContext context,
                                  AsyncSnapshot<ManageSessionModel?> snapshot) {
                                if (!snapshot.hasData) {
                                  return Center(
                                    child: showProgress
                                        ? Center(
                                            child: CircularProgressIndicator())
                                        : SizedBox(),
                                  );
                                } else {
                                  return ListView.builder(
                                    itemBuilder: (context, index) {
                                      return Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            child: Center(
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                  top: 10,
                                                  bottom: 10,
                                                ),
                                                child: Text(
                                                  snapshot.data!.data[index]
                                                          .categoryName ??
                                                      '',
                                                  style: GoogleFonts.roboto(
                                                    color: Colors.black,
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            child: Center(
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                  top: 10,
                                                  bottom: 10,
                                                ),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      snapshot.data!.data[index]
                                                          .time,
                                                      style: GoogleFonts.roboto(
                                                        color: Colors.black,
                                                        fontSize: 12,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: 5,
                                                    ),
                                                    Text(
                                                      snapshot.data!.data[index]
                                                          .time,
                                                      style: GoogleFonts.roboto(
                                                        color: Colors.black,
                                                        fontSize: 10,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                      ),
                                                    ),
                                                    // SizedBox(
                                                    //   height: 5,
                                                    // ),
                                                    // Text(
                                                    //   '6 - 7 pm',
                                                    //   style: GoogleFonts.roboto(
                                                    //     color: Colors.black,
                                                    //     fontSize: 10,
                                                    //     fontWeight:
                                                    //         FontWeight.w500,
                                                    //   ),
                                                    // ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            child: Center(
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                  top: 10,
                                                  bottom: 10,
                                                ),
                                                child: Text(
                                                  snapshot
                                                      .data!.data[index].price,
                                                  style: GoogleFonts.roboto(
                                                    color: Colors.black,
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      );
                                    },
                                    itemCount: snapshot.data!.data.length,
                                    padding: EdgeInsets.zero,
                                    shrinkWrap: true,
                                  );
                                }
                              }),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          )
        : OrderSummary(
            scaffoldKey: widget.scaffoldKey,
            isReturn: (value) {
              setState(() {
                isShowViewSummary = value;
              });
            },
          );
  }

  _showViewDialog() {
    print('call dialog');
    return showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, setState) {
              return Dialog(
                insetPadding: EdgeInsets.symmetric(
                  horizontal: 2.0,
                  vertical: 2.0,
                ),
                // title: Text(
                //   AppStrings.kSearchForTrainer,style: GoogleFonts.roboto(
                //   color: AppColors.kGreen,fontWeight: FontWeight.w900,
                // ),
                // ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                    10.0,
                  ),
                ),
                child: Container(
                  width: 380,
                  height: 700,
                  padding: EdgeInsets.all(
                    20.0,
                  ),
                  child: SingleChildScrollView(
                    child: Form(
                      key: _formKey,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text(
                                AppStrings.kAddSession,
                                style: GoogleFonts.roboto(
                                  color: AppColors.kGreen,
                                  fontWeight: FontWeight.w900,
                                  fontSize: 18,
                                ),
                              ),
                              Spacer(),
                              GestureDetector(
                                child: Icon(
                                  Icons.clear,
                                  color: AppColors.kGreen,
                                ),
                                onTap: () => Navigator.of(context).pop(),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  AppStrings.kCategory + ' :',
                                  style: GoogleFonts.roboto(
                                    color: Colors.black,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Expanded(
                                  child: DropdownButtonFormField(
                                    value: _categoryValue,
                                    validator: (String? value){
                                      if(value!.isEmpty){
                                        return 'Please select category';
                                      }else if(value == 'Select Category'){
                                        return 'Please select category';
                                      }else{
                                        return null;
                                      }
                                    },
                                    items: listCategoryName
                                        .map<DropdownMenuItem<String>>(
                                            (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                                    onChanged: (String? newValue) {
                                      setState(() {
                                        _categoryValue = newValue!;
                                      });
                                    },
                                  ),
                                ),
                              ]),
                          SizedBox(
                            height: 20,
                          ),
                          Wrap(
                              // mainAxisAlignment: MainAxisAlignment.center,
                              // crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  'Class' + ' :',
                                  style: GoogleFonts.roboto(
                                    color: Colors.black,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Expanded(
                                  child: DropdownButtonFormField(
                                    value: _classValue,
                                    validator: (String? value){
                                      if(value!.isEmpty){
                                        return 'Please select class';
                                      }else if(value == 'Select Class'){
                                        return 'Please select class';
                                      }else{
                                        return null;
                                      }
                                    },
                                    items: listClassName
                                        .map<DropdownMenuItem<String>>(
                                            (String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(value,softWrap: true,overflow: TextOverflow.ellipsis,),
                                          );
                                        }).toList(),
                                    onChanged: (String? newValue) {
                                      setState(() {
                                        _classValue = newValue!;
                                      });
                                    },
                                  ),
                                ),
                              ]),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                AppStrings.kAvailableTime + ' :',
                                style: GoogleFonts.roboto(
                                  color: Colors.black,
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: GestureDetector(
                                  onTap: () {
                                    _selectTime(context);
                                  },
                                  child: TextFormField(
                                    controller: _controllerAvailableTime,
                                    focusNode: focusNodeAvailableTime,
                                    maxLines: 1,
                                    enabled: false,
                                    readOnly: true,
                                    decoration: InputDecoration(
                                      contentPadding:
                                          EdgeInsets.symmetric(vertical: 2),
                                      isDense: true,
                                    ),
                                    onFieldSubmitted: (_) {
                                      AppCommon.kChangeFocusNode(
                                          currentFocusNode:
                                              focusNodeAvailableTime,
                                          nextFocusNode:
                                              focusNodeAvailableTimeTo,
                                          context: context);
                                    },
                                  ),
                                ),
                              ),
                              Text(
                                ' To ',
                                style: AppCommon.kConsultationFormTextStyle(),
                              ),
                              Expanded(
                                child: GestureDetector(
                                  onTap: () {
                                    _toSelectTime(context);
                                  },
                                  child: TextFormField(
                                    controller: _controllerAvailableTimeTo,
                                    focusNode: focusNodeAvailableTimeTo,
                                    maxLines: 1,
                                    enabled: false,
                                    readOnly: true,
                                    decoration: InputDecoration(
                                      contentPadding:
                                          EdgeInsets.symmetric(vertical: 2),
                                      isDense: true,
                                    ),
                                    onFieldSubmitted: (_) {
                                      AppCommon.kChangeFocusNode(
                                          currentFocusNode:
                                              focusNodeAvailableTime,
                                          nextFocusNode:
                                              focusNodeAvailableTimeTo,
                                          context: context);
                                    },
                                  ),
                                ),
                              ),
                              IconButton(
                                  onPressed: () {
                                    if ((_controllerAvailableTimeTo
                                            .text.isEmpty) &&
                                        (_controllerAvailableTime
                                            .text.isEmpty)) {
                                      AppCommon.kErrorDialog(
                                          context: context,
                                          errorMsg:
                                              'Please select from and to time');
                                    } else {
                                      listTime.add(
                                          _controllerAvailableTime.text +
                                              '-' +
                                              _controllerAvailableTimeTo.text);
                                      setState(() {
                                        _controllerAvailableTimeTo.text = '';
                                        _controllerAvailableTime.text = '';
                                        selectedTime =
                                            TimeOfDay(hour: 00, minute: 00);
                                        toSelectedTime =
                                            TimeOfDay(hour: 00, minute: 00);
                                      });
                                    }
                                  },
                                  icon: Icon(Icons.add_circle_outline)),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Wrap(
                            children: List.generate(listTime.length, (index) {
                              return Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Chip(
                                  label: Text(
                                    listTime[index],
                                    style: GoogleFonts.roboto(
                                      fontSize: 10,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                  onDeleted: () {
                                    print('delete call 1');
                                  },
                                  deleteIconColor: Colors.black,
                                  deleteIcon: GestureDetector(
                                      onTap: () {
                                        print('delete call');
                                        listTime.removeAt(index);
                                        setState(() {});
                                      },
                                      behavior: HitTestBehavior.translucent,
                                      child: Icon(
                                        Icons.clear_rounded,
                                        color: Colors.black,
                                        size: 16,
                                      )),
                                  useDeleteButtonTooltip: true,
                                  backgroundColor: Color(0xfff2f2f2),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(
                                        30,
                                      ),
                                      side: BorderSide(
                                        color: Color(0xffdddddd),
                                      )),
                                ),
                              );
                            }),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                'Day : ',
                                style: GoogleFonts.roboto(
                                  color: Colors.black,
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: CheckboxListTile(
                                  value: monday,
                                  contentPadding: EdgeInsets.zero,
                                  controlAffinity:
                                      ListTileControlAffinity.leading,
                                  checkColor: Colors.white,
                                  title: Text('Monday',
                                      style: GoogleFonts.roboto(
                                        color: Color(0xff414040),
                                      )),
                                  onChanged: (bool? value) {
                                    print('value changed $value');
                                    setState(() {
                                      monday = value!;
                                    });
                                  },
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: CheckboxListTile(
                                  value: tuesday,
                                  contentPadding: EdgeInsets.zero,
                                  controlAffinity:
                                      ListTileControlAffinity.leading,
                                  checkColor: Colors.white,
                                  title: Text('Tuesday',
                                      style: GoogleFonts.roboto(
                                        color: Color(0xff414040),
                                      )),
                                  onChanged: (bool? value) {
                                    setState(() {
                                      tuesday = value!;
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                '          ',
                                style: AppCommon.kConsultationFormTextStyle(),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: CheckboxListTile(
                                  value: wednesday,
                                  contentPadding: EdgeInsets.zero,
                                  controlAffinity:
                                      ListTileControlAffinity.leading,
                                  checkColor: Colors.white,
                                  title: Text('Wednesday',
                                      style: GoogleFonts.roboto(
                                        color: Color(0xff414040),
                                      )),
                                  onChanged: (bool? value) {
                                    setState(() {
                                      wednesday = value!;
                                    });
                                  },
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: CheckboxListTile(
                                  value: thursday,
                                  contentPadding: EdgeInsets.zero,
                                  controlAffinity:
                                      ListTileControlAffinity.leading,
                                  checkColor: Colors.white,
                                  title: Text('Thursday',
                                      style: GoogleFonts.roboto(
                                        color: Color(0xff414040),
                                      )),
                                  onChanged: (bool? value) {
                                    setState(() {
                                      thursday = value!;
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                '          ',
                                style: AppCommon.kConsultationFormTextStyle(),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: CheckboxListTile(
                                  value: friday,
                                  contentPadding: EdgeInsets.zero,
                                  controlAffinity:
                                      ListTileControlAffinity.leading,
                                  checkColor: Colors.white,
                                  title: Text('Friday',
                                      style: GoogleFonts.roboto(
                                        color: Color(0xff414040),
                                      )),
                                  onChanged: (bool? value) {
                                    setState(() {
                                      friday = value!;
                                    });
                                  },
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: CheckboxListTile(
                                  value: saturday,
                                  contentPadding: EdgeInsets.zero,
                                  controlAffinity:
                                      ListTileControlAffinity.leading,
                                  checkColor: Colors.white,
                                  title: Text('Saturday',
                                      style: GoogleFonts.roboto(
                                        color: Color(0xff414040),
                                      )),
                                  onChanged: (bool? value) {
                                    setState(() {
                                      saturday = value!;
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                '          ',
                                style: AppCommon.kConsultationFormTextStyle(),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: CheckboxListTile(
                                  value: sunday,
                                  contentPadding: EdgeInsets.zero,
                                  controlAffinity:
                                      ListTileControlAffinity.leading,
                                  checkColor: Colors.white,
                                  title: Text('Sunday',
                                      style: GoogleFonts.roboto(
                                        color: Color(0xff414040),
                                      )),
                                  onChanged: (bool? value) {
                                    setState(() {
                                      sunday = value!;
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                'Session Book : ',
                                style: GoogleFonts.roboto(
                                  color: Colors.black,
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Column(
                                children: [
                                  // IconButton(onPressed: ,icon: Icon(Icons.),),
                                  GestureDetector(
                                    child: FaIcon(
                                      FontAwesomeIcons.angleUp,
                                      color: AppColors.kGreen,
                                    ),
                                    onTap: () {
                                      setState(() {
                                        sessionBookValue = sessionBookValue + 1;
                                      });
                                    },
                                  ),
                                  Container(
                                    height: 25,
                                    width: 25,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: AppColors.kGreen,
                                      ),
                                    ),
                                    child: Center(
                                      child: Text(
                                        sessionBookValue.toString(),
                                        style: AppCommon
                                            .kConsultationFormTextStyle(),
                                      ),
                                    ),
                                  ),
                                  GestureDetector(
                                    child: FaIcon(
                                      FontAwesomeIcons.angleDown,
                                      color: AppColors.kGreen,
                                    ),
                                    onTap: () {
                                      if (sessionBookValue > 1) {
                                        setState(() {
                                          sessionBookValue =
                                              sessionBookValue - 1;
                                        });
                                      }
                                    },
                                  ),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(AppStrings.kPrice + ' :',
                                  style: GoogleFonts.roboto(
                                    color: Colors.black,
                                  )),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: TextFormField(
                                  controller: _controllerPrice,
                                  focusNode: focusNodePrice,
                                  maxLines: 1,
                                  textInputAction: TextInputAction.next,
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.symmetric(vertical: 2),
                                    isDense: true,
                                  ),
                                  onFieldSubmitted: (_) {
                                    AppCommon.kChangeFocusNode(
                                        currentFocusNode: focusNodePrice,
                                        nextFocusNode: focusNodePrice,
                                        context: context);
                                  },
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(AppStrings.kClassLocation + ' :',
                                  style: GoogleFonts.roboto(
                                    color: Colors.black,
                                  )),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: TextFormField(
                                  controller: _controllerClassLocation,
                                  focusNode: focusNodeClassLocation,
                                  maxLines: 1,
                                  textInputAction: TextInputAction.done,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.symmetric(vertical: 2),
                                    isDense: true,
                                  ),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(AppStrings.kSessionName + ' :',
                                  style: GoogleFonts.roboto(
                                    color: Colors.black,
                                  )),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: TextFormField(
                                  controller: _controllerDiscription,
                                  focusNode: focusNodeDiscription,
                                  maxLines: 1,
                                  textInputAction: TextInputAction.done,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.symmetric(vertical: 2),
                                    isDense: true,
                                  ),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          GestureDetector(
                            onTap: () {
                              if (_formKey.currentState!.validate()) {
                                _formKey.currentState!.save();
                                progressDialog.show();
                                saveRequest();
                              }
                            },
                            child: Center(
                              child: Container(
                                width: 130,
                                height: 40,
                                decoration: BoxDecoration(
                                  color: AppColors.kGreen,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: Center(
                                  child: Text(
                                    AppStrings.kSave,
                                    style: GoogleFonts.roboto(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            },
          );
        });
  }

  Future<Null> _toSelectTime(BuildContext context) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: toSelectedTime,
    );
    if (picked != null)
      setState(() {
        toSelectedTime = picked;
        _hourTo = toSelectedTime.hour.toString();
        _minuteTo = toSelectedTime.minute.toString();
        _timeTo = _hourTo + ' : ' + _minuteTo;
        _controllerAvailableTimeTo.text = _timeTo;
        _controllerAvailableTimeTo.text = formatDate(
            DateTime(2019, 08, 1, toSelectedTime.hour, toSelectedTime.minute),
            [hh, ':', nn, " ", am]).toString();
      });
  }

  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: selectedTime,
    );
    if (picked != null)
      setState(() {
        selectedTime = picked;
        _hour = selectedTime.hour.toString();
        _minute = selectedTime.minute.toString();
        _time = _hour + ' : ' + _minute;
        _controllerAvailableTime.text = _time;
        _controllerAvailableTime.text = formatDate(
            DateTime(2019, 08, 1, selectedTime.hour, selectedTime.minute),
            [hh, ':', nn, " ", am]).toString();
      });
  }

  Future<void> saveRequest() async {
    try {
      final http.Response response = await http.post(
          Uri.parse(AppStrings.kAddSessionUrl),
          headers: <String, String>{
            AppStrings.kHeaderType: AppStrings.kHeaderValue
          },
          body: jsonEncode(<String, String>{
            'user_id':
                preferences.getString(AppStrings.kPrefUserIdKey).toString(),
            'session_name': _controllerDiscription.text,
            'category_id': '1',
            'time': _controllerAvailableTime.text,
            'day': 'Mon,Frie',
            'session_block': sessionBookValue.toString(),
            'price': _controllerPrice.text,
            'location': 'Ahnedabad, Ghatlodiya',
            'session_lat': '1.2569',
            'session_lng': '2.56989',
          }));

      var _responseData = jsonDecode(response.body);
      print('response from service ${response.body}');

      Navigator.of(context).pop();

      if (response.statusCode == 200) {
        if (_responseData['status'] == 'success') {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            title: AppStrings.kSuccess,
            desc: _responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              setState(() {
                _future = getManageSessionData();
              });
              Navigator.of(context).pop();
            },
          )..show();
        } else {
          AppCommon.kErrorDialog(
              context: context,
              errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
        }
      } else {
        AppCommon.kErrorDialog(
            context: context,
            errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
      }
    } catch (exception) {
      print('exception is $exception');
      Navigator.of(context).pop();
      AppCommon.kErrorDialog(
          context: context, errorMsg: 'Some error occur.html response getting');
    }
  }

  Future<ManageSessionModel?> getManageSessionData() async {
    preferences = await SharedPreferences.getInstance();
    try {
      final response = await http.post(
        Uri.parse(AppStrings.kManageSessionUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue,
        },
        body: jsonEncode(<String, String>{
          'user_id':
              preferences.getString(AppStrings.kPrefUserIdKey).toString(),
          // 'my_id': preferences.getString(AppStrings.kPrefUserIdKey).toString(),
          // 'device_token':
          // preferences.getString(AppStrings.kPrefDeviceToken).toString(),
          // 'device_type':
          // preferences.getString(AppStrings.kPrefDeviceType).toString(),
          // 'language': 'en',
        }),
      );

      var responseData = jsonDecode(response.body);
      print('response from chat data  $responseData');
      if (response.statusCode == 200) {
        if (responseData['status'] == 'success') {
          return ManageSessionModel.fromJson(responseData);
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {},
          )..show();
        }
        setState(() {
          showProgress = false;
          print('progress $showProgress');
        });
      } else {
        throw Exception('Failed to load data');
      }
    } catch (exception) {
      print('exception $exception');
    }
  }

  Future<void> getCategoryData() async {
    preferences = await SharedPreferences.getInstance();
    try {
      final response = await http.post(
        Uri.parse(AppStrings.kGetCategoryUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue,
        },
        body: jsonEncode(<String, String>{
          'user_id':
              preferences.getString(AppStrings.kPrefUserIdKey).toString(),
        }),
      );

      var responseData = jsonDecode(response.body);
      listCategoryData = responseData['data'];
      print('response from category data data  $responseData');
      List.generate(listCategoryData.length, (index) {
        listCategoryId.insert(index + 1, listCategoryData[index]['id']);
        listCategoryName.insert(
            index + 1, listCategoryData[index]['category_name']);
      });
      setState(() {});
    } catch (exception) {
      print('exception $exception');
    }
  }

  Future<void> getClass() async{
    preferences = await SharedPreferences.getInstance();
    try {
      final response = await http.post(
        Uri.parse(AppStrings.kGetClassUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue,
        },
        body: jsonEncode(<String, String>{
          'user_id':
          preferences.getString(AppStrings.kPrefUserIdKey).toString(),
        }),
      );

      var responseData = jsonDecode(response.body);
      listClassData = responseData['data'];
      print('response from class data data  $listClassData');
      List.generate(listClassData.length, (index) {
        listClassId.insert(index + 1, listClassData[index]['id']);
        listClassName.insert(
            index + 1, listClassData[index]['class']);
      });
      setState(() {});
    } catch (exception) {
      print('exception $exception');
    }
  }
}
