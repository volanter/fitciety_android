import 'package:ficiety/ui/notification_page.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

class AuthorisationRequestTrainerDone extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Function(bool) isReturn;

  AuthorisationRequestTrainerDone({required this.scaffoldKey,required this.isReturn});

  @override
  _AuthorisationRequestTrainerDoneState createState() => _AuthorisationRequestTrainerDoneState();
}

class _AuthorisationRequestTrainerDoneState extends State<AuthorisationRequestTrainerDone> {
  late bool isCurrentView;
  late String selectedProgram;
  List<String> listName = [];


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    listName.add('Mark Hemmings');
    listName.add('Jonny Bairstow');
    listName.add('Jonny Evans');
    listName.add('Crystiano Ronaldo');
    listName.add('Lionel Messi');
    listName.add('Mark Hemmings');
    listName.add('Jonny Bairstow');
    listName.add('Jonny Evans');
    listName.add('Crystiano Ronaldo');
    listName.add('Lionel Messi');
    isCurrentView = true;
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return isCurrentView?Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 120,
            color: Color(0xff006e6f),
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Padding(
                  padding: const EdgeInsets.only(
                    top: 10,
                  ),
                  child: Row(
                    children: [
                      GestureDetector(
                        child: Icon(Icons.arrow_back_sharp,color: Colors.white,)/*Image.asset(
                          AppStrings.kImgMenuBar,
                          width: 18,
                          height: 13,
                        )*/,
                        onTap: () {
                          widget.isReturn(true);
                          // if (widget.scaffoldKey.currentState!.isDrawerOpen) {
                          //   Navigator.of(context).pop();
                          // } else {
                          //   widget.scaffoldKey.currentState!.openDrawer();
                          // }
                        },
                      ),
                      // GestureDetector(
                      //   child: Image.asset(
                      //     AppStrings.kImgMenuBar,
                      //     width: 18,
                      //     height: 13,
                      //   ),
                      //   onTap: () {
                      //     if (widget.scaffoldKey.currentState!.isDrawerOpen) {
                      //       Navigator.of(context).pop();
                      //     } else {
                      //       widget.scaffoldKey.currentState!.openDrawer();
                      //     }
                      //   },
                      // ),
                      SizedBox(
                        width: 30,
                      ),
                      Text(
                        AppStrings.kAuthorisationRequest,
                        style: GoogleFonts.roboto(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      Spacer(),
                      GestureDetector(
                        onTap: (){
                          setState(() {
                            isCurrentView = false;
                          });
                        },
                        child: Image.asset(
                          AppStrings.kImgNotification,
                          width: 17.5,
                          height: 18.5,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(
                left: 15,
                right: 15,
              ),
              child: ListView.builder(
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: (){
                      // setState(() {
                      //   isCurrentView = false;
                      // });
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: Container(
                        height: 100.5,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5.0,),
                          border: Border.all(color: Color(0xff66b5b5b5),),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(top: 20,left: 10,right: 10,),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Container(
                                    child: CircleAvatar(
                                      backgroundImage: AssetImage(AppStrings.kImgGymMan,),
                                    ),
                                    width: 35,
                                    height: 35,
                                  ),
                                  SizedBox(width: 10,),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      RichText(
                                        text: TextSpan(
                                          text: listName[index],
                                          style: GoogleFonts.roboto(
                                            color: Color(0xff333333),fontWeight: FontWeight.w900,
                                            fontSize: 12,
                                          ),
                                          children: <TextSpan>[
                                            TextSpan(
                                              text: ' has \nrequested authorization.\nDo you want to \ngrant access?',

                                              // recognizer: TapGestureRecognizer()
                                              //   ..onTap = () => Navigator.of(context).pushNamed(
                                              //     AppStrings.kLoginScreen,
                                              //   ),
                                              style: GoogleFonts.roboto(
                                                color: Color(0xff333333),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  Spacer(),
                                  index==0?_pendingView():index==1?_allowDennyView():index==2?_formView():index==3?_pendingView():index==4?_allowDennyView():index==5?_formView():index==6?_pendingView():index==7?_allowDennyView():index==8?_formView():_pendingView(),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
                itemCount: 10,
                shrinkWrap: true,
                padding: EdgeInsets.zero,
                scrollDirection: Axis.vertical,

              ),
            ),
          )
        ],
      ),
    ):NotificationPage(scaffoldKey: widget.scaffoldKey,isReturn: (value){
      setState(() {
        isCurrentView = value;
      });
    },);
  }

  Widget _pendingView(){
    return  Center(
      child: Container(
        width: 90,
        height: 30,
        decoration: BoxDecoration(
          color: Color(0xffefb861),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Center(
          child:RichText(
            text: TextSpan(
              text: '',
              style: TextStyle(fontFamily: AppStrings.kFontAwsSolid,color: Colors.white,fontSize: 16.5,letterSpacing: 0.05,),
              children: <TextSpan>[
                TextSpan(
                  text: ' Pending',
                  // recognizer: TapGestureRecognizer()
                  //   ..onTap = () => Navigator.of(context).pushNamed(
                  //     AppStrings.kLoginScreen,
                  //   ),
                  style: GoogleFonts.roboto(
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _formView(){
    return Center(
      child: Container(
        width: 110,
        height: 30,
        decoration: BoxDecoration(
          color: Color(0xff006e6f),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Center(
          child:RichText(
            text: TextSpan(
              text: '',
              style: TextStyle(fontFamily: AppStrings.kFontAwsSolid,color: Colors.white,fontSize: 16.5,letterSpacing: 0.05,),
              children: <TextSpan>[
                TextSpan(
                  text: ' View Form',
                  // recognizer: TapGestureRecognizer()
                  //   ..onTap = () => Navigator.of(context).pushNamed(
                  //     AppStrings.kLoginScreen,
                  //   ),
                  style: GoogleFonts.roboto(
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }



  Widget _allowDennyView(){
    return Row(
      children: [
        Center(
          child: Container(
            width: 75,
            height: 30,
            decoration: BoxDecoration(
              color: Color(0xff5dba5d),
              borderRadius: BorderRadius.circular(5),
            ),
            child: Center(
              child:RichText(
                text: TextSpan(
                  text: '',
                  style: TextStyle(fontFamily: AppStrings.kFontAwsSolid,color: Colors.white,fontSize: 16.5,letterSpacing: 0.05,),
                  children: <TextSpan>[
                    TextSpan(
                      text: ' Allow',
                      // recognizer: TapGestureRecognizer()
                      //   ..onTap = () => Navigator.of(context).pushNamed(
                      //     AppStrings.kLoginScreen,
                      //   ),
                      style: GoogleFonts.roboto(
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        SizedBox(width: 10,),
        Center(
          child: Container(
            width: 75,
            height: 30,
            decoration: BoxDecoration(
              color: Color(0xffe45652),
              borderRadius: BorderRadius.circular(5),
            ),
            child: Center(
              child:RichText(
                text: TextSpan(
                  text: '',
                  style: TextStyle(fontFamily: AppStrings.kFontAwsSolid,color: Colors.white,fontSize: 16.5,letterSpacing: 0.05,),
                  children: <TextSpan>[
                    TextSpan(
                      text: ' Deny',
                      // recognizer: TapGestureRecognizer()
                      //   ..onTap = () => Navigator.of(context).pushNamed(
                      //     AppStrings.kLoginScreen,
                      //   ),
                      style: GoogleFonts.roboto(
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  _openDialog() {
    return showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, setState) {
              return Dialog(
                // insetPadding: EdgeInsets.symmetric(
                //   horizontal: 1.0,
                //   vertical: 2.0,
                // ),
                // title: Text(
                //   AppStrings.kSearchForTrainer,style: GoogleFonts.roboto(
                //   color: AppColors.kGreen,fontWeight: FontWeight.w900,
                // ),
                // ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                    10.0,
                  ),
                ),
                child: Container(
                  width: 320,
                  height: 380,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Container(
                          height: 150,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Color(0xff006e6f),
                            borderRadius: BorderRadius.only(topRight: Radius.circular(10.0),topLeft: Radius.circular(10.0)),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              GestureDetector(
                                child: Align(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: AppCommon.kIconBottomSheetTextViewWhite(
                                        fontName: AppStrings.kFontAwsRegular,
                                        content: ''),
                                  ),
                                  alignment: Alignment.topRight,
                                ),
                                onTap: (){
                                  Navigator.of(context).pop();
                                },
                              ),
                              Expanded(
                                child: Center(
                                  child: Text(
                                    'Can Mark Hemmings book a session\nor do they need to complete a\n consultation form?',
                                    textAlign: TextAlign.center,
                                    style: GoogleFonts.roboto(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          SizedBox(width: 30,),
                          AppCommon.kIconTrainerSearchTextView(
                              fontName: AppStrings.kFontAwsRegular,
                              content: ''),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Text(
                              'Book Session',
                              style: GoogleFonts.roboto(
                                color: Color(0xff333333),
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          SizedBox(width: 30,),
                          AppCommon.kIconTrainerSearchTextView(
                              fontName: AppStrings.kFontAwsRegular,
                              content: ''),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Text(
                              'Consultation',
                              style: GoogleFonts.roboto(
                                color: Color(0xff333333),
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          SizedBox(width: 30,),
                          AppCommon.kIconTrainerSearchTextView(
                              fontName: AppStrings.kFontAwsRegular,
                              content: ''),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Text(
                              'Both',
                              style: GoogleFonts.roboto(
                                color: Color(0xff333333),
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 30,),
                      Center(
                        child: Container(
                          width: 130,
                          height: 40,
                          child: ElevatedButton(
                            onPressed: () {},
                            style: ButtonStyle(
                                backgroundColor:
                                MaterialStateProperty.all<Color>(
                                    AppColors.kGreen)),
                            child: Text(
                              'Done',
                              style: GoogleFonts.roboto(
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        });
  }

}
