import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/models/authorisation_request_model.dart';
import 'package:ficiety/trainer_ui/authorisation_request_trainer_done.dart';
import 'package:ficiety/ui/notification_page.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:ficiety/utils/progressdialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:group_radio_button/group_radio_button.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class AuthorisationRequestTrainer extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  // final Function(bool) isReturn;

  AuthorisationRequestTrainer(
      {required this.scaffoldKey /*,required this.isReturn*/
      });

  @override
  _AuthorisationRequestTrainerState createState() =>
      _AuthorisationRequestTrainerState();
}

class _AuthorisationRequestTrainerState
    extends State<AuthorisationRequestTrainer> {
  late bool isCurrentView;
  late String selectedProgram;
  List<String> listName = [];
  late AuthorisationView authorisationView;
  late ProgressDialog progressDialog;
  late Future<AuthorisationRequestModel?> _future;
  late bool isShowLoader;

  String _specialistGroupValue = 'Book Session';

  List<String> _specialist = [
    'Book Session',
    'Consultation',
    'Both',
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    progressDialog = ProgressDialog(context, ProgressDialogType.Normal);
    progressDialog.setMessage(AppStrings.kLoadingMsg);
    _future = getData();
    isShowLoader = true;
    authorisationView = AuthorisationView.Dashboard;
    listName.add('Mark Hemmings');
    listName.add('Jonny Bairstow');
    listName.add('Jonny Evans');
    listName.add('Crystiano Ronaldo');
    listName.add('Lionel Messi');
    listName.add('Mark Hemmings');
    listName.add('Jonny Bairstow');
    listName.add('Jonny Evans');
    listName.add('Crystiano Ronaldo');
    listName.add('Lionel Messi');
    isCurrentView = true;
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return authorisationView == AuthorisationView.Dashboard
        ? Container(
            color: Colors.white,
            height: MediaQuery.of(context).size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 120,
                  color: Color(0xff006e6f),
                  child: SafeArea(
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Padding(
                        padding: const EdgeInsets.only(
                          top: 10,
                        ),
                        child: Row(
                          children: [
                            // GestureDetector(
                            //   child: Icon(Icons.arrow_back_sharp,color: Colors.white,)/*Image.asset(
                            //     AppStrings.kImgMenuBar,
                            //     width: 18,
                            //     height: 13,
                            //   )*/,
                            //   onTap: () {
                            //     widget.isReturn(true);
                            //     // if (widget.scaffoldKey.currentState!.isDrawerOpen) {
                            //     //   Navigator.of(context).pop();
                            //     // } else {
                            //     //   widget.scaffoldKey.currentState!.openDrawer();
                            //     // }
                            //   },
                            // ),
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              child: Image.asset(
                                AppStrings.kImgMenuBar,
                                width: 20,
                                height: 20,
                              ),
                              onTap: () {
                                if (widget
                                    .scaffoldKey.currentState!.isDrawerOpen) {
                                  Navigator.of(context).pop();
                                } else {
                                  widget.scaffoldKey.currentState!.openDrawer();
                                }
                              },
                            ),
                            SizedBox(
                              width: 30,
                            ),
                            Text(
                              AppStrings.kAuthorisationRequest,
                              style: GoogleFonts.roboto(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            Spacer(),
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () {
                                setState(() {
                                  // isCurrentView = false;
                                  authorisationView =
                                      AuthorisationView.Notification;
                                });
                              },
                              child: Image.asset(
                                AppStrings.kImgNotification,
                                width: 17.5,
                                height: 18.5,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(
                      left: 15,
                      right: 15,
                    ),
                    child: FutureBuilder<AuthorisationRequestModel?>(
                      future: _future,
                      builder: (BuildContext context,
                          AsyncSnapshot<AuthorisationRequestModel?> snapshot) {
                        if (!snapshot.hasData) {
                          return Center(child: Container(width:50,height:50,child: CircularProgressIndicator()));
                        } else {
                          return ListView.builder(
                            itemBuilder: (context, index) {
                              return GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                onTap: () {
                                  // setState(() {
                                  //   isCurrentView = false;
                                  // });
                                },
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 15),
                                  child: Container(
                                    height: 100.5,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(
                                        5.0,
                                      ),
                                      border: Border.all(
                                        color: Color(0xff66b5b5b5),
                                      ),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                        top: 20,
                                        left: 10,
                                        right: 10,
                                      ),
                                      child: Column(
                                        children: [
                                          Row(
                                            children: [
                                              Container(
                                                child: CircleAvatar(
                                                  backgroundImage: NetworkImage(
                                                    AppStrings.kImageUrl +
                                                        snapshot
                                                            .data!
                                                            .response[index]
                                                            .image,
                                                  ),
                                                ),
                                                width: 35,
                                                height: 35,
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Expanded(
                                                flex: 2,
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    RichText(
                                                      text: TextSpan(
                                                        text: snapshot
                                                            .data!
                                                            .response[index]
                                                            .fname,
                                                        style:
                                                            GoogleFonts.roboto(
                                                          color:
                                                              Color(0xff333333),
                                                          fontWeight:
                                                              FontWeight.w900,
                                                          fontSize: 12,
                                                        ),
                                                        children: <TextSpan>[
                                                          TextSpan(
                                                            text: snapshot
                                                                .data!
                                                                .response[index]
                                                                .message,

                                                            // recognizer: TapGestureRecognizer()
                                                            //   ..onTap = () => Navigator.of(context).pushNamed(
                                                            //     AppStrings.kLoginScreen,
                                                            //   ),
                                                            style: GoogleFonts
                                                                .roboto(
                                                              color: Color(
                                                                  0xff333333),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              // Spacer(),
                                              Expanded(
                                                flex: 1,
                                                child: Center(
                                                  child: Container(
                                                    width: 75,
                                                    height: 30,
                                                    decoration: BoxDecoration(
                                                      color: Color(0xff5dba5d),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5),
                                                    ),
                                                    child: GestureDetector(
                                                      behavior: HitTestBehavior
                                                          .translucent,
                                                      onTap: () {
                                                        progressDialog.show();
                                                        authRequest(
                                                            action: '1',
                                                            trainerId: snapshot
                                                                .data!
                                                                .response[index]
                                                                .userId);
                                                        // Future data = _openDialog();
                                                        // print('data is ${data.then((value) {
                                                        //   if(value == true)
                                                        //     setState(() {
                                                        //       // isCurrentView = false;
                                                        //       authorisationView = AuthorisationView.AuthorisationSecond;
                                                        //     });
                                                        // })}');
                                                      },
                                                      child: Center(
                                                        child: RichText(
                                                          text: TextSpan(
                                                            text: '',
                                                            style: TextStyle(
                                                              fontFamily: AppStrings
                                                                  .kFontAwsSolid,
                                                              color:
                                                                  Colors.white,
                                                              fontSize: 16.5,
                                                              letterSpacing:
                                                                  0.05,
                                                            ),
                                                            children: <
                                                                TextSpan>[
                                                              TextSpan(
                                                                text: ' Allow',
                                                                // recognizer: TapGestureRecognizer()
                                                                //   ..onTap = () => Navigator.of(context).pushNamed(
                                                                //     AppStrings.kLoginScreen,
                                                                //   ),
                                                                style:
                                                                    GoogleFonts
                                                                        .roboto(
                                                                  color: Colors
                                                                      .white,
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              GestureDetector(
                                                onTap: () {
                                                  progressDialog.show();
                                                  authRequest(
                                                      action: '2',
                                                      trainerId: snapshot
                                                          .data!
                                                          .response[index]
                                                          .userId);
                                                },
                                                child: Center(
                                                  child: Container(
                                                    width: 75,
                                                    height: 30,
                                                    decoration: BoxDecoration(
                                                      color: Color(0xffe45652),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5),
                                                    ),
                                                    child: Center(
                                                      child: RichText(
                                                        text: TextSpan(
                                                          text: '',
                                                          style: TextStyle(
                                                            fontFamily: AppStrings
                                                                .kFontAwsSolid,
                                                            color: Colors.white,
                                                            fontSize: 16.5,
                                                            letterSpacing: 0.05,
                                                          ),
                                                          children: <TextSpan>[
                                                            TextSpan(
                                                              text: ' Deny',
                                                              // recognizer: TapGestureRecognizer()
                                                              //   ..onTap = () => Navigator.of(context).pushNamed(
                                                              //     AppStrings.kLoginScreen,
                                                              //   ),
                                                              style: GoogleFonts
                                                                  .roboto(
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                            itemCount: snapshot.data!.response.length,
                            shrinkWrap: true,
                            padding: EdgeInsets.zero,
                            scrollDirection: Axis.vertical,
                          );
                        }
                      },
                    ),
                  ),
                )
              ],
            ),
          )
        : authorisationView == AuthorisationView.Notification
            ? NotificationPage(
                scaffoldKey: widget.scaffoldKey,
                isReturn: (value) {
                  setState(() {
                    authorisationView = AuthorisationView.Dashboard;
                  });
                },
              )
            : AuthorisationRequestTrainerDone(
                scaffoldKey: widget.scaffoldKey,
                isReturn: (value) {
                  setState(() {
                    authorisationView = AuthorisationView.Dashboard;
                  });
                });
  }

  _openDialog() {
    return showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, setState) {
              return Dialog(
                // insetPadding: EdgeInsets.symmetric(
                //   horizontal: 1.0,
                //   vertical: 2.0,
                // ),
                // title: Text(
                //   AppStrings.kSearchForTrainer,style: GoogleFonts.roboto(
                //   color: AppColors.kGreen,fontWeight: FontWeight.w900,
                // ),
                // ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                    10.0,
                  ),
                ),
                child: Container(
                  width: 320,
                  height: 380,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Container(
                          height: 150,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Color(0xff006e6f),
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(10.0),
                                topLeft: Radius.circular(10.0)),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                child: Align(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child:
                                        AppCommon.kIconBottomSheetTextViewWhite(
                                            fontName:
                                                AppStrings.kFontAwsRegular,
                                            content: ''),
                                  ),
                                  alignment: Alignment.topRight,
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                              Expanded(
                                child: Center(
                                  child: Text(
                                    'Can Mark Hemmings book a session\nor do they need to complete a\n consultation form?',
                                    textAlign: TextAlign.center,
                                    style: GoogleFonts.roboto(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      // Row(
                      //   children: [
                      //     SizedBox(width: 30,),
                      //     AppCommon.kIconTrainerSearchTextView(
                      //         fontName: AppStrings.kFontAwsRegular,
                      //         content: ''),
                      //     SizedBox(
                      //       width: 10,
                      //     ),
                      //     Expanded(
                      //       child: Text(
                      //         'Book Session',
                      //         style: GoogleFonts.roboto(
                      //           color: Color(0xff333333),
                      //           fontSize: 14,
                      //           fontWeight: FontWeight.w500,
                      //         ),
                      //       ),
                      //     )
                      //   ],
                      // ),
                      // SizedBox(
                      //   height: 20,
                      // ),
                      // Row(
                      //   children: [
                      //     SizedBox(width: 30,),
                      //     AppCommon.kIconTrainerSearchTextView(
                      //         fontName: AppStrings.kFontAwsRegular,
                      //         content: ''),
                      //     SizedBox(
                      //       width: 10,
                      //     ),
                      //     Expanded(
                      //       child: Text(
                      //         'Consultation',
                      //         style: GoogleFonts.roboto(
                      //           color: Color(0xff333333),
                      //           fontSize: 14,
                      //           fontWeight: FontWeight.w500,
                      //         ),
                      //       ),
                      //     )
                      //   ],
                      // ),
                      // SizedBox(
                      //   height: 20,
                      // ),
                      // Row(
                      //   children: [
                      //     SizedBox(width: 30,),
                      //     AppCommon.kIconTrainerSearchTextView(
                      //         fontName: AppStrings.kFontAwsRegular,
                      //         content: ''),
                      //     SizedBox(
                      //       width: 10,
                      //     ),
                      //     Expanded(
                      //       child: Text(
                      //         'Both',
                      //         style: GoogleFonts.roboto(
                      //           color: Color(0xff333333),
                      //           fontSize: 14,
                      //           fontWeight: FontWeight.w500,
                      //         ),
                      //       ),
                      //     )
                      //   ],
                      // ),
                      RadioGroup<String>.builder(
                        groupValue: _specialistGroupValue,
                        onChanged: (value) => setState(() {
                          _specialistGroupValue = value;
                        }),
                        items: _specialist,
                        itemBuilder: (item) => RadioButtonBuilder(
                          item,
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Center(
                        child: Container(
                          width: 130,
                          height: 40,
                          child: ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).pop(true);
                              // setState((){});
                            },
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        AppColors.kGreen)),
                            child: Text(
                              'Done',
                              style: GoogleFonts.roboto(
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        });
  }

  Future<AuthorisationRequestModel?> getData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    print('trainer id is ${preferences.getString(AppStrings.kPrefUserIdKey).toString()}');

    // try {
    //   String data =
    //       await DefaultAssetBundle.of(context).loadString('assets/auth.json');
    //   final jsonResult = jsonDecode(data);
    //   return AuthorisationRequestModel.fromJson(jsonResult);
    // } catch (exception) {
    //   print('exception $exception');
    // }


    try{
      final response = await http.post(
        Uri.parse(AppStrings.kGetAuthRequestReportUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue,
        },
        body: jsonEncode(<String, String>{
          'trainer_id': preferences.getString(AppStrings.kPrefUserIdKey).toString(),
        }),
      );
      var responseData = jsonDecode(response.body);
      setState(() {
        isShowLoader = false;
      });
      print('response from auth list report $responseData');
      if (response.statusCode == 200) {
        if (responseData['STATUS'] == 'true') {
          return AuthorisationRequestModel.fromJson(responseData);
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              Navigator.of(context).pop();
            },
          )..show();
        }
      } else {
        throw Exception('Failed to load data');
      }
    }catch (exception){
      print('exception $exception');
    }

  }

  Future<void> authRequest({required String action, required trainerId}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    String jsonBody = jsonEncode(<String, String>{
      'user_id': preferences.getString(AppStrings.kPrefUserIdKey).toString(),
      'trainer_id': trainerId,
      'action': action,
    });

    print('json parms $jsonBody');

    try {
      final http.Response response = await http.post(
        Uri.parse(AppStrings.kAuthRequestActionUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue
        },
        body: jsonBody,
      );

      var _responseData = jsonDecode(response.body);
      print('response from service ${response.body}');

      Navigator.of(context).pop();

      if (response.statusCode == 200) {
        if (_responseData['status'] == 'success') {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            title: AppStrings.kSuccess,
            desc: _responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              setState(() {
                _future = getData();
              });
            },
          )..show();
        } else {
          AppCommon.kErrorDialog(
              context: context,
              errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
        }
      } else {
        AppCommon.kErrorDialog(
            context: context,
            errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
      }
    } catch (exception) {
      print('exception is $exception');
      Navigator.of(context).pop();
      AppCommon.kErrorDialog(
          context: context, errorMsg: 'Some error occur.html response getting');
    }
  }
}

enum AuthorisationView {
  Notification,
  Dashboard,
  AuthorisationSecond,
}
