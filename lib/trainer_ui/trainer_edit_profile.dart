import 'package:ficiety/ui/view_summary.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:group_radio_button/group_radio_button.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

class TrainerEditProfile extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  TrainerEditProfile({required this.scaffoldKey});

  @override
  _TrainerEditProfileState createState() => _TrainerEditProfileState();
}

class _TrainerEditProfileState extends State<TrainerEditProfile> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _controllerFirstName =
      TextEditingController(text: 'Mark');
  TextEditingController _controllerLastName =
      TextEditingController(text: 'Hemmings');
  TextEditingController _controllerEmailAddress =
      TextEditingController(text: 'Mark@gmail.com');
  TextEditingController _controllerPhoneNumber =
      TextEditingController(text: '1234567890');
  TextEditingController _controllerPassword =
      TextEditingController(text: '123456');
  TextEditingController _controllerGender = TextEditingController(text: 'Male');
  TextEditingController _controllerDateOfBirth =
      TextEditingController(text: '15/03/1978');
  TextEditingController _controllerLocation =
  TextEditingController(text: 'Hidden Ridge Road.');
  TextEditingController _controllerAbout =
  TextEditingController(text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididuntut labore et dolore magna aliqua. ');
  TextEditingController _controllerAreaOfExpertise =
  TextEditingController(text: 'Aerobic');

  late FocusNode focusNodeFirstName;
  late FocusNode focusNodeLastName;
  late FocusNode focusNodeEmailAddress;
  late FocusNode focusNodePhoneNumber;
  late FocusNode focusNodePassword;
  late FocusNode focusNodeGender;
  late FocusNode focusNodeDateOfBirth;
  late FocusNode focusNodeLocation;
  late FocusNode focusNodeAbout;
  late FocusNode focusNodeAreaOfExpertise;
  late bool isPasswordShow;

  DateTime selectedDate = DateTime.now();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isPasswordShow = false;
    focusNodeFirstName = FocusNode();
    focusNodeLastName = FocusNode();
    focusNodeEmailAddress = FocusNode();
    focusNodePhoneNumber = FocusNode();
    focusNodePassword = FocusNode();
    focusNodeGender = FocusNode();
    focusNodeDateOfBirth = FocusNode();
    focusNodeLocation = FocusNode();
    focusNodeAbout = FocusNode();
    focusNodeAreaOfExpertise = FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff024346),
    ));
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            clipBehavior: Clip.none,
            fit: StackFit.loose,
            children: [
              Container(
                height: 190,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage(
                        AppStrings.kImgTrainerBg,
                      )),
                ),
                // color: Color(0xff006e6f),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SafeArea(
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Padding(
                          padding: const EdgeInsets.only(
                            top: 10,
                          ),
                          child: Row(
                            children: [
                              GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                child: Image.asset(
                                  AppStrings.kImgMenuBar,
                                  width: 20,
                                  height: 20,
                                ),
                                onTap: () {
                                  if (widget
                                      .scaffoldKey.currentState!.isDrawerOpen) {
                                    Navigator.of(context).pop();
                                  } else {
                                    widget.scaffoldKey.currentState!
                                        .openDrawer();
                                  }
                                },
                              ),
                              SizedBox(
                                width: 30,
                              ),
                              Text(
                                AppStrings.kEditProfile,
                                style: GoogleFonts.roboto(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              Spacer(),
                              Text(
                                AppStrings.kSave,
                                style: GoogleFonts.roboto(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                  top: 130,
                  left: 110,
                  child: Center(
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.white,
                          ),
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            image: AssetImage(
                              AppStrings.kImgGymMan,
                            ),
                          )),
                      width: 150,
                      height: 100,
                    ),
                  )),
              Positioned(
                  top: 130,
                  right: 180,
                  child: Center(
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.circle,
                      ),
                      child: Icon(
                        Icons.edit_sharp,
                        size: 12,
                        color: Colors.black,
                      ),
                      width: 20,
                      height: 20,
                    ),
                  )),
            ],
          ),
          SizedBox(height: 70),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.all(10.0),
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Form(
                    key: _formKey,
                    // autovalidateMode: AutovalidateMode.always,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                              flex: 1,
                              child: TextFormField(
                                controller: _controllerFirstName,
                                focusNode: focusNodeFirstName,
                                maxLines: 1,
                                style: AppCommon.kSearchTextStyle(),
                                textInputAction: TextInputAction.next,
                                keyboardType: TextInputType.text,
                                validator: (value) {
                                  return value!.isEmpty
                                      ? AppStrings.kErrorMSgFirstName
                                      : null;
                                },
                                decoration: InputDecoration(
                                  labelText: AppStrings.kFirstName,
                                  labelStyle:
                                      AppCommon.kProfileLabelTextStyle(),
                                  contentPadding:
                                      EdgeInsets.symmetric(vertical: 2),
                                  isDense: true,
                                  suffix: GestureDetector(
                                      behavior: HitTestBehavior.translucent,
                                      onTap: () {
                                        setState(() {
                                          _controllerFirstName.clear();
                                        });
                                        focusNodeFirstName.unfocus();
                                      },
                                      child: Icon(
                                        Icons.clear_outlined,
                                      )),
                                ),
                                onFieldSubmitted: (_) {
                                  AppCommon.kChangeFocusNode(
                                      currentFocusNode: focusNodeFirstName,
                                      nextFocusNode: focusNodeLastName,
                                      context: context);
                                },
                              ),
                            ),
                            SizedBox(
                              width: 30,
                            ),
                            Expanded(
                              flex: 1,
                              child: TextFormField(
                                controller: _controllerLastName,
                                style: AppCommon.kSearchTextStyle(),
                                focusNode: focusNodeLastName,
                                maxLines: 1,
                                textInputAction: TextInputAction.next,
                                keyboardType: TextInputType.text,
                                validator: (value) {
                                  return value!.isEmpty
                                      ? AppStrings.kErrorMSgLastName
                                      : null;
                                },
                                decoration: InputDecoration(
                                  labelText: AppStrings.kLastName,
                                  labelStyle:
                                      AppCommon.kProfileLabelTextStyle(),
                                  contentPadding:
                                      EdgeInsets.symmetric(vertical: 2),
                                  isDense: true,
                                  suffix: GestureDetector(
                                      behavior: HitTestBehavior.translucent,
                                      onTap: () {
                                        setState(() {
                                          _controllerLastName.clear();
                                        });
                                        focusNodeLastName.unfocus();
                                      },
                                      child: Icon(
                                        Icons.clear_outlined,
                                      )),
                                ),
                                onFieldSubmitted: (_) {
                                  AppCommon.kChangeFocusNode(
                                      currentFocusNode: focusNodeLastName,
                                      nextFocusNode: focusNodeEmailAddress,
                                      context: context);
                                },
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: _controllerEmailAddress,
                          focusNode: focusNodeEmailAddress,
                          style: AppCommon.kSearchTextStyle(),
                          maxLines: 1,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) {
                            return value!.isEmpty
                                ? AppStrings.kErrorMSgEmail
                                : null;
                          },
                          decoration: InputDecoration(
                            labelText: AppStrings.kEmail,
                            labelStyle: AppCommon.kProfileLabelTextStyle(),
                            contentPadding: EdgeInsets.symmetric(vertical: 2),
                            isDense: true,
                            suffix: GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                onTap: () {
                                  setState(() {
                                    _controllerEmailAddress.clear();
                                  });
                                  focusNodeEmailAddress.unfocus();
                                },
                                child: Icon(
                                  Icons.clear_outlined,
                                )),
                          ),
                          onFieldSubmitted: (_) {
                            AppCommon.kChangeFocusNode(
                                currentFocusNode: focusNodeEmailAddress,
                                nextFocusNode: focusNodePhoneNumber,
                                context: context);
                          },
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: _controllerPhoneNumber,
                          focusNode: focusNodePhoneNumber,
                          maxLines: 1,
                          style: AppCommon.kSearchTextStyle(),
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.number,
                          validator: (value) {
                            return value!.isEmpty
                                ? AppStrings.kErrorMsgPhoneNo
                                : null;
                          },
                          decoration: InputDecoration(
                            labelText: AppStrings.kPhoneNumber,
                            labelStyle: AppCommon.kProfileLabelTextStyle(),
                            contentPadding: EdgeInsets.symmetric(vertical: 2),
                            isDense: true,
                            suffix: GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                onTap: () {
                                  setState(() {
                                    _controllerPhoneNumber.clear();
                                  });
                                  focusNodePhoneNumber.unfocus();
                                },
                                child: Icon(
                                  Icons.clear_outlined,
                                )),
                          ),
                          onFieldSubmitted: (_) {
                            AppCommon.kChangeFocusNode(
                                currentFocusNode: focusNodePhoneNumber,
                                nextFocusNode: focusNodePassword,
                                context: context);
                          },
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: _controllerPassword,
                          focusNode: focusNodePassword,
                          maxLines: 1,
                          style: AppCommon.kSearchTextStyle(),
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          obscureText: isPasswordShow ? false : true,
                          validator: (value) {
                            return value!.isEmpty
                                ? AppStrings.kErrorMsgPassword
                                : null;
                          },
                          decoration: InputDecoration(
                            labelText: AppStrings.kPassword,
                            labelStyle: AppCommon.kProfileLabelTextStyle(),
                            contentPadding: EdgeInsets.symmetric(vertical: 2),
                            isDense: true,
                            suffix: GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                onTap: () {
                                  setState(() {
                                    isPasswordShow = !isPasswordShow;
                                  });
                                },
                                child: Icon(
                                  isPasswordShow
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                )),
                          ),
                          onFieldSubmitted: (_) {
                            AppCommon.kChangeFocusNode(
                                currentFocusNode: focusNodePassword,
                                nextFocusNode: focusNodeGender,
                                context: context);
                          },
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: _controllerGender,
                          focusNode: focusNodeGender,
                          maxLines: 1,
                          style: AppCommon.kSearchTextStyle(),
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          validator: (value) {
                            return value!.isEmpty
                                ? AppStrings.kErrorMsgGender
                                : null;
                          },
                          decoration: InputDecoration(
                            labelText: AppStrings.kGender,
                            labelStyle: AppCommon.kProfileLabelTextStyle(),
                            contentPadding: EdgeInsets.symmetric(vertical: 2),
                            isDense: true,
                            // suffix: GestureDetector(
                            //     onTap: () {
                            //     },
                            //     child: Icon(
                            //       isPasswordShow?Icons.visibility:Icons.visibility_off,
                            //     )),
                          ),
                          onFieldSubmitted: (_) {
                            AppCommon.kChangeFocusNode(
                                currentFocusNode: focusNodeGender,
                                nextFocusNode: focusNodeDateOfBirth,
                                context: context);
                          },
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: _controllerDateOfBirth,
                          focusNode: focusNodeDateOfBirth,
                          maxLines: 1,
                          style: AppCommon.kSearchTextStyle(),
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          validator: (value) {
                            return value!.isEmpty
                                ? AppStrings.kErrorMSgDob
                                : null;
                          },
                          decoration: InputDecoration(
                            labelText: AppStrings.kDateOfBirth,
                            labelStyle: AppCommon.kProfileLabelTextStyle(),
                            contentPadding: EdgeInsets.symmetric(vertical: 2),
                            isDense: true,
                            suffix: GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                onTap: () {
                                  setState(() {
                                    _controllerDateOfBirth.clear();
                                  });
                                  focusNodeDateOfBirth.unfocus();
                                },
                                child: Icon(
                                  Icons.clear_outlined,
                                )),
                          ),
                          onFieldSubmitted: (_) {
                            AppCommon.kChangeFocusNode(
                                currentFocusNode: focusNodeDateOfBirth,
                                nextFocusNode: focusNodeLocation,
                                context: context);
                          },
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: _controllerLocation,
                          focusNode: focusNodeLocation,
                          maxLines: 1,
                          style: AppCommon.kSearchTextStyle(),
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          // validator: (value) {
                          //   return value!.isEmpty
                          //       ? AppStrings.kErrorMSgDob
                          //       : null;
                          // },
                          decoration: InputDecoration(
                            labelText: AppStrings.kLocation,
                            labelStyle: AppCommon.kProfileLabelTextStyle(),
                            contentPadding: EdgeInsets.symmetric(vertical: 2),
                            isDense: true,
                            suffix: Icon(
                              Icons.location_on,
                              size: 14,
                              color: Colors.black,
                            ),
                          ),
                          onFieldSubmitted: (_) {
                            AppCommon.kChangeFocusNode(
                                currentFocusNode: focusNodeLocation,
                                nextFocusNode: focusNodeAbout,
                                context: context);
                          },
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: _controllerAbout,
                          focusNode: focusNodeAbout,
                          maxLines: 3,
                          style: AppCommon.kSearchTextStyle(),
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          // validator: (value) {
                          //   return value!.isEmpty
                          //       ? AppStrings.kErrorMSgDob
                          //       : null;
                          // },
                          decoration: InputDecoration(
                            labelText: AppStrings.kAbout,
                            labelStyle: AppCommon.kProfileLabelTextStyle(),
                            contentPadding: EdgeInsets.symmetric(vertical: 2),
                            isDense: true,
                          ),
                          onFieldSubmitted: (_) {
                            AppCommon.kChangeFocusNode(
                                currentFocusNode: focusNodeAbout,
                                nextFocusNode: focusNodeAreaOfExpertise,
                                context: context);
                          },
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: _controllerAreaOfExpertise,
                          focusNode: focusNodeAreaOfExpertise,
                          maxLines: 1,
                          style: AppCommon.kSearchTextStyle(),
                          textInputAction: TextInputAction.done,
                          keyboardType: TextInputType.text,
                          // validator: (value) {
                          //   return value!.isEmpty
                          //       ? AppStrings.kErrorMSgDob
                          //       : null;
                          // },
                          decoration: InputDecoration(
                            labelText: AppStrings.kAreaOfExpertise,
                            labelStyle: AppCommon.kProfileLabelTextStyle(),
                            contentPadding: EdgeInsets.symmetric(vertical: 2),
                            isDense: true,
                          ),
                          onFieldSubmitted: (_) {
                            focusNodeAreaOfExpertise.unfocus();
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _selectDate(BuildContext context) async {
    final DateTime picked = (await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(1800),
      lastDate: DateTime(2025),
    ))!;
    if (picked != selectedDate)
      setState(() {
        selectedDate = picked;
        print('selected data $selectedDate');
        _controllerDateOfBirth = TextEditingController(
            text: '${selectedDate.toLocal()}'.split(' ')[0]);
      });
  }
}
