import 'package:ficiety/graph_ui/bmi_generator.dart';
import 'package:ficiety/graph_ui/body_fat_graph.dart';
import 'package:ficiety/graph_ui/measurement_graph.dart';
import 'package:ficiety/ui/book_session_calender.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class TrainerProfile extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Function  (bool) isReturn;

  TrainerProfile({required this.scaffoldKey, required this.isReturn});

  @override
  _TrainerProfileState createState() => _TrainerProfileState();
}

class _TrainerProfileState extends State<TrainerProfile>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  late bool _isDetailsViewShow;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _isDetailsViewShow = true;
    _tabController = new TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return WillPopScope(
      onWillPop: _onBackPress,
      child: Container(
              color: Colors.white,
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Container(
                      height: 250,
                      child: Stack(
                        clipBehavior: Clip.none,
                        fit: StackFit.loose,
                        alignment: Alignment.topLeft,
                        children: [
                          Container(
                            height: 150,
                            color: Color(0xff006e6f),
                            child: Column(
                              children: [
                                SafeArea(
                                  child: Padding(
                                    padding: const EdgeInsets.all(20.0),
                                    child: Row(
                                      children: [
                                        GestureDetector(
                                          behavior: HitTestBehavior.translucent,
                                          child: Icon(Icons.arrow_back_sharp,color: Colors.white,)/*Image.asset(
                          AppStrings.kImgMenuBar,
                          width: 18,
                          height: 13,
                        )*/,
                                          onTap: () {
                                            widget.isReturn(true);
                                            // if (widget.scaffoldKey.currentState!.isDrawerOpen) {
                                            //   Navigator.of(context).pop();
                                            // } else {
                                            //   widget.scaffoldKey.currentState!.openDrawer();
                                            // }
                                          },
                                        ),
                                        // GestureDetector(
                                        //   child: Image.asset(
                                        //     AppStrings.kImgMenuBar,
                                        //     width: 18,
                                        //     height: 13,
                                        //   ),
                                        //   onTap: () {
                                        //     if (widget.scaffoldKey.currentState!
                                        //         .isDrawerOpen) {
                                        //       Navigator.of(context).pop();
                                        //     } else {
                                        //       widget.scaffoldKey.currentState!
                                        //           .openDrawer();
                                        //     }
                                        //   },
                                        // ),
                                        SizedBox(
                                          width: 30,
                                        ),
                                        Text(
                                          'Mark Hemming Profile',
                                          style: GoogleFonts.roboto(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                        Spacer(),
                                        Image.asset(
                                          AppStrings.kImgNotification,
                                          width: 17.5,
                                          height: 18.5,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            top: 110,
                            left: 30,
                            child: Row(
                              children: [
                                Container(
                                  width: 120,
                                  height: 120,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(
                                        5.0,
                                      ),
                                      border: Border.all(
                                        color: Colors.white,
                                      ),
                                      image: DecorationImage(
                                          fit: BoxFit.fill,
                                          image: AssetImage(
                                            AppStrings.kImgGymMan,
                                          ))),
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      height: 50,
                                    ),
                                    Text(
                                      'Mark Hemmings',
                                      style: GoogleFonts.roboto(
                                        color: Color(0xff006e6f),
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                    SizedBox(height: 5),
                                    Text(
                                      'Location',
                                      style: GoogleFonts.roboto(
                                        color: AppColors.kBlack,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 12,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      children: [
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Current Weight',
                                              style: GoogleFonts.roboto(
                                                color: AppColors.kBlack,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 13,
                                              ),
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Text(
                                              '78.5kg ',
                                              style: GoogleFonts.roboto(
                                                color: Color(0xff006e6f),
                                                fontWeight: FontWeight.bold,
                                                fontSize: 13,
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          width: 15,
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Current Calories',
                                              style: GoogleFonts.roboto(
                                                color: AppColors.kBlack,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 13,
                                              ),
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Text(
                                              '1,750 ',
                                              style: GoogleFonts.roboto(
                                                color: Color(0xff006e6f),
                                                fontWeight: FontWeight.bold,
                                                fontSize: 13,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 30,
                        right: 30,
                        top: 10,
                      ),
                      child: Text(
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                        softWrap: true,
                        overflow: TextOverflow.visible,
                        style: GoogleFonts.roboto(
                            color: AppColors.kBlack, fontSize: 12),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 30,
                        right: 30,
                        top: 10,
                      ),
                      child: Row(
                        children: [
                          // FaIcon(
                          //   FontAwesomeIcons.mapMarkerAlt,
                          //   size: 15,
                          //   color: AppColors.kGreen,
                          // ),
                          // SizedBox(
                          //   width: 15,
                          // ),
                          // Text(
                          //   "Top floor of Sainsbury’s carpark\n2 St James Avenue",
                          //   softWrap: true,
                          //   overflow: TextOverflow.visible,
                          //   style: GoogleFonts.roboto(
                          //     color: AppColors.kBlack,
                          //   ),
                          //   textAlign: TextAlign.left,
                          // ),
                          Spacer(),
                          Container(
                            width: 35,
                            height: 35,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                color: Colors.grey.shade400,
                              ),
                            ),
                            child: Align(
                              child: Icon(
                                Icons.call,
                                color: AppColors.kGreen,
                              ),
                              alignment: Alignment.center,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            width: 35,
                            height: 35,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                color: Colors.grey.shade400,
                              ),
                            ),
                            child: Align(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Image.asset(
                                  AppStrings.kImgMsg,
                                ),
                              ),
                              alignment: Alignment.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                    TabBar(
                      unselectedLabelColor: Colors.grey,
                      labelColor: Colors.black,
                      tabs: [
                        Tab(
                          text: AppStrings.kBMI,
                          // icon: Icon(Icons.people),
                        ),
                        Tab(
                          // icon: Icon(Icons.person),
                          text: AppStrings.kBodyFat,
                        ),
                        Tab(
                          // icon: Icon(Icons.person),
                          text: AppStrings.kMeasurements,
                        )
                      ],
                      controller: _tabController,
                      indicatorSize: TabBarIndicatorSize.tab,
                      labelPadding: EdgeInsets.symmetric(
                        vertical: 0,
                      ),
                    ),
                    Divider(
                      color: Colors.grey,
                    ),
                    Expanded(
                      child: Container(
                        child: TabBarView(
                          children: [
                            _bmiView(),
                            _bodyFatView(),
                            _measurementView()
                          ],
                          controller: _tabController,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )

    );
  }

  Future<bool> _onBackPress() async {
    if (_isDetailsViewShow) {
    } else {
      setState(() {
        _isDetailsViewShow = true;
      });
    }
    print('back call $_isDetailsViewShow');
    return false;
  }

  _bodyFatView() {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            Padding(
              padding:
                  const EdgeInsets.only(top: 10, left: 15, right: 15),
              child: Container(
                height: 330,
                // decoration: BoxDecoration(
                //   color: Colors.white,
                //   border: Border.all(
                //     color: Color(0xff006e6f),
                //     width: 2.0,
                //   ),
                //   borderRadius: BorderRadius.circular(
                //     10.0,
                //   ),
                // ),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: BodyFatGraph(),
                ),
              ),
            ),
            Divider(),
            SizedBox(
              height: 5,
            ),
            Text(
              'Your Current Body Fat',
              style: GoogleFonts.roboto(
                color: Color(0xff21a0a1),
                fontWeight: FontWeight.bold,
                fontSize: 14,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              'Body Fat',
              style: GoogleFonts.roboto(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 10,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              '50 %',
              style: GoogleFonts.roboto(
                color: Color(0xff006e6f),
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              'You lose 30% BMI',
              style: GoogleFonts.roboto(
                color: Color(0xffaaaaaa),
                fontWeight: FontWeight.w500,
                fontSize: 9,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20,right:20,bottom:5,top:10),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        'Date',
                        style: GoogleFonts.roboto(
                          color: Color(0xff006e6f),
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      Text(
                        'Weight',
                        style: GoogleFonts.roboto(
                          color: Color(0xff006e6f),
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      Text(
                        'Height',
                        style: GoogleFonts.roboto(
                          color: Color(0xff006e6f),
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      Text(
                        'Body Fat',
                        style: GoogleFonts.roboto(
                          color: Color(0xff006e6f),
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10,right:10,),
                    child: Divider(),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        '20/01/2020',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      ),
                      Text(
                        '78 LBS',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      ),
                      Text(
                        '170 CM',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      ),
                      Text(
                        '20%',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10,right:10,),
                    child: Divider(),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        '20/01/2020',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      ),
                      Text(
                        '65 LBS',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      ),
                      Text(
                        '185 CM',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      ),
                      Text(
                        '20%',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      )

                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10,right:10,),
                    child: Divider(),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _measurementView() {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Padding(
              padding: const EdgeInsets.only(
                top: 10,
                left: 15,
                right: 15,
              ),
              child: Container(
                height: 255,
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                    color: Color(0xff006e6f),
                    width: 2.0,
                  ),
                  borderRadius: BorderRadius.circular(
                    10.0,
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: MeasurementGraph(),
                ),
              ),
            ),
            Divider(),
            SizedBox(
              height: 5,
            ),
            Text(
              'Your Current Waist & Hips',
              style: GoogleFonts.roboto(
                color: Color(0xff21a0a1),
                fontWeight: FontWeight.bold,
                fontSize: 14,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              'Weight',
              style: GoogleFonts.roboto(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 10,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              '60 LBS',
              style: GoogleFonts.roboto(
                color: Color(0xff006e6f),
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              'You lose 2 LBS Weight',
              style: GoogleFonts.roboto(
                color: Color(0xffaaaaaa),
                fontWeight: FontWeight.w500,
                fontSize: 9,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20,right:20,bottom:5,top:10),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        'Date',
                        style: GoogleFonts.roboto(
                          color: Color(0xff006e6f),
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      Text(
                        'Waist',
                        style: GoogleFonts.roboto(
                          color: Color(0xff006e6f),
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      Text(
                        'Hips',
                        style: GoogleFonts.roboto(
                          color: Color(0xff006e6f),
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10,right:10,),
                    child: Divider(),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        '20/01/2020',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      ),
                      Text(
                        '78 LBS',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      ),
                      Text(
                        '170 CM',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10,right:10,),
                    child: Divider(),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        '20/01/2020',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      ),
                      Text(
                        '65 LBS',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      ),
                      Text(
                        '185 CM',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10,right:10,),
                    child: Divider(),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  _bmiView() {
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            Padding(
              padding:
                  const EdgeInsets.only(top: 10, left: 10, right: 10,),
              child: Container(
                height: 300,
                // decoration: BoxDecoration(
                //   color: Colors.white,
                //   border: Border.all(
                //     color: Color(0xff006e6f),
                //     width: 2.0,
                //   ),
                //   borderRadius: BorderRadius.circular(
                //     10.0,
                //   ),
                // ),
                child: BMIGenerator(),
              ),
            ),
            Divider(),
            SizedBox(
              height: 5,
            ),
            Text(
              'Your Current BMI',
              style: GoogleFonts.roboto(
                color: Color(0xff21a0a1),
                fontWeight: FontWeight.bold,
                fontSize: 14,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              'BMI',
              style: GoogleFonts.roboto(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 10,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              '20 %',
              style: GoogleFonts.roboto(
                color: Color(0xff006e6f),
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              'You lose 80% BMI',
              style: GoogleFonts.roboto(
                color: Color(0xffaaaaaa),
                fontWeight: FontWeight.w500,
                fontSize: 9,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20,right:20,bottom:5,top:10),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        'Date',
                        style: GoogleFonts.roboto(
                          color: Color(0xff006e6f),
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      Text(
                        'Weight',
                        style: GoogleFonts.roboto(
                          color: Color(0xff006e6f),
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      Text(
                        'Height',
                        style: GoogleFonts.roboto(
                          color: Color(0xff006e6f),
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      Text(
                        'BMI',
                        style: GoogleFonts.roboto(
                          color: Color(0xff006e6f),
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10,right:10,),
                    child: Divider(),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        '20/01/2020',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      ),
                      Text(
                        '78 LBS',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      ),
                      Text(
                        '170 CM',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      ),
                      Text(
                        '20%',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10,right:10,),
                    child: Divider(),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        '20/01/2020',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      ),
                      Text(
                        '65 LBS',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      ),
                      Text(
                        '185 CM',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      ),
                      Text(
                        '20%',
                        style: GoogleFonts.roboto(
                          color: Color(0xff333333),
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      )

                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10,right:10,),
                    child: Divider(),
                  ),
                ],
              ),
            ),

          ],
        ),
      ),
    );
  }
}
