import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:ficiety/utils/progressdialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class PaymentDetails extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  PaymentDetails({required this.scaffoldKey});


  @override
  _PaymentDetailsState createState() => _PaymentDetailsState();
}

class _PaymentDetailsState extends State<PaymentDetails> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _controllerBankAccountName = TextEditingController();
  TextEditingController _controllerBankAccountNumbers = TextEditingController();
  TextEditingController _controllerIFSCCode = TextEditingController();
  TextEditingController _controllerPayPalEmail = TextEditingController();

  late FocusNode focusNodeBankAccountName;
  late FocusNode focusNodeBankAccountNumbers;
  late FocusNode focusNodeIFSCCode;
  late FocusNode focusNodePayPalEmail;
  late ProgressDialog progressDialog;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    progressDialog = ProgressDialog(context, ProgressDialogType.Normal);
    progressDialog.setMessage(AppStrings.kLoadingMsg);
    focusNodeBankAccountName = FocusNode();
    focusNodeBankAccountNumbers = FocusNode();
    focusNodeIFSCCode = FocusNode();
    focusNodePayPalEmail = FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: Scaffold.of(context).appBarMaxHeight,
            color: Color(0xff006e6f),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SafeArea(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        top: 10,
                      ),
                      child: Row(
                        children: [
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: Image.asset(
                              AppStrings.kImgMenuBar,
                              width: 20,
                              height: 20,
                            ),
                            onTap: () {
                              if (widget
                                  .scaffoldKey.currentState!.isDrawerOpen) {
                                Navigator.of(context).pop();
                              } else {
                                widget.scaffoldKey.currentState!.openDrawer();
                              }
                            },
                          ),
                          SizedBox(
                            width: 30,
                          ),
                          Text(
                            AppStrings.kPaymentDetails,
                            style: GoogleFonts.roboto(
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Spacer(),
                          Image.asset(
                            AppStrings.kImgNotification,
                            width: 17.5,
                            height: 18.5,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height:30),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.all(10.0),
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.only(top: 5,right: 15,left: 15,bottom: 10,),
                  child: Form(
                    key: _formKey,
                    // autovalidateMode: AutovalidateMode.always,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          AppStrings.kBankDetails,
                          style: GoogleFonts.roboto(
                            color: Color(0xff006e70),
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 20,),
                        TextFormField(
                          controller: _controllerBankAccountName,
                          focusNode: focusNodeBankAccountName,
                          style: AppCommon.kSearchTextStyle(),
                          maxLines: 1,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          validator: (value) {
                            return value!.isEmpty
                                ? AppStrings.kErrorMSgBankAccountName
                                : null;
                          },
                          decoration: InputDecoration(
                            labelText: AppStrings.kBankAccountName,
                            labelStyle:
                            AppCommon.kProfileLabelTextStyle(),
                            contentPadding:
                            EdgeInsets.symmetric(vertical: 2),
                            isDense: true,
                          ),
                          onFieldSubmitted: (_) {
                            AppCommon.kChangeFocusNode(
                                currentFocusNode: focusNodeBankAccountName,
                                nextFocusNode: focusNodeBankAccountNumbers,
                                context: context);
                          },
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: _controllerBankAccountNumbers,
                          focusNode: focusNodeBankAccountNumbers,
                          style: AppCommon.kSearchTextStyle(),
                          maxLines: 1,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          validator: (value) {
                            return value!.isEmpty
                                ? AppStrings.kErrorMSgBankAccountNumber
                                : null;
                          },
                          decoration: InputDecoration(
                            labelText: AppStrings.kBankAccountNumbers,
                            labelStyle:
                            AppCommon.kProfileLabelTextStyle(),
                            contentPadding:
                            EdgeInsets.symmetric(vertical: 2),
                            isDense: true,
                          ),
                          onFieldSubmitted: (_) {
                            AppCommon.kChangeFocusNode(
                                currentFocusNode: focusNodeBankAccountNumbers,
                                nextFocusNode: focusNodeIFSCCode,
                                context: context);
                          },
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: _controllerIFSCCode,
                          focusNode: focusNodeIFSCCode,
                          style: AppCommon.kSearchTextStyle(),
                          maxLines: 1,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          validator: (value) {
                            return value!.isEmpty
                                ? AppStrings.kErrorMSgIfScCode
                                : null;
                          },
                          decoration: InputDecoration(
                            labelText: AppStrings.kIFSCCode,
                            labelStyle:
                            AppCommon.kProfileLabelTextStyle(),
                            contentPadding:
                            EdgeInsets.symmetric(vertical: 2),
                            isDense: true,
                          ),
                            onFieldSubmitted: (_) {
                              AppCommon.kChangeFocusNode(
                                  currentFocusNode: focusNodeIFSCCode,
                                  nextFocusNode: focusNodePayPalEmail,
                                  context: context);
                            },
                        ),
                        SizedBox(height: 20,),
                        Text(
                          AppStrings.kPaymentDetails,
                          style: GoogleFonts.roboto(
                            color: Color(0xff006e70),
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 20,),
                        TextFormField(
                          controller: _controllerPayPalEmail,
                          focusNode: focusNodePayPalEmail,
                          style: AppCommon.kSearchTextStyle(),
                          maxLines: 1,
                          textInputAction: TextInputAction.done,
                          keyboardType: TextInputType.text,
                          validator: (value) {
                            return value!.isEmpty
                                ? AppStrings.kErrorMSgPayPalEmail
                                : null;
                          },
                          decoration: InputDecoration(
                            labelText: AppStrings.kPayPalEmail,
                            labelStyle:
                            AppCommon.kProfileLabelTextStyle(),
                            contentPadding:
                            EdgeInsets.symmetric(vertical: 2),
                            isDense: true,
                          ),
                          onFieldSubmitted: (_) {
                            focusNodePayPalEmail.unfocus();
                          },
                        ),
                        SizedBox(height: 50,),
                        Align(
                          alignment: Alignment.bottomLeft,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10,
                              right: 10,),
                            child: Container(
                              width: double.infinity,
                              height: 34.5,
                              child: ElevatedButton(
                                onPressed: () {
                                  if(_formKey.currentState!.validate()){
                                    _formKey.currentState!.save();
                                    progressDialog.show();
                                    savePaymentDetails();
                                  }
                                  // print('view profile clicked');
                                  // widget.callback(AppStrings.kTrainerDetails);
                                },
                                style: ButtonStyle(
                                    backgroundColor:
                                    MaterialStateProperty.all<Color>(AppColors.kGreen)),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    AppStrings.kSave,
                                    style: GoogleFonts.roboto(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> savePaymentDetails() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();

    String jsonBody = jsonEncode(<String, String>{
      'trainer_id':
      preferences.getString(AppStrings.kPrefUserIdKey).toString(),
      'account_number': _controllerBankAccountNumbers.text,
      'account_name': _controllerBankAccountName.text,
      'ifsc': _controllerIFSCCode.text,
      'paypal_email': _controllerIFSCCode.text,
    });

    print('json parms $jsonBody');

    try {
      final http.Response response = await http.post(
        Uri.parse(AppStrings.kPaymentDetailsUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue
        },
        body: jsonBody,);

      var _responseData = jsonDecode(response.body);
      print('response from service ${response.body}');

      Navigator.of(context).pop();

      if (response.statusCode == 200) {
        if (_responseData['status'] == 'success') {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            title: AppStrings.kSuccess,
            desc: _responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () {},
          )..show();
        } else {
          AppCommon.kErrorDialog(
              context: context,
              errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
        }
      } else {
        AppCommon.kErrorDialog(
            context: context,
            errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
      }
    } catch (exception) {
      print('exception is $exception');
      Navigator.of(context).pop();
      AppCommon.kErrorDialog(
          context: context, errorMsg: 'Some error occur.html response getting');
    }
  }
}
