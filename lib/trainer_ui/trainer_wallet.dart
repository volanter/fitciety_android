import 'package:ficiety/trainer_ui/trainer_payment_history.dart';
import 'package:ficiety/ui/order_summary.dart';
import 'package:ficiety/ui/success_payment.dart';
import 'package:ficiety/ui/user_exercise_program.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class TrainerWallet extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  TrainerWallet({required this.scaffoldKey});

  @override
  _TrainerWalletState createState() => _TrainerWalletState();
}

class _TrainerWalletState extends State<TrainerWallet> {
  late bool isWalletView;
  late String selectedProgram;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isWalletView = true;
  }

  @override
  Widget build(BuildContext context) {
    return isWalletView
        ? Container(
            color: Colors.white,
            height: MediaQuery.of(context).size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 230,
                  color: Color(0xff006e6f),
                  child: SafeArea(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Padding(
                            padding: const EdgeInsets.only(
                              top: 10,
                            ),
                            child: Row(
                              children: [
                                GestureDetector(
                                  child: Image.asset(
                                    AppStrings.kImgMenuBar,
                                    width: 18,
                                    height: 13,
                                  ),
                                  onTap: () {
                                    if (widget.scaffoldKey.currentState!
                                        .isDrawerOpen) {
                                      Navigator.of(context).pop();
                                    } else {
                                      widget.scaffoldKey.currentState!
                                          .openDrawer();
                                    }
                                  },
                                ),
                                SizedBox(
                                  width: 30,
                                ),
                                Text(
                                  AppStrings.kWallet,
                                  style: GoogleFonts.roboto(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                Spacer(),
                                Image.asset(
                                  AppStrings.kImgNotification,
                                  width: 17.5,
                                  height: 18.5,
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          '£1000',
                          style: GoogleFonts.roboto(
                            color: Colors.white,
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          'Total Balance',
                          style: GoogleFonts.roboto(
                            color: Colors.white,
                            fontSize: 9,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        GestureDetector(
                          onTap: () {
                            _openDialog();
                          },
                          child: Container(
                            width: 150,
                            height: 27,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(
                                5.0,
                              ),
                            ),
                            child: Center(
                              child: Text(
                                'Request For Payment',
                                style: GoogleFonts.roboto(
                                  color: Color(0xff006e6f),
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 20,
                    left: 30,
                    right: 30,
                  ),
                  child: Text(
                    'Wallet History',
                    style: GoogleFonts.roboto(
                      color: Color(0xff026f72),
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(
                      left: 30,
                      right: 30,
                    ),
                    child: ListView.builder(
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () {
                            setState(() {
                              isWalletView = false;
                            });
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(top: 15),
                            child: Container(
                              height: 60.5,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                  5.0,
                                ),
                                border: Border.all(
                                  color: Color(0xff66b5b5b5),
                                ),
                              ),
                              child: ListTile(
                                leading: Container(
                                  child: Align(
                                      alignment: Alignment.bottomCenter,
                                      child: AppCommon.kIconTextView20(
                                          fontName: AppStrings.kFontAwsSolid,
                                          content: '')),
                                  width: 30,
                                  height: 30,
                                  decoration: BoxDecoration(
                                    color: Color(0xff006e70),
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                title: Text(
                                  'Recived Money From Mark Hemming',
                                  style: GoogleFonts.roboto(
                                    color: Color(0xff333333),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12,
                                  ),
                                ),
                                subtitle: Text(
                                  'For Online Training',
                                  style: GoogleFonts.roboto(
                                    color: Color(0xff333333),
                                    fontSize: 10,
                                  ),
                                ),
                                trailing: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      '£100',
                                      style: GoogleFonts.roboto(
                                        color: Color(0xff006e70),
                                        fontSize: 10,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 3,
                                    ),
                                    Text(
                                      'Mar 25',
                                      style: GoogleFonts.roboto(
                                        color: Color(0xff006e70),
                                        fontSize: 10,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                      itemCount: 10,
                      shrinkWrap: true,
                      padding: EdgeInsets.zero,
                      scrollDirection: Axis.vertical,
                    ),
                  ),
                )
              ],
            ),
          )
        : TrainerPaymentHistory(scaffoldKey: widget.scaffoldKey);
  }

  _openDialog() {
    return showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, setState) {
              return Dialog(
                insetPadding: EdgeInsets.symmetric(
                  horizontal: 2.0,
                  vertical: 2.0,
                ),
                // title: Text(
                //   AppStrings.kSearchForTrainer,style: GoogleFonts.roboto(
                //   color: AppColors.kGreen,fontWeight: FontWeight.w900,
                // ),
                // ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                    10.0,
                  ),
                ),
                child: Container(
                  width: 380,
                  height: 500,
                  padding: EdgeInsets.all(
                    20.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Container(
                          height: 150,
                          width: 150,
                          decoration: BoxDecoration(
                            color: Color(0xff074748),
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                'Trainer',
                                style: GoogleFonts.roboto(
                                  color: Colors.white,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Text(
                                '£ 8.99',
                                style: GoogleFonts.roboto(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                'Per Month',
                                style: GoogleFonts.roboto(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        'Trainer',
                        style: GoogleFonts.roboto(
                          color: Color(0xff026f72),
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          AppCommon.kIconTrainerSearchTextView(
                              fontName: AppStrings.kFontAwsRegular,
                              content: ''),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Text(
                              'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                              softWrap: true,
                              overflow: TextOverflow.visible,
                              style: GoogleFonts.roboto(
                                color: Color(0xff333333),
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          AppCommon.kIconTrainerSearchTextView(
                              fontName: AppStrings.kFontAwsRegular,
                              content: ''),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Text(
                              'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. ',
                              softWrap: true,
                              overflow: TextOverflow.visible,
                              style: GoogleFonts.roboto(
                                color: Color(0xff333333),
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          AppCommon.kIconTrainerSearchTextView(
                              fontName: AppStrings.kFontAwsRegular,
                              content: ''),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Text(
                              'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.',
                              softWrap: true,
                              overflow: TextOverflow.visible,
                              style: GoogleFonts.roboto(
                                color: Color(0xff333333),
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Center(
                        child: Container(
                          width: 130,
                          height: 40,
                          child: ElevatedButton(
                            onPressed: () {},
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        AppColors.kGreen)),
                            child: Text(
                              'GO PREMIUM',
                              style: GoogleFonts.roboto(
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        });
  }
}
