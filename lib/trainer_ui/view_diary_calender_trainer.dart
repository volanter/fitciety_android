import 'package:ficiety/ui/notification_page.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

class ViewDiaryCalender extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  ViewDiaryCalender({required this.scaffoldKey});

  @override
  _ViewDiaryCalenderState createState() => _ViewDiaryCalenderState();
}

class _ViewDiaryCalenderState extends State<ViewDiaryCalender> {

  late bool isShowView;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isShowView = true;
  }
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff006e6f),
    ));
    return isShowView?Container(
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 120,
            color: Color(0xff006e6f),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SafeArea(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        top: 10,
                      ),
                      child: Row(
                        children: [
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: Image.asset(
                              AppStrings.kImgMenuBar,
                              width: 20,
                              height: 20,
                            ),
                            onTap: () {
                              if (widget
                                  .scaffoldKey.currentState!.isDrawerOpen) {
                                Navigator.of(context).pop();
                              } else {
                                widget.scaffoldKey.currentState!.openDrawer();
                              }
                            },
                          ),
                          SizedBox(
                            width: 30,
                          ),
                          Text(
                            'View Diary / Calender',
                            style: GoogleFonts.roboto(
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Spacer(),
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: (){
                              setState(() {
                                isShowView = false;
                              });
                            },
                            child: Image.asset(
                              AppStrings.kImgNotification,
                              width: 20,
                              height: 20,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: SfCalendar(
              view: CalendarView.day,
              showCurrentTimeIndicator: true,
              showDatePickerButton: true,
              showNavigationArrow: true,
              dataSource: MeetingDataSource(_getDataSource()),
              appointmentTextStyle: TextStyle(
                fontSize: 16,
              ),
              // onTap: (CalendarTapDetails details){
              //   _showViewDialog();
              // },
              // by default the month appointment display mode set as Indicator, we can
              // change the display mode as appointment using the appointment display
              // mode property
              monthViewSettings: MonthViewSettings(
                  appointmentDisplayMode:
                  MonthAppointmentDisplayMode.appointment),
            ),
          ),
          SizedBox(height:30),
        ],
      ),
    ):NotificationPage(scaffoldKey: widget.scaffoldKey, isReturn: (bool ) {
      setState(() {
        isShowView = bool;
      });
    },);
  }
}
List<Meeting> _getDataSource() {
  final List<Meeting> meetings = <Meeting>[];
  final DateTime today = DateTime.now();
  final DateTime startTime =
  DateTime(today.year, today.month, today.day, 9, 0, 0);
  final DateTime endTime = startTime.add(const Duration(hours: 2));
  meetings.add(Meeting(
      '   Mark Hemmings\n   Online Training\n   Left 4 Session', startTime, endTime, const Color(0xFF07595f), false,Icon(Icons.person,color: Colors.white,)));
  meetings.add(Meeting(
      'Mark Hemmings\n Online Training\n Left 4 Session',  DateTime(today.year, today.month, today.day, 20, 0, 0), endTime, const Color(0xFF07595f), false,Icon(Icons.person,color: Colors.white,)));
  return meetings;
}



class MeetingDataSource extends CalendarDataSource {
  /// Creates a meeting data source, which used to set the appointment
  /// collection to the calendar
  MeetingDataSource(List<Meeting> source) {
    appointments = source;
  }


  @override
  DateTime getStartTime(int index) {
    return appointments![index].from;
  }

  @override
  DateTime getEndTime(int index) {
    return appointments![index].to;
  }

  @override
  String getSubject(int index) {
    return appointments![index].eventName;
  }

  @override
  Color getColor(int index) {
    return appointments![index].background;
  }

  @override
  bool isAllDay(int index) {
    return appointments![index].isAllDay;
  }
}

class Meeting {
  /// Creates a meeting class with required details.
  Meeting(this.eventName, this.from, this.to, this.background, this.isAllDay,this.icons);

  /// Event name which is equivalent to subject property of [Appointment].
  String eventName;

  /// From which is equivalent to start time property of [Appointment].
  DateTime from;

  /// To which is equivalent to end time property of [Appointment].
  DateTime to;

  /// Background which is equivalent to color property of [Appointment].
  Color background;

  /// IsAllDay which is equivalent to isAllDay property of [Appointment].
  bool isAllDay;

  Icon icons;
}

