import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:ficiety/models/upcomming_session_model.dart';
import 'package:ficiety/trainer_ui/booking_request_trainer.dart';
import 'package:ficiety/trainer_ui/client_list.dart';
import 'package:ficiety/trainer_ui/payment_details.dart';
import 'package:ficiety/trainer_ui/trainer_edit_profile.dart';
import 'package:ficiety/trainer_ui/trainer_payment_history.dart';
import 'package:ficiety/trainer_ui/trainer_user_dashboard.dart';
import 'package:ficiety/trainer_ui/trainer_wallet.dart';
import 'package:ficiety/trainer_ui/view_diary_calender_trainer.dart';
import 'package:ficiety/ui/bmi_measurements.dart';
import 'package:ficiety/ui/body_fat_measurements.dart';
import 'package:ficiety/ui/change_password.dart';
import 'package:ficiety/ui/chat.dart';
import 'package:ficiety/ui/consultation.dart';
import 'package:ficiety/ui/diet_planing.dart';
import 'package:ficiety/utils/progressdialog.dart';
import 'package:http/http.dart' as http;
import 'package:ficiety/ui/edit_profile.dart';
import 'package:ficiety/ui/my_session_booking.dart';
import 'package:ficiety/ui/notification_page.dart';
import 'package:ficiety/ui/order_summary.dart';
import 'package:ficiety/ui/pt_program.dart';
import 'package:ficiety/ui/session_details.dart';
import 'package:ficiety/ui/settings.dart';
import 'package:ficiety/ui/success_payment.dart';
import 'package:ficiety/ui/trainer_details.dart';
import 'package:ficiety/ui/trainer_listview.dart';
import 'package:ficiety/ui/user_exercise.dart';
import 'package:ficiety/ui/waist_hips_measurements.dart';
import 'package:ficiety/utils/app_colors.dart';
import 'package:ficiety/utils/app_common.dart';
import 'package:ficiety/utils/app_strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:group_radio_button/group_radio_button.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'authorisation_request_trainer.dart';
import 'manage_session_trainer.dart';

class TrainerDashboard extends StatefulWidget {
  @override
  _TrainerDashboardState createState() => _TrainerDashboardState();
}

class _TrainerDashboardState extends State<TrainerDashboard>
    with SingleTickerProviderStateMixin {
  late ProgressDialog progressDialog;
  late String selectedText;
  List<String> listName = [];
  List<Text> listIcon = [];
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  late bool _isShowDialog;

  List<String> listGridName = [];
  List<Image> listContent = [];
  int _crossAxisCount = 2;
  late Future<UpCommingSessionModel?> _future;

  String _genderGroupValue = 'Male';

  List<String> _gender = ['Male', 'Female'];

  String _specialistGroupValue = 'Personal Training';
  late TabController _controller;
  int _selectedIndex = 0;

  List<String> _specialist = [
    'Personal Training',
    'Calisthenics',
    'Boxing',
    'Yoga / Pilates',
    'Strength & Conditioning',
    'Rehabiliation'
  ];

  @override
  void dispose() {
    // TODO: implement dispose
    _controller.dispose();
    super.dispose();
  }

  Future<UpCommingSessionModel?> _getSessionData() async {

    SharedPreferences preferences = await SharedPreferences.getInstance();

    try {
      final response = await http.post(
        Uri.parse(AppStrings.kGetUpcomingSessionUrl),
        headers: <String, String>{
          AppStrings.kHeaderType: AppStrings.kHeaderValue,
        },
        body: jsonEncode(<String, String>{
          'user_id':
          preferences.getString(AppStrings.kPrefUserIdKey).toString(),
        }),
      );
      var responseData = jsonDecode(response.body);
      print('response from upcomming Sesion data $responseData');
      if (response.statusCode == 200) {
        if (responseData['status'] == 'success') {
          return UpCommingSessionModel.fromJson(responseData);
        } else {
          // AwesomeDialog(
          //   context: context,
          //   dialogType: DialogType.ERROR,
          //   animType: AnimType.BOTTOMSLIDE,
          //   title: 'Error',
          //   desc: responseData['message'],
          //   btnCancelOnPress: () {},
          //   btnOkOnPress: () {},
          // )..show();
        }
      } else {
        throw Exception('Failed to load data');
      }
    } catch (exception) {
      print('exception $exception');
    }
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    progressDialog = new ProgressDialog(context, ProgressDialogType.Normal);
    progressDialog.setMessage(AppStrings.kLoadingMsg);
    _isShowDialog = false;
    _future = _getSessionData();
    _controller = TabController(length: 3, vsync: this);
    _controller.addListener(() {
      setState(() {
        _selectedIndex = _controller.index;
      });
    });

    listGridName.add(AppStrings.kViewDiaryCalender);
    listGridName.add('Manage\nSession');
    listGridName.add('Client\nList');
    listGridName.add('Payment\nHistory');

    listContent.add(Image.asset(
      AppStrings.kImgViewCalender,
      width: 51,
      height: 51,
    ));

    listContent.add(Image.asset(
      AppStrings.kImgPtProgram,
      width: 51,
      height: 51,
    ));
    listContent.add(Image.asset(
      AppStrings.kImgMeasurements,
      width: 51,
      height: 51,
    ));
    listContent.add(Image.asset(
      AppStrings.kImgDietaryLog,
      width: 51,
      height: 51,
    ));

    selectedText = AppStrings.kDashboard;
    listName.add(AppStrings.kProfile);
    listName.add(AppStrings.kManageSession);
    listName.add(AppStrings.kDashboard);
    listName.add(AppStrings.kClientList);
    // listName.add(AppStrings.kConsultation);
    listName.add(AppStrings.kMessageUser);
    listName.add(AppStrings.kNotification);
    listName.add(AppStrings. /*kMyWallet*/ kBookingRequest);
    listName.add(AppStrings.kPaymentMethod);
    listName.add(AppStrings.kSetting);
    // listName.add(AppStrings.kPrivacyPolicy);
    listName.add(AppStrings.kAuthorisationRequest);
    listName.add(AppStrings.kSignOut);

    listIcon.add(AppCommon.kIconTextView(
      content: '',
      fontName: AppStrings.kFontAwsRegular,
    ));
    listIcon.add(AppCommon.kIconTextView(
      content: '',
      fontName: AppStrings.kFontAwsSolid,
    ));
    listIcon.add(AppCommon.kIconTextView12(
      content: '',
      fontName: AppStrings.kFontAwsSolid,
    ));
    listIcon.add(AppCommon.kIconTextView12(
      content: '',
      fontName: AppStrings.kFontAwsSolid,
    ));
    // listIcon.add(AppCommon.kIconTextView(
    //   content: '',
    //   fontName: AppStrings.kFontAwsSolid,
    // ));
    listIcon.add(AppCommon.kIconTextView12(
      content: '',
      fontName: AppStrings.kFontAwsSolid,
    ));
    listIcon.add(AppCommon.kIconTextView(
      content: '',
      fontName: AppStrings.kFontAwsRegular,
    ));
    listIcon.add(AppCommon.kIconTextView(
      content: '',
      fontName: AppStrings.kFontAwsSolid,
    ));
    listIcon.add(AppCommon.kIconTextView12(
      content: '',
      fontName: AppStrings.kFontAwsSolid,
    ));
    listIcon.add(AppCommon.kIconTextView(
      content: '',
      fontName: AppStrings.kFontAwsSolid,
    ));
    // listIcon.add(AppCommon.kIconTextView20(
    //   content: '',
    //   fontName: AppStrings.kFontAwsSolid,
    // ));
    listIcon.add(AppCommon.kIconTextView20(
      content: '',
      fontName: AppStrings.kFontAwsSolid,
    ));
    listIcon.add(AppCommon.kIconTextView(
      content: '',
      fontName: AppStrings.kFontAwsSolid,
    ));

    // listIcon.add(Icon(
    //   Icons.pages_rounded,
    //   color: Colors.white,
    // ));
    // listIcon.add(Icon(
    //   Icons.pages_rounded,
    //   color: Colors.white,
    // ));
    // listIcon.add(Icon(
    //   Icons.pages_rounded,
    //   color: Colors.white,
    // ));
    // listIcon.add(Icon(
    //   Icons.pages_rounded,
    //   color: Colors.white,
    // ));
    // listIcon.add(Icon(
    //   Icons.pages_rounded,
    //   color: Colors.white,
    // ));
    // listIcon.add(Icon(
    //   Icons.pages_rounded,
    //   color: Colors.white,
    // ));
    // listIcon.add(Icon(
    //   Icons.pages_rounded,
    //   color: Colors.white,
    // ));
    // listIcon.add(Icon(
    //   Icons.pages_rounded,
    //   color: Colors.white,
    // ));
    // listIcon.add(Icon(
    //   Icons.pages_rounded,
    //   color: Colors.white,
    // ));
    // listIcon.add(Icon(
    //   Icons.pages_rounded,
    //   color: Colors.white,
    // ));
    // listIcon.add(Icon(
    //   Icons.pages_rounded,
    //   color: Colors.white,
    // ));
    // listIcon.add(Icon(
    //   Icons.pages_rounded,
    //   color: Colors.white,
    // ));
    // listIcon.add(Icon(
    //   Icons.pages_rounded,
    //   color: Colors.white,
    // ));
    // listIcon.add(Icon(
    //   Icons.pages_rounded,
    //   color: Colors.white,
    // ));
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff087D8D),
    ));
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        key: _scaffoldKey,
        drawer: Drawer(
          child: Container(
            color: Color(0xff087D8D),
            child: Padding(
              padding: const EdgeInsets.only(
                left: 10,
              ),
              child: Column(
                children: [
                  SafeArea(
                    child: GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () => Navigator.of(context).pop(),
                      child: Align(
                        alignment: Alignment.topRight,
                        child: Padding(
                          padding: const EdgeInsets.only(
                            top: 20,
                            right: 20,
                            bottom: 10,
                          ),
                          child: Icon(
                            Icons.clear_outlined,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Image.asset(
                    AppStrings.kImgProfilePic,
                    width: 70,
                    height: 70,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Center(
                    child: Text(
                      'Magdalena Kosciuszko',
                      style: GoogleFonts.roboto(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemCount: listName.length,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () {
                            if (_selectedIndex != 0) {
                              _controller.animateTo(0);
                            }
                            if (listName[index] == AppStrings.kSignOut) {
                              // await preferences.clear();
                              sessionClear();
                            } else if (listName[index] ==
                                AppStrings.kMessageUser) {
                              print('click on messageuser');
                              // Navigator.of(context).pop();
                              _controller.animateTo(2);
                              setState(() {
                                selectedText = listName[index];
                                print('selected index ${selectedText}');
                                Navigator.of(context).pop();
                              });
                            } else {
                              setState(() {
                                selectedText = listName[index];
                                print('selected index ${selectedText}');
                                Navigator.of(context).pop();
                              });
                            }
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(
                              top: 10,
                            ),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20),
                                  bottomLeft: Radius.circular(20),
                                ),
                                color: selectedText == listName[index]
                                    ? Color(0xff00484d)
                                    : Color(0xff087D8D),
                              ),
                              height: 35,
                              child: Padding(
                                padding: const EdgeInsets.only(
                                  left: 15,
                                ),
                                child: AppCommon.kListTileCommonTextStyle1(
                                  text: listName[index],
                                  textIcon: listIcon[index],
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        bottomNavigationBar: TabBar(
          controller: _controller,
          dragStartBehavior: DragStartBehavior.down,
          labelColor: Color(0xff006E6F),
          isScrollable: false,
          physics: NeverScrollableScrollPhysics(),
          unselectedLabelColor: Colors.grey,
          indicatorSize: TabBarIndicatorSize.tab,
          indicatorPadding: EdgeInsets.all(5.0),
          indicatorColor: Color(0xff006E6F),
          tabs: [
            Tab(
              icon: Icon(
                Icons.home,
              ),
            ),
            Tab(
              icon: Icon(
                Icons.person_rounded,
              ),
            ),
            Tab(
              icon: Image.asset(
                AppStrings.kImgChat,
                width: 24,
                height: 24,
              ),
            ),
          ],
        ),
        body: TabBarView(controller: _controller,physics: NeverScrollableScrollPhysics(), children: [
          selectedText == AppStrings.kDashboard
              ? _homeView()
              : selectedText == AppStrings.kTrainerList
                  ? TrainerListView(
                      scaffoldKey: _scaffoldKey,
                      // callback: (value) {
                      //   print('callback call $value');
                      //   setState(() {
                      //     selectedText = value;
                      //   });
                      //   return selectedText;
                      // },
                    )
                  : selectedText == AppStrings.kViewDiaryCalender
                      ? ViewDiaryCalender(
                          scaffoldKey: _scaffoldKey,
                        )
                      : selectedText == AppStrings.kClientList
                          ? ClientList(
                              scaffoldKey: _scaffoldKey,
                            )
                          : selectedText == AppStrings.kPTProgrammes
                              ? PTProgram(
                                  scaffoldKey: _scaffoldKey,
                                )
                              : selectedText == AppStrings.kExercise
                                  ? UserExercise(
                                      scaffoldKey: _scaffoldKey,
                                    )
                                  : selectedText == AppStrings.kMessagePT
                                      ? Chat(
                                          scaffoldKey: _scaffoldKey,
                                        )
                                      : selectedText ==
                                              AppStrings.kPaymentMethod
                                          ? PaymentDetails(
                                              scaffoldKey: _scaffoldKey,
                                            )
                                          // : selectedText ==
                                          //         AppStrings.kMessageUser
                                          //     ? TrainerUserDashboard()
                                              : selectedText ==
                                                      AppStrings
                                                          .kWaistMeasurements
                                                  ? WaistHipsMeasurements(
                                                      isReturn: (value) {
                                                        setState(() {
                                                          selectedText =
                                                              AppStrings
                                                                  .kDashboard;
                                                        });
                                                      },
                                                      scaffoldKey: _scaffoldKey,
                                                    )
                                                  : selectedText ==
                                                          AppStrings
                                                              .kNotification
                                                      ? NotificationPage(
                                                          scaffoldKey:
                                                              _scaffoldKey,
                                                          isReturn: (value) {
                                                            setState(() {
                                                              selectedText =
                                                                  AppStrings
                                                                      .kDashboard;
                                                            });
                                                          },
                                                        )
                                                      : selectedText ==
                                                              AppStrings
                                                                  .kConsultation
                                                          ? Consultation(
                                                              scaffoldKey:
                                                                  _scaffoldKey)
                                                          : selectedText ==
                                                                  AppStrings
                                                                      .kSetting
                                                              ? Settings(
                                                                  scaffoldKey:
                                                                      _scaffoldKey,
                                                                  // name:
                                                                  //     (value) {
                                                                  //   if (value ==
                                                                  //       'Change\nPassword') {
                                                                  //     setState(
                                                                  //         () {
                                                                  //       selectedText =
                                                                  //           AppStrings.kChangePassword;
                                                                  //     });
                                                                  //   } else if (value ==
                                                                  //       'Payment\nHistory') {
                                                                  //     setState(
                                                                  //         () {
                                                                  //       selectedText =
                                                                  //           AppStrings.kPaymentHistory;
                                                                  //     });
                                                                  //   } else if (value ==
                                                                  //       'Profile') {
                                                                  //     setState(
                                                                  //         () {
                                                                  //       selectedText =
                                                                  //           AppStrings.kProfile;
                                                                  //     });
                                                                  //   }
                                                                  // },
                                                                )
                                                              : selectedText ==
                                                                      AppStrings
                                                                          .kProfile
                                                                  ? TrainerEditProfile(
                                                                      scaffoldKey:
                                                                          _scaffoldKey)
                                                                  : selectedText ==
                                                                          AppStrings
                                                                              .kManageSession
                                                                      ? ManageSessionTrainer(
                                                                          scaffoldKey:
                                                                              _scaffoldKey)
                                                                      : selectedText ==
                                                                              AppStrings.kBookingRequest
                                                                          ? BookingRequestTrainer(scaffoldKey: _scaffoldKey)
                                                                          : selectedText == AppStrings.kDietaryLog
                                                                              ? DietPlanning(
                                                                                  scaffoldKey: _scaffoldKey,
                                                                                )
                                                                              : selectedText == AppStrings.kPaymentHistory
                                                                                  ? TrainerPaymentHistory(scaffoldKey: _scaffoldKey)
              : selectedText == AppStrings.kAuthorisationRequest
              ? AuthorisationRequestTrainer(scaffoldKey: _scaffoldKey)
                                                                                  : selectedText == AppStrings.kSessionDetails
                                                                                      ? SessionDetails(
                                                                                          scaffoldKey: _scaffoldKey,
                                                                                          isReturn: (value) {
                                                                                            setState(() {
                                                                                              selectedText = AppStrings.kDashboard;
                                                                                            });
                                                                                          }, sessionId: '',
                                                                                        )
                                                                                      : Container(),
          TrainerEditProfile(scaffoldKey: _scaffoldKey),
          Chat(
            scaffoldKey: _scaffoldKey,
          ),
        ]),
      ),
    );
  }

  // Widget _trainerListView() {
  //   return Container(
  //     color: Colors.white,
  //     child: Column(
  //       children: [
  //         Stack(
  //           clipBehavior: Clip.none,
  //           children: [
  //             Container(
  //               height: 200.5,
  //               color: Color(0xff057A87),
  //               child: Column(
  //                 children: [
  //                   SafeArea(
  //                     child: Padding(
  //                       padding: const EdgeInsets.all(20.0),
  //                       child: Row(
  //                         children: [
  //                           GestureDetector(
  //                             child: Image.asset(
  //                               AppStrings.kImgMenuBar,
  //                               width: 18,
  //                               height: 13,
  //                             ),
  //                             onTap: () {
  //                               if (_scaffoldKey.currentState.isDrawerOpen) {
  //                                 Navigator.of(context).pop();
  //                               } else {
  //                                 _scaffoldKey.currentState.openDrawer();
  //                               }
  //                             },
  //                           ),
  //                           SizedBox(
  //                             width: 30,
  //                           ),
  //                           Text(
  //                             AppStrings.kTrainerList,
  //                             style: GoogleFonts.roboto(
  //                               color: Colors.white,
  //                               fontWeight: FontWeight.w700,
  //                             ),
  //                           ),
  //                           Spacer(),
  //                           IconButton(
  //                             icon: Icon(
  //                               Icons.wrap_text,
  //                               color: Colors.white,
  //                             ),
  //                             onPressed: () => _openSearchTrainerDialog(),
  //                           ),
  //                           // Image.asset(
  //                           //   AppStrings.kImgNotification,
  //                           //   width: 17.5,
  //                           //   height: 18.5,
  //                           // ),
  //                         ],
  //                       ),
  //                     ),
  //                   ),
  //                 ],
  //               ),
  //             ),
  //             Positioned(
  //               top: 90,
  //               right: 0,
  //               left: 0,
  //               child: SizedBox(
  //                 height: 670,
  //                 child: Padding(
  //                   padding: const EdgeInsets.only(
  //                     left: 20,
  //                   ),
  //                   child: ListView.builder(
  //                       primary: false,
  //                       itemCount: 5,
  //                       shrinkWrap: true,
  //                       scrollDirection: Axis.horizontal,
  //                       itemBuilder: (context, index) {
  //                         return Container(
  //                           width: 320,
  //                           child: Padding(
  //                             padding: const EdgeInsets.all(5.0),
  //                             child: Card(
  //                               color: Colors.white,
  //                               child: Padding(
  //                                 padding: const EdgeInsets.all(10.0),
  //                                 child: Column(
  //                                   crossAxisAlignment:
  //                                       CrossAxisAlignment.start,
  //                                   mainAxisAlignment: MainAxisAlignment.start,
  //                                   children: [
  //                                     Image.asset(
  //                                       AppStrings.kImgTrainerList,
  //                                       width: 320,
  //                                       height: 140,
  //                                       fit: BoxFit.fill,
  //                                     ),
  //                                     Padding(
  //                                       padding: const EdgeInsets.only(
  //                                         top: 20,
  //                                       ),
  //                                       child: Row(
  //                                         children: [
  //                                           Image.asset(
  //                                             AppStrings.kImgProfilePic,
  //                                             width: 67.5,
  //                                             height: 67.5,
  //                                             fit: BoxFit.fill,
  //                                           ),
  //                                           SizedBox(
  //                                             width: 20,
  //                                           ),
  //                                           Column(
  //                                             mainAxisAlignment:
  //                                                 MainAxisAlignment.start,
  //                                             crossAxisAlignment:
  //                                                 CrossAxisAlignment.start,
  //                                             children: [
  //                                               Text(
  //                                                 'Mark Hemmings',
  //                                                 style: GoogleFonts.roboto(
  //                                                   color: AppColors.kGreen,
  //                                                   fontSize: 18,
  //                                                   fontWeight: FontWeight.w700,
  //                                                 ),
  //                                               ),
  //                                               SizedBox(height: 5),
  //                                               Align(
  //                                                 alignment: Alignment.topLeft,
  //                                                 child: Row(
  //                                                   children: [
  //                                                     Icon(
  //                                                       Icons.star,
  //                                                       size: 15,
  //                                                       color:
  //                                                           Color(0xffffcc00),
  //                                                     ),
  //                                                     Icon(
  //                                                       Icons.star,
  //                                                       size: 15,
  //                                                       color:
  //                                                           Color(0xffffcc00),
  //                                                     ),
  //                                                     Icon(
  //                                                       Icons.star,
  //                                                       size: 15,
  //                                                       color:
  //                                                           Color(0xffffcc00),
  //                                                     ),
  //                                                     Icon(
  //                                                       Icons.star,
  //                                                       size: 15,
  //                                                       color:
  //                                                           Color(0xffffcc00),
  //                                                     ),
  //                                                     Icon(
  //                                                       Icons.star,
  //                                                       size: 15,
  //                                                       color:
  //                                                           Color(0xffffcc00),
  //                                                     ),
  //                                                   ],
  //                                                 ),
  //                                               ),
  //                                             ],
  //                                           ),
  //                                         ],
  //                                       ),
  //                                     ),
  //                                     Padding(
  //                                       padding: const EdgeInsets.only(
  //                                         top: 20,
  //                                       ),
  //                                       child: Text(
  //                                         'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
  //                                         softWrap: true,
  //                                         overflow: TextOverflow.visible,
  //                                         style: GoogleFonts.roboto(
  //                                           color: AppColors.kBlack,
  //                                         ),
  //                                         textAlign: TextAlign.left,
  //                                       ),
  //                                     ),
  //                                     SizedBox(height: 20),
  //                                     Text(
  //                                       AppStrings.kAreaOfExpertise,
  //                                       style: GoogleFonts.roboto(
  //                                         color: AppColors.kGreen,
  //                                         fontSize: 16,
  //                                         fontWeight: FontWeight.w700,
  //                                       ),
  //                                     ),
  //                                     SizedBox(
  //                                       height: 15,
  //                                     ),
  //                                     Row(
  //                                       children: [
  //                                         FaIcon(
  //                                           FontAwesomeIcons.checkCircle,
  //                                           size: 15,
  //                                           color: AppColors.kGreen,
  //                                         ),
  //                                         SizedBox(
  //                                           width: 15,
  //                                         ),
  //                                         Text(
  //                                           'Personal Training',
  //                                           style: GoogleFonts.roboto(
  //                                             color: AppColors.kBlack,
  //                                           ),
  //                                           textAlign: TextAlign.left,
  //                                         ),
  //                                       ],
  //                                     ),
  //                                     SizedBox(
  //                                       height: 10,
  //                                     ),
  //                                     Row(
  //                                       children: [
  //                                         FaIcon(
  //                                           FontAwesomeIcons.checkCircle,
  //                                           size: 15,
  //                                           color: AppColors.kGreen,
  //                                         ),
  //                                         SizedBox(
  //                                           width: 15,
  //                                         ),
  //                                         Text(
  //                                           'Yoga',
  //                                           style: GoogleFonts.roboto(
  //                                             color: AppColors.kBlack,
  //                                           ),
  //                                           textAlign: TextAlign.left,
  //                                         ),
  //                                       ],
  //                                     ),
  //                                     SizedBox(
  //                                       height: 10,
  //                                     ),
  //                                     Row(
  //                                       children: [
  //                                         FaIcon(
  //                                           FontAwesomeIcons.checkCircle,
  //                                           size: 15,
  //                                           color: AppColors.kGreen,
  //                                         ),
  //                                         SizedBox(
  //                                           width: 15,
  //                                         ),
  //                                         Text(
  //                                           'Boxing',
  //                                           style: GoogleFonts.roboto(
  //                                             color: AppColors.kBlack,
  //                                           ),
  //                                           textAlign: TextAlign.left,
  //                                         ),
  //                                       ],
  //                                     ),
  //                                     SizedBox(
  //                                       height: 20,
  //                                     ),
  //                                     Row(
  //                                       children: [
  //                                         FaIcon(
  //                                           FontAwesomeIcons.mapMarkerAlt,
  //                                           size: 15,
  //                                           color: AppColors.kGreen,
  //                                         ),
  //                                         SizedBox(
  //                                           width: 15,
  //                                         ),
  //                                         Expanded(
  //                                           child: Text(
  //                                             "Top floor of Sainsbury’s carpark 2 St James Avenue",
  //                                             softWrap: true,
  //                                             overflow: TextOverflow.visible,
  //                                             style: GoogleFonts.roboto(
  //                                               color: AppColors.kBlack,
  //                                             ),
  //                                             textAlign: TextAlign.left,
  //                                           ),
  //                                         ),
  //                                       ],
  //                                     ),
  //                                     SizedBox(
  //                                       height: 10,
  //                                     ),
  //                                     Divider(
  //                                       color: Colors.grey,
  //                                     ),
  //                                     Row(
  //                                       mainAxisAlignment:
  //                                           MainAxisAlignment.spaceEvenly,
  //                                       children: [
  //                                         Column(
  //                                           children: [
  //                                             FaIcon(
  //                                               FontAwesomeIcons.personBooth,
  //                                               size: 15,
  //                                               color: AppColors.kGreen,
  //                                             ),
  //                                             SizedBox(
  //                                               height: 2,
  //                                             ),
  //                                             Text(
  //                                               '100',
  //                                               style: GoogleFonts.roboto(
  //                                                 color: AppColors.kBlack,
  //                                               ),
  //                                               textAlign: TextAlign.left,
  //                                             )
  //                                           ],
  //                                         ),
  //                                         Column(
  //                                           children: [
  //                                             FaIcon(
  //                                               FontAwesomeIcons.personBooth,
  //                                               size: 15,
  //                                               color: AppColors.kGreen,
  //                                             ),
  //                                             SizedBox(
  //                                               height: 2,
  //                                             ),
  //                                             Text(
  //                                               '50',
  //                                               style: GoogleFonts.roboto(
  //                                                 color: AppColors.kBlack,
  //                                               ),
  //                                               textAlign: TextAlign.left,
  //                                             )
  //                                           ],
  //                                         ),
  //                                         Column(
  //                                           children: [
  //                                             FaIcon(
  //                                               FontAwesomeIcons.personBooth,
  //                                               size: 15,
  //                                               color: AppColors.kGreen,
  //                                             ),
  //                                             SizedBox(
  //                                               height: 2,
  //                                             ),
  //                                             Text(
  //                                               '25',
  //                                               style: GoogleFonts.roboto(
  //                                                 color: AppColors.kBlack,
  //                                               ),
  //                                               textAlign: TextAlign.left,
  //                                             )
  //                                           ],
  //                                         ),
  //                                       ],
  //                                     ),
  //                                     SizedBox(
  //                                       height: 5,
  //                                     ),
  //                                     Row(
  //                                       mainAxisAlignment:
  //                                           MainAxisAlignment.spaceEvenly,
  //                                       children: [
  //                                         ElevatedButton(
  //                                           onPressed: () {},
  //                                           style: ButtonStyle(
  //                                               backgroundColor:
  //                                                   MaterialStateProperty.all<
  //                                                           Color>(
  //                                                       AppColors.kGreen)),
  //                                           child: Text(
  //                                             'View Profile',
  //                                             style: GoogleFonts.roboto(
  //                                               color: Colors.white,
  //                                               fontWeight: FontWeight.w500,
  //                                             ),
  //                                           ),
  //                                         ),
  //                                         ElevatedButton(
  //                                           onPressed: () {},
  //                                           style: ButtonStyle(
  //                                               backgroundColor:
  //                                                   MaterialStateProperty.all<
  //                                                           Color>(
  //                                                       AppColors.kGreen)),
  //                                           child: Text(
  //                                             'Contact Trainer',
  //                                             style: GoogleFonts.roboto(
  //                                               color: Colors.white,
  //                                               fontWeight: FontWeight.w500,
  //                                             ),
  //                                           ),
  //                                         ),
  //                                       ],
  //                                     )
  //                                   ],
  //                                 ),
  //                               ),
  //                             ),
  //                           ),
  //                         );
  //                       }),
  //                 ),
  //               ),
  //             ),
  //           ],
  //         ),
  //       ],
  //     ),
  //   );
  // }

  Future<void> _logoutCall() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    try {
      final http.Response response = await http.post(
          Uri.parse(AppStrings.kLogoutUrl),
          headers: <String, String>{
            AppStrings.kHeaderType: AppStrings.kHeaderValue
          },
          body: jsonEncode(<String, String>{
            'user_id':
            preferences.getString(AppStrings.kPrefUserIdKey).toString(),
            'device_token':
            preferences.getString(AppStrings.kPrefDeviceToken).toString(),
            'device_type':
            preferences.getString(AppStrings.kPrefDeviceType).toString(),
            'language': 'en',
          }));

      var _responseData = jsonDecode(response.body);
      print('response from service ${response.body}');

      Navigator.of(context).pop();

      if (response.statusCode == 200) {
        if (_responseData['status'] == 'success') {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            title: AppStrings.kSuccess,
            desc: _responseData['message'],
            btnCancelOnPress: () {},
            btnOkOnPress: () async {
              await preferences.clear();
              Navigator.of(context).pop();
              Navigator.of(context).pop();
            },
          )..show();
        } else {
          AppCommon.kErrorDialog(
              context: context,
              errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
        }
      } else {
        AppCommon.kErrorDialog(
            context: context,
            errorMsg: _responseData[AppStrings.kResponseErrorMessage]);
      }
    } catch (exception) {
      print('exception is $exception');
      Navigator.of(context).pop();
      AppCommon.kErrorDialog(
          context: context, errorMsg: 'Some error occur.html response getting');
    }
  }

  Widget _homeView() {
    return Container(
      color: _isShowDialog ? Colors.grey : Colors.white,
      child: Column(
        children: [
          Container(
            height: 200,
            child: Stack(
              clipBehavior: Clip.none,
              children: [
                Container(
                  height: 150.5,
                  color: Color(0xff057A87),
                  child: Column(
                    children: [
                      SafeArea(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Row(
                            children: [
                              GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                child: Image.asset(
                                  AppStrings.kImgMenuBar,
                                  width: 20,
                                  height: 20,
                                ),
                                onTap: () {
                                  if (_scaffoldKey.currentState!.isDrawerOpen) {
                                    Navigator.of(context).pop();
                                  } else {
                                    _scaffoldKey.currentState!.openDrawer();
                                  }
                                },
                              ),
                              SizedBox(
                                width: 30,
                              ),
                              Text(
                                AppStrings.kTrainerDashboard,
                                style: GoogleFonts.roboto(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              Spacer(),
                              GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                onTap: (){
                                  print('1234');
                                  setState(() {
                                    selectedText = AppStrings
                                        .kNotification;
                                  });
                                },
                                child: Image.asset(
                                  AppStrings.kImgNotification,
                                  width: 17.5,
                                  height: 18.5,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: 100,
                  right: 0,
                  left: 0,
                  child: SizedBox(
                    height: 120,
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 20,
                      ),
                      child: FutureBuilder<UpCommingSessionModel?>(
                          future: _future,
                          builder: (BuildContext context,
                              AsyncSnapshot<UpCommingSessionModel?> snapshot) {
                            if(!snapshot.hasData){
                              return SizedBox();
                            }else{
                              return ListView.builder(
                                  itemCount: snapshot.data!.data.length,
                                  shrinkWrap: true,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder: (context, index) {
                                    if (!snapshot.hasData) {
                                      return SizedBox();
                                    } else {
                                      return Padding(
                                        padding: const EdgeInsets.all(5.0),
                                        child: GestureDetector(
                                          behavior: HitTestBehavior.translucent,
                                          onTap: () {
                                            setState(() {
                                              selectedText =
                                                  AppStrings.kSessionDetails;
                                            });
                                          },
                                          child: Container(
                                            width: 160,
                                            decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  fit: BoxFit.cover,
                                                  image: NetworkImage(
                                                    '${AppStrings.kImageUrl}${snapshot.data?.data[index].coverImage}',
                                                  )),
                                              borderRadius:
                                              BorderRadius.circular(5.0),
                                              border: Border.all(
                                                width: 2,
                                                color: Colors.white,
                                              ),
                                            ),
                                            child: Row(
                                              children: [
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Column(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceEvenly,
                                                  children: [
                                                    FaIcon(
                                                        FontAwesomeIcons.userTie,
                                                        color: AppColors.kWhite,
                                                        size: 12),
                                                    FaIcon(
                                                      FontAwesomeIcons.clock,
                                                      color: AppColors.kWhite,
                                                      size: 12,
                                                    ),
                                                    FaIcon(
                                                      FontAwesomeIcons.calendar,
                                                      color: AppColors.kWhite,
                                                      size: 12,
                                                    ),
                                                    FaIcon(
                                                      FontAwesomeIcons.th,
                                                      color: AppColors.kWhite,
                                                      size: 12,
                                                    ),
                                                    // FaIcon(FontAwesomeIcons.handshake,color: AppColors.kGreen,size: 12,),
                                                  ],
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Column(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceEvenly,
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      '${snapshot.data?.data[index].fullname}',
                                                      style: GoogleFonts.roboto(
                                                          color: AppColors.kWhite,
                                                          fontSize: 12,
                                                          fontWeight:
                                                          FontWeight.bold),
                                                    ),
                                                    Text(
                                                      snapshot.data?.data[index].startTime??'',
                                                      style: GoogleFonts.roboto(
                                                          color: AppColors.kWhite,
                                                          fontSize: 12,
                                                          fontWeight:
                                                          FontWeight.bold),
                                                    ),
                                                    Text(
                                                      snapshot.data?.data[index].startDate??'',
                                                      style: GoogleFonts.roboto(
                                                          color: AppColors.kWhite,
                                                          fontSize: 12,
                                                          fontWeight:
                                                          FontWeight.bold),
                                                    ),
                                                    Text(
                                                      snapshot.data?.data[index].categoryName??'',
                                                      style: GoogleFonts.roboto(
                                                          color: AppColors.kWhite,
                                                          fontSize: 12,
                                                          fontWeight:
                                                          FontWeight.bold),
                                                    ),
                                                    // Text('4 Session Left',style: GoogleFonts.roboto(color: AppColors.kBlack,fontSize: 12,fontWeight: FontWeight.w500),),
                                                  ],
                                                ),
                                              ],
                                            ),
                                            // child: Image.asset(
                                            //   AppStrings.kImgCard,
                                            //   width: 130,
                                            //   height: 120,
                                            // ),
                                          ),
                                        ),
                                      );
                                    }
                                  });

                            }
                          }),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Flexible(
            child: Padding(
                padding: const EdgeInsets.only(
                  left: 30,
                  right: 30,
                ),
                child: GridView.builder(
                  shrinkWrap: true,
                  itemCount: listGridName.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () {
                        if (listGridName[index] ==
                            AppStrings.kViewDiaryCalender) {
                          setState(() {
                            selectedText = AppStrings.kViewDiaryCalender;
                          });
                        } else if (listGridName[index] == 'Manage\nSession') {
                          setState(() {
                            selectedText = AppStrings.kManageSession;
                          });
                        } else if (listGridName[index] == 'Client\nList') {
                          setState(() {
                            selectedText = AppStrings.kClientList;
                          });
                        } else if (listGridName[index] == 'Payment\nHistory') {
                          setState(() {
                            selectedText = AppStrings.kPaymentHistory;
                          });
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(
                          right: 10,
                          bottom: 15,
                        ),
                        child: Container(
                          // height: 131,
                          // width: 142.5,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                5.0,
                              ),
                              border: Border.all(
                                color: Color(0xff006E6F),
                              )),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              listContent[index],
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                listGridName[index],
                                // AppStrings.kPTProgrammesTrainer,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.roboto(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: _crossAxisCount,
                    crossAxisSpacing: 5.0,
                    mainAxisSpacing: 2.0,
                    childAspectRatio:
                        (MediaQuery.of(context).size.height / 2.5) /
                            (MediaQuery.of(context).size.height - 480),
                  ),
                )),
          ),
          // Padding(
          //   padding: const EdgeInsets.only(
          //     left: 30,
          //     right: 50,
          //   ),
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.start,
          //     children: [
          //       GestureDetector(
          //         onTap: () {
          //           setState(() {
          //             selectedText = AppStrings.kViewDiaryCalender;
          //           });
          //         },
          //         child: Container(
          //           height: 131,
          //           width: 142.5,
          //           decoration: BoxDecoration(
          //               borderRadius: BorderRadius.circular(
          //                 5.0,
          //               ),
          //               border: Border.all(
          //                 color: Color(0xff006E6F),
          //               )),
          //           child: Column(
          //             mainAxisAlignment: MainAxisAlignment.center,
          //             children: [
          //               // AppCommon.kIconDashboardTextView(
          //               //   content: '',
          //               //   fontName: AppStrings.kFontAwsSolid,
          //               // ),
          //               Image.asset(
          //                 AppStrings.kImgViewCalender,
          //                 width: 51,
          //                 height: 51,
          //               ),
          //               SizedBox(
          //                 height: 10,
          //               ),
          //               Text(
          //                 AppStrings.kViewDiaryCalender,
          //                 textAlign: TextAlign.center,
          //                 style: GoogleFonts.roboto(
          //                   color: Colors.black,
          //                   fontWeight: FontWeight.w700,
          //                 ),
          //               ),
          //             ],
          //           ),
          //         ),
          //       ),
          //       Spacer(),
          //       GestureDetector(
          //         onTap: () {
          //           setState(() {
          //             selectedText = /*AppStrings.kPTProgrammesTrainer*/
          //                 AppStrings.kManageSession;
          //           });
          //         },
          //         child: Container(
          //           height: 131,
          //           width: 142.5,
          //           decoration: BoxDecoration(
          //               borderRadius: BorderRadius.circular(
          //                 5.0,
          //               ),
          //               border: Border.all(
          //                 color: Color(0xff006E6F),
          //               )),
          //           child: Column(
          //             mainAxisAlignment: MainAxisAlignment.center,
          //             children: [
          //               // AppCommon.kIconDashboardTextView(
          //               //   content: '',
          //               //   fontName: AppStrings.kFontAwsSolid,
          //               // ),
          //               Image.asset(
          //                 AppStrings.kImgPtProgram,
          //                 width: 51,
          //                 height: 51,
          //               ),
          //               SizedBox(
          //                 height: 10,
          //               ),
          //               Text(
          //                 // AppStrings.kPTProgrammesTrainer,
          //                 'Manage\nSession',
          //                 textAlign: TextAlign.center,
          //                 style: GoogleFonts.roboto(
          //                   color: Colors.black,
          //                   fontWeight: FontWeight.w700,
          //                 ),
          //               ),
          //             ],
          //           ),
          //         ),
          //       ),
          //     ],
          //   ),
          // ),
          // SizedBox(
          //   height: 30,
          // ),
          // Padding(
          //   padding: const EdgeInsets.only(
          //     left: 30,
          //     right: 50,
          //   ),
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.start,
          //     children: [
          //       GestureDetector(
          //         child: Container(
          //           height: 131,
          //           width: 142.5,
          //           decoration: BoxDecoration(
          //               borderRadius: BorderRadius.circular(
          //                 5.0,
          //               ),
          //               border: Border.all(
          //                 color: Color(0xff006E6F),
          //               )),
          //           child: Column(
          //             mainAxisAlignment: MainAxisAlignment.center,
          //             children: [
          //               // AppCommon.kIconDashboardTextView(
          //               //   content: '',
          //               //   fontName: AppStrings.kFontAwsSolid,
          //               // ),
          //               Image.asset(
          //                 AppStrings.kImgMeasurements,
          //                 width: 51,
          //                 height: 51,
          //               ),
          //               SizedBox(
          //                 height: 10,
          //               ),
          //               Text(
          //                 // AppStrings.kMeasurementsProgressReport,
          //                 'Client\nList',
          //                 textAlign: TextAlign.center,
          //                 style: GoogleFonts.roboto(
          //                   color: Colors.black,
          //                   fontWeight: FontWeight.w700,
          //                 ),
          //               ),
          //             ],
          //           ),
          //         ),
          //         onTap: () {
          //           setState(() {
          //             selectedText = AppStrings.kClientList;
          //           });
          //         },
          //       ),
          //       Spacer(),
          //       GestureDetector(
          //         onTap: () {
          //           setState(() {
          //             selectedText = AppStrings.kPaymentHistory;
          //           });
          //         },
          //         child: Container(
          //           height: 131,
          //           width: 142.5,
          //           decoration: BoxDecoration(
          //               borderRadius: BorderRadius.circular(
          //                 5.0,
          //               ),
          //               border: Border.all(
          //                 color: Color(0xff006E6F),
          //               )),
          //           child: Column(
          //             mainAxisAlignment: MainAxisAlignment.center,
          //             children: [
          //               // AppCommon.kIconDashboardTextView(
          //               //     fontName: AppStrings.kFontAwsSolid, content: ''),
          //               Image.asset(
          //                 AppStrings.kImgDietaryLog,
          //                 width: 51,
          //                 height: 51,
          //               ),
          //               SizedBox(
          //                 height: 10,
          //               ),
          //               Text(
          //                 /*AppStrings.kDietaryLogTrainer*/
          //                 'Payment\nHistory',
          //                 textAlign: TextAlign.center,
          //                 style: GoogleFonts.roboto(
          //                   color: Colors.black,
          //                   fontWeight: FontWeight.w700,
          //                 ),
          //               ),
          //             ],
          //           ),
          //         ),
          //       ),
          //     ],
          //   ),
          // ),
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 30,
              right: 50,
            ),
            child: Container(
              height: 44.5,
              width: double.infinity,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(
                    5.0,
                  ),
                  border: Border.all(
                    color: Color(0xff006E6F),
                  )),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Stack(
                    children: [
                      AppCommon.kIconMessageTextView(
                        content: '',
                        fontName: AppStrings.kFontAwsSolid,
                      ),
                      // Image.asset(
                      //   AppStrings.kImgMsg,
                      //   width: 39.5,
                      //   height: 31,
                      // ),
                      // Positioned(
                      //   top:10,
                      //   left: 10,
                      //   bottom: 2,
                      //   child: Container(
                      //     width: 30,
                      //     height: 30,
                      //     child: CircleAvatar(
                      //       backgroundColor: Colors.white,
                      //       child: CircleAvatar(
                      //         backgroundColor: Color(0xffFECF33),
                      //         child: Text('1',style: GoogleFonts.roboto(color: Colors.white,),),
                      //       ),
                      //     ),
                      //   ),
                      // )
                    ],
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Text(
                    AppStrings.kMessageYourClient,
                    textAlign: TextAlign.center,
                    style: GoogleFonts.roboto(
                      color: Colors.black,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  _openSearchTrainerDialog() {
    return showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, setState) {
              return Dialog(
                insetPadding: EdgeInsets.symmetric(
                  horizontal: 2.0,
                  vertical: 2.0,
                ),
                // title: Text(
                //   AppStrings.kSearchForTrainer,style: GoogleFonts.roboto(
                //   color: AppColors.kGreen,fontWeight: FontWeight.w900,
                // ),
                // ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                    10.0,
                  ),
                ),
                child: Container(
                  width: 380,
                  height: 630,
                  padding: EdgeInsets.all(
                    20.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            AppStrings.kSearchForTrainer,
                            style: GoogleFonts.roboto(
                              color: AppColors.kGreen,
                              fontWeight: FontWeight.w900,
                              fontSize: 18,
                            ),
                          ),
                          Spacer(),
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: Icon(
                              Icons.cancel_outlined,
                              color: AppColors.kGreen,
                            ),
                            onTap: () => Navigator.of(context).pop(),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        style: AppCommon.kSearchTextStyle(),
                        decoration: InputDecoration(
                          hintText: AppStrings.kSearchTrainerName,
                          hintStyle: AppCommon.kSearchTextStyle()
                              .copyWith(color: Color(0xff414040)),
                          suffixIconConstraints: BoxConstraints(maxHeight: 30),
                          suffixIcon: Padding(
                            padding: const EdgeInsets.only(
                              right: 10.0,
                            ),
                            child: FaIcon(
                              FontAwesomeIcons.search,
                              size: 15,
                              color: AppColors.kGreen,
                            ),
                          ),
                        ),
                      ),
                      TextFormField(
                        style: AppCommon.kSearchTextStyle(),
                        decoration: InputDecoration(
                          hintText: AppStrings.kSelectCategory,
                          hintStyle: AppCommon.kSearchTextStyle()
                              .copyWith(color: Color(0xff414040)),
                          suffixIconConstraints: BoxConstraints(maxHeight: 30),
                          suffixIcon: Padding(
                            padding: const EdgeInsets.only(
                              right: 10.0,
                            ),
                            child: FaIcon(
                              FontAwesomeIcons.arrowDown,
                              size: 15,
                              color: AppColors.kGreen,
                            ),
                          ),
                        ),
                      ),
                      TextFormField(
                        style: AppCommon.kSearchTextStyle(),
                        decoration: InputDecoration(
                          hintText: AppStrings.kSearchNearByMe,
                          hintStyle: AppCommon.kSearchTextStyle()
                              .copyWith(color: Color(0xff414040)),
                          suffixIconConstraints: BoxConstraints(maxHeight: 30),
                          suffixIcon: Padding(
                            padding: const EdgeInsets.only(
                              right: 10.0,
                            ),
                            child: FaIcon(
                              FontAwesomeIcons.mapMarkerAlt,
                              size: 15,
                              color: AppColors.kGreen,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        AppStrings.kSelectGender,
                        style: AppCommon.kSearchTextStyle().copyWith(
                          color: Color(0xff414040),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      RadioGroup<String>.builder(
                        groupValue: _genderGroupValue,
                        onChanged: (value) {
                          print('value changed $value');
                          setState(() {
                            _genderGroupValue = value;
                          });
                        },
                        items: _gender,
                        itemBuilder: (item) => RadioButtonBuilder(
                          item,
                        ),
                      ),
                      Divider(
                        color: Colors.grey,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        'Select Specialist',
                        style: AppCommon.kSearchTextStyle().copyWith(
                          color: Color(0xff414040),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      RadioGroup<String>.builder(
                        groupValue: _specialistGroupValue,
                        onChanged: (value) => setState(() {
                          _specialistGroupValue = value;
                        }),
                        items: _specialist,
                        itemBuilder: (item) => RadioButtonBuilder(
                          item,
                        ),
                      ),
                      Divider(
                        color: Colors.grey,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Center(
                        child: Container(
                          width: 130,
                          height: 40,
                          child: ElevatedButton(
                            onPressed: () {},
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        AppColors.kGreen)),
                            child: Text(
                              'Search',
                              style: GoogleFonts.roboto(
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        });
  }

  void sessionClear(){

    AwesomeDialog(
      context: context,
      dialogType: DialogType.INFO,
      animType: AnimType.BOTTOMSLIDE,
      title: 'Sing Out',
      desc: 'Are you sure want sign out?',
      btnCancelOnPress: () {},
      btnOkOnPress: () {
        progressDialog.show();
        _logoutCall();
      },
    ).show();

  }
}
