import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'app_colors.dart';
import 'app_strings.dart';

class AppCommon{

  static AwesomeDialog kErrorDialog({required BuildContext context, required String errorMsg}) {
    return AwesomeDialog(
      context: context,
      dialogType: DialogType.ERROR,
      animType: AnimType.BOTTOMSLIDE,
      title: AppStrings.kError,
      desc: errorMsg,
      btnCancelOnPress: () {},
      btnOkOnPress: () {},
    )..show();
  }


  static Widget kCommonButton({required String btnText}){
    return Padding(
      padding: const EdgeInsets.only(right: 25,left:25,top: 10,),
      child: Container(
        height: 35,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(70.0),
          color: Color(0xff46B7CD),
        ),
        child: Center(
          child: Text(btnText,
              style: GoogleFonts.roboto(
                color: Colors.white,
                fontWeight: FontWeight.w900,
              )),
        ),
      ),
    );
  }

  static Text kIconTextView({required String fontName,required String content}){
    return Text(content,style: TextStyle(fontFamily: fontName,color: Colors.white,fontSize: 16.5,letterSpacing: 0.05,),);
  }

  static Text kIconTextView12({required String fontName,required String content}){
    return Text(content,style: TextStyle(fontFamily: fontName,color: Colors.white,fontSize: 12.5,letterSpacing: 0.05,),);
  }


  static Text kIconTextView20({required String fontName,required String content}){
    return Text(content,style: TextStyle(fontFamily: fontName,color: Colors.white,fontSize: 20,),);
  }

  static Text kIconSettings({required String fontName,required String content}){
    return Text(content,style: TextStyle(fontFamily: fontName,color: Color(0xff006e6f),fontSize: 50,),);
  }


  static Widget kIconTrainerListTextView({required String fontName,required String content}){
    return Text(content,style: TextStyle(fontFamily: fontName,color: Colors.white,fontSize: 20,letterSpacing: 0.05),);
  }

  static Widget kIconTrainerSearchTextView({required String fontName,required String content}){
    return Text(content,style: TextStyle(fontFamily: fontName,color: Color(0xff055156),fontSize: 12,),);
  }


  static Widget kIconDashboardTextView({required String fontName,required String content}){
    return Text(content,style: TextStyle(fontFamily: fontName,color: Color(0xff006e6f),fontSize: 50,),);

  }
  static Widget kIconDashboardTextView40({required String fontName,required String content}){
    return Text(content,style: TextStyle(fontFamily: fontName,color: Color(0xff006e6f),fontSize: 40,),);

  }


  static Widget kIconBottomSheetTextView({required String fontName,required String content}){
    return Text(content,style: TextStyle(fontFamily: fontName,color: Color(0xff006e6f),fontSize: 20.5,),);

  }
  static Widget kIconBottomSheetTextViewWhite({required String fontName,required String content}){
    return Text(content,style: TextStyle(fontFamily: fontName,color: Colors.white,fontSize: 20.5,),);

  }

  static Widget kIconBottomSheetItemTextView({required String fontName,required String content}){
    return Text(content,style: TextStyle(fontFamily: fontName,color: Color(0xff006e6f),fontSize: 14,),);

  }
  static Widget kIconTrainerListView({required String fontName,required String content}){
    return Text(content,style: TextStyle(fontFamily: fontName,color: Color(0xff54b0a7),fontSize: 14,),);

  }




  static Widget kIconMessageTextView({required String fontName,required String content}){
    return Text(content,style: TextStyle(fontFamily: fontName,color: Color(0xff006e6f),fontSize: 35,),);

  }


  static TextStyle kSearchTextStyle(){
    return GoogleFonts.roboto(
      color: Colors.black,
      fontWeight: FontWeight.w500,
    );
  }

  static TextStyle kConsultationFormTextStyle(){
    return GoogleFonts.roboto(
      color: Colors.black,
      fontWeight: FontWeight.bold,

    );
  }
  static TextStyle kProfileLabelTextStyle(){
    return GoogleFonts.roboto(
      color: Color(0xff999999),

    );
  }



  static void kChangeFocusNode({required FocusNode currentFocusNode, required FocusNode nextFocusNode,required BuildContext context}){
    currentFocusNode.unfocus();
    FocusScope.of(context).requestFocus(nextFocusNode);
  }



  static Row kListTileCommonTextStyle1({required String text,required Text textIcon}){
    return Row(
      children: [
        Container(child: textIcon,width: 16,),
        SizedBox(
          width: 20,
        ),
        Text(
          text,
          style: GoogleFonts.roboto(
            color: Colors.white,
            fontSize: 14,
            fontWeight: FontWeight.w500,
          ),
        ),
      ],
    );
  }


  static ListTile kListTileCommonTextStyle({required String text,required Icon icon}){
    return ListTile(
      contentPadding: EdgeInsets.symmetric(vertical: 0.0, horizontal:
      15.0,),
      dense:true,
      title: Text(
        text,
        style: GoogleFonts.roboto(
          color: Colors.white,
          fontWeight: FontWeight.w500,
        ),
      ),
      leading: icon,
    );
  }

  static InputBorder kUnderlineBorder(){
    return UnderlineInputBorder(
      borderSide: BorderSide(color: AppColors.kOffGrey),
    );
  }

  static TextStyle kEditTextStyle(){
    return TextStyle(
      color: AppColors.kWhite,
    );
  }
}