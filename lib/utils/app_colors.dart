

import 'package:flutter/cupertino.dart';

class AppColors{

  static const Color kBlue= Color(0xff006E6F);
  static const Color kGreen= Color(0xff006e6f);
  static const Color kOffGrey= Color(0x99ffffff);
  static const Color kBlack= Color(0xff000000);
  static const Color kWhite= Color(0xffffffff);
}